# -*- encoding : utf-8 -*-
Rails.application.routes.draw do
  #/login, /logout
  devise_for :users, :path => "/", :path_names => {:sign_in => 'login', :sign_out => 'logout'}, :controllers => {:sessions => "sessions"}
  #/login, /logout

  get '404', :to => 'application#page_not_found', as: :no_route

  #manager
  namespace :manager do
    get 'calculations' => 'calculations#index'
    scope :calculation do
      get 'new' => 'calculation#new', as: :new_calculation
      post '/' => 'calculation#create', as: :create_calculation
      get ':id' => 'calculation#edit', as: :edit_calculation
      post ':id' => 'calculation#save', as: :save_calculation
    end

    scope :variable do
      match ':id' => 'variable#show', as: :variable, via: [:get, :post]
    end

    scope :multiple do
      get ':id' => 'multiple#new', as: :new_multiple
      post ':id' => 'multiple#create', as: :create_multiple
    end

    get 'api/density/:id' => 'api#density', as: :api_density
    get 'api/layout' => 'api#layout', as: :api_layout

    get 'clients' => 'clients#index'
    scope :client do
      get 'new' => 'client#new', as: :new_client
      post '/' => 'client#create', as: :create_client
      delete ':id' => 'client#destroy', as: :destroy_client
      get ':id' => 'client#edit', as: :edit_client
      post ':id' => 'client#update', as: :update_client

      get ':id/branches' => 'branches#index', as: :branches
      scope ':id/branch' do
        get 'new' => 'branch#new', as: :new_branch
        post '/' => 'branch#create', as: :create_branch
      end

      get ':id/specifications' => 'specifications#index', as: :specifications
      scope ':id/specification' do
        get 'new' => 'specification#new', as: :new_specification
        post '/' => 'specification#create', as: :create_specification
      end

      get ':id/transfered_specifications' => 'transfered_specifications#index', as: :transfered_specifications
      get ':id/transfered_branches' => 'transfered_branches#index', as: :transfered_branches
    end

    scope 'branch' do
      delete ':id' => 'branch#destroy', as: :destroy_branch
      get ':id' => 'branch#edit', as: :edit_branch
      post ':id' => 'branch#update', as: :update_branch
    end

    scope 'specification' do
      delete ':id' => 'specification#destroy', as: :destroy_specification
      get ':id' => 'specification#edit', as: :edit_specification
      post ':id' => 'specification#update', as: :update_specification
    end

    get 'transfered_clients' => 'transfered_clients#index'
    scope :transfered_client do
      post '/' => 'application#page_not_found'
      post ':id' => 'transfered_client#update', as: :update_transfered_client
    end

    scope 'transfered_specification' do
      post ':id' => 'transfered_specification#update', as: :update_transfered_specification
      get ':id' => 'transfered_specification#edit', as: :edit_transfered_specification
    end

    scope 'transfered_branch' do
      post ':id' => 'transfered_branch#update', as: :update_transfered_branch
      get ':id' => 'transfered_branch#edit', as: :edit_transfered_branch
    end
  end
  #manager - end

  #admin
  get 'product_paddings', :to => 'admin/product_paddings#index'
  resource :product_padding, :only => [] do
    get ':id' => 'admin/product_padding#edit', :as => :edit
    post ':id' => 'admin/product_padding#update', :as => :update
  end

  get 'additionals', :to => 'admin/additionals#index'
  resource :additional, :only => [] do
    get ':id' => 'admin/additional#edit', :as => :edit
    post ':id' => 'admin/additional#update', :as => :update
  end

  get 'uv_polish_prices', :to => 'admin/uv_polish_prices#index'
  resource :uv_polish_price, :only => [] do
    get ':id' => 'admin/uv_polish_price#edit', as: :edit
    post ':id' => 'admin/uv_polish_price#update', as: :update
  end

  get 'currencies', :to => 'admin/currencies#index'
  resource :currency, :only => [] do
    get ':id' => 'admin/currency#edit', :as => :edit
    post ':id' => 'admin/currency#update', :as => :update
  end

  get 'products', :to => 'admin/products#index'
  resource :product, :only => [] do
    get 'new' => 'admin/product#new', :as => :new
    post '/' => 'admin/product#create', :as => :create
    delete ':id' => 'admin/product#destroy', :as => :destroy
    get ':id' => 'admin/product#edit', :as => :edit
    post ':id' => 'admin/product#update', :as => :update
  end

  get 'clients', :to => 'admin/clients#index'
  resource :client, :only => [] do
    get 'new' => 'admin/client#new', :as => :new
    post '/' => 'admin/client#create', :as => :create
    delete ':id' => 'admin/client#destroy', :as => :destroy
    get ':id' => 'admin/client#edit', :as => :edit
    post ':id' => 'admin/client#update', :as => :update
    get ':id/branches' => 'admin/branches#index', :as => :branches
  end

  #branch
  get 'client/:id/branch/new' => 'admin/branch#new', :as => :new_branch
  post 'client/:id/branch' => 'admin/branch#create', :as => :create_branch
  resource :branch, :only => [] do
    delete ':id' => 'admin/branch#destroy', :as => :destroy
    get ':id' => 'admin/branch#edit', :as => :edit
    post ':id' => 'admin/branch#update', :as => :update
  end

  get 'managers', :to => 'admin/managers#index'
  resource :manager, :only => [] do
    get 'new' => 'admin/manager#new', :as => :new
    post '/' => 'admin/manager#create', :as => :create
    delete ':id' => 'admin/manager#destroy', :as => :destroy
    post ':id/block' => 'admin/manager#block', :as => :block
    post ':id/unblock' => 'admin/manager#unblock', :as => :unblock
    get ':id' => 'admin/manager#edit', :as => :edit
    post ':id' => 'admin/manager#update', :as => :update
  end

  get 'papers', :to => 'admin/papers#index'
  resource :paper, :only => [] do
    get 'new' => 'admin/paper#new', :as => :new
    post '/' => 'admin/paper#create', :as => :create
    delete ':id' => 'admin/paper#destroy', :as => :destroy
    get ':id' => 'admin/paper#edit', :as => :edit
    post ':id' => 'admin/paper#update', :as => :update
    get ':id/features' => 'admin/features#index', :as => :features
  end

  get 'machines', :to => 'admin/machines#index'
  resource :machine, :only => [] do
    get 'new' => 'admin/machine#new', :as => :new
    post '/' => 'admin/machine#create', :as => :create
    delete ':id' => 'admin/machine#destroy', :as => :destroy
    get ':id' => 'admin/machine#edit', :as => :edit
    post ':id' => 'admin/machine#update', :as => :update
  end

  get 'materials', :to => 'admin/materials#index'
  resource :material, :only => [] do
    get 'new' => 'admin/material#new', :as => :new
    post '/' => 'admin/material#create', :as => :create
    delete ':id' => 'admin/material#destroy', :as => :destroy
    get ':id' => 'admin/material#edit', :as => :edit
    post ':id' => 'admin/material#update', :as => :update
  end

  get 'cover_work_types', :to => 'admin/cover_work_types#index'
  resource :cover_work_type, :only => [] do
    get 'new' => 'admin/cover_work_type#new', :as => :new
    post '/' => 'admin/cover_work_type#create', :as => :create
    delete ':id' => 'admin/cover_work_type#destroy', :as => :destroy
    get ':id' => 'admin/cover_work_type#edit', :as => :edit
    post ':id' => 'admin/cover_work_type#update', :as => :update
  end

  get 'paper/:id/feature/new' => 'admin/feature#new', :as => :new_feature
  post 'paper/:id/feature' => 'admin/feature#create', :as => :create_feature
  resource :feature, :only => [] do
    delete ':id' => 'admin/feature#destroy', :as => :destroy
    get ':id' => 'admin/feature#edit', :as => :edit
    post ':id' => 'admin/feature#update', :as => :update
    get ':id/clone' => 'admin/feature#clone', :as => :clone
    get ':id/cuts' => 'admin/cuts#index', :as => :cuts
  end

  #cut
  get 'feature/:id/cut/new' => 'admin/cut#new', :as => :new_cut
  post 'feature/:id/cut' => 'admin/cut#create', :as => :create_cut
  resource :cut, :only => [] do
    delete ':id' => 'admin/cut#destroy', :as => :destroy
    get ':id' => 'admin/cut#edit', :as => :edit
    post ':id' => 'admin/cut#update', :as => :update
  end

  get 'discounts', :to => 'admin/discounts#index'
  resource :discount, :only => [] do
    get 'new' => 'admin/discount#new', :as => :new
    post '/' => 'admin/discount#create', :as => :create
    delete ':id' => 'admin/discount#destroy', :as => :destroy
    get ':id' => 'admin/discount#edit', :as => :edit
    post ':id' => 'admin/discount#update', :as => :update
  end
  #admin - end

  #root
  root :to => 'root#index'
  #root - end

end
