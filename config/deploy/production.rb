# -*- encoding : utf-8 -*-
server "85.113.55.24", user: 'sm00th', port: 10022, roles: %w{web app db}

after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end
end
