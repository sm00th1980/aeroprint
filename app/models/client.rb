# -*- encoding : utf-8 -*-
class Client < ActiveRecord::Base
  validates :fio, :discount_id, :manager_id, :contract_status_id, :'client/not_blank' => true

  belongs_to :manager, class_name: 'User'
  has_many :calculations
  belongs_to :discount
  has_many :branches
  belongs_to :contract_status
  has_many :specifications

  def manager
    return super if super.present?
    User.admin
  end

end
