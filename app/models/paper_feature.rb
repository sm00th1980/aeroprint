# -*- encoding : utf-8 -*-
class PaperFeature < ActiveRecord::Base
  validates :paper, :density, :gor_sm, :ver_sm, :price_per_1_ton, :currency, :uv_polish_format, presence: true
  validates_with PaperFeature::UniqueValidator

  belongs_to :paper
  belongs_to :currency
  has_many :components
  has_many :cuts, class_name: 'PaperCut'
  belongs_to :uv_polish_format

  def weight_kg_per_1_list
    ((density/(1000.to_f)).round(15) * (gor_sm/(100.to_f)).round(15) * (ver_sm/(100.to_f)).round(15)).round(5)
  end

  def price_per_1_list_rus(price_per_1_ton)
    if price_per_1_ton.blank?
      (full_price * currency.exchange_rates[0] * weight_kg_per_1_list/1000.to_f).round(5)
    else
      (price_per_1_ton * Additional.paper_full_price * currency.exchange_rates[0] * weight_kg_per_1_list/1000.to_f).round(5)
    end
  end

  def name
    "Плотность: #{density}(гр/м2), размеры: #{gor_sm}(см) на #{ver_sm}(см)"
  end

  def self.densities(paper)
    where(paper: paper).map { |p| p.density }.uniq.sort
  end

  def discount_price
    price_per_1_ton * Additional.paper_discount_price
  end

  def full_price
    price_per_1_ton * Additional.paper_full_price
  end

end
