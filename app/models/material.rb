# -*- encoding : utf-8 -*-
class Material < ActiveRecord::Base
  validates :name, :price, presence: true
end
