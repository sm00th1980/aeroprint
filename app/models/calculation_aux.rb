# -*- encoding : utf-8 -*-
class CalculationAux

  def initialize(component, form_price_index, price_per_hour_index, price_per_1_ton)
    @component = component

    @form_price_index = form_price_index
    @price_per_hour_index = price_per_hour_index
    @price_per_1_ton = price_per_1_ton

    @paper_cut_with_minimal_print_run_price = paper_cuts.min_by do |paper_cut|
      print_run_price_with(paper_cut)
    end
  end

  def success?
    return true if @paper_cut_with_minimal_print_run_price.present?

    false
  end

  def price_per_1_ton
    return @price_per_1_ton if @price_per_1_ton

    @paper_cut_with_minimal_print_run_price.paper_feature.price_per_1_ton
  end

  def machine
    @paper_cut_with_minimal_print_run_price.machine
  end

  def paper_feature
    @paper_cut_with_minimal_print_run_price.paper_feature
  end

  def paper_expense
    paper_expense_with(@paper_cut_with_minimal_print_run_price)
  end

  def print_price
    print_price_with(@paper_cut_with_minimal_print_run_price)
  end

  def print_time
    print_time_with(@paper_cut_with_minimal_print_run_price).round(3)
  end

  def form_price
    form_price_with(@paper_cut_with_minimal_print_run_price)
  end

  def paper_price
    paper_price_with(@paper_cut_with_minimal_print_run_price)
  end

  def material_price
    material_price_with(@paper_cut_with_minimal_print_run_price)
  end

  def print_run_price
    print_run_price_with(@paper_cut_with_minimal_print_run_price)
  end

  def imposition
    imposition_with(@paper_cut_with_minimal_print_run_price)
  end

  private
  def paper_cuts
    if @component.calculation.use_extended_cuts?
      PaperCut.with_extended(@component.paper, @component.density).select { |paper_cut| imposition_with(paper_cut) > 0 }
    else
      PaperCut.without_extended(@component.paper, @component.density).select { |paper_cut| imposition_with(paper_cut) > 0 }
    end
  end

  def material_price_with(paper_cut)
    (paper_price_with(paper_cut) * Additional.material_price).round(2)
  end

  def print_run_price_with(paper_cut)
    (paper_price_with(paper_cut) + material_price_with(paper_cut) + form_price_with(paper_cut) + print_price_with(paper_cut) + @component.calculation.cover_works_price).round(2) #TODO add additioanl works = slashing + uv_polish
  end

  def paper_price_with(paper_cut)
    (paper_expense_with(paper_cut) * paper_cut.paper_feature.price_per_1_list_rus(@price_per_1_ton)).round(2)
  end

  def imposition_with(paper_cut)
    Imposition.imposition(paper_cut.gor_mm, paper_cut.ver_mm, product_size.gor_mm, product_size.ver_mm)
  end

  def paper_expense_with(paper_cut)
    PaperExpense.paper_expense(
        @component.calculation.product.type,
        @component.cover?,
        @component.calculation.binding_type,
        @component.stripes_count,
        product_size.gor_mm,
        product_size.ver_mm,
        paper_cut.gor_mm,
        paper_cut.ver_mm,
        @component.calculation.print_run,
        @component.cmyk_colors,
        @component.pantone_colors,
        Additional.preparation_cover_work,
        paper_cut.chunk_count
    )
  end

  def product_size
    if @component.calculation.product.type.many_page? and @component.calculation.binding_type.glue? and @component.cover?
      internal_block = @component.calculation.components.reject { |c| c.id == @component.id }.first

      ProductSize.product_size(@component.product_gor_mm, @component.product_ver_mm, @component.calculation.product.type, @component.calculation.binding_type, @component.cover?, internal_block.paper.type, internal_block.density, internal_block.stripes_count)
    else
      ProductSize.product_size(@component.product_gor_mm, @component.product_ver_mm, @component.calculation.product.type, @component.calculation.binding_type, @component.cover?, nil, nil, nil)
    end
  end

  def form_price_with(paper_cut)
    (@component.colors_count * paper_cut.machine.form_prices[@form_price_index]).round(2)
  end

  def print_price_with(paper_cut)
    (print_time_with(paper_cut) * paper_cut.machine.prices_per_hour[@price_per_hour_index]).round(2)
  end

  def print_time_with(paper_cut)
    _papers_count = paper_cut.machine.format.A2? ? 2 : 4
    _papers_run = paper_cut.machine.format.A2? ? @component.colors_count / 4 : @component.colors_count / 2

    (((paper_expense_with(paper_cut) * _papers_count * _papers_run) / paper_cut.machine.efficiency.to_f) + paper_cut.machine.preparation_time * @component.colors_count + paper_cut.machine.additional_time).round(2)
  end

end
