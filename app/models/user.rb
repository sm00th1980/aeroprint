# -*- encoding : utf-8 -*-
class User < ActiveRecord::Base
  devise :database_authenticatable, :trackable, :validatable

  validates :substitute, :'manager/substitute' => true

  has_many :clients, foreign_key: 'manager_id'
  belongs_to :status, class_name: 'ManagerStatus'

  belongs_to :substitute, class_name: 'User', foreign_key: 'substitute_manager_id' #замещающий
  has_one :primary, class_name: 'User', foreign_key: 'substitute_manager_id' #замещаемый

  scope :managers, -> { where(admin: false) } #все кроме админа
  scope :admin, -> { find_by(admin: true) }

  def manager?
    !admin
  end

  def calculations
    clients.map{|c| c.calculations(saved: true)}.flatten
  end

  def transfered_clients
    Client.where(manager: primary)
  end
end
