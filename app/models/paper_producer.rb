# -*- encoding : utf-8 -*-
class PaperProducer < ActiveRecord::Base
  validates :name, :internal_name, presence: true

  has_many :papers

  def self.bereg
    find_by(internal_name: 'bereg')
  end

  def self.bumajnik
    find_by(internal_name: 'bumajnik')
  end

end
