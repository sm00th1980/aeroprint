# -*- encoding : utf-8 -*-
class ProductPadding < ActiveRecord::Base
  validates :product_type, :gor_mm, :ver_mm, presence: true

  belongs_to :product_type
  belongs_to :binding_type

  def self.list
    find_by(product_type: ProductType.list)
  end

  def self.many_page_with_clip
    find_by(product_type: ProductType.many_page, binding_type: BindingType.clip)
  end

  def self.many_page_with_glue
    find_by(product_type: ProductType.many_page, binding_type: BindingType.glue)
  end

end
