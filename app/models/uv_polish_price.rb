# -*- encoding : utf-8 -*-
class UvPolishPrice < ActiveRecord::Base

  validates :type, :format, presence: true
  validates :print_price, :'uv_polish_price/price' => true
  validates :preparation_price, :'uv_polish_price/price' => true

  belongs_to :type, class_name: 'UvPolishType'
  belongs_to :format, class_name: 'UvPolishFormat'

  def self.price(type, format)
    where(type: type, format: format).first
  end

  def self.print_price(print_run, type, format, count)
    price_ = price(type, format)
    (print_run * price_.print_price + price_.preparation_price) * count.value
  end
end
