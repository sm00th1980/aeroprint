# -*- encoding : utf-8 -*-
class CoverWorkOperation < ActiveRecord::Base
  has_many :cover_work_types

  def manual?
    self == self.class.find_by(internal_name: 'manual')
  end

  def automatic?
    self == self.class.find_by(internal_name: 'automatic')
  end

  def self.manual
    find_by(internal_name: 'manual')
  end

  def self.automatic
    find_by(internal_name: 'automatic')
  end
end
