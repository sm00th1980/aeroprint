# -*- encoding : utf-8 -*-
class BindingType < ActiveRecord::Base
  has_many :calculations
  has_many :product_paddings

  def self.clip
    find_by(internal_name: 'clip')
  end

  def self.glue
    find_by(internal_name: 'glue')
  end

  def clip?
    internal_name == 'clip'
  end

  def glue?
    internal_name == 'glue'
  end

  # def spring?
  #   internal_name == 'spring'
  # end
  #
  # def valve?
  #   internal_name == 'valve'
  # end
  #
  # def hard_cover?
  #   internal_name == 'hard_cover'
  # end

end
