# -*- encoding : utf-8 -*-
class PaperType < ActiveRecord::Base
  validates :name, :internal_name, presence: true

  has_many :papers

  def self.melovka
    find_by(internal_name: 'melovka')
  end

  def self.karton_1_str
    find_by(internal_name: 'karton_1_str')
  end

  def self.karton_2_str
    find_by(internal_name: 'karton_2_str')
  end

  def self.offset
    find_by(internal_name: 'offset')
  end

  def self.karton_tis
    find_by(internal_name: 'karton_tis')
  end

  def self.self_coping
    find_by(internal_name: 'self_coping')
  end

  def self.newspaper
    find_by(internal_name: 'newspaper')
  end

  def self.self_glue
    find_by(internal_name: 'self_glue')
  end

  def melovka?
    internal_name == 'melovka'
  end

  def newspaper?
    internal_name == 'newspaper'
  end

  def offset?
    internal_name == 'offset'
  end

end
