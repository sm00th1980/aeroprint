# -*- encoding : utf-8 -*-
require 'chunky_png'

class Layout

  attr_reader :image, :machine, :width

  BG_COLORS = [
      ChunkyPNG::Color.from_hex('#337ab7')
  ]

  LINE_COLOR = ChunkyPNG::Color::BLACK
  FILL_COLORS = [
      ChunkyPNG::Color.from_hex('#eeeeee')
  ]

  def initialize(width)
    @paper_feature = PaperFeature.all.sample
    @machine = Machine.all.sample
    @width = width.to_i

    @image = generate
  end

  def space_filling
    "#{rand(80).to_i + 1}%"
  end

  def full_size
    "#{full_width}x#{full_height}"
  end

  def offsets
    "Шкала: #{@machine.scale}, Кресты: #{@machine.cross}, Клапан: #{@machine.valve}"
  end

  def source
    @image.to_data_url
  end

  def generate
    @image = ChunkyPNG::Image.new(@width, height, BG_COLORS.sample)
    @image.rect(left_offset, top_offset, @width - right_offset, height - bottom_offset, LINE_COLOR, FILL_COLORS.sample)
    @image.line(left_offset, top_offset, @width - right_offset, height - bottom_offset, LINE_COLOR)
    @image.line(@width - right_offset, top_offset, left_offset, height - bottom_offset, LINE_COLOR)
  end

  def top_offset
    (@machine.scale * ratio).to_i
  end

  def left_offset
    (@machine.cross * ratio).to_i
  end

  def right_offset
    left_offset
  end

  def bottom_offset
    (@machine.valve * ratio).to_i
  end

  def full_width
    (@paper_feature.gor_sm * 10).to_i
  end

  def full_height
    (@paper_feature.ver_sm * 10).to_i
  end

  def height
    (@width * ratio).to_i
  end

  def ratio
    (full_height.to_f/full_width.to_f)
  end


end




