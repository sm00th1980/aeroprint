# -*- encoding : utf-8 -*-
class Paper < ActiveRecord::Base
  validates :name, :type, :producer, :country, presence: true

  belongs_to :type, class_name: 'PaperType'
  belongs_to :producer, class_name: 'PaperProducer'
  belongs_to :country, class_name: 'PaperCountry'

  has_many :features, class_name: 'PaperFeature'

  def producer_name
    "#{name} (#{producer.name})"
  end

  def self.with_features
    PaperFeature.all.select { |f| f.cuts.count > 0 }.map { |f| f.paper_id }.uniq.map { |paper_id| Paper.find_by(id: paper_id) }.compact
  end

  #TODO - dirty fix (after Paper.features_with_cuts should be variated use extended cuts or not from frontend)
  def features_with_cuts
    PaperFeature.all.select { |f| f.cuts.where(extended: false).count > 0 }.compact.uniq
  end
end
