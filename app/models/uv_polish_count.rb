# -*- encoding : utf-8 -*-
class UvPolishCount < ActiveRecord::Base
  include Reference

  reference_for :one_side, :both_side
end
