# -*- encoding : utf-8 -*-
class Discount < ActiveRecord::Base
  validates :name, :value, presence: true
  has_many :clients
end
