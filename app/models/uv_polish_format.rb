# -*- encoding : utf-8 -*-
class UvPolishFormat < ActiveRecord::Base
  has_many :paper_features
  has_many :uv_polish_prices

  include Reference

  reference_for :A1, :A2, :A3, :A2_plus, :A3_plus
end
