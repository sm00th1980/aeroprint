# -*- encoding : utf-8 -*-
class Additional < ActiveRecord::Base

  def self.cmyk_press_preparation
    find_by(internal_name: 'cmyk_press_preparation').value
  end

  def self.pantone_press_preparation
    find_by(internal_name: 'pantone_press_preparation').value
  end

  def self.material_price
    find_by(internal_name: 'material_price').value
  end

  def self.nds
    (find_by(internal_name: 'nds').value / 100.0 + 1.0).round(3)
  end

  def self.preparation_cover_work
    find_by(internal_name: 'preparation_cover_work').value
  end

  def self.paper_discount_price
    (find_by(internal_name: 'paper_discount_price').value / 100.0 + 1.0).round(3)
  end

  def self.paper_full_price
    (find_by(internal_name: 'paper_full_price').value / 100.0 + 1.0).round(3)
  end

end
