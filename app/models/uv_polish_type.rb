# -*- encoding : utf-8 -*-
class UvPolishType < ActiveRecord::Base
  has_many :uv_polish_prices

  include Reference

  reference_for :glossy, :opaque
end
