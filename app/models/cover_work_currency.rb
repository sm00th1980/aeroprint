# -*- encoding : utf-8 -*-
class CoverWorkCurrency < ActiveRecord::Base
  has_many :cover_work_types

  def usd?
    self == self.class.find_by(internal_name: 'usd')
  end

  def euro?
    self == self.class.find_by(internal_name: 'euro')
  end

  def self.usd
    find_by(internal_name: 'usd')
  end

  def self.euro
    find_by(internal_name: 'euro')
  end
end
