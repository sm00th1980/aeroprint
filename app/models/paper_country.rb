# -*- encoding : utf-8 -*-
class PaperCountry < ActiveRecord::Base
  validates :name, :internal_name, presence: true

  has_many :papers

  def self.russia
    find_by(internal_name: 'russia')
  end

  def self.china
    find_by(internal_name: 'china')
  end

  def self.europe
    find_by(internal_name: 'europe')
  end

end
