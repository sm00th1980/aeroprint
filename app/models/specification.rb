# -*- encoding : utf-8 -*-
class Specification < ActiveRecord::Base
  validates :client, presence: true

  belongs_to :client

  has_attached_file :document
  validates_attachment_file_name :document, :matches => [/pdf\Z/, /odf\Z/, /doc?x\Z/, /xls?x\Z/]
end
