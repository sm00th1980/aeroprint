# -*- encoding : utf-8 -*-
class Machine < ActiveRecord::Base
  validates :name, :efficiency, :color_count, :preparation_time, :additional_time,
            :skin, :max_x, :max_y, :format, :scale, :cross, :valve, presence: true

  validates :form_prices, :'machine/form_prices' => true
  validates :prices_per_hour, :'machine/prices_per_hour' => true

  belongs_to :format, class_name: 'MachineFormat'

  has_many :paper_cuts

  def self.man
    find_by(name: 'MAN')
  end

  def self.ryobi
    find_by(name: 'RYOBI')
  end

  def man?
    name.downcase == 'man'
  end

  def ryobi?
    name.downcase == 'ryobi'
  end

  def prices
    prices_per_hour.map { |v| v.to_f }.sort { |x, y| y<=>x }.map { |v| "#{name}=#{v}" }
  end

  module Reference
    FORM_PRICE_COUNT = 2
    PRICE_PER_HOUR_COUNT = 3

    def self.form_prices
      Hash[['По прайсу', 'Со скидкой'].map.with_index { |value, index| [index, value] }]
    end

    def self.prices_per_hour
      Hash[[Machine.man.prices, Machine.ryobi.prices].transpose.map { |v| "#{v.first}, #{v.last}" }.map.with_index { |value, index| [index, value] }]
    end
  end

end
