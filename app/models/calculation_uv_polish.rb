# -*- encoding : utf-8 -*-
class CalculationUvPolish < ActiveRecord::Base

  belongs_to :calculation
  belongs_to :type, class_name: 'UvPolishType'
  belongs_to :count, class_name: 'UvPolishCount'

end
