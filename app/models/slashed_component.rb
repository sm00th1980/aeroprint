# -*- encoding : utf-8 -*-
class SlashedComponent < ActiveRecord::Base
  belongs_to :calculation

  belongs_to :front_cmyk_color, class_name: 'CmykColor'
  belongs_to :front_pantone_color, class_name: 'PantoneColor'
  belongs_to :front_polish, class_name: 'Polish'

  belongs_to :back_cmyk_color, class_name: 'CmykColor'
  belongs_to :back_pantone_color, class_name: 'PantoneColor'
  belongs_to :back_polish, class_name: 'Polish'

  belongs_to :paper
end
