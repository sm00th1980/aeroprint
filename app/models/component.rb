# -*- encoding : utf-8 -*-
class Component < ActiveRecord::Base
  belongs_to :calculation

  belongs_to :front_cmyk_color, class_name: 'CmykColor'
  belongs_to :front_pantone_color, class_name: 'PantoneColor'
  belongs_to :front_polish, class_name: 'Polish'

  belongs_to :back_cmyk_color, class_name: 'CmykColor'
  belongs_to :back_pantone_color, class_name: 'PantoneColor'
  belongs_to :back_polish, class_name: 'Polish'

  belongs_to :paper

  has_many :results, class_name: 'ComponentResult'

  def cover?
    self == calculation.components.min_by { |c| c.id }
  end

  def colors_count
    front_cmyk_color.value + back_cmyk_color.value + front_pantone_color.value + back_pantone_color.value
  end

  def cmyk_colors
    front_cmyk_color.value + back_cmyk_color.value
  end

  def pantone_colors
    front_pantone_color.value + back_pantone_color.value
  end

  def calculate_first
    calculate(0, 0, nil)
  end

  def calculate_all
    Machine::Reference.form_prices.keys.map do |form_price_index|
      Machine::Reference.prices_per_hour.keys.map do |price_per_hour_index|
        calculate(form_price_index, price_per_hour_index, nil)
      end
    end
  end

  def calculate(form_price_index, price_per_hour_index, price_per_1_ton)
    calculation_aux_ = CalculationAux.new(self, form_price_index, price_per_hour_index, price_per_1_ton)

    if calculation_aux_.success?
      ComponentResult.where(
          component: self,
          variable_form_price_index: form_price_index,
          variable_price_per_hour_index: price_per_hour_index,
          variable_price_per_1_ton: calculation_aux_.price_per_1_ton
      ).delete_all #drop old results

      ComponentResult.create! do |result|
        result.component = self
        result.variable_form_price_index = form_price_index
        result.variable_price_per_hour_index = price_per_hour_index
        result.variable_price_per_1_ton = calculation_aux_.price_per_1_ton
        result.base_price_per_1_ton = price_per_1_ton.blank? ? true : false

        result.paper_expense = calculation_aux_.paper_expense
        result.machine = calculation_aux_.machine.name
        result.paper_price = calculation_aux_.paper_price
        result.print_time = calculation_aux_.print_time
        result.print_run_weight = print_run_weight
        result.material_price = calculation_aux_.material_price
        result.print_run_price = calculation_aux_.print_run_price


        result.form_price = calculation_aux_.form_price
        result.print_price = calculation_aux_.print_price

        result.imposition = calculation_aux_.imposition
        result.chosen_paper = chosen_paper(calculation_aux_.paper_feature)
        result.front_colors = front_colors
        result.back_colors = back_colors
        result.print_run_price_without_nds = (result.print_run_price / Additional.nds).round(2)
        result.price_per_unit = (result.print_run_price / calculation.print_run.to_f).round(2)
        result.price_per_unit_without_nds = (result.price_per_unit / Additional.nds).round(2)

        result.uv_polish_price = uv_polish_price_for(self, calculation_aux_.paper_feature.uv_polish_format)
      end

      self.calculation.success = true
      self.calculation.save!
    end

  end

  private
  def print_run_weight
    product_size_ = (product_gor_mm/1000.0) * (product_ver_mm/1000.0).round(4)
    paper_density_ = (density / 1000.0).round(4)

    (product_size_ * paper_density_ * calculation.print_run).round(4)
  end

  def chosen_paper(paper_feature)
    "#{paper_feature.paper.name} (#{paper_feature.gor_sm}x#{paper_feature.ver_sm}, #{paper_feature.density})"
  end

  def front_colors
    "#{front_cmyk_color.value}/#{front_pantone_color.value}"
  end

  def back_colors
    "#{back_cmyk_color.value}/#{back_pantone_color.value}"
  end

  def uv_polish_price_for(component, uv_polish_format)
    summa_ = 0
    component.calculation.uv_polishes.where('? = ANY (components)', component.id).find_each do |uv_polish|
      summa_ += UvPolishPrice.print_price(component.calculation.print_run, uv_polish.type, uv_polish_format, uv_polish.count)
    end

    summa_
  end

end
