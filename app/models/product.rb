# -*- encoding : utf-8 -*-
class Product < ActiveRecord::Base
  validates :name, :type, presence: true

  has_many :calculations
  belongs_to :type, class_name: 'ProductType', foreign_key: 'product_type_id'
end
