# -*- encoding : utf-8 -*-
class ManagerStatus < ActiveRecord::Base
  has_one :manager, class_name: 'User'

  validates :name, :internal_name, presence: true

  def self.active
    find_by(internal_name: :active)
  end

  def self.blocked
    find_by(internal_name: :blocked)
  end

  def active?
    self == self.class.active
  end

  def blocked?
    self == self.class.blocked
  end

end
