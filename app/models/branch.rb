# -*- encoding : utf-8 -*-
class Branch < ActiveRecord::Base
  validates :name, :discount_id, :client_id, :'client/not_blank' => true
  belongs_to :client
  belongs_to :discount
end
