# -*- encoding : utf-8 -*-
class Calculation < ActiveRecord::Base
  belongs_to :client
  belongs_to :product
  belongs_to :binding_type

  has_many :components
  has_many :cover_works, class_name: 'CalculationCoverWork'
  has_many :uv_polishes, class_name: 'CalculationUvPolish'

  def self.clean
    where(saved: false).find_each do |calculation|
      calculation.components.map { |c| c.results.destroy_all }
      calculation.components.destroy_all
      calculation.cover_works.destroy_all
      calculation.delete
    end
  end

  def cover_works_price
    summa_ = 0
    cover_works.find_each do |cover_work|
      if cover_work.type.owner.product?
        summa_ += cover_work.full_price
      end

      if cover_work.type.owner.component?
        summa_ += cover_work.full_price * components.to_a.sum(&:stripes_count)
      end
    end

    summa_
  end

end
