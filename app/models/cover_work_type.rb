# -*- encoding : utf-8 -*-
class CoverWorkType < ActiveRecord::Base
  WORKING_HOURS_PER_MONTH = 168
  EXTRA_CHARGE = 1.5

  validates :name, :owner, :currency, :operation, :count_of_copies_per_hour, :source_material_price, presence: true

  belongs_to :owner, class_name: 'CoverWorkOwner', foreign_key: 'owner_id'
  belongs_to :currency, class_name: 'CoverWorkCurrency', foreign_key: 'currency_id'
  belongs_to :operation, class_name: 'CoverWorkOperation', foreign_key: 'operation_id'

  has_many :calculations, class_name: 'CalculationCoverWork'

  def salary
    (operation.price / WORKING_HOURS_PER_MONTH.to_f / count_of_copies_per_hour.to_f).round(3)
  end

  def material_price
    (source_material_price * currency.exchange_rate).round(3)
  end

  def price
    ((salary + material_price)*EXTRA_CHARGE).round(2)
  end
end
