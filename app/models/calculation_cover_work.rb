# -*- encoding : utf-8 -*-
class CalculationCoverWork < ActiveRecord::Base
  validates :calculation, :type, :price, presence: true

  belongs_to :calculation
  belongs_to :type, class_name: 'CoverWorkType', foreign_key: :cover_work_type_id

  def full_price
    price * calculation.print_run
  end

end
