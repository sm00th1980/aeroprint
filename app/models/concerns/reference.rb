# -*- encoding : utf-8 -*-
module Reference

  def self.included(base)
    base.extend ClassMethods
  end

  module ClassMethods
    def reference_for(*args)

      #class getter
      args.each do |arg|
        method_name = arg.to_s.to_sym
        self.class.send :define_method, method_name do
          find_by(internal_name: arg)
        end
      end

      #question
      args.each do |arg|
        method_name = (arg.to_s + "?").to_sym
        send :define_method, method_name do
          internal_name.to_s == arg.to_s
        end
      end
    end
  end

end
