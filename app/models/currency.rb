# -*- encoding : utf-8 -*-
class Currency < ActiveRecord::Base

  EXCHANGE_RATE_COUNT = 4

  validates :name, :internal_name, presence: true
  validates :exchange_rates, :'currency/exchange_rates' => true

  has_many :paper_features
end
