# -*- encoding : utf-8 -*-
class CoverWorkOwner < ActiveRecord::Base
  has_many :cover_work_types

  def product?
    self == self.class.find_by(internal_name: 'product')
  end

  def component?
    self == self.class.find_by(internal_name: 'component')
  end

  def self.product
    find_by(internal_name: 'product')
  end

  def self.component
    find_by(internal_name: 'component')
  end

end
