# -*- encoding : utf-8 -*-
class MachineFormat < ActiveRecord::Base
  has_many :machines

  def A2?
    self == self.class.find_by(internal_name: 'A2')
  end

  def A3?
    self == self.class.find_by(internal_name: 'A3')
  end

end
