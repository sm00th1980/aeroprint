# -*- encoding : utf-8 -*-
class ProductType < ActiveRecord::Base
  has_many :products
  has_many :product_paddings

  def many_page?
    internal_name == 'many_page'
  end

  def list?
    internal_name == 'list'
  end

  def self.many_page
    find_by(internal_name: 'many_page')
  end

  def self.list
    find_by(internal_name: 'list')
  end

end
