# -*- encoding : utf-8 -*-
class PaperCut < ActiveRecord::Base
  validates :gor_mm, :ver_mm, :machine, :chunk_count, presence: true

  belongs_to :paper_feature
  belongs_to :machine

  def self.without_extended(paper, density)
    PaperFeature.where(paper: paper, density: density).map { |paper_feature| paper_feature.cuts.where(extended: false) }.flatten.uniq
  end

  def self.with_extended(paper, density)
    PaperFeature.where(paper: paper, density: density).map { |paper_feature| paper_feature.cuts }.flatten.uniq
  end
end
