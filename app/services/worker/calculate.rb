# -*- encoding : utf-8 -*-

class Worker::Calculate
  include Sidekiq::Worker

  def perform(component_id)
    component = Component.find_by(id: component_id)

    without_first_element.map do |form_price_index, price_per_hour_index|
      component.calculate(form_price_index, price_per_hour_index, nil)
    end
  end

  private
  def without_first_element
    indexes_ = []
    Machine::Reference.form_prices.keys.map do |form_price_index|
      Machine::Reference.prices_per_hour.keys.map do |price_per_hour_index|
        indexes_ << [form_price_index, price_per_hour_index]
      end
    end

    indexes_.reject { |el| el == [0, 0] }
  end

end
