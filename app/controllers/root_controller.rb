# -*- encoding : utf-8 -*-
class RootController < ApplicationController
  before_action :check_user!

  def index
    if current_user.admin?
      redirect_to managers_path
    else
      redirect_to manager_calculations_path
    end
  end

  private
  def check_user!
    if not user_signed_in?
      redirect_to new_user_session_path
    end
  end
end
