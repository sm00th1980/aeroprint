# -*- encoding : utf-8 -*-
class Admin::BranchController < Admin::ApplicationController

  before_action :find_client!, only: [:new, :create]
  before_action :find_branch!, only: [:destroy, :edit, :update]

  def create
    new_branch = Branch.new do |branch|
      branch.name = branch_params[:name]
      branch.phone = branch_params[:phone]
      branch.email = branch_params[:email]
      branch.contract = branch_params[:contract]
      branch.payer = branch_params[:payer]
      branch.description = branch_params[:description]
      branch.client = @client
      branch.discount = Discount.find_by(id: branch_params[:discount])
      branch.shipping_address = branch_params[:shipping_address]
    end

    if new_branch.save
      flash[:notice] = I18n.t('branch.success.created')
      path = branches_client_path(@client)
    else
      flash[:alert] = new_branch.errors.full_messages
      path = new_branch_path(@client)
    end

    redirect_to path
  end

  def destroy
    _client = @branch.client
    @branch.destroy
    redirect_to branches_client_path(_client), notice: I18n.t('branch.success.deleted')
  end

  def update
    @branch.name = branch_params[:name]
    @branch.phone = branch_params[:phone]
    @branch.email = branch_params[:email]
    @branch.contract = branch_params[:contract]
    @branch.payer = branch_params[:payer]
    @branch.description = branch_params[:description]
    @branch.discount = Discount.find_by(id: branch_params[:discount])
    @branch.shipping_address = branch_params[:shipping_address]

    if @branch.save
      flash[:notice] = I18n.t('branch.success.updated')
      path = branches_client_path(@branch.client)
    else
      flash[:alert] = @branch.errors.full_messages
      path = edit_branch_path(@branch)
    end

    redirect_to path
  end

  private
  def branch_params
    params.permit(:id, :name, :phone, :email, :contract, :payer, :description, :discount, :shipping_address)
  end

  def find_client!
    @client = Client.find_by(id: branch_params[:id])
    if @client.nil?
      redirect_to clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

  def find_branch!
    @branch = Branch.find_by(id: branch_params[:id])
    if @branch.nil?
      redirect_to clients_path, alert: I18n.t('branch.failure.not_found')
    end
  end

end
