# -*- encoding : utf-8 -*-
class Admin::ProductsController < Admin::ApplicationController

  def index
    @products = Admin::ProductsDecorator.decorate_collection(Product.all.order(:name))
  end

end
