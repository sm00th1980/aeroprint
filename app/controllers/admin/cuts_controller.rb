# -*- encoding : utf-8 -*-
class Admin::CutsController < Admin::ApplicationController

  before_action :find_feature!, only: [:index]

  def index
    @paper_cuts = Admin::PaperCutsDecorator.decorate_collection(@paper_feature.cuts)
  end

  private
  def cuts_params
    params.permit(:id)
  end

  def find_feature!
    @paper_feature = PaperFeature.find_by(id: cuts_params[:id])
    if @paper_feature.nil?
      redirect_to papers_path, alert: I18n.t('paper_feature.failure.not_found')
    end
  end

end
