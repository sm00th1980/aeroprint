# -*- encoding : utf-8 -*-
class Admin::ProductController < Admin::ApplicationController

  before_action :find_product!, only: [:destroy, :edit, :update]

  def create
    new_product = Product.new

    new_product.name = product_params[:name]
    new_product.type = ProductType.find_by(id: product_params[:type])

    if new_product.save
      flash[:notice] = I18n.t('product.success.created')
      path = products_path
    else
      flash[:alert] = new_product.errors.full_messages
      path = new_product_path
    end

    redirect_to path
  end

  def destroy
    @product.destroy
    redirect_to products_path, notice: I18n.t('product.success.deleted')
  end
  
  def update
    @product.name = product_params[:name]
    @product.type = ProductType.find_by(id: product_params[:type])

    if @product.save
      flash[:notice] = I18n.t('product.success.updated')
      path = products_path
    else
      flash[:alert] = @product.errors.full_messages
      path = edit_product_path(@product)
    end

    redirect_to path
  end

  private
  def product_params
    params.permit(:id, :name, :type)
  end

  def find_product!
    @product = Product.find_by(id: product_params[:id])
    if @product.nil?
      redirect_to products_path, alert: I18n.t('product.failure.not_found')
    end
  end

end
