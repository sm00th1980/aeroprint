# -*- encoding : utf-8 -*-
class Admin::BranchesController < Admin::ApplicationController

  before_action :find_client!, only: [:index]

  def index
    @branches = Admin::BranchesDecorator.decorate_collection(branches)
  end

  private
  def branches_params
    params.permit(:id)
  end

  def find_client!
    @client = Client.find_by(id: branches_params[:id])
    if @client.nil?
      redirect_to clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

  def branches
    @client.branches
  end

end
