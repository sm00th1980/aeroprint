# -*- encoding : utf-8 -*-
class Admin::UvPolishPriceController < Admin::ApplicationController

  before_action :find_uv_polish_price!, only: [:edit, :update]

  def update
    @uv_polish_price.print_price = uv_polish_price_params[:print_price]
    @uv_polish_price.preparation_price = uv_polish_price_params[:preparation_price]

    if @uv_polish_price.save
      flash[:notice] = I18n.t('uv_polish_price.success.updated')
      path = uv_polish_prices_path
    else
      flash[:alert] = @uv_polish_price.errors.full_messages
      path = edit_uv_polish_price_path(@uv_polish_price)
    end

    redirect_to path
  end

  private
  def uv_polish_price_params
    params.permit(:id, :print_price, :preparation_price)
  end

  def find_uv_polish_price!
    @uv_polish_price = UvPolishPrice.find_by(id: uv_polish_price_params[:id])
    if @uv_polish_price.blank?
      redirect_to uv_polish_prices_path, alert: I18n.t('uv_polish_price.failure.not_found')
    end
  end

end
