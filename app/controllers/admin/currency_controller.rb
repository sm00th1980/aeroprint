# -*- encoding : utf-8 -*-
class Admin::CurrencyController < Admin::ApplicationController

  before_action :find_currency!, only: [:edit, :update]

  def update
    @currency.exchange_rates = [currency_params[:exchange_rate_0], currency_params[:exchange_rate_1], currency_params[:exchange_rate_2], currency_params[:exchange_rate_3]]

    if @currency.save
      flash[:notice] = I18n.t('currency.success.updated')
      path = currencies_path
    else
      flash[:alert] = @currency.errors.full_messages
      path = edit_currency_path(@currency)
    end

    redirect_to path
  end

  private
  def currency_params
    params.permit(:id, :exchange_rate_0, :exchange_rate_1, :exchange_rate_2, :exchange_rate_3)
  end

  def find_currency!
    @currency = Currency.find_by(id: currency_params[:id])
    if @currency.nil?
      redirect_to currencies_path, alert: I18n.t('currency.failure.not_found')
    end
  end

end
