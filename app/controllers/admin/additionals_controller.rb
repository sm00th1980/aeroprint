# -*- encoding : utf-8 -*-
class Admin::AdditionalsController < Admin::ApplicationController
  def index
    @additionals = Admin::AdditionalsDecorator.decorate_collection(Additional.order(:name))
  end
end
