# -*- encoding : utf-8 -*-
class Admin::UvPolishPricesController < Admin::ApplicationController
  def index
    @uv_polish_prices = Admin::UvPolishPricesDecorator.decorate_collection(UvPolishPrice.order(:type_id, :format_id))
  end
end
