# -*- encoding : utf-8 -*-
class Admin::ProductPaddingsController < Admin::ApplicationController

  def index
    @product_paddings = Admin::ProductPaddingsDecorator.decorate_collection(ProductPadding.order(:id))
  end

end
