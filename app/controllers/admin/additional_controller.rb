# -*- encoding : utf-8 -*-
class Admin::AdditionalController < Admin::ApplicationController

  before_action :find_additional!, only: [:edit, :update]

  def update
    @additional.value = additional_params[:value]

    if @additional.save
      flash[:notice] = I18n.t('additional.success.updated')
      path = additionals_path
    else
      flash[:alert] = @additional.errors.full_messages
      path = edit_additional_path(@additional)
    end

    redirect_to path
  end

  private
  def additional_params
    params.permit(:id, :value)
  end

  def find_additional!
    @additional = Additional.find_by(id: additional_params[:id])
    if @additional.nil?
      redirect_to additionals_path, alert: I18n.t('additional.failure.not_found')
    end
  end

end
