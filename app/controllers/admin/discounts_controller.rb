# -*- encoding : utf-8 -*-
class Admin::DiscountsController < Admin::ApplicationController
  def index
    @discounts = Admin::DiscountsDecorator.decorate_collection(Discount.all.order(:value))
  end
end
