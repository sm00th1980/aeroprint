# -*- encoding : utf-8 -*-
class Admin::ClientController < Admin::ApplicationController

  before_action :find_client!, only: [:destroy, :edit, :update]

  def create
    new_client = Client.new

    new_client.fio = client_params[:fio]
    new_client.phone = client_params[:phone]
    new_client.email = client_params[:email]
    new_client.contract = client_params[:contract]
    new_client.payer = client_params[:payer]
    new_client.description = client_params[:description]
    new_client.manager = User.find_by(id: client_params[:manager])
    new_client.discount = Discount.find_by(id: client_params[:discount])
    new_client.shipping_address = client_params[:shipping_address]
    new_client.contract_status = ContractStatus.find_by(id: client_params[:contract_status])

    if new_client.save
      flash[:notice] = I18n.t('client.success.created')
      path = clients_path
    else
      flash[:alert] = new_client.errors.full_messages
      path = new_client_path
    end

    redirect_to path
  end

  def destroy
    @client.destroy
    redirect_to clients_path, notice: I18n.t('client.success.deleted')
  end

  def update
    @client.fio = client_params[:fio]
    @client.phone = client_params[:phone]
    @client.email = client_params[:email]
    @client.contract = client_params[:contract]
    @client.payer = client_params[:payer]
    @client.description = client_params[:description]
    @client.manager = User.find_by(id: client_params[:manager])
    @client.discount = Discount.find_by(id: client_params[:discount])
    @client.shipping_address = client_params[:shipping_address]
    @client.contract_status = ContractStatus.find_by(id: client_params[:contract_status])

    if @client.save
      flash[:notice] = I18n.t('client.success.updated')
      path = clients_path
    else
      flash[:alert] = @client.errors.full_messages
      path = edit_client_path(@client)
    end

    redirect_to path
  end

  private
  def client_params
    params.permit(:id, :fio, :phone, :email, :contract, :payer, :description, :manager, :discount, :shipping_address, :contract_status)
  end

  def find_client!
    @client = Client.find_by(id: client_params[:id])
    if @client.nil?
      redirect_to clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

end
