# -*- encoding : utf-8 -*-
class Admin::CoverWorkTypesController < Admin::ApplicationController

  def index
    @cover_work_types = Admin::CoverWorkTypesDecorator.decorate_collection(CoverWorkType.all.order(:name))
  end

end
