# -*- encoding : utf-8 -*-
class Admin::CoverWorkTypeController < Admin::ApplicationController

  before_action :find_cover_work_type!, only: [:destroy, :edit, :update]

  def create
    new_cover_work_type = CoverWorkType.new

    new_cover_work_type.name = cover_work_type_params[:name]
    new_cover_work_type.count_of_copies_per_hour = cover_work_type_params[:count_of_copies_per_hour]
    new_cover_work_type.source_material_price = cover_work_type_params[:material_price]

    new_cover_work_type.owner = CoverWorkOwner.find_by(id: cover_work_type_params[:owner])
    new_cover_work_type.currency = CoverWorkCurrency.find_by(id: cover_work_type_params[:currency])
    new_cover_work_type.operation = CoverWorkOperation.find_by(id: cover_work_type_params[:operation])

    if new_cover_work_type.save
      flash[:notice] = I18n.t('cover_work_type.success.created')
      path = cover_work_types_path
    else
      flash[:alert] = new_cover_work_type.errors.full_messages
      path = new_cover_work_type_path
    end

    redirect_to path
  end

  def destroy
    @cover_work_type.destroy
    redirect_to cover_work_types_path, notice: I18n.t('cover_work_type.success.deleted')
  end

  def update
    @cover_work_type.name = cover_work_type_params[:name]
    @cover_work_type.count_of_copies_per_hour = cover_work_type_params[:count_of_copies_per_hour]
    @cover_work_type.source_material_price = cover_work_type_params[:material_price]

    @cover_work_type.owner = CoverWorkOwner.find_by(id: cover_work_type_params[:owner])
    @cover_work_type.currency = CoverWorkCurrency.find_by(id: cover_work_type_params[:currency])
    @cover_work_type.operation = CoverWorkOperation.find_by(id: cover_work_type_params[:operation])

    if @cover_work_type.save
      flash[:notice] = I18n.t('cover_work_type.success.updated')
      path = cover_work_types_path
    else
      flash[:alert] = @cover_work_type.errors.full_messages
      path = edit_cover_work_type_path(@cover_work_type)
    end

    redirect_to path
  end

  private
  def cover_work_type_params
    params.permit(:id, :name, :count_of_copies_per_hour, :material_price, :owner, :currency, :operation)
  end

  def find_cover_work_type!
    @cover_work_type = CoverWorkType.find_by(id: cover_work_type_params[:id])
    if @cover_work_type.nil?
      redirect_to cover_work_types_path, alert: I18n.t('cover_work_type.failure.not_found')
    end
  end

end
