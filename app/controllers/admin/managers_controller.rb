# -*- encoding : utf-8 -*-
class Admin::ManagersController < Admin::ApplicationController

  def index
    @managers = Admin::ManagersDecorator.decorate_collection(User.managers.order(:email))
  end

end
