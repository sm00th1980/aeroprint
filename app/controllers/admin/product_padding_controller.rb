# -*- encoding : utf-8 -*-
class Admin::ProductPaddingController < Admin::ApplicationController

  before_action :find_product_padding!, only: [:edit, :update]

  def update
    @product_padding.gor_mm = product_padding_params[:gor_mm]
    @product_padding.ver_mm = product_padding_params[:ver_mm]

    if @product_padding.save
      flash[:notice] = I18n.t('product_padding.success.updated')
      path = product_paddings_path
    else
      flash[:alert] = @product_padding.errors.full_messages
      path = edit_product_padding_path(@product_padding)
    end

    redirect_to path
  end

  private
  def product_padding_params
    params.permit(:id, :gor_mm, :ver_mm)
  end

  def find_product_padding!
    _product_padding = ProductPadding.find_by(id: product_padding_params[:id])
    if _product_padding.nil?
      redirect_to product_paddings_path, alert: I18n.t('product_padding.failure.not_found')
    else
      @product_padding = Admin::ProductPaddingsDecorator.decorate(_product_padding)
    end
  end

end
