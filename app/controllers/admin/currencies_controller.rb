# -*- encoding : utf-8 -*-
class Admin::CurrenciesController < Admin::ApplicationController
  def index
    @currencies = Admin::CurrenciesDecorator.decorate_collection(Currency.order(:name))
  end
end
