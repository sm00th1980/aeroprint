# -*- encoding : utf-8 -*-
class Admin::ApplicationController < ApplicationController
  before_action :admin_only

  def admin_only
    if current_user.present?
      if current_user.admin?
        #admin
        return nil
      else
        #user
        flash[:alert] = I18n.t('failure.admin_only')
        redirect_to root_path
      end
    else
      #non auth
      flash[:alert] = I18n.t('devise.failure.user.invalid')
      redirect_to new_user_session_path
    end
  end
end
