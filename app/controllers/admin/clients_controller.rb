# -*- encoding : utf-8 -*-
class Admin::ClientsController < Admin::ApplicationController

  before_action :find_search!, :find_clients!, only: [:index]

  def index
    @clients = Admin::ClientsDecorator.decorate_collection(@filtered_clients)
  end

  private
  def clients_params
    params.permit(:search)
  end

  def find_clients!
    if @search.blank?
      @filtered_clients = Client.order('created_at DESC')
    else
      _search = "%#{@search}%"

      _by_params = Client.where("fio ILIKE ? or phone ilike ? or email ilike ? or contract ilike ? or description ilike ? or payer ilike ?", _search, _search, _search, _search, _search, _search).to_a
      _by_manager = Client.where(manager: User.managers.where("fio ilike ?", _search))

      @filtered_clients = [_by_params, _by_manager].flatten.uniq.sort{|c1, c2| c1.created_at <=> c2.created_at}

      flash.now[:notice] = I18n.t('client.success.found') % @filtered_clients.count
    end
  end

  def find_search!
    @search = clients_params[:search]
  end
end
