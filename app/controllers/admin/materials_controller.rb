# -*- encoding : utf-8 -*-
class Admin::MaterialsController < Admin::ApplicationController

  def index
    @materials = Admin::MaterialsDecorator.decorate_collection(Material.all.order(:id))
  end

end
