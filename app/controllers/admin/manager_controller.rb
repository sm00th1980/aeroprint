# -*- encoding : utf-8 -*-
class Admin::ManagerController < Admin::ApplicationController

  before_action :find_manager!, only: [:destroy, :block, :unblock, :edit, :update]

  def create
    new_user = User.new
    new_user.email = manager_params[:email]
    new_user.password = manager_params[:password]
    new_user.password_confirmation = manager_params[:password_confirmation]
    new_user.admin = false
    new_user.fio = manager_params[:fio]
    new_user.description = manager_params[:description]
    new_user.status = ManagerStatus.active
    new_user.substitute = User.managers.find_by(id: manager_params[:substitute])

    if new_user.save
      flash[:notice] = I18n.t('manager.success.created')
      path = managers_path
    else
      flash[:alert] = new_user.errors.full_messages
      path = new_manager_path
    end

    redirect_to path
  end

  def destroy
    @manager.destroy
    redirect_to managers_path, notice: I18n.t('manager.success.deleted')
  end

  def update
    @manager.email = manager_params[:email]
    @manager.fio = manager_params[:fio]
    @manager.description = manager_params[:description]
    @manager.substitute = User.managers.find_by(id: manager_params[:substitute])

    if @manager.save
      flash[:notice] = I18n.t('manager.success.updated')
      path = managers_path
    else
      flash[:alert] = @manager.errors.full_messages
      path = edit_manager_path(@manager)
    end

    redirect_to path
  end

  def block
    @manager.status = ManagerStatus.blocked
    @manager.save!
    redirect_to managers_path, notice: I18n.t('manager.success.blocked')
  end

  def unblock
    @manager.status = ManagerStatus.active
    @manager.save!
    redirect_to managers_path, notice: I18n.t('manager.success.unblocked')
  end

  private
  def manager_params
    params.permit(:id, :email, :password, :password_confirmation, :fio, :description, :substitute)
  end

  def find_manager!
    _manager = User.find_by(id: manager_params[:id])
    if _manager.nil?
      redirect_to managers_path, alert: I18n.t('manager.failure.not_found')
    else
      @manager = Admin::ManagerDecorator.decorate(_manager)
    end
  end

end
