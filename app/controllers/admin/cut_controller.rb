# -*- encoding : utf-8 -*-
class Admin::CutController < Admin::ApplicationController

  before_action :find_paper_feature!, only: [:new, :create]
  before_action :find_paper_cut!, only: [:destroy, :edit, :update, :clone]

  def create
    new_paper_cut = PaperCut.new

    new_paper_cut.paper_feature = @paper_feature
    new_paper_cut.gor_mm = cut_params[:gor_mm]
    new_paper_cut.ver_mm = cut_params[:ver_mm]
    new_paper_cut.machine = Machine.find_by(id: cut_params[:machine])
    new_paper_cut.chunk_count = cut_params[:chunk_count]
    new_paper_cut.extended = cut_params[:extended]

    if new_paper_cut.save
      flash[:notice] = I18n.t('paper.feature.cut.success.created')
      path = cuts_feature_path(@paper_feature)
    else
      flash[:alert] = new_paper_cut.errors.full_messages
      path = new_cut_path(@paper_feature)
    end

    redirect_to path
  end

  def destroy
    _paper_feature = @paper_cut.paper_feature
    @paper_cut.destroy
    redirect_to cuts_feature_path(_paper_feature), notice: I18n.t('paper.feature.cut.success.deleted')
  end

  def update
    @paper_cut.gor_mm = cut_params[:gor_mm]
    @paper_cut.ver_mm = cut_params[:ver_mm]
    @paper_cut.chunk_count = cut_params[:chunk_count]
    @paper_cut.machine = Machine.find_by(id: cut_params[:machine])
    @paper_cut.extended = cut_params[:extended]

    if @paper_cut.save
      flash[:notice] = I18n.t('paper.feature.cut.success.updated')
      path = cuts_feature_path(@paper_cut.paper_feature)
    else
      flash[:alert] = @paper_cut.errors.full_messages
      path = edit_cut_path(@paper_cut)
    end

    redirect_to path
  end

  private
  def cut_params
    params.permit(:id, :gor_mm, :ver_mm, :machine, :chunk_count, :extended)
  end

  def find_paper_feature!
    @paper_feature = PaperFeature.find_by(id: cut_params[:id])
    if @paper_feature.nil?
      redirect_to papers_path, alert: I18n.t('paper.feature.failure.not_found')
    end
  end

  def find_paper_cut!
    @paper_cut = PaperCut.find_by(id: cut_params[:id])
    if @paper_cut.nil?
      redirect_to papers_path, alert: I18n.t('paper.feature.cut.failure.not_found')
    end
  end

end
