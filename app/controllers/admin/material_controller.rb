# -*- encoding : utf-8 -*-
class Admin::MaterialController < Admin::ApplicationController

  before_action :find_material!, only: [:destroy, :edit, :update]

  def create
    new_material = Material.new

    new_material.name = material_params[:name]
    new_material.price = material_params[:price]

    if new_material.save
      flash[:notice] = I18n.t('material.success.created')
      path = materials_path
    else
      flash[:alert] = new_material.errors.full_messages
      path = new_material_path
    end

    redirect_to path
  end

  def destroy
    @material.destroy
    redirect_to materials_path, notice: I18n.t('material.success.deleted')
  end

  def update
    @material.name = material_params[:name]
    @material.price = material_params[:price]

    if @material.save
      flash[:notice] = I18n.t('material.success.updated')
      path = materials_path
    else
      flash[:alert] = @material.errors.full_messages
      path = edit_material_path(@material)
    end

    redirect_to path
  end

  private
  def material_params
    params.permit(:id, :name, :price)
  end

  def find_material!
    @material = Material.find_by(id: material_params[:id])
    if @material.nil?
      redirect_to materials_path, alert: I18n.t('material.failure.not_found')
    end
  end

end
