# -*- encoding : utf-8 -*-
class Admin::DiscountController < Admin::ApplicationController

  before_action :find_discount!, only: [:destroy, :edit, :update]

  def create
    new_discount = Discount.new

    new_discount.name = discount_params[:name]
    new_discount.value = discount_params[:value]

    if new_discount.save
      flash[:notice] = I18n.t('discount.success.created')
      path = discounts_path
    else
      flash[:alert] = new_discount.errors.full_messages
      path = new_discount_path
    end

    redirect_to path
  end

  def destroy
    @discount.destroy
    redirect_to discounts_path, notice: I18n.t('discount.success.deleted')
  end

  def update
    @discount.name = discount_params[:name]
    @discount.value = discount_params[:value]

    if @discount.save
      flash[:notice] = I18n.t('discount.success.updated')
      path = discounts_path
    else
      flash[:alert] = @discount.errors.full_messages
      path = edit_discount_path(@discount)
    end

    redirect_to path
  end

  private
  def discount_params
    params.permit(:id, :name, :value)
  end

  def find_discount!
    @discount = Discount.find_by(id: discount_params[:id])
    if @discount.nil?
      redirect_to discounts_path, alert: I18n.t('discount.failure.not_found')
    end
  end

end
