# -*- encoding : utf-8 -*-
class Admin::MachinesController < Admin::ApplicationController

  def index
    @machines = Admin::MachinesDecorator.decorate_collection(Machine.all.order(:name))
  end

end
