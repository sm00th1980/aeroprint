# -*- encoding : utf-8 -*-
class Admin::FeaturesController < Admin::ApplicationController
  before_action :find_paper!, only: [:index]

  def index
    @paper_features = Admin::PaperFeaturesDecorator.decorate_collection(@paper.features.order(:density, :gor_sm, :ver_sm))
  end

  private
  def features_params
    params.permit(:id)
  end

  def find_paper!
    @paper = Paper.find_by(id: features_params[:id])
    if @paper.nil?
      redirect_to papers_path, alert: I18n.t('paper.failure.not_found')
    end
  end

end
