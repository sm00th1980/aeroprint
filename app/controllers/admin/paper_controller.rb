# -*- encoding : utf-8 -*-
class Admin::PaperController < Admin::ApplicationController

  before_action :find_paper!, only: [:destroy, :edit, :update]

  def create
    new_paper = Paper.new

    new_paper.name = paper_params[:name]
    new_paper.type = paper_type
    new_paper.producer = paper_producer
    new_paper.country = paper_country

    if new_paper.save
      flash[:notice] = I18n.t('paper.success.created')
      path = papers_path
    else
      flash[:alert] = new_paper.errors.full_messages
      path = new_paper_path
    end

    redirect_to path
  end

  def destroy
    @paper.destroy
    redirect_to papers_path, notice: I18n.t('paper.success.deleted')
  end

  def update
    @paper.name = paper_params[:name]
    @paper.type = paper_type
    @paper.producer = paper_producer
    @paper.country = paper_country

    if @paper.save
      flash[:notice] = I18n.t('paper.success.updated')
      path = papers_path
    else
      flash[:alert] = @paper.errors.full_messages
      path = edit_paper_path(@paper)
    end

    redirect_to path
  end

  private
  def paper_params
    params.permit(:id, :name, :type, :producer, :country)
  end

  def find_paper!
    @paper = Paper.find_by(id: paper_params[:id])
    if @paper.nil?
      redirect_to papers_path, alert: I18n.t('paper.failure.not_found')
    end
  end

  def paper_type
    PaperType.find_by(id: paper_params[:type])
  end

  def paper_producer
    PaperProducer.find_by(id: paper_params[:producer])
  end

  def paper_country
    PaperCountry.find_by(id: paper_params[:country])
  end

end
