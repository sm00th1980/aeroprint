# -*- encoding : utf-8 -*-
class Admin::FeatureController < Admin::ApplicationController

  before_action :find_paper!, only: [:new, :create]
  before_action :find_paper_feature!, only: [:destroy, :edit, :update, :clone]

  def create
    new_paper_feature = PaperFeature.new

    new_paper_feature.paper = @paper
    new_paper_feature.density = feature_params[:density]
    new_paper_feature.gor_sm = feature_params[:gor_sm]
    new_paper_feature.ver_sm = feature_params[:ver_sm]
    new_paper_feature.price_per_1_ton = feature_params[:price_per_1_ton]
    new_paper_feature.currency = Currency.find_by(id: feature_params[:currency])
    new_paper_feature.uv_polish_format = UvPolishFormat.find_by(id: feature_params[:uv_polish_format])

    if new_paper_feature.save
      flash[:notice] = I18n.t('paper.feature.success.created')
      path = features_paper_path(@paper)
    else
      flash[:alert] = new_paper_feature.errors.full_messages
      path = new_feature_path(@paper)
    end

    redirect_to path
  end

  def destroy
    _paper = @paper_feature.paper
    @paper_feature.destroy
    redirect_to features_paper_path(_paper), notice: I18n.t('paper.feature.success.deleted')
  end

  def update
    @paper_feature.density = feature_params[:density]
    @paper_feature.gor_sm = feature_params[:gor_sm]
    @paper_feature.ver_sm = feature_params[:ver_sm]
    @paper_feature.price_per_1_ton = feature_params[:price_per_1_ton]
    @paper_feature.currency = Currency.find_by(id: feature_params[:currency])
    @paper_feature.uv_polish_format = UvPolishFormat.find_by(id: feature_params[:uv_polish_format])

    if @paper_feature.save
      flash[:notice] = I18n.t('paper.feature.success.updated')
      path = features_paper_path(@paper_feature.paper)
    else
      flash[:alert] = @paper_feature.errors.full_messages
      path = edit_feature_path(@paper_feature)
    end

    redirect_to path
  end

  private
  def feature_params
    params.permit(:id, :density, :gor_sm, :ver_sm, :price_per_1_ton, :currency, :uv_polish_format)
  end

  def find_paper!
    @paper = Paper.find_by(id: feature_params[:id])
    if @paper.nil?
      redirect_to papers_path, alert: I18n.t('paper.failure.not_found')
    end
  end

  def find_paper_feature!
    @paper_feature = PaperFeature.find_by(id: feature_params[:id])
    if @paper_feature.nil?
      redirect_to papers_path, alert: I18n.t('paper.feature.failure.not_found')
    end
  end

end
