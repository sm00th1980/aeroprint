# -*- encoding : utf-8 -*-
class Admin::PapersController < Admin::ApplicationController

  before_action :selected_paper, only: [:index]

  def index
    @papers = Admin::PapersDecorator.decorate_collection(Paper.all.order(:name))

    @features = Admin::PaperFeaturesDecorator.decorate_collection(@selected_paper.features.order(:weight, :gor_sm, :ver_sm, :gor_mm, :ver_mm))
  end

  private
  def paper_params
    params.permit(:selected_paper)
  end

  def selected_paper
    @selected_paper = Paper.find_by(id: paper_params[:selected_paper])
    if @selected_paper.nil?
      @selected_paper = Paper.first
    end
  end

end
