# -*- encoding : utf-8 -*-
class Admin::MachineController < Admin::ApplicationController

  before_action :find_machine!, only: [:destroy, :edit, :update]

  def create
    new_machine = Machine.new

    new_machine.name = machine_params[:name]
    new_machine.efficiency = machine_params[:efficiency]
    new_machine.color_count = machine_params[:color_count]
    new_machine.preparation_time = machine_params[:preparation_time]
    new_machine.additional_time = machine_params[:additional_time]
    new_machine.prices_per_hour = [machine_params[:price_per_hour_1], machine_params[:price_per_hour_2], machine_params[:price_per_hour_3]]
    new_machine.form_prices = [machine_params[:form_price_1], machine_params[:form_price_2]]
    new_machine.skin = machine_params[:skin]
    new_machine.max_x = machine_params[:max_x]
    new_machine.max_y = machine_params[:max_y]
    new_machine.format = MachineFormat.find_by(id: machine_params[:format])
    new_machine.scale = machine_params[:scale]
    new_machine.cross = machine_params[:cross]
    new_machine.valve = machine_params[:valve]

    if new_machine.save
      flash[:notice] = I18n.t('machine.success.created')
      path = machines_path
    else
      flash[:alert] = new_machine.errors.full_messages
      path = new_machine_path
    end

    redirect_to path
  end


  def destroy
    @machine.destroy
    redirect_to machines_path, notice: I18n.t('machine.success.deleted')
  end

  def update
    @machine.name = machine_params[:name]
    @machine.efficiency = machine_params[:efficiency]
    @machine.color_count = machine_params[:color_count]
    @machine.preparation_time = machine_params[:preparation_time]
    @machine.additional_time = machine_params[:additional_time]
    @machine.prices_per_hour = [machine_params[:price_per_hour_1], machine_params[:price_per_hour_2], machine_params[:price_per_hour_3]]
    @machine.form_prices = [machine_params[:form_price_1], machine_params[:form_price_2]]
    @machine.skin = machine_params[:skin]
    @machine.max_x = machine_params[:max_x]
    @machine.max_y = machine_params[:max_y]
    @machine.format = MachineFormat.find_by(id: machine_params[:format])
    @machine.scale = machine_params[:scale]
    @machine.cross = machine_params[:cross]
    @machine.valve = machine_params[:valve]

    if @machine.save
      flash[:notice] = I18n.t('machine.success.updated')
      path = machines_path
    else
      flash[:alert] = @machine.errors.full_messages
      path = edit_machine_path(@machine)
    end

    redirect_to path
  end

  private
  def machine_params
    params.permit(:id, :name, :efficiency, :color_count, :preparation_time, :additional_time,
                  :price_per_hour_1, :price_per_hour_2, :price_per_hour_3,
                  :form_price_1, :form_price_2,
                  :skin, :max_x, :max_y,
                  :format, :scale, :cross, :valve)
  end

  def find_machine!
    @machine = Machine.find_by(id: machine_params[:id])
    if @machine.nil?
      redirect_to machines_path, alert: I18n.t('machine.failure.not_found')
    end
  end

end
