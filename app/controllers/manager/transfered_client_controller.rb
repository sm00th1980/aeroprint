# -*- encoding : utf-8 -*-
class Manager::TransferedClientController < Manager::ApplicationController
  before_action :find_client!, only: [:update]

  def update
    @client.fio = client_params[:fio]
    @client.phone = client_params[:phone]
    @client.email = client_params[:email]
    @client.contract = client_params[:contract]
    @client.payer = client_params[:payer]
    @client.description = client_params[:description]
    @client.discount = Discount.find_by(id: client_params[:discount])
    @client.shipping_address = client_params[:shipping_address]
    @client.contract_status = ContractStatus.find_by(id: client_params[:contract_status])

    if @client.save
      success = true
      message = I18n.t('client.success.updated')
    else
      success = false
      message = @client.errors.full_messages
    end

    render json: {success: success, message: message}
  end

  private
  def client_params
    params.permit(:id, :fio, :phone, :email, :contract, :payer, :description, :discount, :shipping_address, :contract_status)
  end

  def find_client!
    @client = Client.find_by(id: client_params[:id], manager: current_user.primary)
    if @client.nil?
      render json: {success: false, message: I18n.t('client.failure.not_found')}
    end
  end

end
