# -*- encoding : utf-8 -*-
class Manager::ClientController < Manager::ApplicationController

  before_action :find_client!, only: [:destroy, :edit, :update]

  def create
    new_client = Client.new

    new_client.fio = client_params[:fio]
    new_client.phone = client_params[:phone]
    new_client.email = client_params[:email]
    new_client.contract = client_params[:contract]
    new_client.payer = client_params[:payer]
    new_client.description = client_params[:description]
    new_client.manager = current_user
    new_client.discount = Discount.find_by(id: client_params[:discount])
    new_client.shipping_address = client_params[:shipping_address]
    new_client.contract_status = ContractStatus.find_by(id: client_params[:contract_status])

    if new_client.save
      flash[:notice] = I18n.t('client.success.created')
      path = manager_clients_path
    else
      flash[:alert] = new_client.errors.full_messages
      path = manager_new_client_path
    end

    redirect_to path
  end

  def destroy
    @client.destroy

    message = I18n.t('client.success.deleted')

    respond_to do |format|
      format.json { render json: {success: true, message: message} }
      format.html { redirect_to manager_clients_path, notice: message }
    end
  end

  def update
    @client.fio = client_params[:fio]
    @client.phone = client_params[:phone]
    @client.email = client_params[:email]
    @client.contract = client_params[:contract]
    @client.payer = client_params[:payer]
    @client.description = client_params[:description]
    @client.discount = Discount.find_by(id: client_params[:discount])
    @client.shipping_address = client_params[:shipping_address]
    @client.contract_status = ContractStatus.find_by(id: client_params[:contract_status])

    if @client.save
      success = true
      message = I18n.t('client.success.updated')
    else
      success = false
      message = @client.errors.full_messages
    end

    respond_to do |format|
      format.json { render json: {success: success, message: message} }
      format.html do

        if success
          flash[:notice] = message
          path = manager_clients_path
        else
          flash[:alert] = message
          path = manager_edit_client_path(@client)
        end

        redirect_to path
      end
    end
  end

  private
  def client_params
    params.permit(:id, :fio, :phone, :email, :contract, :payer, :description, :discount, :shipping_address, :contract_status)
  end

  def find_client!
    @client = Client.find_by(id: client_params[:id], manager: current_user)
    if @client.nil?
      redirect_to manager_clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

end
