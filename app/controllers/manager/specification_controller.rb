# -*- encoding : utf-8 -*-
class Manager::SpecificationController < Manager::ApplicationController

  before_action :find_client!, only: [:new, :create]
  before_action :find_specification!, only: [:destroy, :edit, :update]

  def create
    new_specification = Specification.new do |specification|
      specification.number = specification_params[:number]
      specification.date = specification_params[:date]
      specification.client = @client
      specification.document = specification_params[:document] if file_uploaded?
    end

    if new_specification.save
      flash[:notice] = I18n.t('specification.success.created')
      path = manager_specifications_path(@client)
    else
      flash[:alert] = new_specification.errors.full_messages
      path = manager_new_specification_path(@client)
    end

    redirect_to path
  end

  def destroy
    _client = @specification.client
    @specification.destroy
    redirect_to manager_specifications_path(_client), notice: I18n.t('specification.success.deleted')
  end

  def update
    @specification.number = specification_params[:number]
    @specification.date = specification_params[:date]
    @specification.document = specification_params[:document] if file_uploaded?

    if @specification.save
      flash[:notice] = I18n.t('specification.success.updated')
      path = manager_specifications_path(@specification.client)
    else
      flash[:alert] = @specification.errors.full_messages
      path = manager_edit_specification_path(@specification)
    end

    redirect_to path
  end

  private
  def specification_params
    params.permit(:id, :number, :date, :document)
  end

  def find_client!
    @client = Client.find_by(id: specification_params[:id], manager: current_user)
    if @client.nil?
      redirect_to manager_clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

  def find_specification!
    @specification = Specification.find_by(id: specification_params[:id], client: current_user.clients)
    if @specification.nil?
      redirect_to manager_clients_path, alert: I18n.t('specification.failure.not_found')
    end
  end

  def file_uploaded?
    specification_params[:document].class == Rack::Test::UploadedFile
  end

end
