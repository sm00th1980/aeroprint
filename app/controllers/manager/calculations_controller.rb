# -*- encoding : utf-8 -*-
class Manager::CalculationsController < Manager::ApplicationController
  def index
    @calculations = Manager::CalculationsDecorator.decorate_collection(Calculation.where(client: current_user.clients, saved: true))
  end
end
