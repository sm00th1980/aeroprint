# -*- encoding : utf-8 -*-
class Manager::TransferedSpecificationsController < Manager::ApplicationController

  before_action :find_client!, only: [:index]

  def index
    @specifications = Manager::TransferedSpecificationsDecorator.decorate_collection(specifications)
  end

  private
  def specifications_params
    params.permit(:id)
  end

  def find_client!
    @client = Client.find_by(id: specifications_params[:id], manager: current_user.primary)
    if @client.nil?
      redirect_to manager_transfered_clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

  def specifications
    @client.specifications
  end

end
