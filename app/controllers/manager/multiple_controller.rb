# -*- encoding : utf-8 -*-
class Manager::MultipleController < Manager::ApplicationController

  before_action :find_calculation!, only: [:new, :create]

  def create
    _calculations = print_runs.map { |p| create_calculation(p) }
    @calculations = _calculations.map { |c| {calculation: c, components: ResultDecorator.decorate_collection(c.components)} }

    stream = render_to_string(template: 'manager/multiple/report', formats: [:xls])
    send_data(stream, type: 'text/xml', filename: filename)
  end

  private
  def calculation_params
    params.permit(:id).tap do |whitelisted|
      whitelisted[:print_runs] = params[:print_runs]
    end
  end

  def find_calculation!
    @calculation = Calculation.find_by(id: calculation_params[:id], client: current_user.clients)
    if @calculation.nil?
      redirect_to manager_calculations_path, alert: I18n.t('calculation.failure.not_found')
    end
  end

  def filename
    "report_#{Date.today.strftime('%Y-%m-%d')}.xls"
  end

  def create_calculation(print_run)
    new_calculation = Calculation.new

    new_calculation.product = @calculation.product
    new_calculation.print_run = print_run
    new_calculation.client = @calculation.client
    new_calculation.saved = false
    new_calculation.use_extended_cuts = @calculation.use_extended_cuts
    new_calculation.binding_type = @calculation.binding_type
    new_calculation.save!

    new_calculation.components = @calculation.components.sort { |c1, c2| c1.id<=>c2.id }.map { |c| cloned_component(c, new_calculation) }
    new_calculation.components.map { |c| c.calculate_all }

    new_calculation.save!

    new_calculation
  end

  def parse_print_run(name)
    calculation_params[:print_runs].select { |k, v| k[0..(name.length-1)]+'_'== name + '_' }.sort { |x, y| x.first<=>y.first }.map { |v| v.last }
  end

  def print_runs
    parse_print_run('print_run').map { |e| e.to_i rescue 0 }.select { |e| e > 0 }.uniq.sort
  end

  def cloned_component(component, calculation)
    new_component = Component.new

    new_component.calculation = calculation
    new_component.name = component.name
    new_component.paper = component.paper
    new_component.product_gor_mm = component.product_gor_mm
    new_component.product_ver_mm = component.product_ver_mm
    new_component.density = component.density
    new_component.stripes_count = component.stripes_count

    new_component.front_cmyk_color = component.front_cmyk_color
    new_component.front_pantone_color = component.front_pantone_color
    new_component.front_polish = component.front_polish

    new_component.back_cmyk_color = component.back_cmyk_color
    new_component.back_pantone_color = component.back_pantone_color
    new_component.back_polish = component.back_polish
    new_component.frontend_component_id = component.frontend_component_id

    new_component.save!

    new_component
  end

end
