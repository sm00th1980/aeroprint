# -*- encoding : utf-8 -*-
class Manager::SpecificationsController < Manager::ApplicationController

  before_action :find_client!, only: [:index]

  def index
    @specifications = Manager::SpecificationsDecorator.decorate_collection(specifications)
  end

  private
  def specifications_params
    params.permit(:id)
  end

  def find_client!
    @client = Client.find_by(id: specifications_params[:id], manager: current_user)
    if @client.nil?
      redirect_to manager_clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

  def specifications
    @client.specifications
  end

end
