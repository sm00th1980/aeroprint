# -*- encoding : utf-8 -*-
class Manager::TransferedClientsController < Manager::ApplicationController

  def index
    @transfered_clients = Manager::ClientsDecorator.decorate_collection(current_user.transfered_clients.order(:fio))
  end

end
