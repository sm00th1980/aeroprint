# -*- encoding : utf-8 -*-
class Manager::TransferedBranchController < Manager::ApplicationController

  before_action :find_branch!, only: [:edit, :update]

  def update
    @branch.name = branch_params[:name]
    @branch.phone = branch_params[:phone]
    @branch.email = branch_params[:email]
    @branch.contract = branch_params[:contract]
    @branch.payer = branch_params[:payer]
    @branch.description = branch_params[:description]
    @branch.discount = Discount.find_by(id: branch_params[:discount])
    @branch.shipping_address = branch_params[:shipping_address]

    if @branch.save
      flash[:notice] = I18n.t('branch.success.updated')
      path = manager_transfered_branches_path(@branch.client)
    else
      flash[:alert] = @branch.errors.full_messages
      path = manager_edit_transfered_branch_path(@branch)
    end

    redirect_to path
  end

  private
  def branch_params
    params.permit(:id, :name, :phone, :email, :contract, :payer, :description, :discount, :shipping_address)
  end

  def find_branch!
    @branch = Branch.find_by(id: branch_params[:id], client: clients)
    if @branch.nil?
      redirect_to manager_transfered_clients_path, alert: I18n.t('branch.failure.not_found')
    end
  end

  def clients
    current_user.primary.clients if current_user.primary
  end

end
