# -*- encoding : utf-8 -*-
class Manager::ClientsController < Manager::ApplicationController

  def index
    @clients = Manager::ClientsDecorator.decorate_collection(current_user.clients.order(:fio))
  end

end
