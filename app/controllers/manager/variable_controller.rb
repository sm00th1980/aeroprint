# -*- encoding : utf-8 -*-
class Manager::VariableController < Manager::ApplicationController
  include VariableHelper

  before_action :find_calculation!, :find_form_price, :find_price_per_hour, :find_prices_per_1_ton, only: [:show]

  def show
    @results = Manager::VariableDecorator.decorate_collection(results)
  end

  private
  def calculation_params
    #params.permit(:id, :form_price, :price_per_hour).permit!(:prices_per_1_ton)
    params.permit(:id, :form_price, :price_per_hour).tap do |whitelisted|
      whitelisted[:prices_per_1_ton] = params[:prices_per_1_ton]
    end
  end

  def find_calculation!
    @calculation = Calculation.find_by(id: calculation_params[:id], client: current_user.clients)
    if @calculation.nil?
      redirect_to manager_calculations_path, alert: I18n.t('calculation.failure.not_found')
    end
  end

  def find_form_price
    @form_price = form_price_values.map { |v| v.to_s }.include?(calculation_params[:form_price]) ? calculation_params[:form_price].to_i : form_price_values.first
  end

  def find_price_per_hour
    @price_per_hour = price_per_hour_values.map { |v| v.to_s }.include?(calculation_params[:price_per_hour]) ? calculation_params[:price_per_hour].to_i : price_per_hour_values.first
  end

  def find_prices_per_1_ton
    if calculation_params[:prices_per_1_ton].blank?
      #use base prices per 1 ton -> get from db
      @prices_per_1_ton = ComponentResult.
          select('variable_price_per_1_ton').
          where(
              component: @calculation.components,
              variable_form_price_index: @form_price,
              variable_price_per_hour_index: @price_per_hour,
              base_price_per_1_ton: true
          ).group('variable_price_per_1_ton').map { |v| v.variable_price_per_1_ton }
    else
      #create additional prices if needed
      calculation_params[:prices_per_1_ton].keys.each do |component_id|
        generate_result(component_id, calculation_params[:prices_per_1_ton][component_id])
      end

      @prices_per_1_ton = calculation_params[:prices_per_1_ton].values.map { |p| p.to_f }
    end
  end

  def results
    ComponentResult.where(
        component: @calculation.components,
        variable_form_price_index: @form_price,
        variable_price_per_hour_index: @price_per_hour,
        variable_price_per_1_ton: @prices_per_1_ton
    )
  end

  def generate_result(component_id, price_per_1_ton)
    component = Component.find_by(id: component_id)
    if component and ComponentResult.where(component: component, variable_form_price_index: @form_price, variable_price_per_hour_index: @price_per_hour, variable_price_per_1_ton: price_per_1_ton).count == 0
      component.calculate(@form_price, @price_per_hour, (price_per_1_ton.to_f rescue nil))
    end
  end

end
