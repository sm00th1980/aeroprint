# -*- encoding : utf-8 -*-
class Manager::ApplicationController < ApplicationController
  before_action :check_user!
  before_action :manager_only

  private
  def check_user!
    if not user_signed_in?
      sign_out current_user if current_user.present?
      flash[:alert] = I18n.t('devise.failure.user.invalid')
      redirect_to new_user_session_path
    end
  end

  def manager_only
    if current_user.present?
      if current_user.admin?
        #admin
        redirect_to root_path
      else
        #user
        return nil
      end
    end
  end

end
