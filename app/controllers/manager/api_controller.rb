# -*- encoding : utf-8 -*-
class Manager::ApiController < Manager::ApplicationController
  before_action :find_paper, only: [:density]

  def density
    if @paper
      json = {success: true, data: PaperFeature.densities(@paper)}
    else
      error = I18n.t('api.failure.paper_not_found')
      json = {success: false, error: error}
    end

    render json: json
  end

  def layout
    layout = Layout.new(api_params[:width])
    render json: {success: true, :'image_data' => layout.source, :'space_filling' => layout.space_filling, :'full_size' => layout.full_size, :'offsets' => layout.offsets}
  end

  private
  def api_params
    params.permit(:id, :width)
  end

  def find_paper
    @paper = Paper.find_by(id: api_params[:id])
  end
end
