# -*- encoding : utf-8 -*-
class Manager::TransferedBranchesController < Manager::ApplicationController

  before_action :find_client!, only: [:index]

  def index
    @branches = Manager::TransferedBranchesDecorator.decorate_collection(branches)
  end

  private
  def branches_params
    params.permit(:id)
  end

  def find_client!
    @client = Client.find_by(id: branches_params[:id], manager: current_user.primary)
    if @client.nil?
      redirect_to manager_transfered_clients_path, alert: I18n.t('client.failure.not_found')
    end
  end

  def branches
    @client.branches
  end


end
