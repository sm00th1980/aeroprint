# -*- encoding : utf-8 -*-
class Manager::TransferedSpecificationController < Manager::ApplicationController

  before_action :find_specification!, only: [:edit, :update]

  def update
    @specification.number = specification_params[:number]
    @specification.date = specification_params[:date]
    @specification.document = specification_params[:document]

    if @specification.save
      flash[:notice] = I18n.t('specification.success.updated')
      path = manager_transfered_specifications_path(@specification.client)
    else
      flash[:alert] = @specification.errors.full_messages
      path = manager_edit_transfered_specification_path(@specification)
    end

    redirect_to path
  end

  private
  def specification_params
    params.permit(:id, :number, :date, :document)
  end

  def find_specification!
    @specification = Specification.find_by(id: specification_params[:id], client: clients)
    if @specification.nil?
      redirect_to manager_transfered_clients_path, alert: I18n.t('specification.failure.not_found')
    end
  end

  def clients
    current_user.primary.clients if current_user.primary
  end

end
