# -*- encoding : utf-8 -*-
class Manager::CalculationController < Manager::ApplicationController

  before_action :validate_calculation!, only: [:create]
  before_action :create_calculation, only: [:create]
  before_action :find_calculation!, only: [:edit, :save]

  def new
    @clients = current_user.clients.order(:fio)
    render 'manager/calculation/new/new'
  end

  def create
    if @calculation.success?
      @components = ResultDecorator.decorate_collection(@calculation.components)
      render json: {success: true, url: manager_edit_calculation_path(@calculation)}
    else
      render json: {success: false, error: I18n.t('calculation.failure.no_paper_cut_available')}
    end
  end

  def edit
    @components = ResultDecorator.decorate_collection(@calculation.components)
    render 'manager/calculation/edit/edit'
  end

  def save
    @calculation.saved = true
    @calculation.name = calculation_params[:name]
    @calculation.save!

    redirect_to manager_calculations_path, notice: I18n.t('calculation.success.updated')
  end

  private
  def calculation_params
    params.permit(
        :id, :client, :product, :print_run, :use_extended_cuts, :name, :binding_type,
        cover_works: [:type, :price],
        components: [:id, :name, :paper, :density, :product_gor_mm, :product_ver_mm, :stripes_count,
                     :front_cmyk_color, :front_pantone_color, :front_polish,
                     :back_cmyk_color, :back_pantone_color, :back_polish
        ],
        uv_polishes: [:type, :count, :component]
    )
  end

  def components
    _components = []

    calculation_params[:components].values.each do |component_param|
      _component = Component.new

      _component.calculation = @calculation
      _component.name = component_param[:name]
      _component.paper = Paper.find_by(id: component_param[:paper])
      _component.density = component_param[:density]
      _component.product_gor_mm = component_param[:product_gor_mm]
      _component.product_ver_mm = component_param[:product_ver_mm]
      _component.stripes_count = component_param[:stripes_count]

      _component.front_cmyk_color = CmykColor.find_by(id: component_param[:front_cmyk_color])
      _component.front_pantone_color = PantoneColor.find_by(id: component_param[:front_pantone_color])
      _component.front_polish = Polish.find_by(id: component_param[:front_polish])

      _component.back_cmyk_color = CmykColor.find_by(id: component_param[:back_cmyk_color])
      _component.back_pantone_color = PantoneColor.find_by(id: component_param[:back_pantone_color])
      _component.back_polish = Polish.find_by(id: component_param[:back_polish])

      _component.frontend_component_id = component_param[:id]

      _component.save!

      _components << _component
    end

    _components.map { |c| c.calculate_first }

    _components.map { |c| Worker::Calculate.perform_async(c.id) } #start calculate all combinations in background


    _components
  end

  def create_calculation
    @calculation = Calculation.new

    @calculation.client = Client.find_by(id: calculation_params[:client])
    @calculation.product = Product.find_by(id: calculation_params[:product])
    @calculation.binding_type = BindingType.find_by(id: calculation_params[:binding_type])
    @calculation.print_run = calculation_params[:print_run]
    @calculation.saved = false
    @calculation.use_extended_cuts = calculation_params[:use_extended_cuts]
    @calculation.components = components
    @calculation.cover_works = cover_works
    @calculation.uv_polishes = uv_polishes(@calculation.components)

    @calculation.save!
  end

  def validate_calculation!
    product = Product.find_by(id: calculation_params[:product])
    binding_type = BindingType.find_by(id: calculation_params[:binding_type])

    if product and product.type.many_page? and binding_type and binding_type.glue? and calculation_params[:components].values.count == 1
      render json: {success: false, error: I18n.t('calculation.failure.glue_with_only_one_component')} and return
    end

    if product and product.slashed?
      render json: {success: false, error: I18n.t('calculation.failure.slashed_not_ready_yet')} and return
    end

  end

  def find_calculation!
    @calculation = Calculation.find_by(id: calculation_params[:id], client: current_user.clients)
    if @calculation.nil?
      redirect_to manager_calculations_path, alert: I18n.t('calculation.failure.not_found')
    end
  end

  def cover_works
    _cover_works = []
    if calculation_params[:cover_works].present?
      calculation_params[:cover_works].values.each do |cover_work_param|
        new_calculation_cover_work = CalculationCoverWork.new

        new_calculation_cover_work.calculation = @calculation
        new_calculation_cover_work.type = CoverWorkType.find_by(id: cover_work_param[:type])
        new_calculation_cover_work.price = CoverWorkType.find_by(id: cover_work_param[:type]).price

        new_calculation_cover_work.save!

        _cover_works << new_calculation_cover_work
      end
    end

    _cover_works
  end

  def uv_polishes(components)
    uv_polishes_ = []
    if calculation_params[:uv_polishes].present?
      calculation_params[:uv_polishes].values.each do |uv_polish_param|
        new_calculation_uv_polish = CalculationUvPolish.new

        new_calculation_uv_polish.calculation = @calculation
        new_calculation_uv_polish.type = UvPolishType.find_by(id: uv_polish_param[:type])
        new_calculation_uv_polish.count = UvPolishCount.find_by(id: uv_polish_param[:count])
        new_calculation_uv_polish.components = uv_polish_components(uv_polish_param[:component], components)

        new_calculation_uv_polish.save!

        uv_polishes_ << new_calculation_uv_polish
      end
    end

    uv_polishes_
  end

  def uv_polish_components(frontend_component_id, components)
    if frontend_component_id.to_s == "-1" #use all component_ids
      return components.map { |c| c.id }
    end

    if components.map { |c| c.frontend_component_id.to_s }.include?(frontend_component_id.to_s)
      return components.select { |c| c.frontend_component_id.to_s == frontend_component_id.to_s }.map { |c| c.id }
    end

    #default
    [0]
  end

end
