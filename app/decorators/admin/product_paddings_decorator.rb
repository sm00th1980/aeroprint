# -*- encoding : utf-8 -*-
class Admin::ProductPaddingsDecorator < Draper::Decorator
  delegate_all

  def tr_class(models)
    if models.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def product_type
    model.product_type.name
  end

  def binding_type
    model.binding_type.name if model.binding_type
  end

  def edit_button
    h.link_to(I18n.t('product_padding.edit'), h.edit_product_padding_path(model), class: ['btn', 'btn-primary', 'pull-right', 'btn-sm'])
  end

end
