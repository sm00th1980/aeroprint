# -*- encoding : utf-8 -*-
class Admin::DiscountsDecorator < Draper::Decorator
  delegate_all

  def tr_class(discounts)
    if discounts.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_discount_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данную скидку?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_discount_path(model))
  end

end
