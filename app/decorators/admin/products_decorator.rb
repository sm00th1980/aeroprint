# -*- encoding : utf-8 -*-
class Admin::ProductsDecorator < Draper::Decorator
  delegate_all

  def tr_class(products)
    if products.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def created_at
    Russian::strftime(model.created_at, "%d %B %Y")
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_product_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данное изделие?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_product_path(model))
  end

end
