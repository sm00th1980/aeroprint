# -*- encoding : utf-8 -*-
class Admin::PaperCutsDecorator < Draper::Decorator
  delegate_all

  def tr_class(cuts)
    if cuts.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def extended
    if model.extended?
      return I18n.t('paper.feature.cut.extended_yes')
    end

    I18n.t('paper.feature.cut.extended_no')
  end

  def remove_button
    h.link_to(I18n.t('paper.feature.cut.remove'), h.destroy_cut_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данный способ разреза бумаги?'})
  end

  def edit_button
    h.link_to(I18n.t('paper.feature.cut.edit'), h.edit_cut_path(model))
  end

end
