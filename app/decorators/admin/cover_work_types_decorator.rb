# -*- encoding : utf-8 -*-
class Admin::CoverWorkTypesDecorator < Draper::Decorator
  delegate_all

  def tr_class(cover_work_types)
    if cover_work_types.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_cover_work_type_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данный тип переплётных работ?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_cover_work_type_path(model))
  end

end
