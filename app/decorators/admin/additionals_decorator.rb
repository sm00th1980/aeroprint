# -*- encoding : utf-8 -*-
class Admin::AdditionalsDecorator < Draper::Decorator
  delegate_all

  def tr_class(additionals)
    if additionals.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def edit_button
    h.link_to(I18n.t('additional.edit'),  h.edit_additional_path(model), class: ['btn', 'btn-primary', 'pull-right', 'btn-sm'])
  end

end
