# -*- encoding : utf-8 -*-
class Admin::ManagersDecorator < Draper::Decorator
  delegate_all

  def status
    css_class = model.status.active? ? 'bg-success' : 'bg-warning'
    h.content_tag(:span, model.status.name, :class => ['label', css_class, 'text-sm'])
  end

  def tr_class(managers)
    if managers.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def switch_button
    h.link_to(I18n.t('Switch_user'), h.switch_user_path(:scope_identifier => "user_#{model.id}"))
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_manager_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данного менеджера?'})
  end

  def block_button
    if model.status.active?
      h.link_to(I18n.t('Block_user'), h.block_manager_path(model), :method => :post, :data => {:confirm => 'Вы уверены что хотите заблокировать данного менеджера?'})
    else
      h.link_to(I18n.t('Unblock_user'), h.unblock_manager_path(model), :method => :post, :data => {:confirm => 'Вы уверены что хотите разблокировать данного менеджера?'})
    end
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_manager_path(model))
  end

  def substitute
    if model.substitute
      model.substitute.fio
    else
      I18n.t('manager.has_no_substitute')
    end
  end

end
