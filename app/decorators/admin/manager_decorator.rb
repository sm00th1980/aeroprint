# -*- encoding : utf-8 -*-
class Admin::ManagerDecorator < ApplicationDecorator
  decorates :user
  delegate_all

  def initialize(manager)
    @manager = manager

    super(manager)
  end

  def possible_substitutes
    h.select("manager", "substitute", subsitutes, {selected: selected_substitute}, {class: "form-control", name: 'substitute'})
  end

  private
  def subsitutes
    User.managers.where.not(id: @manager.id).map { |m| [m.fio, m.id] }.insert(0, ["Без заместителя", 0])
  end

  def selected_substitute
    if @manager.substitute
      @manager.substitute.id
    end
  end

end
