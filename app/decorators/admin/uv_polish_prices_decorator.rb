# -*- encoding : utf-8 -*-
class Admin::UvPolishPricesDecorator < Draper::Decorator
  delegate_all

  def tr_class(models)
    if models.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def edit_button
    h.link_to(I18n.t('uv_polish_price.edit'),  h.edit_uv_polish_price_path(model), class: ['btn', 'btn-primary', 'pull-right', 'btn-sm'])
  end

end
