# -*- encoding : utf-8 -*-
class Admin::PapersDecorator < Draper::Decorator
  delegate_all

  def tr_class(papers)
    if papers.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_paper_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данную бумагу?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_paper_path(model))
  end

  def features_button
    h.link_to(I18n.t('paper.features'), h.features_paper_path(model))
  end

end
