# -*- encoding : utf-8 -*-
class Admin::PaperFeaturesDecorator < Draper::Decorator
  delegate_all

  def tr_class(features)
    if features.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def remove_button
    h.link_to(I18n.t('paper.feature.remove'), h.destroy_feature_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данный вариант бумаги?'})
  end

  def edit_button
    h.link_to(I18n.t('paper.feature.edit'), h.edit_feature_path(model))
  end

  def clone_button
    h.link_to(I18n.t('paper.feature.clone'), h.clone_feature_path(model))
  end

  def cuts_button
    h.link_to(I18n.t('paper.feature.cuts'), h.cuts_feature_path(model))
  end

end
