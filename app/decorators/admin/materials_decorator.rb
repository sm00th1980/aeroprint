# -*- encoding : utf-8 -*-
class Admin::MaterialsDecorator < Draper::Decorator
  delegate_all

  def tr_class(materials)
    if materials.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_material_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данный расходный материал?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_material_path(model))
  end

end
