# -*- encoding : utf-8 -*-
class Admin::ClientsDecorator < Draper::Decorator
  delegate_all

  def tr_class(clients)
    if clients.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def contract
    css_class = "btn-#{model.contract_status.color}"
    h.content_tag(:span, model.contract, :class => ['label', css_class, 'text-sm'])
  end

  def created_at
    Russian::strftime(model.created_at, "%d %B %Y")
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_client_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данного клиента?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_client_path(model))
  end

  def branches_button
    h.link_to(I18n.t('client.branches'), h.branches_client_path(model))
  end

end
