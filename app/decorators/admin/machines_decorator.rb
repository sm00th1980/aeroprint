# -*- encoding : utf-8 -*-
class Admin::MachinesDecorator < Draper::Decorator
  delegate_all

  def tr_class(machines)
    if machines.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.destroy_machine_path(model), :method => :delete, :data => {:confirm => 'Вы уверены что хотите удалить данную машину?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.edit_machine_path(model))
  end

end
