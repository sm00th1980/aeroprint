# -*- encoding : utf-8 -*-
class Manager::CalculationsDecorator < Draper::Decorator
  delegate_all

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def tr_class(calculations)
    if calculations.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def created_at
    Russian::strftime(model.created_at, "%d %B %Y")
  end

  def print_run_price
    components.map { |c| c.results[0].print_run_price }.sum
  end

  def paper_expense
    components.map { |c| c.results[0].paper_expense }.sum
  end

  def edit_button
    h.link_to(I18n.t('calculation.detail'), h.manager_edit_calculation_path(model), class: 'btn btn-sm btn-info')
  end

end
