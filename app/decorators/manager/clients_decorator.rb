# -*- encoding : utf-8 -*-
class Manager::ClientsDecorator < Draper::Decorator
  delegate_all

  def tr_class(clients)
    if clients.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def discount
    model.discount_id
  end

  def contract
    css_class = "btn-#{model.contract_status.color}"
    h.content_tag(:span, model.contract, class: ['label2', css_class, 'text-sm'], title: model.contract_status.name)
  end

  def created_at
    Russian::strftime(model.created_at, "%d %B %Y")
  end

  def remove_button
    h.link_to(I18n.t('Remove'), h.manager_destroy_client_path(model), method: :delete, data: {confirm: 'Вы уверены что хотите удалить данного клиента?'})
  end

  def edit_button
    h.link_to(I18n.t('Edit'), h.manager_edit_client_path(model))
  end

  def branches_button
    h.link_to(I18n.t('client.branches'), h.manager_branches_path(model))
  end

  def to_jsx
    model.as_json.merge(discount: model.discount_id, manager: model.manager_id, contract_status: model.contract_status_id)
  end

end
