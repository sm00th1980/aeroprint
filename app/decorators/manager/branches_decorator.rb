# -*- encoding : utf-8 -*-
class Manager::BranchesDecorator < Draper::Decorator
  delegate_all

  def tr_class(branches)
    if branches.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def remove_button
    h.link_to(I18n.t('branch.remove'), h.manager_destroy_branch_path(model), method: :delete, data: {confirm: 'Вы уверены что хотите удалить данное подразделение?'})
  end

  def edit_button
    h.link_to(I18n.t('branch.edit'), h.manager_edit_branch_path(model))
  end

end
