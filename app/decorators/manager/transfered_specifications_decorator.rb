# -*- encoding : utf-8 -*-
class Manager::TransferedSpecificationsDecorator < Manager::SpecificationsDecorator

  def edit_button
    h.link_to(I18n.t('specification.edit'), h.manager_edit_transfered_specification_path(model))
  end

end
