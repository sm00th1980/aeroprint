# -*- encoding : utf-8 -*-
class Manager::TransferedBranchesDecorator < Manager::BranchesDecorator

  def edit_button
    h.link_to(I18n.t('branch.edit'), h.manager_edit_transfered_branch_path(model))
  end

end
