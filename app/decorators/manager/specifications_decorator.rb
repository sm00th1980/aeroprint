# -*- encoding : utf-8 -*-
class Manager::SpecificationsDecorator < Draper::Decorator
  delegate_all

  def tr_class(specifications)
    if specifications.find_index(model) % 2 == 0
      'footable-odd'
    else
      'footable-even'
    end
  end

  def link_to_file
    if model.document_file_name
      h.link_to model.document_file_name, model.document.url
    else
      I18n.t('specification.no_document')
    end
  end

  def remove_button
    h.link_to(I18n.t('specification.remove'), h.manager_destroy_specification_path(model), method: :delete, data: {confirm: 'Вы уверены, что хотите удалить данную спецификацию?'})
  end

  def edit_button
    h.link_to(I18n.t('specification.edit'), h.manager_edit_specification_path(model))
  end

end
