# -*- encoding : utf-8 -*-
class Manager::VariableDecorator < Manager::ApplicationDecorator
  decorates :component_result
  delegate_all

  def size
    "#{component.product_gor_mm}x#{component.product_ver_mm}"
  end

  # def paper
  #   result.chosen_paper
  # end
  #
  # def paper_expense
  #   result.paper_expense
  # end
  #
  # def paper_price
  #   result.paper_price
  # end
  #
  # def print_time
  #   result.print_time
  # end
  #
  # def print_price
  #   result.print_price
  # end
  #
  # def material_price
  #   result.material_price
  # end
  #
  # def print_run_weight
  #   result.print_run_weight
  # end
  #
  # def machine
  #   result.machine
  # end
  #
  # def form_price
  #   result.form_price
  # end
  #
  # def imposition
  #   result.imposition
  # end
  #
  # def front_colors
  #   result.front_colors
  # end
  #
  # def back_colors
  #   result.back_colors
  # end
  #
  # def print_run_price
  #   result.print_run_price
  # end
  #
  # def print_run_price_without_nds
  #   result.print_run_price_without_nds
  # end
  #
  # def price_per_unit
  #   result.price_per_unit
  # end
  #
  # def price_per_unit_without_nds
  #   result.price_per_unit_without_nds
  # end
  #
  # private
  # def result
  #   model.results[0]
  # end

end
