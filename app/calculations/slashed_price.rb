# -*- encoding : utf-8 -*-
class SlashedPrice

  def self.price(print_run, format)
    if format == :A3
      return 1200 if print_run < 1_000
      return 1.2 * print_run if (1_000...3_000).include? print_run
      return 1.15 * print_run if (3_000...5_000).include? print_run
      return 1.1 * print_run if (5_000...7_000).include? print_run
      return 1.05 * print_run if (7_000...10_000).include? print_run
      return 1.0 * print_run if (10_000...15_000).include? print_run
      return 0.9 * print_run if (15_000...20_000).include? print_run
      return 0.8 * print_run if (20_000...30_000).include? print_run
      return 0.7 * print_run if (30_000...50_000).include? print_run
      return 0.6 * print_run if print_run >= 50_000
    end

    if format == :A2
      return 2500 if print_run < 1_000
      return 2.5 * print_run if (1_000...3_000).include? print_run
      return 2.4 * print_run if (3_000...5_000).include? print_run
      return 2.3 * print_run if (5_000...7_000).include? print_run
      return 2.2 * print_run if (7_000...10_000).include? print_run
      return 2.0 * print_run if (10_000...15_000).include? print_run
      return 1.8 * print_run if (15_000...20_000).include? print_run
      return 1.5 * print_run if (20_000...30_000).include? print_run
      return 1.2 * print_run if (30_000...50_000).include? print_run
      return 0.9 * print_run if print_run >= 50_000
    end
  end

end
