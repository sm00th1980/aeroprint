# -*- encoding : utf-8 -*-
class Imposition

  def self.imposition(source_gor_mm, source_ver_mm, product_gor_mm, product_ver_mm)
    gor_count_ = (source_gor_mm.to_i / product_gor_mm.to_i) * (source_ver_mm.to_i / product_ver_mm.to_i)
    ver_count_ = (source_gor_mm.to_i / product_ver_mm.to_i) * (source_ver_mm.to_i / product_gor_mm.to_i)

    [gor_count_, ver_count_].max.to_f
  end

end
