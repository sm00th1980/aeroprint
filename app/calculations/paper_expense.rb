# -*- encoding : utf-8 -*-
class PaperExpense

  def self.paper_expense(product_type, cover, binding_type, stripes_count, product_gor_mm, product_ver_mm, cut_gor_mm, cut_ver_mm, print_run, cmyk_colors, pantone_colors, preparation_cover_work, chunk_count)

    paper_expense_ = 0

    if product_type.list?
      paper_expense_ = (print_run / Imposition.imposition(cut_gor_mm, cut_ver_mm, product_gor_mm, product_ver_mm).to_f + 25*cmyk_colors + 50*pantone_colors + preparation_cover_work) / chunk_count.to_f
    end

    if product_type.many_page?
      if not cover

        stripes_divider_ = 4.0 if binding_type.clip?
        stripes_divider_ = 2.0 if binding_type.glue?

        paper_expense_ = (((stripes_count / stripes_divider_.to_f) / Imposition.imposition(cut_gor_mm, cut_ver_mm, product_gor_mm, product_ver_mm).to_f) * (print_run + 25*cmyk_colors + 50*pantone_colors + preparation_cover_work)) / chunk_count.to_f
      end

      if cover
        stripes_divider_ = 4.0

        paper_expense_ = (((stripes_count / stripes_divider_.to_f) / Imposition.imposition(cut_gor_mm, cut_ver_mm, product_gor_mm, product_ver_mm).to_f) * (print_run + preparation_cover_work) + 25*cmyk_colors + 50*pantone_colors) / chunk_count.to_f
      end
    end

    paper_expense_.ceil
  end

end
