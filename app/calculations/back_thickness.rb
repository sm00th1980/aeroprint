# -*- encoding : utf-8 -*-
class BackThickness

  # расчёт толщины корешка для многостраничной продукции с переплётом на клей и только для обложки

  def self.back_thickness(paper_type, density, pages_count)
    thickness_ = nil

    if paper_type.newspaper?
      thickness_ = 0.068
    end

    if paper_type.offset?
      thickness_ = 0.102
    end

    if paper_type.melovka?
      thickness_ = 0.063 if density.to_i == 80
      thickness_ = 0.073 if density.to_i == 90
      thickness_ = 0.079 if density.to_i == 104
      thickness_ = 0.077 if density.to_i == 105
      thickness_ = 0.085 if density.to_i == 115
      thickness_ = 0.096 if density.to_i == 128
      thickness_ = 0.100 if density.to_i == 130
      thickness_ = 0.129 if density.to_i == 148
      thickness_ = 0.119 if density.to_i == 150
      thickness_ = 0.134 if density.to_i == 157
      thickness_ = 0.123 if density.to_i == 170
      thickness_ = 0.170 if density.to_i == 200
    end

    if thickness_
      return (thickness_ * pages_count).round(0) # используется математическое округление
    end

    thickness_
  end

end
