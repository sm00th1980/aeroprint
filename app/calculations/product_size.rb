# -*- encoding : utf-8 -*-
class ProductSize

  PRODUCT_SIZE = Struct.new(:gor_mm, :ver_mm)

  def self.product_size(original_gor_mm, original_ver_mm, product_type, binding_type, cover, block_paper_type, block_density, block_stripes_count)
    gor_mm_ = 0
    ver_mm_ = 0

    if product_type.list?
      gor_mm_ = ProductPadding.list.gor_mm + original_gor_mm + ProductPadding.list.gor_mm
      ver_mm_ = ProductPadding.list.ver_mm + original_ver_mm + ProductPadding.list.ver_mm
    end

    if product_type.many_page?

      if binding_type.clip?
        gor_mm_ = ProductPadding.many_page_with_clip.gor_mm + original_gor_mm * 2 + ProductPadding.many_page_with_clip.gor_mm
        ver_mm_ = ProductPadding.many_page_with_clip.ver_mm + original_ver_mm + ProductPadding.many_page_with_clip.ver_mm
      end

      if binding_type.glue?
        if cover
          gor_mm_ = (original_gor_mm + ProductPadding.many_page_with_glue.gor_mm)*2 + BackThickness.back_thickness(block_paper_type, block_density, block_stripes_count / 2)
        else
          gor_mm_ = original_gor_mm + ProductPadding.many_page_with_glue.gor_mm
        end

        ver_mm_ = ProductPadding.many_page_with_glue.ver_mm + original_ver_mm + ProductPadding.many_page_with_glue.ver_mm
      end

    end

    PRODUCT_SIZE.new(gor_mm_, ver_mm_)
  end

end
