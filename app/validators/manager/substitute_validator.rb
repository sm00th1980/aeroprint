# -*- encoding : utf-8 -*-
class Manager::SubstituteValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)

    if record and value and record.id == value.id
      record.errors.add attribute, I18n.t('manager.failure.invalid_substitute')
    end

  end
end
