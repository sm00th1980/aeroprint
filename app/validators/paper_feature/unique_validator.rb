# -*- encoding : utf-8 -*-
class PaperFeature::UniqueValidator < ActiveModel::Validator

  def validate(record)
    if PaperFeature.where(paper: record.paper, density: record.density, gor_sm: record.gor_sm, ver_sm: record.ver_sm).count > 0
      record.errors[:base] << I18n.t('paper_feature.failure.non_unique')
    end
  end

end
