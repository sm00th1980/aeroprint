# -*- encoding : utf-8 -*-
class Machine::FormPricesValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)

    if value.blank? or (value.present? and value.compact.size != Machine::Reference::FORM_PRICE_COUNT)
      record.errors.add attribute, I18n.t('machine.failure.form_prices_count_is_invalid')
    else
      begin
        value.compact.map { |v| Float(v) }
      rescue
        record.errors.add attribute, I18n.t('machine.failure.form_price_is_invalid')
      else
        value.compact.map do |v|
          if Float(v) <= 0
            record.errors.add attribute, I18n.t('machine.failure.form_price_is_invalid')
          end
        end
      end
    end

  end
end
