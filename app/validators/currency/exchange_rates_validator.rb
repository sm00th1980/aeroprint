# -*- encoding : utf-8 -*-
class Currency::ExchangeRatesValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)

    if value.blank? or (value.present? and value.compact.size != Currency::EXCHANGE_RATE_COUNT)
      record.errors.add attribute, I18n.t('currency.failure.exchange_rates_count_is_invalid')
    else
      begin
        value.compact.map { |v| Float(v) }
      rescue
        record.errors.add attribute, I18n.t('currency.failure.exchange_rate_is_invalid')
      else
        value.compact.map do |v|
          if Float(v) <= 0
            record.errors.add attribute, I18n.t('currency.failure.exchange_rate_is_invalid')
          end
        end
      end
    end

  end
end
