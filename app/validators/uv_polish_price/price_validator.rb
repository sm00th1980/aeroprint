# -*- encoding : utf-8 -*-
class UvPolishPrice::PriceValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)

    if value.blank?
      record.errors.add attribute, I18n.t('uv_polish_price.failure.price_is_invalid')
    else
      begin
        Float(value)
      rescue
        record.errors.add attribute, I18n.t('uv_polish_price.failure.price_is_invalid')
      else
        if Float(value) <= 0
          record.errors.add attribute, I18n.t('uv_polish_price.failure.price_is_invalid')
        end
      end
    end

  end
end
