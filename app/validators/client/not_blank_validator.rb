# -*- encoding : utf-8 -*-
class Client::NotBlankValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value.blank?
      record.errors.add attribute, I18n.t('client.failure.is_blank')
    end
  end
end
