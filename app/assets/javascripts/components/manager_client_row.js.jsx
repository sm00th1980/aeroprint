var ManagerClientRow = React.createClass({
    getInitialState: function () {
        return {
            edit: false,
            client: $.extend({}, this.props.client) //clone original object
        };
    },
    onChange: function (property, value) {
        var updated_client = this.state.client;
        updated_client[property] = value;

        this.setState({client: updated_client});
    },
    toggleEdit: function () {
        this.setState({edit: !this.state.edit});
    },
    cancelUpdate: function () {
        this.setState({edit: false, client: this.props.client});
    },
    onSave: function () {
        var self = this;

        $.ajax({
            type: "POST",
            url: this.props.update_url,
            data: this.state.client,
            success: function (response) {
                if (response.success) {
                    EventSystem.publish('flash_message.show', {alert: false, message: response.message});
                } else {
                    self.cancelUpdate();
                    EventSystem.publish('flash_message.show', {alert: true, message: response.message});
                }
            },
            error: function () {
                self.cancelUpdate();
                EventSystem.publish('flash_message.show', {alert: false, message: 'backend server error'});
            }
        });

        self.setState({edit: false});
    },
    onDrop: function () {
        $.ajax({
            type: "DELETE",
            url: this.props.destroy_url,
            success: function (response) {
                if (response.success) {
                    location.reload();
                } else {
                    EventSystem.publish('flash_message.show', {alert: true, message: response.message});
                }
            },
            error: function () {
                EventSystem.publish('flash_message.show', {alert: false, message: 'backend server error'});
            }
        });
    },

    _textParams: function (property, value) {
        return {
            property: property,
            value: value,
            edit: this.state.edit,
            onEdit: this.onChange,
            onSave: this.onSave,
            onCancel: this.cancelUpdate
        }
    },

    _selectParams: function (property, value, options) {
        return {
            property: property,
            value: parseInt(value, 10),
            options: options,
            edit: this.state.edit,
            onEdit: this.onChange
        }
    },

    _textSelectParams: function (text_property, text_value, option_property, option_value, options) {
        return {
            text_property: text_property,
            text_value: text_value,
            option_property: option_property,
            option_value: parseInt(option_value, 10),
            options: options,
            edit: this.state.edit,
            onEdit: this.onChange,
            onSave: this.onSave,
            onCancel: this.cancelUpdate
        }
    },

    render: function () {

        return (
            <tr>
                <td className="footable-visible footable-first-column v-middle">
                    <TableRowTextField params={ this._textParams("fio", this.state.client.fio) } />
                </td>

                <td className="footable-visible v-middle">
                    <TableRowTextField params={ this._textParams("phone", this.state.client.phone) } />
                </td>

                <td className="footable-visible v-middle">
                    <TableRowTextField params={ this._textParams("email", this.state.client.email) } />
                </td>

                <td className="footable-visible v-middle">
                    <TableRowTextSelectField params={ this._textSelectParams("contract", this.state.client.contract, "contract_status", this.state.client.contract_status, this.props.contract_statuses) } />
                </td>

                <td className="footable-visible v-middle">
                    <TableRowTextField params={ this._textParams("payer", this.state.client.payer) } />
                </td>

                <td className="footable-visible v-middle">
                    <TableRowTextField params={ this._textParams("description", this.state.client.description) } />
                </td>

                <td className="footable-visible v-middle">
                    <TableRowSelectField params={ this._selectParams("discount", this.state.client.discount, this.props.discounts) } />
                </td>

                <td className="footable-visible v-middle">
                    <TableRowTextField params={ this._textParams("shipping_address", this.state.client.shipping_address) } />
                </td>

                <td className="footable-visible footable-last-column v-middle">
                    <ManagerClientTableRowActionMenu
                        edit={this.state.edit}
                        icon={true}
                        specifications_url={this.props.specifications_url}
                        branches_url={this.props.branches_url}
                        onEdit={this.toggleEdit}
                        onSave={this.onSave}
                        onCancel={this.cancelUpdate}
                        onDrop={this.onDrop}
                        dropConfirm={this.props.dropConfirm}
                    />
                </td>
            </tr>
        );
    }
});