var MultiplePrintRunLabel = React.createClass({
    render: function () {
        var labelName = function (prefix, index) {
            return prefix + (index + 1);
        };

        return <label className="col-sm-1 control-label btn-sm">{labelName(this.props.labelPrefix, this.props.index)}</label>;
    }
});

var MultiplePrintRunInput = React.createClass({
    getInitialState: function () {
        return {text: this.props.count};
    },
    onChange: function (e) {
        this.setState({text: e.target.value});
    },
    componentWillReceiveProps: function (prop) {
        this.setState({text: prop.count});
    },
    render: function () {
        var inputName = function (index) {
            return "print_runs[print_run_" + (index + 1) + "]";
        };

        return (
            <div className="col-sm-10">
                <input className="form-control input-sm" name={inputName(this.props.index)} onChange={this.onChange} value={this.state.text}/>
            </div>
        );
    }
});

var MultiplePrintRunRemoveButton = React.createClass({
    onClick: function () {
        this.props.onClick(this.props.index);
    },
    render: function () {
        return (
            <a className="btn btn-danger btn-sm" onClick={this.onClick}>{this.props.buttonName}</a>
        );
    }
});

var MultiplePrintRunRow = React.createClass({
    getInitialState: function () {
        return {showButton: this.showButton()};
    },
    showButton: function () {
        if (this.props.index > 0) {
            return true;
        }

        return false;
    },
    render: function () {
        return (
            <div className="form-group">
                <MultiplePrintRunLabel index={this.props.index} labelPrefix={this.props.labelPrefix} />
                <MultiplePrintRunInput index={this.props.index} count={this.props.count} />
                    { this.state.showButton ? <MultiplePrintRunRemoveButton index={this.props.index} buttonName={this.props.buttonName} onClick={this.props.onClick} /> : null }
            </div>
        )
    }
});

var MultiplePrintRun = React.createClass({
    getInitialState: function () {
        return {
            print_runs: this.props.print_runs,
            count_increment: 1000
        };
    },
    onClick: function (index) {
        var print_runs = this.state.print_runs;
        print_runs.splice(index, 1);
        this.setState({print_runs: print_runs});
    },
    createNewPrintRun: function () {
        if (this.isMounted()) {
            var print_runs = this.state.print_runs;
            var last_print_run = print_runs[print_runs.length - 1];
            print_runs.push(last_print_run + this.state.count_increment);

            this.setState({print_runs: print_runs});
        }
    },
    componentDidMount: function () {
        EventSystem.subscribe('print_run.create', this.createNewPrintRun);
    },
    render: function () {
        var addRow = function (count, index) {
            return <MultiplePrintRunRow key={index} index={index} count={count} labelPrefix={this.props.labelPrefix} buttonName={this.props.buttonName} onClick={this.onClick}/>;
        };

        return (
            <div className="wrapper-md">
                {this.state.print_runs.map(addRow, this)}
            </div>
        );
    }
});

var MultiplePrintRunAddButton = React.createClass({
    onClick: function () {
        EventSystem.publish('print_run.create');
    },
    render: function () {
        return (
            <a className="btn btn-success pull-right btn-sm" onClick={this.onClick}>{this.props.buttonName}</a>
        );
    }
});