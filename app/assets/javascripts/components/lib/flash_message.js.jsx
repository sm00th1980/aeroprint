var FlashMessage = React.createClass({
    getInitialState: function () {
        return {
            alert: this.props.alert,
            show: false,
            message: $.extend({}, this.props.message) //clone original object
        }
    },
    componentDidMount: function () {
        EventSystem.subscribe('flash_message.show', this.show);
    },
    show: function (obj) {
        this.setState({show: true, alert: obj.alert, message: obj.message});
    },
    hide: function (obj) {
        this.setState({show: false});
    },
    render: function () {
        if (this.state.show) {
            if (this.state.alert) {
                return (
                    <div className="wrapper-md p-b-none">
                        <div className="alert alert-danger m-b-none" role="alert">
                            <button type="button" className="close" onClick={this.hide}>
                                <span aria-hidden="true">&times;</span>
                                <span className="sr-only">Close</span>
                            </button>
                            <strong>Внимание!</strong> {this.state.message}
                        </div>
                    </div>
                );
            } else {
                return (
                    <div className="wrapper-md p-b-none">
                        <div className="alert alert-info m-b-none" role="alert">
                            <button type="button" className="close" onClick={this.hide}>
                                <span aria-hidden="true">&times;</span>
                                <span className="sr-only">Close</span>
                            </button>
                        {this.state.message}
                        </div>
                    </div>
                );
            }
        } else {
            return null;
        }
    }
});