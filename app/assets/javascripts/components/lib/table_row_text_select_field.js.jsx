var TableRowTextSelectField = React.createClass({
    propTypes: {
        params: React.PropTypes.shape({
            text_property: React.PropTypes.string.isRequired,
            text_value: React.PropTypes.string.isRequired,
            option_property: React.PropTypes.string.isRequired,
            option_value: React.PropTypes.number.isRequired,
            options: React.PropTypes.array.isRequired,
            edit: React.PropTypes.bool.isRequired,
            onEdit: React.PropTypes.func.isRequired,
            onSave: React.PropTypes.func.isRequired,
            onCancel: React.PropTypes.func.isRequired
        })
    },

    getInitialState: function () {
        return {text_value: this.props.params.text_value}
    },

    _name: function () {
        var _option = this._findByID(this.props.params.option_value);
        if (_option) {
            return _option.name;
        }

        return null;
    },
    _color: function () {
        var _option = this._findByID(this.props.params.option_value);
        if (_option) {
            return 'btn-' + _option.color;
        }

        return null;
    },
    _findByID: function (id) {
        var self = this;
        return $.grep(self.props.params.options, function (el) {
            return el.id == id;
        })[0];
    },
    _onTextChange: function (e) {
        this.setState({text_value: e.target.value});
        this.props.params.onEdit(this.props.params.text_property, e.target.value); //send value to parent component
    },
    _onOptionChange: function (e) {
        this.props.params.onEdit(this.props.params.option_property, e.target.value); //send value to parent component
    },
    _onKeyUp: function (e) {
        if (e.keyCode == 13) { //enter pressed
            this.props.params.onEdit(this.props.params.text_property, this.state.text_value); //send value to parent component
            this.props.params.onSave();
        }

        if (e.keyCode == 27) { //escape pressed
            this.props.params.onCancel();
        }
    },

    render: function () {
        if (this.props.params.edit) {
            var option = function (el) {
                return <option value={el.id}>{el.name}</option>
            };

            return (
                <div>
                    <input className="form-control input-sm" onChange={this._onTextChange} value={this.state.text_value} onKeyUp={this._onKeyUp}/>
                    <select className="form-control input-sm m-t-xs" value={this.props.params.option_value} onChange={this._onOptionChange}>
                    {this.props.params.options.map(option)}
                    </select>
                </div>
            )
        } else {
            return (
                <span className={ "label text-sm " + this._color() } title={this._name()}>{ this.props.params.text_value}</span>
            )
        }
    }
});