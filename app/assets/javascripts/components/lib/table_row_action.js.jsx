var TableRowActionSave = React.createClass({
    render: function () {
        if (this.props.icon) {
            return (
                <a className="btn btn-icon btn-danger" onClick={this.props.onClick}>
                    <i className="fa fa-save"></i>
                </a>
            )
        } else {
            return <a className="btn btn-danger btn-sm" onClick={this.props.onClick}>Сохранить</a>
        }
    }
});

var TableRowActionCancel = React.createClass({
    render: function () {
        if (this.props.icon) {
            return (
                <a className="btn btn-icon btn-default m-l-xs" onClick={this.props.onClick}>
                    <i className="fa fa-undo"></i>
                </a>
            )
        } else {
            return <a className="btn btn-default btn-sm m-l-xs" onClick={this.props.onClick}>Отмена</a>
        }
    }
});

var TableRowActionEdit = React.createClass({
    render: function () {
        if (this.props.icon) {
            return (
                <a className="btn btn-icon btn-success" onClick={this.props.onClick}>
                    <i className="glyphicon glyphicon-edit"></i>
                </a>
            )
        } else {
            return <a className="btn btn-success btn-sm" onClick={this.props.onClick}>Изменить</a>
        }
    }
});

var TableRowActionDrop = React.createClass({
    render: function () {
        if (this.props.icon) {
            return (
                <a className="btn btn-icon btn-danger m-l-xs" onClick={this.props.onClick}>
                    <i className="glyphicon glyphicon-remove"></i>
                </a>
            )
        } else {
            return <a className="btn btn-danger btn-sm m-l-xs" onClick={this.props.onClick}>Удалить</a>
        }
    }
});

var TableRowAction = React.createClass({

    propTypes: {
        params: React.PropTypes.shape({
            edit: React.PropTypes.bool.isRequired,
            onSave: React.PropTypes.func.isRequired,
            onCancel: React.PropTypes.func.isRequired,
            onEdit: React.PropTypes.func.isRequired,
            onDrop: React.PropTypes.func.isRequired,
            dropConfirm: React.PropTypes.string,
            icon: React.PropTypes.bool
        })
    },
    _onDrop: function () {
        if (this.props.params.dropConfirm) {
            if (confirm(this.props.params.dropConfirm)) {
                this.props.params.onDrop();
            }
        } else {
            this.props.params.onDrop();
        }
    },
    render: function () {
        if (this.props.params.edit) {
            return (
                <div>
                    <TableRowActionSave onClick={this.props.params.onSave} icon={this.props.params.icon}/>
                    <TableRowActionCancel onClick={this.props.params.onCancel} icon={this.props.params.icon}/>
                </div>
            )
        } else {
            return (
                <div>
                    <TableRowActionEdit onClick={this.props.params.onEdit} icon={this.props.params.icon}/>
                    <TableRowActionDrop onClick={this._onDrop} icon={this.props.params.icon}/>
                </div>
            )
        }
    }
});