var TableRowTextField = React.createClass({

    propTypes: {
        params: React.PropTypes.shape({
            property: React.PropTypes.string.isRequired,
            value: React.PropTypes.string.isRequired,
            edit: React.PropTypes.bool.isRequired,
            onEdit: React.PropTypes.func.isRequired,
            onSave: React.PropTypes.func.isRequired,
            onCancel: React.PropTypes.func.isRequired
        })
    },

    getInitialState: function () {
        return {text: this.props.params.value}
    },

    _onChange: function (e) {
        this.setState({text: e.target.value});
        this.props.params.onEdit(this.props.params.property, e.target.value); //send value to parent component
    },
    _onKeyUp: function (e) {
        if (e.keyCode == 13) { //enter pressed
            this.props.params.onEdit(this.props.params.property, this.state.text); //send value to parent component
            this.props.params.onSave();
        }

        if (e.keyCode == 27) { //escape pressed
            this.props.params.onCancel();
        }
    },

    render: function () {
        if (this.props.params.edit) {
            return (
                <input className="form-control input-sm" onChange={this._onChange} onKeyUp={this._onKeyUp} value={this.props.params.value}/>
            )
        } else {
            return (
                <span className="footable-toggle">{this.props.params.value}</span>
            )
        }
    }

});