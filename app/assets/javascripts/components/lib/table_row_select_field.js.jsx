var TableRowSelectField = React.createClass({
    propTypes: {
        params: React.PropTypes.shape({
            property: React.PropTypes.string.isRequired,
            value: React.PropTypes.number.isRequired,
            options: React.PropTypes.array.isRequired,
            edit: React.PropTypes.bool.isRequired,
            onEdit: React.PropTypes.func.isRequired
        })
    },

    _name: function () {
        var _option = this._findByID(this.props.params.value);
        if (_option) {
            return _option.name;
        }

        return null;
    },
    _onChange: function (e) {
        this.props.params.onEdit(this.props.params.property, e.target.value); //send value to parent component
    },
    _findByID: function (id) {
        var self = this;
        return $.grep(self.props.params.options, function (el) {
            return el.id == id;
        })[0];
    },

    render: function () {
        if (this.props.params.edit) {
            var option = function (el) {
                return <option value={el.id}>{el.name}</option>
            };

            return (
                <select className="form-control input-sm" value={this.props.params.value} onChange={this._onChange}>
                    {this.props.params.options.map(option)}
                </select>
            )
        } else {
            return (
                <span className="footable-toggle">{this._name()}</span>
            )
        }
    }
});