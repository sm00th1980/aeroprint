var ManagerCalculationComponents = React.createClass({

    getInitialState: function () {
        return {
            components: [this._firstComponent()],
            showComponentsAsList: false,
            selectedComponentId: 0
        };
    },
    componentDidMount: function () {
        this.props.componentsUpdate(this.state.components); //pass components to parent
    },

    _newComponent: function (id, name, stripes_count) {
        return {
            id: id,
            name: name,
            stripes_count: stripes_count,
            paper: this.props.papers[0].id,
            density: this.props.papers[0].densities[0],
            product_ver_mm: 297,
            product_gor_mm: 210,
            front_cmyk_color: this.props.cmyk_colors[0].id,
            back_cmyk_color: this.props.cmyk_colors[0].id,
            front_pantone_color: this.props.pantone_colors[0].id,
            back_pantone_color: this.props.pantone_colors[0].id,
            front_polish: this.props.polishes[0].id,
            back_polish: this.props.polishes[0].id
        }
    },

    _onChangeCount: function (count) {
        var self = this;
        if (!isNaN(parseInt(count, 10))) {
            var _components = [this._firstComponent()];
            _(parseInt(count, 10) - 1).times(function (index) {
                var _id = index + 1;

                _components.push(self._newComponent(_id, 'Блок ' + _id, 1));
            });

            this.setState({components: _components});

            this.props.componentsUpdate(_components); //pass components to parent
        }
    },
    _onChangeShow: function (count) {
        this.setState({showComponentsAsList: !this.state.showComponentsAsList});
    },
    _firstComponent: function () {
        return this._newComponent(0, 'Обложка', 4);
    },
    _onChangeComponent: function (component_id) {
        this.setState({selectedComponentId: component_id});
    },

    _onUpdateComponent: function (component_id, property, value) {
        var updated_component = _.find(this.state.components, function (component) {
            return component.id == component_id;
        });
        updated_component[property] = value;

        var components = _.reject(this.state.components, function (component) {
            return component.id == component_id;
        });

        components.push(updated_component);

        this.setState({
            components: _.sortBy(components, function (component) {
                return component.id
            })
        });

        this.props.componentsUpdate(this.state.components); //pass components to parent
    },

    render: function () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <ManagerCalculationComponentsPanel
                        components={this.state.components}
                        showComponentsAsList={this.state.showComponentsAsList}
                        value={this.state.selectedComponentId}
                        onChangeCount={this._onChangeCount}
                        onChangeShow={this._onChangeShow}
                        onChangeComponent={this._onChangeComponent}
                        onUpdateComponent={this._onUpdateComponent}
                        papers={this.props.papers}
                        cmyk_colors={this.props.cmyk_colors}
                        pantone_colors={this.props.pantone_colors}
                        polishes={this.props.polishes}
                        product={this.props.product}
                    />
                </div>
            </div>
        )
    }

});