var ManagerCalculationComponentSettingsPaper = React.createClass({

    _onUpdate: function (property, value) {
        this.props.onUpdateComponent(this.props.component.id, property, value);
    },
    _densities: function (paper_id) {
        var paper = _.find(this.props.papers, function (paper) {
            return paper.id == paper_id;
        });

        return _.map(paper.densities, function (density) {
            return {id: density, name: density}
        });
    },

    render: function () {
        return (
            <div>

                <div className="row">

                    <div className="col-lg-12 b-r">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.paper}
                                options={ this.props.papers }
                                header="Бумага"
                                name="paper"
                                helpContent="Тип бумаги для компоненты"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                </div>

                <div className="row">

                    <div className="col-lg-12 b-r">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.density}
                                options={ this._densities(this.props.component.paper) }
                                header="Плотность(гр/м2)"
                                name="density"
                                helpContent="Плотность бумаги(грамм на метр в квадрате)"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                </div>

            </div>
        )
    }

});