var ManagerCalculationComponentSettings = React.createClass({

    render: function () {
        return (
            <div className="row">
                <div className="col-lg-2">
                    <ManagerCalculationComponentSettingsName
                        component={this.props.component}
                        product={this.props.product}
                        onUpdateComponent={this.props.onUpdateComponent}
                    />
                </div>

                <div className="col-lg-2">
                    <ManagerCalculationComponentSettingsPaper
                        component={this.props.component}
                        papers={this.props.papers}
                        onUpdateComponent={this.props.onUpdateComponent}
                    />
                </div>

                <div className="col-lg-2">
                    <ManagerCalculationComponentSettingsSize
                        component={this.props.component}
                        onUpdateComponent={this.props.onUpdateComponent}
                    />
                </div>

                <div className="col-lg-6">
                    <ManagerCalculationComponentSettingsColors
                        component={this.props.component}
                        cmyk_colors={this.props.cmyk_colors}
                        pantone_colors={this.props.pantone_colors}
                        polishes={this.props.polishes}
                        onUpdateComponent={this.props.onUpdateComponent}
                    />
                </div>
            </div>
        )
    }

});