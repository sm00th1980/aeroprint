var ManagerCalculationComponentSettingsName = React.createClass({

    _onUpdate: function (property, value) {
        this.props.onUpdateComponent(this.props.component.id, property, value);
    },
    _stripes_count: function () {
        if (this.props.product.many_page) {
            return (
                <ManagerCalculationInput
                    value={this.props.component.stripes_count}
                    header="Число полос"
                    name="stripes_count"
                    helpContent="Число полос для компоненты"
                    onChange={this._onUpdate}
                />
            )
        } else {
            return null;
        }

    },

    render: function () {
        return (
            <div>

                <div className="row">

                    <div className="col-lg-12 b-r">
                        <div className="form-group">
                            <ManagerCalculationInput
                                value={this.props.component.name}
                                header="Наименование"
                                name="name"
                                helpContent="Индивидуальное имя для компоненты"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                </div>

                <div className="row">

                    <div className="col-lg-12 b-r">
                        <div className="form-group">
                            {this._stripes_count()}
                        </div>
                    </div>

                </div>

            </div>
        )
    }

});