var ManagerCalculationComponentSettingsSize = React.createClass({

    _onUpdate: function (property, value) {
        this.props.onUpdateComponent(this.props.component.id, property, value);
    },

    render: function () {
        return (
            <div>

                <div className="row">

                    <div className="col-lg-12 b-r">
                        <div className="form-group">
                            <ManagerCalculationInput
                                value={this.props.component.product_gor_mm}
                                header="Размер по гор.(мм)"
                                name="product_gor_mm"
                                helpContent="Размер продукции по горизонтали в мм"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                </div>

                <div className="row">

                    <div className="col-lg-12 b-r">
                        <div className="form-group">
                            <ManagerCalculationInput
                                value={this.props.component.product_ver_mm}
                                header="Размер по верт.(мм)"
                                name="product_ver_mm"
                                helpContent="Размер продукции по вертикали в мм"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                </div>

            </div>
        )
    }

});