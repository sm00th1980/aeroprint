var ManagerCalculationComponentSettingsColors = React.createClass({

    _onUpdate: function (property, value) {
        this.props.onUpdateComponent(this.props.component.id, property, value);
    },

    render: function () {
        return (
            <div>

                <div className="row">

                    <div className="col-lg-4">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.front_cmyk_color}
                                options={ this.props.cmyk_colors }
                                header="CMYK(лицо)"
                                name="front_cmyk_color"
                                helpContent="Краски CMYK для лица"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                    <div className="col-lg-4">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.front_pantone_colors}
                                options={ this.props.pantone_colors }
                                header="Pantone(лицо)"
                                name="front_pantone_color"
                                helpContent="Краски PANTONE для лица"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                    <div className="col-lg-4">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.front_polish}
                                options={ this.props.polishes }
                                header="Лак(лицо)"
                                name="front_polish"
                                helpContent="Использовать лак для лица"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>
                </div>

                <div className="row">

                    <div className="col-lg-4">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.back_cmyk_color}
                                options={ this.props.cmyk_colors }
                                header="CMYK(оборот)"
                                name="back_cmyk_color"
                                helpContent="Краски CMYK для оборота"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                    <div className="col-lg-4">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.back_pantone_colors}
                                options={ this.props.pantone_colors }
                                header="Pantone(оборот)"
                                name="back_pantone_color"
                                helpContent="Краски PANTONE для оборота"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>

                    <div className="col-lg-4">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.component.back_polish}
                                options={ this.props.polishes }
                                header="Лак(оборот)"
                                name="back_polish"
                                helpContent="Использовать лак для оборота"
                                onChange={this._onUpdate}
                            />
                        </div>
                    </div>
                </div>

            </div>
        )
    }

});