var ManagerCalculationComponentsPanel = React.createClass({

    _newRow: function (component) {
        return (
            <li className="list-group-item">
                <ManagerCalculationComponentSettings
                    component={component}
                    papers={this.props.papers}
                    cmyk_colors={this.props.cmyk_colors}
                    pantone_colors={this.props.pantone_colors}
                    polishes={this.props.polishes}
                    onUpdateComponent={this.props.onUpdateComponent}
                    product={this.props.product}
                />
            </li>
        )
    },
    _checked: function () {
        if (this.props.showComponentsAsList) {
            return 'checked';
        }
    },
    _onChangeCount: function (e) {
        this.props.onChangeCount(e.target.value);
    },
    _onChangeShow: function () {
        this.props.onChangeShow();
    },
    _onChangeComponent: function (e) {
        this.props.onChangeComponent(e.target.value);
    },
    _selectComponent: function () {
        if (!this.props.showComponentsAsList) {
            var option = function (el) {
                return <option value={el.id}>{el.name}</option>
            };

            return (
                <div className="col-lg-6">
                    <div className="form-group">
                        <label data-toggle="popover" data-content="Текущая компонента">Компонента</label>
                        <select className="form-control" value={this.props.value} onChange={this._onChangeComponent}>
                                {this.props.components.map(option)}
                        </select>
                    </div>
                </div>
            )
        }

        return null;
    },
    _header: function () {
        return (
            <li className="list-group-item">
                <div className="row">
                    <div className="col-lg-6">
                        <div className="form-group">
                            <label data-toggle="popover" data-content="Общее количество компонент в продукции">Число компонент</label>
                            <input className="form-control" value={this.props.components.length} onChange={this._onChangeCount}/>
                        </div>
                    </div>

                    {this._selectComponent()}
                </div>
            </li>
        )
    },
    _components: function () {
        var self = this;
        if (self.props.showComponentsAsList) {
            //show all components
            return _.map(self.props.components, self._newRow);
        } else {
            //show only selected component
            var component = _.find(self.props.components, function (component) {
                return component.id == self.props.value;
            });
            return self._newRow(component);
        }
    },

    render: function () {
        if (this.props.components.length > 0) {
            return (
                <div className="panel panel-default">
                    <div className="panel-heading wrapper b-b b-light">

                        <div className="row">
                            <div className="col-lg-6 pull-right">
                                <label className="i-switch i-switch-lg bg-warning pull-right" data-toggle="popover" data-content="Переключатель режима показа компонент - списком или выбором по одному">
                                    <input type="checkbox" checked={this._checked()} onChange={this._onChangeShow}/>
                                    <i></i>
                                </label>
                            </div>

                            <div className="col-lg-6">
                                <h4 className="font-thin m-b-xs m-t-xs">Компоненты</h4>
                            </div>
                        </div>
                    </div>

                    <ul className="list-group list-group-lg">
                        {this._header()}
                        {this._components()}
                    </ul>

                </div>
            )
        } else {
            return null;
        }
    }

});