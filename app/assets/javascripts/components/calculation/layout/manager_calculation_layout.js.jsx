var ManagerCalculationLayout = React.createClass({

    getInitialState: function () {
        return {
            src: '',
            spaceFilling: 0,
            fullSize: ''
        }
    },

    componentDidMount: function () {
        var self = this;

        $.ajax({
            url: this.props.url,
            data: {
                width: $(React.findDOMNode(this.refs.rootNode)).width()
            },
            success: function (result) {
                if (result['success']) {
                    self.setState({
                        src: result['image_data'],
                        spaceFilling: result['space_filling'],
                        fullSize: result['full_size']
                    });
                } else {
                    alert("Ошибка на стороне клиента. Пожалуйста, обратитесь в техподдержку.");
                }
            },
            error: function () {
                alert("Ошибка на стороне сервера. Пожалуйста, обратитесь в техподдержку.");
            }
        });
    },

    render: function () {
        return (
            <div ref="rootNode">
                <div className="form-group">
                    <label>Раскладка на лист</label>
                    <img src={this.state.src}/>
                </div>

                <div className="form-group">
                    <ManagerCalculationLayoutTable
                        spaceFilling={this.state.spaceFilling}
                        fullSize={this.state.fullSize}
                    />
                </div>
            </div>
        )
    }

});