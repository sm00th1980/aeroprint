var ManagerCalculationLayoutTable = React.createClass({

    render: function () {
        return (
            <div className="panel panel-default">
                <table className="table table-striped table-bordered">
                    <tbody>
                        <tr>
                            <td>Занятость</td>
                            <td>{this.props.spaceFilling}</td>
                        </tr>
                        <tr>
                            <td>Размер</td>
                            <td>{this.props.fullSize}</td>
                        </tr>
                        <tr>
                            <td>Шкала</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Кресты</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Клапан</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }

});