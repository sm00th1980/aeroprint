var ManagerCalculation = React.createClass({

    getInitialState: function () {
        return {
            header: {
                client: this.props.clients[0].id,
                product: this.props.products[0].id,
                print_run: 1000,
                binding_type: this.props.binding_types[0].id,
                extended_cut: this.props.extended_cuts[0].id,
                service_margin: this.props.service_margins[0].id
            },
            product: this.props.products[0],
            components: [],
            cover_works: [],
            uv_polishes: []
        };
    },

    _onClick: function () {
        var self = this;

        $.ajax({
            type: "POST",
            url: self.props.create_url,
            data: self._params(),
            success: function (response) {
                if (response.success) {
                    window.location.href = response.url;
                } else {
                    alert(response.error);
                }
            },
            error: function () {
                alert('Возникла ошибка на сервере. Обратитесь с поддержку.');
            }
        });
    },
    _params: function () {
        if (!this.state.product.slashed) {
            //for non slashed product
            return {
                client: this.state.header.client,
                product: this.state.header.product,
                print_run: this.state.header.print_run,
                binding_type: this.state.header.binding_type,
                use_extended_cuts: this.state.header.extended_cut,
                service_margin: this.state.header.service_margin,
                components: this.state.components,
                cover_works: this.state.cover_works,
                uv_polishes: this.state.uv_polishes
            }
        } else {
            //for slashed product
            return {
                client: this.state.header.client,
                product: this.state.header.product,
                print_run: this.state.header.print_run,
                use_extended_cuts: this.state.header.extended_cut,
                service_margin: this.state.header.service_margin,
                components: this.state.components
            }
        }
    },
    _headerUpdate: function (property, value) {
        var header = $.extend(true, [], this.state.header);
        header[property] = value;
        this.setState({header: header});

        if (property == 'product') {
            this._changeProduct(value);
        }
    },
    _componentsUpdate: function (components) {
        this.setState({components: components});
    },
    _componentUpdate: function (paper, format, stamp, cmyk, pantone, polish) {

        //construct new component for slashed product
        var component = {
            paper: paper,
            format: format,
            stamp: stamp,
            cmyk: cmyk,
            pantone: pantone,
            polish: polish
        };

        this.setState({components: [component]});
    },
    _coverWorksUpdate: function (cover_works) {
        this.setState({cover_works: cover_works});
    },
    _uvPolishesUpdate: function (uv_polishes) {
        this.setState({uv_polishes: uv_polishes});
    },
    _changeProduct: function (product_id) {
        var product = _.find(this.props.products, function (product) {
            return product.id == product_id;
        });

        this.setState({product: product});
    },
    _componentsSettings: function () {
        if (this.state.product && this.state.product.slashed) {
            return (
                <div className="row">
                    <div className="col-lg-12">
                        <ManagerCalculationSlashedProductComponentPanel
                            papers={this.props.papers}
                            cmyk_colors={this.props.cmyk_colors}
                            pantone_colors={this.props.pantone_colors}
                            polishes={this.props.polishes}
                            componentUpdate={this._componentUpdate}
                            />
                    </div>
                </div>
            )
        } else {
            return (
                <div className="row">
                    <div className="col-lg-12">
                        <ManagerCalculationComponents
                            papers={this.props.papers}
                            cmyk_colors={this.props.cmyk_colors}
                            pantone_colors={this.props.pantone_colors}
                            polishes={this.props.polishes}
                            componentsUpdate={this._componentsUpdate}
                            product={this.state.product}
                            />
                    </div>
                </div>
            )
        }
    },


    render: function () {
        return (
            <div>
                <div className="bg-light lter b-b wrapper-md hidden-print">
                    <button className="btn btn-primary pull-right btn-sm m-l-xs" onClick={this._onClick}>Рассчитать
                    </button>
                    <h1 className="m-n font-thin h3">Новый расчёт</h1>
                </div>

                <div className="wrapper-md">
                    <div className="row">
                        <div className="col-lg-12">

                            <div className="row">
                                <div className="col-lg-10">

                                    <div className="row">
                                        <ManagerCalculationHeader
                                            clients={this.props.clients}
                                            extended_cuts={this.props.extended_cuts}
                                            service_margins={this.props.service_margins}
                                            products={this.props.products}
                                            binding_types={this.props.binding_types}
                                            onChange={this._headerUpdate}
                                            header={this.state.header}
                                            />
                                    </div>

                                    {this._componentsSettings()}

                                    <div className="row">
                                        <div className="col-lg-6">
                                            <ManagerCalculationCoverWorks
                                                types={this.props.types}
                                                coverWorksUpdate={this._coverWorksUpdate}
                                                />
                                        </div>

                                        <div className="col-lg-6">
                                            <ManagerCalculationUVPolish
                                                components={this.state.components}
                                                types={this.props.uv_polish_types}
                                                counts={this.props.uv_polish_counts}
                                                uvPolishesUpdate={this._uvPolishesUpdate}
                                                />
                                        </div>
                                    </div>
                                </div>

                                <div className="col-lg-2">
                                    <ManagerCalculationLayout url={this.props.url}/>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        )
    }

});