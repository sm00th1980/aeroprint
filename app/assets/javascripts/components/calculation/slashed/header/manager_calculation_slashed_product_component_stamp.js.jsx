var ManagerCalculationSlashedProductComponentStamp = React.createClass({

    getInitialState: function () {
        return {
            exists: this.props.stamp.exists,
            price: this.props.stamp.price,
            showPrice: !this.props.stamp.exists
        };
    },

    _stamps: function () {
        return [{id: true, name: 'Уже есть'}, {id: false, name: 'Ещё нет'}];
    },
    _onStampChange: function (name, value) {
        var exists = this.state.exists;
        var price = this.state.price;

        if (name == 'exists') {
            exists = value;
            this._toggleShowPrice();
        }

        if (name == 'price') {
            price = value;
        }

        this.props.stampUpdate({exists: exists, price: price});  //pass to parent
    },
    _toggleShowPrice: function () {
        this.setState({showPrice: !this.state.showPrice});
    },

    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.stamp.exists}
                                options={ this._stamps() }
                                header="Штамп"
                                name="exists"
                                onChange={this._onStampChange}
                            />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="form-group">
                            <ManagerCalculationInput
                                value={this.props.stamp.price}
                                show={this.state.showPrice}
                                header="Стоимость штампа"
                                name="price"
                                onChange={this._onStampChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});