var ManagerCalculationSlashedProductComponentPaper = React.createClass({

    getInitialState: function () {
        return {
            id: this.props.paper.id,
            density: this.props.paper.density
        };
    },

    _onPaperChange: function (name, value) {
        var id = this.state.id;
        var density = this.state.density;

        if (name == 'paper') {
            id = value;
        }

        if (name == 'density') {
            density = value;
        }

        this.props.paperUpdate({id: id, density: density});  //pass to parent
    },

    _densities: function () {
        var self = this;
        var paper = _.find(this.props.papers, function (paper) {
            return paper.id == self.props.paper.id;
        });

        return _.map(paper.densities, function (density) {
            return {id: density, name: density}
        });
    },

    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.paper.id}
                                options={ this.props.papers }
                                header="Бумага"
                                name="paper"
                                onChange={this._onPaperChange}
                            />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.paper.density}
                                options={ this._densities() }
                                header="Плотность(гр/м2)"
                                name="density"
                                onChange={this._onPaperChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});