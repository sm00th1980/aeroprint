var ManagerCalculationSlashedProductComponentHeader = React.createClass({

    _stampUpdate: function (stamp) {
        this._onHeaderChange(this.props.paper, this.props.format, stamp);
    },
    _formatUpdate: function (format) {
        this._onHeaderChange(this.props.paper, format, this.props.stamp);
    },
    _paperUpdate: function (paper) {
        this._onHeaderChange(paper, this.props.format, this.props.stamp);
    },
    _onHeaderChange: function (paper, format, stamp) {
        this.props.headerUpdate(paper, format, stamp);
    },

    render: function () {
        return (
            <div className="row">
                <div className="col-lg-4">
                    <ManagerCalculationSlashedProductComponentPaper
                        paper={this.props.paper}
                        paperUpdate={this._paperUpdate}
                        papers={this.props.papers}
                    />
                </div>

                <div className="col-lg-4">
                    <ManagerCalculationSlashedProductComponentFormat
                        format={this.props.format}
                        formatUpdate={this._formatUpdate}
                    />
                </div>

                <div className="col-lg-4">
                    <ManagerCalculationSlashedProductComponentStamp
                        stamp={this.props.stamp}
                        stampUpdate={this._stampUpdate}
                    />
                </div>
            </div>
        )
    }

});