var ManagerCalculationSlashedProductComponentFormat = React.createClass({

    getInitialState: function () {
        return {
            a2: this.props.format.a2,
            repeat_count: this.props.format.repeat_count
        };
    },

    _formats: function () {
        return [{id: true, name: 'A2'}, {id: false, name: 'A3'}];
    },

    _onFormatChange: function (name, value) {
        var a2 = this.state.a2;
        var repeat_count = this.state.repeat_count;

        if (name == 'a2') {
            a2 = value;
        }

        if (name == 'repeat_count') {
            repeat_count = value;
        }

        this.props.formatUpdate({a2: a2, repeat_count: repeat_count});  //pass to parent
    },

    render: function () {
        return (
            <div>
                <div className="row">
                    <div className="col-lg-12">
                        <div className="form-group">
                            <ManagerCalculationSelect
                                value={this.props.format.a2}
                                options={ this._formats() }
                                header="Формат листа"
                                name="a2"
                                onChange={this._onFormatChange}
                            />
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-12">
                        <div className="form-group">
                            <ManagerCalculationInput
                                value={this.props.format.repeat_count}
                                header="Количество повторов на листе"
                                name="repeat_count"
                                onChange={this._onFormatChange}
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

});