var ManagerCalculationSlashedProductComponentFooter = React.createClass({

    _cmykUpdate: function (cmyk) {
        this._onFooterChange(cmyk, this.props.pantone, this.props.polish);
    },
    _pantoneUpdate: function (pantone) {
        this._onFooterChange(this.props.cmyk, pantone, this.props.polish);
    },
    _polishUpdate: function (polish) {
        this._onFooterChange(this.props.cmyk, this.props.pantone, polish);
    },
    _onFooterChange: function (cmyk, pantone, polish) {
        this.props.footerUpdate(cmyk, pantone, polish);
    },
    _cmykParams: function () {
        return {
            front: {
                value: this.props.cmyk.front,
                header: "CMYK(лицо)"
            },
            back: {
                value: this.props.cmyk.back,
                header: "CMYK(оборот)"
            }
        }
    },
    _pantoneParams: function () {
        return {
            front: {
                value: this.props.pantone.front,
                header: "Pantone(лицо)"
            },
            back: {
                value: this.props.pantone.back,
                header: "Pantone(оборот)"
            }
        }
    },
    _polishParams: function () {
        return {
            front: {
                value: this.props.polish.front,
                header: "Лак(лицо)"
            },
            back: {
                value: this.props.polish.back,
                header: "Лак(оборот)"
            }
        }
    },

    render: function () {
        return (
            <div className="row">
                <div className="col-lg-4">
                    <ManagerCalculationSlashedProductComponentSelects
                        front={this._cmykParams().front}
                        back={this._cmykParams().back}
                        options={ this.props.cmyk_colors }
                        onUpdate={this._cmykUpdate}
                    />
                </div>

                <div className="col-lg-4">
                    <ManagerCalculationSlashedProductComponentSelects
                        front={this._pantoneParams().front}
                        back={this._pantoneParams().back}
                        options={ this.props.pantone_colors }
                        onUpdate={this._pantoneUpdate}
                    />
                </div>

                <div className="col-lg-4">
                    <ManagerCalculationSlashedProductComponentSelects
                        front={this._polishParams().front}
                        back={this._polishParams().back}
                        options={ this.props.polishes }
                        onUpdate={this._polishUpdate}
                    />
                </div>

            </div>
        )
    }

});