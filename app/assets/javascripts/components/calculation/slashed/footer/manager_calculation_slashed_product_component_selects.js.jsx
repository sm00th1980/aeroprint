var ManagerCalculationSlashedProductComponentSelects = React.createClass({

    _onChange: function (name, value) {
        var front = this.props.front.value;
        var back = this.props.back.value;

        if (name == 'front') {
            front = value;
        }

        if (name == 'back') {
            back = value;
        }

        this.props.onUpdate({front: front, back: back});  //pass to parent
    },

    _frontSelect: function () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="form-group">
                        <ManagerCalculationSelect
                            value={this.props.front.value}
                            options={ this.props.options }
                            header={this.props.front.header}
                            name="front"
                            onChange={this._onChange}
                        />
                    </div>
                </div>
            </div>
        )
    },
    _backSelect: function () {
        return (
            <div className="row">
                <div className="col-lg-12">
                    <div className="form-group">
                        <ManagerCalculationSelect
                            value={this.props.back.value}
                            options={ this.props.options }
                            header={this.props.back.header}
                            name="back"
                            onChange={this._onChange}
                        />
                    </div>
                </div>
            </div>
        )
    },


    render: function () {
        return (
            <div>
                {this._frontSelect()}
                {this._backSelect()}
            </div>
        )
    }

});