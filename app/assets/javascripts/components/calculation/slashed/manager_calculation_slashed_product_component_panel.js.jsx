var ManagerCalculationSlashedProductComponentPanel = React.createClass({
    getInitialState: function () {
        return {
            paper: {
                id: this.props.papers[0].id,
                density: this.props.papers[0].densities[0]
            },
            format: {
                a2: true,
                repeat_count: 1
            },
            stamp: {
                exists: true,
                price: 0
            },
            cmyk: {
                front: this.props.cmyk_colors[0].id,
                back: this.props.cmyk_colors[0].id
            },
            pantone: {
                front: this.props.pantone_colors[0].id,
                back: this.props.pantone_colors[0].id
            },
            polish: {
                front: this.props.polishes[0].id,
                back: this.props.polishes[0].id
            }
        };
    },

    _onHeaderChange: function (paper, format, stamp) {
        this.setState({paper: paper, format: format, stamp: stamp});
        this.props.componentUpdate(paper, format, stamp, this.state.cmyk, this.state.pantone, this.state.polish); //pass to parent
    },

    _onFooterChange: function (cmyk, pantone, polish) {
        this.setState({cmyk: cmyk, pantone: pantone, polish: polish});
        this.props.componentUpdate(this.state.paper, this.state.format, this.state.stamp, cmyk, pantone, polish); //pass to parent
    },

    render: function () {
        return (
            <div className="panel panel-default">
                <div className="panel-heading wrapper b-b b-light">

                    <div className="row">
                        <div className="col-lg-12">
                            <h4 className="font-thin m-b-xs m-t-xs">Компонент для вырубки</h4>
                        </div>
                    </div>
                </div>

                <ul className="list-group list-group-lg">
                    <li className="list-group-item">
                        <ManagerCalculationSlashedProductComponentHeader
                            paper={this.state.paper}
                            format={this.state.format}
                            stamp={this.state.stamp}
                            papers={this.props.papers}
                            headerUpdate={this._onHeaderChange}
                        />
                    </li>

                    <li className="list-group-item">
                        <ManagerCalculationSlashedProductComponentFooter
                            cmyk={this.state.cmyk}
                            pantone={this.state.pantone}
                            polish={this.state.polish}
                            cmyk_colors={this.props.cmyk_colors}
                            pantone_colors={this.props.pantone_colors}
                            polishes={this.props.polishes}
                            footerUpdate={this._onFooterChange}
                        />
                    </li>
                </ul>

            </div>
        )
    }

});