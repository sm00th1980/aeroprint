var ManagerCalculationUVPolishRow = React.createClass({
    propTypes: {
        uv_polish: React.PropTypes.object.isRequired,
        components: React.PropTypes.array.isRequired,
        types: React.PropTypes.array.isRequired,
        counts: React.PropTypes.array.isRequired
    },

    getInitialState: function () {
        return {edit: false}
    },

    _toggleEdit: function () {
        this.setState({edit: !this.state.edit});
    },
    _onDrop: function () {
        this.props.onDrop(this.props.index);
    },
    _onSave: function () {
        this.props.onSave();
        this._toggleEdit();
    },
    _onChange: function (property, new_value) {
        this.props.onChange(this.props.index, property, new_value);
    },
    _onCancel: function () {
        this.props.onCancel();
        this._toggleEdit();
    },
    _componentSelectParams: function () {
        return {
            property: "component",
            value: parseInt(this.props.uv_polish.component, 10),
            options: this.props.components,
            edit: this.state.edit,
            onEdit: this._onChange
        }
    },
    _typeSelectParams: function () {
        return {
            property: "type",
            value: parseInt(this.props.uv_polish.type, 10),
            options: this.props.types,
            edit: this.state.edit,
            onEdit: this._onChange
        }
    },
    _countSelectParams: function () {
        return {
            property: "count",
            value: parseInt(this.props.uv_polish.count, 10),
            options: this.props.counts,
            edit: this.state.edit,
            onEdit: this._onChange
        }
    },

    render: function () {

        return (
            <tr>
                <td className="footable-visible footable-first-column v-middle">
                    <TableRowSelectField params={ this._typeSelectParams() }/>
                </td>

                <td className="footable-visible footable-first-column v-middle">
                    <TableRowSelectField params={ this._countSelectParams() }/>
                </td>

                <td className="footable-visible footable-first-column v-middle">
                    <TableRowSelectField params={ this._componentSelectParams() }/>
                </td>

                <td className="footable-visible footable-last-column v-middle">
                    <TableRowAction params={ {
                        edit: this.state.edit,
                        onEdit: this._toggleEdit,
                        onSave: this._onSave,
                        onCancel: this._onCancel,
                        onDrop: this._onDrop,
                        icon: true
                    } }/>
                </td>
            </tr>
        );
    }

});