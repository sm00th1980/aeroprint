var ManagerCalculationUVPolishTable = React.createClass({

    _newRow: function (data, index) {
        return (
            <ManagerCalculationUVPolishRow
                index={index}
                uv_polish={this.props.uv_polishes[index]}
                types={this.props.types}
                counts={this.props.counts}
                onDrop={this.props.onDrop}
                onSave={this.props.onSave}
                onChange={this.props.onChange}
                onCancel={this.props.onCancel}
                components={this.props.components}
                />
        )
    },

    render: function () {
        if (this.props.uv_polishes.length > 0) {
            return (
                <table className="table table-striped table-bordered b-t b-light m-b-none">
                    <thead>
                    <tr>
                        <th>Тип</th>
                        <th>Число сторон</th>
                        <th>Компонент</th>
                        <th width="104">Действия</th>
                    </tr>
                    </thead>

                    <tbody>
                    {this.props.uv_polishes.map(this._newRow, this)}
                    </tbody>
                </table>
            );
        } else {
            return null;
        }
    }

});