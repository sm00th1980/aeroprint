var ManagerCalculationUVPolish = React.createClass({

    propTypes: {
        types: React.PropTypes.array.isRequired
    },

    getInitialState: function () {
        return {
            uv_polishes: [], //rows
            copy_uv_polishes: [] //copy_rows
        };
    },

    _addRow: function () {
        var rows = $.extend(true, [], this.state.uv_polishes); //deep copy array
        rows.push({
            type: this.props.types[0].id,
            count: this.props.counts[0].id,
            component: this._components()[0].id
        });

        this.setState({uv_polishes: rows, copy_uv_polishes: rows});
        this.props.uvPolishesUpdate(rows); //pass it to parent
    },
    _onDrop: function (index) {
        if (typeof index !== 'undefined' && index >= 0) {
            var rows = $.extend(true, [], this.state.uv_polishes); //deep copy array
            rows.splice(index, 1);

            this.setState({uv_polishes: rows, copy_uv_polishes: rows});
        }
    },
    _onSave: function () {
        this.setState({uv_polishes: this.state.uv_polishes, copy_uv_polishes: this.state.uv_polishes});

        this.props.uvPolishesUpdate(this.state.uv_polishes); //pass it to parent
    },
    _onCancel: function () {
        this.setState({uv_polishes: this.state.copy_uv_polishes, copy_uv_polishes: this.state.copy_uv_polishes});
    },
    _onChange: function (index, property, new_value) {
        //updating
        var rows = $.extend(true, [], this.state.uv_polishes); //deep copy array
        var uv_polish = rows[index];
        uv_polish[property] = new_value;

        rows[index] = uv_polish;
        this.setState({uv_polishes: rows, copy_uv_polishes: this.state.copy_uv_polishes});
    },
    _components: function () {
        var rows = $.extend(true, [], this.props.components); //deep copy array
        rows.push({
            id: -1,
            name: 'Все компоненты'
        });

        return rows;
    },

    render: function () {
        return (
            <div className="panel panel-default">

                <div className="panel-heading wrapper b-b b-light">

                    <div className="row">
                        <div className="col-lg-10">
                            <h4 className="font-thin m-b-xs m-t-xs">УФ лак</h4>
                        </div>

                        <div className="col-lg-2">
                            <a className="btn btn-icon btn-success btn-sm pull-right" onClick={this._addRow}>
                                <i className="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                </div>

                <ManagerCalculationUVPolishTable
                    uv_polishes={this.state.uv_polishes}
                    components={this._components()}
                    types={this.props.types}
                    counts={this.props.counts}
                    onDrop={this._onDrop}
                    onSave={this._onSave}
                    onCancel={this._onCancel}
                    onChange={this._onChange}
                    />

            </div>
        )
    }

});