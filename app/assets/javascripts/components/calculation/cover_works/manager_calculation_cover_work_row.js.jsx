var ManagerCalculationCoverWorkRow = React.createClass({
    propTypes: {
        types: React.PropTypes.array.isRequired
    },

    getInitialState: function () {
        return {edit: false}
    },

    _toggleEdit: function () {
        this.setState({edit: !this.state.edit});
    },
    _onDrop: function () {
        this.props.onDrop(this.props.index);
    },
    _onSave: function () {
        this.props.onSave();
        this._toggleEdit();
    },
    _onChange: function (property, new_value) {
        this.props.onChange(this.props.index, property, new_value);
    },
    _onCancel: function () {
        this.props.onCancel();
        this._toggleEdit();
    },
    _selectParams: function () {
        return {
            property: "type",
            value: parseInt(this.props.cover_work.type, 10),
            options: this.props.types,
            edit: this.state.edit,
            onEdit: this._onChange
        }
    },

    render: function () {

        return (
            <tr>
                <td className="footable-visible footable-first-column v-middle">
                    <TableRowSelectField params={ this._selectParams() } />
                </td>

                <td className="footable-visible footable-last-column v-middle">
                    <TableRowAction params={ {
                        edit: this.state.edit,
                        onEdit: this._toggleEdit,
                        onSave: this._onSave,
                        onCancel: this._onCancel,
                        onDrop: this._onDrop,
                        icon: true
                    } } />
                </td>
            </tr>
        );
    }

});