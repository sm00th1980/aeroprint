var ManagerCalculationCoverWorks = React.createClass({

    propTypes: {
        types: React.PropTypes.array.isRequired
    },

    getInitialState: function () {
        return {
            cover_works: [], //rows
            copy_cover_works: [] //copy_rows
        };
    },

    _addRow: function () {
        var rows = $.extend(true, [], this.state.cover_works); //deep copy array
        rows.push({type: this.props.types[0].id, price: rows.length + 1});

        this.setState({cover_works: rows, copy_cover_works: rows});
    },
    _onDrop: function (index) {
        if (typeof index !== 'undefined' && index >= 0) {
            var rows = $.extend(true, [], this.state.cover_works); //deep copy array
            rows.splice(index, 1);

            this.setState({cover_works: rows, copy_cover_works: rows});
        }
    },
    _onSave: function () {
        this.setState({cover_works: this.state.cover_works, copy_cover_works: this.state.cover_works});

        this.props.coverWorksUpdate(this.state.cover_works); //pass it to parent
    },
    _onCancel: function () {
        this.setState({cover_works: this.state.copy_cover_works, copy_cover_works: this.state.copy_cover_works});
    },
    _onChange: function (index, property, new_value) {
        //updating
        var rows = $.extend(true, [], this.state.cover_works); //deep copy array
        var cover_work = rows[index];
        cover_work[property] = new_value;

        rows[index] = cover_work;
        this.setState({cover_works: rows, copy_cover_works: this.state.copy_cover_works});
    },
    render: function () {
        return (
            <div className="panel panel-default">

                <div className="panel-heading wrapper b-b b-light">

                    <div className="row">
                        <div className="col-lg-10">
                            <h4 className="font-thin m-b-xs m-t-xs">Переплётные работы</h4>
                        </div>

                        <div className="col-lg-2">
                            <a className="btn btn-icon btn-success btn-sm pull-right" onClick={this._addRow}>
                                <i className="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                </div>

                <ManagerCalculationCoverWorksTable
                    cover_works={this.state.cover_works}
                    types={this.props.types}
                    onDrop={this._onDrop}
                    onSave={this._onSave}
                    onCancel={this._onCancel}
                    onChange={this._onChange}
                />

            </div>
        )
    }

});