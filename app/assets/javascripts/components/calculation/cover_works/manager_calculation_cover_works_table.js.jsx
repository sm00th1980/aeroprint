var ManagerCalculationCoverWorksTable = React.createClass({

    _newRow: function (data, index) {
        return (
            <ManagerCalculationCoverWorkRow
                index={index}
                cover_work={this.props.cover_works[index]}
                onDrop={this.props.onDrop}
                onSave={this.props.onSave}
                onChange={this.props.onChange}
                onCancel={this.props.onCancel}
                types={this.props.types}
            />
        )
    },

    render: function () {
        if (this.props.cover_works.length > 0) {
            return (
                <table className="table table-striped table-bordered b-t b-light m-b-none">
                    <thead>
                        <tr>
                            <th>Тип работы</th>
                            <th width="104">Действия</th>
                        </tr>
                    </thead>

                    <tbody>
                            {this.props.cover_works.map(this._newRow, this)}
                    </tbody>
                </table>
            );
        } else {
            return null;
        }
    }

});