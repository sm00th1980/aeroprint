var ManagerCalculationInput = React.createClass({
    getDefaultProps: function () {
        return {
            show: true
        };
    },

    componentDidMount: function () {
        $('[data-toggle="popover"]').popover({
            trigger: 'hover',
            placement: 'top'
        });
    },

    _onChange: function (e) {
        this.props.onChange(this.props.name, e.target.value);
    },

    render: function () {
        if (this.props.show) {
            return (
                <div className="form-group">
                    <label data-toggle="popover" title={this.props.helpTitle} data-content={this.props.helpContent}>
                {this.props.header}
                    </label>

                    <input className="form-control" name={this.props.name} value={this.props.value} onChange={this._onChange}/>
                </div>
            )
        } else {
            return null;
        }
    }

});