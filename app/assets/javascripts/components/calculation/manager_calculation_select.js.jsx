var ManagerCalculationSelect = React.createClass({
    getDefaultProps: function () {
        return {
            show: true
        };
    },

    _onChange: function (e) {
        this.props.onChange(this.props.name, e.target.value);
    },

    render: function () {
        if (this.props.show) {
            var option = function (el) {
                return <option value={el.id}>{el.name}</option>
            };

            return (
                <div className="form-group">
                    <label data-toggle="popover" data-toggle="popover" title={this.props.helpTitle} data-content={this.props.helpContent}>
                        {this.props.header}
                    </label>

                    <select className="form-control" value={this.props.value} name={this.props.name} onChange={this._onChange}>
                    {this.props.options.map(option)}
                    </select>
                </div>
            )
        } else {
            return null;
        }
    }

});