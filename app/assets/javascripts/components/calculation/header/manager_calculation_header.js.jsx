var ManagerCalculationHeader = React.createClass({

    getInitialState: function () {
        return {
            showBindingType: this.props.products[0].many_page
        };
    },

    _onChangeProduct: function (property, product_id) {
        /*
         if selected product has flag many page and not list -> show binding_type selection, otherwise hide it
         */

        var product = this._findByID(product_id, this.props.products);
        if (product && product.many_page && !product.slashed) {
            this.setState({showBindingType: true});
        } else {
            this.setState({showBindingType: false});
        }

        this.props.onChange(property, product_id);
    },
    _findByID: function (id, options) {
        var self = this;
        return $.grep(options, function (el) {
            return el.id == id;
        })[0];
    },

    render: function () {
        return (
            <div className="col-lg-12">

                <div className="row">
                    <div className="col-lg-4">
                        <ManagerCalculationSelect
                            value={this.props.header.client}
                            options={ this.props.clients }
                            header="Клиент"
                            name="client"
                            helpContent="Клиент для которого будет производиться расчёт"
                            onChange={this.props.onChange}
                        />
                    </div>

                    <div className="col-lg-4">
                        <ManagerCalculationInput
                            value={this.props.header.print_run}
                            header="Тираж"
                            name="print_run"
                            helpContent="Тираж для продукции(число)"
                            onChange={this.props.onChange}
                        />
                    </div>

                    <div className="col-lg-4">
                        <ManagerCalculationSelect
                            value={ this.props.header.extended_cut }
                            options={ this.props.extended_cuts }
                            header="Расширенные разрезы"
                            name="extended_cut"
                            helpContent="Использовать или нет нестандартные(расширенные) разрезы бумаги(разрезы на 3 и 6 частей)"
                            onChange={this.props.onChange}
                        />
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-4">
                        <ManagerCalculationSelect
                            value={ this.props.header.product }
                            options={ this.props.products }
                            header="Изделие"
                            name="product"
                            helpContent="Изделие(продукция) для которой будет производиться расчёт"
                            onChange={this._onChangeProduct}
                        />
                    </div>

                    <div className="col-lg-4">
                        <ManagerCalculationSelect
                            value={ this.props.header.binding_type }
                            options={ this.props.binding_types }
                            header="Тип скрепления"
                            show={this.state.showBindingType}
                            name="binding_type"
                            helpContent="Тип скрепления(только для многостраничной продукции)"
                            onChange={this.props.onChange}
                        />
                    </div>
                    <div className="col-lg-4">
                        <ManagerCalculationSelect
                            value={ this.props.header.service_margin }
                            options={ this.props.service_margins }
                            header="Технические поля"
                            name="service_margin"
                            helpContent="Использовать или нет технические поля"
                            onChange={this.props.onChange}
                        />
                    </div>
                </div>

            </div>
        )
    }

});