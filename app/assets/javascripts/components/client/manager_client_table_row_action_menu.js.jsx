var ManagerClientTableRowActionMenu = React.createClass({

    propTypes: {
        edit: React.PropTypes.bool.isRequired,
        specifications_url: React.PropTypes.object.isRequired,
        branches_url: React.PropTypes.string.isRequired,
        icon: React.PropTypes.bool,
        onEdit: React.PropTypes.func.isRequired,
        onSave: React.PropTypes.func.isRequired,
        onCancel: React.PropTypes.func.isRequired,
        onDrop: React.PropTypes.func,
        dropConfirm: React.PropTypes.string
    },

    _onDrop: function () {
        if (this.props.dropConfirm) {
            if (confirm(this.props.dropConfirm)) {
                this.props.onDrop();
            }
        } else {
            this.props.onDrop();
        }
    },
    _dropLink: function () {
        if (this.props.onDrop && this.props.dropConfirm) {
            return (
                <li>
                    <a onClick={this._onDrop}>Удалить</a>
                </li>
            )
        }
        else {
            return null;
        }
    },

    render: function () {
        if (this.props.edit) {
            return (
                <div>
                    <TableRowActionSave onClick={this.props.onSave} icon={this.props.icon}/>
                    <TableRowActionCancel onClick={this.props.onCancel} icon={this.props.icon}/>
                </div>
            )
        } else {
            return (
                <div className="btn-group dropdown">
                    <button className="btn btn-default btn-bg btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span className="dropdown-label">Меню</span>
                        <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu text-left text-sm pull-right">
                        <li>
                            <a href={this.props.specifications_url['url']}>Спецификации ({this.props.specifications_url['count']})</a>
                        </li>
                        <li>
                            <a href={this.props.branches_url['url']}>Подразделения ({this.props.branches_url['count']})</a>
                        </li>
                        <li className="divider"></li>
                        <li>
                            <a onClick={this.props.onEdit}>Редактировать</a>
                        </li>
                        {this._dropLink()}
                    </ul>
                </div>
            )
        }
    }

});