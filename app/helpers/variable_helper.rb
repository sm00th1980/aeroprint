# -*- encoding : utf-8 -*-
module VariableHelper

  def form_prices
    select("variable", "form_price", Machine::Reference.form_prices.map{|key, value| [value, key]}, {selected: @form_price}, {class: 'form-control', name: 'form_price'})
  end

  def prices_per_hour
    select("variable", "price_per_hour", Machine::Reference.prices_per_hour.map{|key, value| [value, key]}, {selected: @price_per_hour}, {class: 'form-control', name: 'price_per_hour'})
  end

  private
  def form_price_values
    Machine::Reference.form_prices.keys
  end

  def price_per_hour_values
    Machine::Reference.prices_per_hour.keys
  end

end
