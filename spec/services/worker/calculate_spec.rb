# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Worker::Calculate do

  describe "perform generate component results" do
    before(:each) do
      paper = Paper.first

      currency_ = create(:currency, exchange_rates: [65, 75, 85, 95])

      paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency_)

      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 264, chunk_count: 3, extended: true)
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 280, chunk_count: 6, extended: true)

      paper_feature2 = create(:paper_feature, paper: paper, density: 90, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1802.66, currency: currency_)

      create(:paper_cut, paper_feature: paper_feature2, machine: Machine.man, gor_mm: 602, ver_mm: 440, chunk_count: 2, extended: false)
      create(:paper_cut, paper_feature: paper_feature2, machine: Machine.man, gor_mm: 602, ver_mm: 277, chunk_count: 3, extended: true)
      create(:paper_cut, paper_feature: paper_feature2, machine: Machine.ryobi, gor_mm: 290, ver_mm: 452, chunk_count: 4, extended: false)
      create(:paper_cut, paper_feature: paper_feature2, machine: Machine.ryobi, gor_mm: 290, ver_mm: 295, chunk_count: 6, extended: true)

      front_cmyk_color = create(:cmyk_color, value: 4)
      front_pantone_color = create(:pantone_color, value: 0)

      Additional.find_by(internal_name: 'material_price').update_attribute(:value, 0.2)
      Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 100)

      @component = create(:component,
                          calculation: create(:calculation, print_run: 1000, product: create(:list_product)),
                          front_cmyk_color: front_cmyk_color,
                          back_cmyk_color: front_cmyk_color,
                          front_pantone_color: front_pantone_color,
                          back_pantone_color: front_pantone_color,
                          product_gor_mm: 210,
                          product_ver_mm: 297,
                          paper: paper,
                          density: 90,
                          stripes_count: rand(10) + 1
      )
      @component.calculate_first
    end

    it "should perform generate component results" do
      Sidekiq::Testing.inline! do
        expect(ComponentResult.where(component: @component).count).to eq 1
        Worker::Calculate.perform_async(@component.id)
        expect(ComponentResult.where(component: @component).count).to be > 0
        expect(ComponentResult.where(component: @component).count).to eq Machine::Reference.form_prices.count * Machine::Reference.prices_per_hour.count
      end
    end

  end

end
