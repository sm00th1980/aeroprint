# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  before(:all) do
    paper = Paper.first
    paper_feature = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: create(:currency, exchange_rates: [65, 75, 85, 95]))
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)

    @calculation_with_many_components = create(:calculation, print_run: 1000, product: create(:list_product))

    3.times { create(:component, calculation: @calculation_with_many_components, density: 90, paper: paper) }
  end

  it "should be cover for only first component and other should be false" do
    expect(@calculation_with_many_components.components.min_by { |c| c.id }.cover?).to be true

    @calculation_with_many_components.components.sort { |c1, c2| c1.id<=>c2.id }[1..-1].each do |component|
      expect(component.cover?).to be false
    end
  end

end
