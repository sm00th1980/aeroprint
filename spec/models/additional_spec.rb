# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Additional, :type => :model do

  before(:each) do
    Additional.delete_all

    create(:addition_cmyk_press_preparation)
    create(:addition_pantone_press_preparation)
    create(:addition_material_price)
    create(:addition_nds)
    create(:addition_preparation_cover_work)
    create(:addition_paper_discount_price)
    create(:addition_paper_full_price)
  end

  after(:each) do
    Additional.delete_all
  end

  it "should cmyk_press_preparation exists" do
    expect(Additional.cmyk_press_preparation).to eq Additional.find_by(internal_name: 'cmyk_press_preparation').value
  end

  it "should pantone_press_preparation exists" do
    expect(Additional.pantone_press_preparation).to eq Additional.find_by(internal_name: 'pantone_press_preparation').value
  end

  it "should material_price exists" do
    expect(Additional.material_price).to eq Additional.find_by(internal_name: 'material_price').value
  end

  it "should nds exists" do
    nds_ = (1 + (Additional.find_by(internal_name: 'nds').value) /100.0).round(2)
    expect(Additional.nds).to eq nds_
  end

  it "should preparation_cover_work exists" do
    expect(Additional.preparation_cover_work).to eq Additional.find_by(internal_name: 'preparation_cover_work').value
  end

  it "should paper_discount_price exists" do
    expect(Additional.paper_discount_price).to eq (Additional.find_by(internal_name: 'paper_discount_price').value / 100.0 + 1).round(3)
  end

  it "should paper_full_price exists" do
    expect(Additional.paper_full_price).to eq (Additional.find_by(internal_name: 'paper_full_price').value / 100.0 + 1).round(3)
  end

end
