# -*- encoding : utf-8 -*-
require 'spec_helper'

RSpec.describe ProductPadding, type: :model do
  before(:all) do
    ProductPadding.delete_all

    create(:product_padding_list)
    create(:product_padding_many_page_clip)
    create(:product_padding_many_page_glue)
  end

  after(:all) do
    ProductPadding.delete_all
  end

  it "should return list padding" do
    padding_list = ProductPadding.list

    expect(padding_list.product_type).to eq ProductType.list
    expect(padding_list.binding_type).to be_nil
  end

  it "should return many-page with clip padding" do
    padding_many_page_with_clip = ProductPadding.many_page_with_clip

    expect(padding_many_page_with_clip.product_type).to eq ProductType.many_page
    expect(padding_many_page_with_clip.binding_type).to eq BindingType.clip
  end

  it "should return many-page with glue padding" do
    padding_many_page_with_glue = ProductPadding.many_page_with_glue

    expect(padding_many_page_with_glue.product_type).to eq ProductType.many_page
    expect(padding_many_page_with_glue.binding_type).to eq BindingType.glue
  end
end
