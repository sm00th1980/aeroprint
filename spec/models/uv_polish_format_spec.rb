# -*- encoding : utf-8 -*-
require 'spec_helper'

describe UvPolishFormat, :type => :model do

  before(:all) do
    UvPolishFormat.delete_all

    @a1 = create(:uv_polish_formats_a1)
    @a2 = create(:uv_polish_formats_a2)
    @a3 = create(:uv_polish_formats_a3)
    @a2_plus = create(:uv_polish_formats_a2_plus)
    @a3_plus = create(:uv_polish_formats_a3_plus)
  end

  after(:all) do
    UvPolishFormat.delete_all
  end

  it "should exists A1" do
    expect(@a1.A1?).to be true
    expect(@a1.A2?).to be false
    expect(@a1.A3?).to be false
    expect(@a1.A2_plus?).to be false
    expect(@a1.A3_plus?).to be false
    expect(@a1.internal_name).to eq 'A1'

    expect(UvPolishFormat.A1).to eq @a1
  end

  it "should exists A2" do
    expect(@a2.A1?).to be false
    expect(@a2.A2?).to be true
    expect(@a2.A3?).to be false
    expect(@a2.A2_plus?).to be false
    expect(@a2.A3_plus?).to be false
    expect(@a2.internal_name).to eq 'A2'

    expect(UvPolishFormat.A2).to eq @a2
  end

  it "should exists A3" do
    expect(@a3.A1?).to be false
    expect(@a3.A2?).to be false
    expect(@a3.A3?).to be true
    expect(@a3.A2_plus?).to be false
    expect(@a3.A3_plus?).to be false
    expect(@a3.internal_name).to eq 'A3'

    expect(UvPolishFormat.A3).to eq @a3
  end

  it "should exists A2 plus" do
    expect(@a2_plus.A1?).to be false
    expect(@a2_plus.A2?).to be false
    expect(@a2_plus.A3?).to be false
    expect(@a2_plus.A2_plus?).to be true
    expect(@a2_plus.A3_plus?).to be false
    expect(@a2_plus.internal_name).to eq 'A2_plus'

    expect(UvPolishFormat.A2_plus).to eq @a2_plus
  end

  it "should exists A3 plus" do
    expect(@a3_plus.A1?).to be false
    expect(@a3_plus.A2?).to be false
    expect(@a3_plus.A3?).to be false
    expect(@a3_plus.A2_plus?).to be false
    expect(@a3_plus.A3_plus?).to be true
    expect(@a3_plus.internal_name).to eq 'A3_plus'

    expect(UvPolishFormat.A3_plus).to eq @a3_plus
  end

end
