# -*- encoding : utf-8 -*-
require 'spec_helper'

describe CoverWorkCurrency, :type => :model do

  before(:all) do
    CoverWorkCurrency.delete_all

    @usd = create(:cover_work_currency_usd)
    @euro = create(:cover_work_currency_euro)
  end

  after(:all) do
    CoverWorkCurrency.delete_all
  end

  it "should usd currency be usd" do
    expect(@usd.usd?).to eq true
    expect(@usd.euro?).to eq false
    expect(@usd.internal_name).to eq 'usd'
    expect(CoverWorkCurrency.usd).to eq @usd
  end

  it "should euro currency be euro" do
    expect(@euro.usd?).to eq false
    expect(@euro.euro?).to eq true
    expect(@euro.internal_name).to eq 'euro'
    expect(CoverWorkCurrency.euro).to eq @euro
  end

end
