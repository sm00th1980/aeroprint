# -*- encoding : utf-8 -*-
require 'spec_helper'

describe PaperFeature, :type => :model do

  before(:each) do
    @currency = create(:currency, exchange_rates: [65.0, 75, 85, 95])
    @paper_feature = create(:paper_feature, density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1000, currency: @currency)
  end

  it "should correct calculate weight_kg_per_1_list" do
    weight_ = ((@paper_feature.density/1000.to_f).round(15) * (@paper_feature.gor_sm/100.to_f).round(15) * (@paper_feature.ver_sm/100.to_f).round(15)).round(5)
    expect(@paper_feature.weight_kg_per_1_list).to eq weight_
    expect(@paper_feature.weight_kg_per_1_list).to be > 0
  end

  it "should correct calculate price_per_1_list" do
    price_per_1_list_ = @paper_feature.weight_kg_per_1_list/1000.to_f * @paper_feature.full_price
    expect(@paper_feature.price_per_1_list_rus(nil)).to eq((price_per_1_list_ * @paper_feature.currency.exchange_rates[0]).round(5))
    expect(@paper_feature.price_per_1_list_rus(nil)).to be > 0
  end

  it "should have name attribute" do
    name = "Плотность: %s(гр/м2), размеры: %s(см) на %s(см)" % [@paper_feature.density, @paper_feature.gor_sm, @paper_feature.ver_sm]
    expect(@paper_feature.name).to eq name
  end

  it "should correct calculate price_per_1_list for specific paper_feature" do
    paper_feature = create(:paper_feature, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: @currency)
    expect(paper_feature.price_per_1_list_rus(nil)).to eq 7.41058 #с учётом наценки
  end

  it "should correct calculate weight_kg_per_1_list for specific paper_feature" do
    paper_feature = create(:paper_feature, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: @currency)
    expect(paper_feature.weight_kg_per_1_list).to eq 0.05184
  end

  it "should correct calculate price_per_1_list with his own price_per_1_ton" do
    paper_feature = create(:paper_feature, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: @currency)
    expect(paper_feature.price_per_1_list_rus(123.123)).to eq 0.50615 #с учётом наценки
  end

end
