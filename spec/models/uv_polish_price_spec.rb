# -*- encoding : utf-8 -*-
require 'spec_helper'

describe UvPolishPrice, :type => :model do

  before(:all) do
    UvPolishPrice.delete_all

    @opaque_a1 = create(:uv_polish_price, type: UvPolishType.opaque, format: UvPolishFormat.A1, print_price: 8.5, preparation_price: 4000)
    @opaque_a2 = create(:uv_polish_price, type: UvPolishType.opaque, format: UvPolishFormat.A2, print_price: 3.0, preparation_price: 2800)
    @opaque_a3 = create(:uv_polish_price, type: UvPolishType.opaque, format: UvPolishFormat.A3, print_price: 2.0, preparation_price: 2100)
    @opaque_a2_plus = create(:uv_polish_price, type: UvPolishType.opaque, format: UvPolishFormat.A2_plus, print_price: 3.3, preparation_price: 2900)
    @opaque_a3_plus = create(:uv_polish_price, type: UvPolishType.opaque, format: UvPolishFormat.A3_plus, print_price: 2.2, preparation_price: 2200)

    @glossy_a2 = create(:uv_polish_price, type: UvPolishType.glossy, format: UvPolishFormat.A2, print_price: 6.0, preparation_price: 2800)
    @glossy_a3 = create(:uv_polish_price, type: UvPolishType.glossy, format: UvPolishFormat.A3, print_price: 4.5, preparation_price: 2100)
    @glossy_a2_plus = create(:uv_polish_price, type: UvPolishType.glossy, format: UvPolishFormat.A2_plus, print_price: 6.0 * 1.25, preparation_price: 2800 * 1.25)
    @glossy_a3_plus = create(:uv_polish_price, type: UvPolishType.glossy, format: UvPolishFormat.A3_plus, print_price: 4.5 * 1.25, preparation_price: 2100 * 1.25)
  end

  after(:all) do
    UvPolishPrice.delete_all
  end

  it "should return opaque prices" do
    expect(UvPolishPrice.price(UvPolishType.opaque, UvPolishFormat.A1)).to eq @opaque_a1
    expect(UvPolishPrice.price(UvPolishType.opaque, UvPolishFormat.A2)).to eq @opaque_a2
    expect(UvPolishPrice.price(UvPolishType.opaque, UvPolishFormat.A3)).to eq @opaque_a3
    expect(UvPolishPrice.price(UvPolishType.opaque, UvPolishFormat.A2_plus)).to eq @opaque_a2_plus
    expect(UvPolishPrice.price(UvPolishType.opaque, UvPolishFormat.A3_plus)).to eq @opaque_a3_plus
  end

  it "should exists glossy prices" do
    expect(UvPolishPrice.price(UvPolishType.glossy, UvPolishFormat.A2)).to eq @glossy_a2
    expect(UvPolishPrice.price(UvPolishType.glossy, UvPolishFormat.A3)).to eq @glossy_a3
    expect(UvPolishPrice.price(UvPolishType.glossy, UvPolishFormat.A2_plus)).to eq @glossy_a2_plus
    expect(UvPolishPrice.price(UvPolishType.glossy, UvPolishFormat.A3_plus)).to eq @glossy_a3_plus
  end

end
