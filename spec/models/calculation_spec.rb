# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Calculation, :type => :model do

  before(:each) do
    @manager = create(:manager)

    paper = Paper.first
    paper_feature = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: create(:currency, exchange_rates: [65, 75, 85, 95]))
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)

    @calculation_not_saved = create(:calculation, saved: false, product: create(:list_product), client: create(:client, manager: @manager))
    @calculation_saved = create(:calculation, saved: true, product: create(:list_product), client: create(:client, manager: @manager))

    @unused_component = create(:component, calculation: @calculation_not_saved, density: 90, paper: paper)
    @unused_cover_work = create(:calculation_cover_work, price: rand(100) + 1, calculation: @calculation_not_saved, type: create(:cover_work_type, owner: CoverWorkOwner.component))
    @unused_component.calculate_all
  end

  it "should drop only not saved calculations" do
    assert_difference 'Calculation.count', -1 do
      Calculation.clean
    end

    expect(Calculation.exists?(id: @calculation_saved)).to eq true
    expect(Calculation.exists?(id: @calculation_not_saved)).to eq false
    expect(Component.exists?(id: @unused_component)).to eq false
    expect(CalculationCoverWork.exists?(id: @unused_cover_work)).to eq false
    expect(ComponentResult.where(component_id: @unused_component).count).to eq 0
  end

end
