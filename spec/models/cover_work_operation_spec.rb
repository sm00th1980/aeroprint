# -*- encoding : utf-8 -*-
require 'spec_helper'

describe CoverWorkOperation, :type => :model do

  before(:all) do
    CoverWorkOperation.delete_all

    @manual = create(:cover_work_operation_manual)
    @automatic = create(:cover_work_operation_automatic)
  end

  after(:all) do
    CoverWorkOperation.delete_all
  end

  it "should manual opeation be manual" do
    expect(@manual.manual?).to eq true
    expect(@manual.automatic?).to eq false
    expect(@manual.internal_name).to eq 'manual'
    expect(CoverWorkOperation.manual).to eq @manual
  end

  it "should automatic opeation be automatic" do
    expect(@automatic.manual?).to eq false
    expect(@automatic.automatic?).to eq true
    expect(@automatic.internal_name).to eq 'automatic'
    expect(CoverWorkOperation.automatic).to eq @automatic
  end

end
