# -*- encoding : utf-8 -*-
require 'spec_helper'

describe CoverWorkOwner, :type => :model do

  before(:all) do
    CoverWorkOwner.delete_all

    @product = create(:cover_work_product_owner)
    @component = create(:cover_work_component_owner)
  end

  after(:all) do
    CoverWorkOwner.delete_all
  end

  it "should cover type for product be product" do
    expect(@product.product?).to eq true
    expect(@product.component?).to eq false
  end

  it "should cover type for component be component" do
    expect(@component.product?).to eq false
    expect(@component.component?).to eq true
  end

end
