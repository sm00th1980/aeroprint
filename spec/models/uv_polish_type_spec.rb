# -*- encoding : utf-8 -*-
require 'spec_helper'

describe UvPolishType, :type => :model do

  before(:all) do
    UvPolishType.delete_all

    @opaque = create(:uv_polish_type_opaque)
    @glossy = create(:uv_polish_type_glossy)
  end

  after(:all) do
    UvPolishType.delete_all
  end

  it "should exists opaque" do
    expect(@opaque.opaque?).to be true
    expect(@opaque.glossy?).to be false
    expect(@opaque.internal_name).to eq 'opaque'

    expect(UvPolishType.opaque).to eq @opaque
  end

  it "should exists glossy" do
    expect(@glossy.opaque?).to be false
    expect(@glossy.glossy?).to be true
    expect(@glossy.internal_name).to eq 'glossy'

    expect(UvPolishType.glossy).to eq @glossy
  end

end
