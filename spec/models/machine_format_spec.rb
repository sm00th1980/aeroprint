# -*- encoding : utf-8 -*-
require 'spec_helper'

describe MachineFormat, :type => :model do

  before(:all) do
    MachineFormat.delete_all
  end

  after(:all) do
    MachineFormat.delete_all
  end

  it "should A2 format be A2" do
    machine_format_a2 = create(:machine_format_a2)

    expect(machine_format_a2.A2?).to be true
    expect(machine_format_a2.A3?).to be false
  end

  it "should A3 format be A3" do
    machine_format_a3 = create(:machine_format_a3)

    expect(machine_format_a3.A2?).to be false
    expect(machine_format_a3.A3?).to be true
  end

end
