# -*- encoding : utf-8 -*-
require 'spec_helper'

describe CoverWorkType, type: :model do

  before(:all) do
    @salaries = [
        {cover_work_type: create(:cover_work_type, operation: CoverWorkOperation.manual, count_of_copies_per_hour: 350), salary: 0.221},
        {cover_work_type: create(:cover_work_type, operation: CoverWorkOperation.manual, count_of_copies_per_hour: 250), salary: 0.310},
        {cover_work_type: create(:cover_work_type, operation: CoverWorkOperation.manual, count_of_copies_per_hour: 500), salary: 0.155}
    ]
  end

  it "should correct calculate salary for manual operations" do
    @salaries.each do |salary|
      expect(salary[:cover_work_type].salary).to eq salary[:salary]
    end
  end

end
