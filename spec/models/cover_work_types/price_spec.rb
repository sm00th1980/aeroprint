# -*- encoding : utf-8 -*-
require 'spec_helper'

describe CoverWorkType, type: :model do

  before(:all) do
    @salaries = [
        {cover_work_type: create(:cover_work_type,
                                 currency: CoverWorkCurrency.usd,
                                 source_material_price: 0.0001,
                                 operation: CoverWorkOperation.automatic,
                                 count_of_copies_per_hour: 3500), price: 0.05},

        {cover_work_type: create(:cover_work_type,
                                 currency: CoverWorkCurrency.usd,
                                 source_material_price: 0.062,
                                 operation: CoverWorkOperation.automatic,
                                 count_of_copies_per_hour: 4000), price: 4.68},

        {cover_work_type: create(:cover_work_type,
                                 currency: CoverWorkCurrency.usd,
                                 source_material_price: 0.062,
                                 operation: CoverWorkOperation.manual,
                                 count_of_copies_per_hour: 350), price: 4.98}
    ]
  end

  it "should correct calculate price" do
    @salaries.each do |salary|
      expect(salary[:cover_work_type].price).to eq salary[:price]
    end
  end

end
