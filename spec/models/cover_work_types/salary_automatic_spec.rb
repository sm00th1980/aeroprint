# -*- encoding : utf-8 -*-
require 'spec_helper'

describe CoverWorkType, type: :model do

  before(:all) do
    @salaries = [
        {cover_work_type: create(:cover_work_type, operation: CoverWorkOperation.automatic, count_of_copies_per_hour: 3500), salary: 0.026},
        {cover_work_type: create(:cover_work_type, operation: CoverWorkOperation.automatic, count_of_copies_per_hour: 4000), salary: 0.022},
        {cover_work_type: create(:cover_work_type, operation: CoverWorkOperation.automatic, count_of_copies_per_hour: 6700), salary: 0.013}
    ]
  end

  it "should correct calculate salary for automatic operations" do
    @salaries.each do |salary|
      expect(salary[:cover_work_type].salary).to eq salary[:salary]
    end
  end

end
