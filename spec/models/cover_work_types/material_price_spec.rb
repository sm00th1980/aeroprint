# -*- encoding : utf-8 -*-
require 'spec_helper'

describe CoverWorkType, type: :model do

  before(:all) do
    @salaries = [
        {cover_work_type: create(:cover_work_type, currency: CoverWorkCurrency.usd, source_material_price: 0.0001), material_price: 0.005},
        {cover_work_type: create(:cover_work_type, currency: CoverWorkCurrency.usd, source_material_price: 0.062), material_price: 3.1},
        {cover_work_type: create(:cover_work_type, currency: CoverWorkCurrency.euro, source_material_price: 0.00012), material_price: 0.006}
    ]
  end

  it "should correct calculate material price" do
    @salaries.each do |salary|
      expect(salary[:cover_work_type].material_price).to eq salary[:material_price]
    end
  end

end
