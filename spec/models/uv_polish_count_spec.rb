# -*- encoding : utf-8 -*-
require 'spec_helper'

describe UvPolishCount, :type => :model do

  before(:all) do
    UvPolishCount.delete_all

    @one = create(:uv_polish_count_one_side)
    @both = create(:uv_polish_count_both_side)
  end

  after(:all) do
    UvPolishCount.delete_all
  end

  it "should exists one side" do
    expect(@one.one_side?).to be true
    expect(@one.both_side?).to be false
    expect(@one.internal_name).to eq 'one_side'

    expect(UvPolishCount.one_side).to eq @one
  end

  it "should exists both side" do
    expect(@both.one_side?).to be false
    expect(@both.both_side?).to be true
    expect(@both.internal_name).to eq 'both_side'

    expect(UvPolishCount.both_side).to eq @both
  end

end
