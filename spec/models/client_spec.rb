# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Client, :type => :model do

  before(:each) do
    User.delete_all
    Client.delete_all

    @admin = create(:admin)
    @manager = create(:manager)
    @client = create(:client, manager: @manager)
  end

  after(:each) do
    User.delete_all
    Client.delete_all
  end

  it "should return admin as manager if own manager was dropped" do
    @manager.destroy
    expect(@client.reload.manager).to eq @admin
  end

end
