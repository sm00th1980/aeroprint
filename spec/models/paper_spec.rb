# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Paper, :type => :model do

  before(:each) do
    Paper.delete_all
    PaperFeature.delete_all
    PaperCut.delete_all
  end

  after(:each) do
    Paper.delete_all
    PaperFeature.delete_all
    PaperCut.delete_all
  end

  it "should have producer_name attribute" do
    paper = create(:paper)
    producer_name = "%s (%s)" % [paper.name, paper.producer.name]
    expect(paper.producer_name).to eq producer_name
  end

  it "should have return only papers with features" do
    paper_without_features = create(:paper)
    paper_with_features = create(:paper)
    create(:paper_feature, paper: paper_with_features, density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1000, currency: create(:currency))

    expect(Paper.with_features).to eq [paper_with_features]
  end

  it "should have return only papers with features and cuts" do
    paper_feature_without_cuts = create(:paper_feature, paper: create(:paper), density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1000, currency: create(:currency))
    paper_feature_without_cuts.cuts.map { |c| c.delete }

    expect(paper_feature_without_cuts.cuts.count).to eq 0
    expect(Paper.with_features).to eq []
  end

  it "should not return paper_features without cuts" do
    paper = create(:paper)
    paper.features.map { |f| f.delete }

    paper_feature_without_cuts = create(:paper_feature, paper: paper, density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1000, currency: create(:currency))
    paper_feature_without_cuts.cuts.map { |c| c.delete }

    expect(paper_feature_without_cuts.cuts.count).to eq 0
    expect(paper.features_with_cuts).to eq []
  end

  it "should return only paper_features with cuts" do
    paper = create(:paper)
    paper.features.map { |f| f.delete }

    paper_feature_with_cuts = create(:paper_feature, paper: paper, density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1000, currency: create(:currency))

    expect(paper_feature_with_cuts.cuts.count).to be > 0
    expect(paper.features_with_cuts).to eq [paper_feature_with_cuts]
  end

  #TODO - dirty fix (after Paper.features_with_cuts should be variated use extended cuts or not)
  it "should return only paper_features with cuts and cut should not be extended" do
    paper = create(:paper)
    paper.features.map { |f| f.delete }
    paper_feature_with_cuts = create(:paper_feature, paper: paper, density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1000, currency: create(:currency))

    expect(paper_feature_with_cuts.cuts.count).to be > 0
    paper_feature_with_cuts.cuts.each do |cut|
      cut.extended = true
      cut.save!
    end

    expect(paper.features_with_cuts).to eq []
  end

end
