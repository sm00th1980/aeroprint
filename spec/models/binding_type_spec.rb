# -*- encoding : utf-8 -*-
require 'spec_helper'

describe BindingType, :type => :model do

  before(:all) do
    BindingType.delete_all

    @clip = create(:binding_type_clip)
    @glue = create(:binding_type_glue)
    # @spring = create(:binding_type_spring)
    # @valve = create(:binding_type_valve)
    # @hard_cover = create(:binding_type_hard_cover)
  end

  after(:all) do
    BindingType.delete_all
  end

  it "should clip be clip" do
    expect(@clip.clip?).to eq true
    expect(@clip.glue?).to eq false
    # expect(@clip.spring?).to eq false
    # expect(@clip.valve?).to eq false
    # expect(@clip.hard_cover?).to eq false
  end

  it "should glue be glue" do
    expect(@glue.clip?).to eq false
    expect(@glue.glue?).to eq true
    # expect(@glue.spring?).to eq false
    # expect(@glue.valve?).to eq false
    # expect(@glue.hard_cover?).to eq false
  end

  # it "should spring be spring" do
  #   expect(@spring.clip?).to eq false
  #   expect(@spring.glue?).to eq false
  #   expect(@spring.spring?).to eq true
  #   expect(@spring.valve?).to eq false
  #   expect(@spring.hard_cover?).to eq false
  # end
  #
  # it "should valve be valve" do
  #   expect(@valve.clip?).to eq false
  #   expect(@valve.glue?).to eq false
  #   expect(@valve.spring?).to eq false
  #   expect(@valve.valve?).to eq true
  #   expect(@valve.hard_cover?).to eq false
  # end
  #
  # it "should hard_cover be hard_cover" do
  #   expect(@hard_cover.clip?).to eq false
  #   expect(@hard_cover.glue?).to eq false
  #   expect(@hard_cover.spring?).to eq false
  #   expect(@hard_cover.valve?).to eq false
  #   expect(@hard_cover.hard_cover?).to eq true
  # end

end
