# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  before(:all) do
    paper = Paper.first

    currency = create(:currency, exchange_rates: [65, 75, 85, 95])

    paper_feature1 = create(:paper_feature, paper: paper, density: 150, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency)
    paper_feature1.cuts.destroy_all

    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)

    paper_feature2 = create(:paper_feature, paper: paper, density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1802.66, currency: currency)
    paper_feature2.cuts.destroy_all

    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.man, gor_mm: 602, ver_mm: 440, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.ryobi, gor_mm: 290, ver_mm: 452, chunk_count: 4, extended: false)

    front_cmyk_color = create(:cmyk_color, value: 4)
    front_pantone_color = create(:pantone_color, value: 0)

    Additional.find_by(internal_name: 'material_price').update_attribute(:value, 0.2)
    Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 100)

    calculation = create(:calculation, print_run: 1000, product: create(:many_page_product), binding_type: BindingType.clip)

    #cover
    @cover = create(:component,
                    calculation: calculation,
                    front_cmyk_color: front_cmyk_color,
                    back_cmyk_color: front_cmyk_color,
                    front_pantone_color: front_pantone_color,
                    back_pantone_color: front_pantone_color,
                    product_gor_mm: 210,
                    product_ver_mm: 297,
                    paper: paper,
                    density: 150,
                    stripes_count: 4
    )

    #block
    @block = create(:component,
                    calculation: calculation,
                    front_cmyk_color: front_cmyk_color,
                    back_cmyk_color: front_cmyk_color,
                    front_pantone_color: front_pantone_color,
                    back_pantone_color: front_pantone_color,
                    product_gor_mm: 210,
                    product_ver_mm: 297,
                    paper: paper,
                    density: 80,
                    stripes_count: 24
    )

    @cover.calculate_first
    @block.calculate_first
  end

  it "should correct calculate for cover" do
    expect(@cover.results[0].paper_expense).to eq 325
    expect(@cover.results[0].machine).to eq Machine.ryobi.name
    # expect(@cover.paper_feature.gor_sm).to eq 64
    # expect(@cover.paper_feature.ver_sm).to eq 90
  end

  it "should correct calculate for block" do
    expect(@block.results[0].paper_expense).to eq 1950
    expect(@block.results[0].machine).to eq Machine.man.name
    # expect(@block.paper_feature.gor_sm).to eq 62
    # expect(@block.paper_feature.ver_sm).to eq 94
  end

end
