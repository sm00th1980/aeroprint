# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  it "should correct calculate paper_expense for component" do
    paper = Paper.first

    currency_ = create(:currency, exchange_rates: [65, 75, 85, 95])

    paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency_)

    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 264, chunk_count: 3, extended: true)
    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)
    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 280, chunk_count: 6, extended: true)

    paper_feature2 = create(:paper_feature, paper: paper, density: 90, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1802.66, currency: currency_)

    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.man, gor_mm: 602, ver_mm: 440, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.man, gor_mm: 602, ver_mm: 277, chunk_count: 3, extended: true)
    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.ryobi, gor_mm: 290, ver_mm: 452, chunk_count: 4, extended: false)
    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.ryobi, gor_mm: 290, ver_mm: 295, chunk_count: 6, extended: true)

    front_cmyk_color = create(:cmyk_color, value: 4)
    front_pantone_color = create(:pantone_color, value: 0)

    Additional.find_by(internal_name: 'material_price').update_attribute(:value, 0.2)
    Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 100)

    component = create(:component,
                       calculation: create(:calculation, print_run: 1000, product: create(:list_product)),
                       front_cmyk_color: front_cmyk_color,
                       back_cmyk_color: front_cmyk_color,
                       front_pantone_color: front_pantone_color,
                       back_pantone_color: front_pantone_color,
                       product_gor_mm: 210,
                       product_ver_mm: 297,
                       paper: paper,
                       density: 90,
                       stripes_count: nil
    )

    create(:calculation_cover_work, price: rand(100) + 1, calculation: component.calculation, type: create(:cover_work_type, owner: CoverWorkOwner.product))
    create(:calculation_cover_work, price: rand(100) + 1, calculation: component.calculation, type: create(:cover_work_type, owner: CoverWorkOwner.component))

    component.calculate_first

    expect(component.results[0].paper_expense).to eq 200
    expect(component.results[0].form_price).to eq 560.0
    expect(component.results[0].machine).to eq Machine.ryobi.name
    expect(component.results[0].material_price).to eq 296.42
    expect(component.results[0].paper_price).to eq 1482.12
    expect(component.results[0].price_per_unit).to eq((component.results[0].print_run_price / component.calculation.print_run.to_f).round(2))
    expect(component.results[0].price_per_unit_without_nds).to eq((component.results[0].price_per_unit / Additional.nds).round(2))
    expect(component.results[0].print_price).to eq 3294.0
    expect(component.results[0].print_run_price).to eq 5632.54 + cover_works_price(component.calculation)
    expect(component.results[0].print_run_price_without_nds).to eq((component.results[0].print_run_price / Additional.nds).round(2))
    expect(component.results[0].print_run_weight).to eq 5.6133
    expect(component.results[0].print_time).to eq 1.83
    expect(component.calculation.cover_works_price).to eq cover_works_price(component.calculation)
  end

  private
  def cover_works_price(calculation)
    summa_ = 0
    calculation.cover_works.each do |cover_work|
      if cover_work.type.owner.product?
        summa_ += cover_work.price
      end

      if cover_work.type.owner.component?
        summa_ += cover_work.price * calculation.components.to_a.sum(&:stripes_count)
      end
    end

    summa_ * calculation.print_run
  end

end
