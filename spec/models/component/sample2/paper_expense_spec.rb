# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  it "should correct calculate paper_expense for component" do
    paper = Paper.first

    currency_ = create(:currency, exchange_rates: [65, 75, 85, 95])

    paper_feature = create(:paper_feature, paper: paper, density: 90, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1802.66, currency: currency_)

    create(:paper_cut, paper_feature: paper_feature, machine: Machine.man, gor_mm: 602, ver_mm: 440, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.man, gor_mm: 602, ver_mm: 227, chunk_count: 3, extended: true)
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.ryobi, gor_mm: 290, ver_mm: 452, chunk_count: 4, extended: false)
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.ryobi, gor_mm: 290, ver_mm: 295, chunk_count: 6, extended: true)

    front_cmyk_color = create(:cmyk_color, value: 4)
    front_pantone_color = create(:pantone_color, value: 0)

    Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 250)

    component = create(:component,
                       calculation: create(:calculation, print_run: 100000, product: create(:list_product)),
                       front_cmyk_color: front_cmyk_color,
                       back_cmyk_color: front_cmyk_color,
                       front_pantone_color: front_pantone_color,
                       back_pantone_color: front_pantone_color,
                       product_gor_mm: 210,
                       product_ver_mm: 297,
                       paper: paper,
                       density: 90,
                       stripes_count: nil
    )

    component.calculate_first

    expect(component.results[0].paper_expense).to eq 12725
  end

end
