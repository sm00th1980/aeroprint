# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  it "should correct calculate paper_expense for component" do
    paper = Paper.first

    currency_ = create(:currency, exchange_rates: [65, 75, 85, 95])

    paper_feature = create(:paper_feature, paper: paper, density: 80, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency_)

    create(:paper_cut, paper_feature: paper_feature, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.man, gor_mm: 622, ver_mm: 264, chunk_count: 3, extended: true)
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)
    create(:paper_cut, paper_feature: paper_feature, machine: Machine.ryobi, gor_mm: 300, ver_mm: 280, chunk_count: 6, extended: true)

    cmyk_color = create(:cmyk_color, value: 0)
    front_pantone_color = create(:pantone_color, value: 2)
    back_pantone_color = create(:pantone_color, value: 0)

    Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 50)

    component = create(:component,
                       calculation: create(:calculation, print_run: 1000, product: create(:list_product)),
                       front_cmyk_color: cmyk_color,
                       back_cmyk_color: cmyk_color,
                       front_pantone_color: front_pantone_color,
                       back_pantone_color: back_pantone_color,
                       product_gor_mm: 210,
                       product_ver_mm: 297,
                       paper: paper,
                       density: 80,
                       stripes_count: nil
    )

    component.calculate_first

    expect(component.results[0].paper_expense).to eq 163

  end

end
