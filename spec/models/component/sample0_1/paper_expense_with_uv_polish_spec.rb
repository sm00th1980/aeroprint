# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  it "should correct calculate paper_expense for component" do
    paper = Paper.first

    currency_ = create(:currency, exchange_rates: [65, 75, 85, 95])

    paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency_)

    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)

    front_cmyk_color = create(:cmyk_color, value: 4)
    front_pantone_color = create(:pantone_color, value: 0)

    Additional.find_by(internal_name: 'material_price').update_attribute(:value, 0.2)
    Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 100)

    component = create(:component,
                       calculation: create(:calculation, print_run: 1000, product: create(:list_product)),
                       front_cmyk_color: front_cmyk_color,
                       back_cmyk_color: front_cmyk_color,
                       front_pantone_color: front_pantone_color,
                       back_pantone_color: front_pantone_color,
                       product_gor_mm: 210,
                       product_ver_mm: 297,
                       paper: paper,
                       density: 90,
                       stripes_count: nil
    )

    calculation_uv_polish = create(:calculation_uv_polish,
                                   calculation: component.calculation,
                                   type: UvPolishType.all.sample,
                                   count: UvPolishCount.all.sample,
                                   components: [component.id]
    )

    component.calculate_first

    expect(component.results[0].uv_polish_price).to eq UvPolishPrice.print_price(component.calculation.print_run, calculation_uv_polish.type, paper_feature1.uv_polish_format, calculation_uv_polish.count)
    expect(component.results[0].uv_polish_price).to be > 0
  end

end
