# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ManagerStatus, :type => :model do

  before(:all) do
    ManagerStatus.delete_all
  end

  after(:all) do
    ManagerStatus.delete_all
  end

  it "should active status be active" do
    active_status = FactoryGirl.create(:manager_status_active)

    expect(active_status.active?).to be true
    expect(active_status.blocked?).to be false
    expect(active_status.internal_name).to eq 'active'

    expect(ManagerStatus.active).to eq active_status
  end

  it "should blocked status be blocked" do
    blocked_status = FactoryGirl.create(:manager_status_blocked)

    expect(blocked_status.active?).to be false
    expect(blocked_status.blocked?).to be true
    expect(blocked_status.internal_name).to eq 'blocked'

    expect(ManagerStatus.blocked).to eq blocked_status
  end

end
