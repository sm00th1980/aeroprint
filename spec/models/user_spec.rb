# -*- encoding : utf-8 -*-
require 'spec_helper'

describe User, :type => :model do

  it "should manager be manager and not admin" do
    manager = create(:manager)
    expect(manager.admin?).to be false
    expect(manager.manager?).to be true
  end

  it "should admin be admin and not manager" do
    admin = create(:admin)
    expect(admin.admin?).to be true
    expect(admin.manager?).to be false
  end

end
