# -*- encoding : utf-8 -*-
require 'spec_helper'

describe User, :type => :model do

  describe "his own clients" do
    before(:each) do
      @manager_with_clients = create(:manager)
      5.times { create(:client, manager: @manager_with_clients) }

      @manager_with_transfered_clients = create(:manager)
    end

    it "should manager with clients has clients" do
      expect(@manager_with_clients.clients.sort).to eq Client.where(manager: @manager_with_clients).sort
    end
  end

  describe "transfered clients" do
    before(:each) do
      @primary_manager = create(:manager)
      5.times { create(:client, manager: @primary_manager) }

      @substitute_manager = create(:manager)

      @primary_manager.substitute = @substitute_manager
      @primary_manager.save!
    end

    it "should substitute manager has primary manager clients" do
      expect(@substitute_manager.transfered_clients.sort).to eq Client.where(manager: @primary_manager).sort
    end

    it "should substitute manager be sustitute for primary" do
      expect(@primary_manager.substitute).to eq @substitute_manager
    end

    it "should primary manager be primary for substitute" do
      expect(@substitute_manager.primary).to eq @primary_manager
    end
  end

end
