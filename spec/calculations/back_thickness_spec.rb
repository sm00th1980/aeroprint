# -*- encoding : utf-8 -*-
require 'spec_helper'

describe BackThickness do

  it "should correct calculate thickness" do
    expect(BackThickness.back_thickness(PaperType.newspaper, nil, 60)).to eq 4
    expect(BackThickness.back_thickness(PaperType.offset, 80, 60)).to eq 6
    expect(BackThickness.back_thickness(PaperType.melovka, 80, 60)).to eq 4
    expect(BackThickness.back_thickness(PaperType.melovka, 90, 60)).to eq 4
    expect(BackThickness.back_thickness(PaperType.melovka, 104, 60)).to eq 5
    expect(BackThickness.back_thickness(PaperType.melovka, 105, 60)).to eq 5
    expect(BackThickness.back_thickness(PaperType.melovka, 115, 60)).to eq 5
    expect(BackThickness.back_thickness(PaperType.melovka, 128, 60)).to eq 6
    expect(BackThickness.back_thickness(PaperType.melovka, 130, 60)).to eq 6
    expect(BackThickness.back_thickness(PaperType.melovka, 148, 60)).to eq 8
    expect(BackThickness.back_thickness(PaperType.melovka, 150, 60)).to eq 7
    expect(BackThickness.back_thickness(PaperType.melovka, 157, 60)).to eq 8
    expect(BackThickness.back_thickness(PaperType.melovka, 170, 60)).to eq 7
    expect(BackThickness.back_thickness(PaperType.melovka, 200, 60)).to eq 10
  end

end
