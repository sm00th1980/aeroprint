# -*- encoding : utf-8 -*-
require 'spec_helper'

describe SlashedPrice do

  describe "A3 calculate slashed price" do
    before(:each) do
      format = :A3
      @data = [
          {print_run: 10, format: format, price: 1200},
          {print_run: 500, format: format, price: 1200},

          {print_run: 1000, format: format, price: 1.2 * 1000},
          {print_run: 1500, format: format, price: 1.2 * 1500},

          {print_run: 3000, format: format, price: 1.15 * 3000},
          {print_run: 3001, format: format, price: 1.15 * 3001},

          {print_run: 5000, format: format, price: 1.1 * 5000},
          {print_run: 5100, format: format, price: 1.1 * 5100},

          {print_run: 7000, format: format, price: 1.05 * 7000},
          {print_run: 7325, format: format, price: 1.05 * 7325},

          {print_run: 10000, format: format, price: 1.0 * 10000},
          {print_run: 11111, format: format, price: 1.0 * 11111},

          {print_run: 15000, format: format, price: 0.9 * 15000},
          {print_run: 15423, format: format, price: 0.9 * 15423},

          {print_run: 20000, format: format, price: 0.8 * 20000},
          {print_run: 20123, format: format, price: 0.8 * 20123},

          {print_run: 30000, format: format, price: 0.7 * 30000},
          {print_run: 34523, format: format, price: 0.7 * 34523},

          {print_run: 50000, format: format, price: 0.6 * 50000},
          {print_run: 50001, format: format, price: 0.6 * 50001},
          {print_run: 100001, format: format, price: 0.6 * 100001}
      ]
    end

    #A3
    it "should correct calculate slashed price for A3 format" do
      @data.each do |row|
        expect(SlashedPrice.price(row[:print_run], row[:format])).to eq(row[:price])
      end
    end
  end

  describe "A2 calculate slashed price" do
    before(:each) do
      format = :A2
      @data = [
          {print_run: 10, format: format, price: 2500},
          {print_run: 500, format: format, price: 2500},

          {print_run: 1000, format: format, price: 2.5 * 1000},
          {print_run: 1500, format: format, price: 2.5 * 1500},

          {print_run: 3000, format: format, price: 2.4 * 3000},
          {print_run: 3001, format: format, price: 2.4 * 3001},

          {print_run: 5000, format: format, price: 2.3 * 5000},
          {print_run: 5100, format: format, price: 2.3 * 5100},

          {print_run: 7000, format: format, price: 2.2 * 7000},
          {print_run: 7325, format: format, price: 2.2 * 7325},

          {print_run: 10000, format: format, price: 2.0 * 10000},
          {print_run: 11111, format: format, price: 2.0 * 11111},

          {print_run: 15000, format: format, price: 1.8 * 15000},
          {print_run: 15423, format: format, price: 1.8 * 15423},

          {print_run: 20000, format: format, price: 1.5 * 20000},
          {print_run: 20123, format: format, price: 1.5 * 20123},

          {print_run: 30000, format: format, price: 1.2 * 30000},
          {print_run: 34523, format: format, price: 1.2 * 34523},

          {print_run: 50000, format: format, price: 0.9 * 50000},
          {print_run: 50001, format: format, price: 0.9 * 50001},
          {print_run: 100001, format: format, price: 0.9 * 100001}
      ]
    end

    #A3
    it "should correct calculate slashed price for A3 format" do
      @data.each do |row|
        expect(SlashedPrice.price(row[:print_run], row[:format])).to eq(row[:price])
      end
    end
  end

end
