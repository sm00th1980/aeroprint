# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Imposition do

  it "should correct calculate imposition" do
    expect(Imposition.imposition(212, 299, 210, 297)).to eq 1
    expect(Imposition.imposition(432, 300, 299, 212)).to eq 2
    expect(Imposition.imposition(432, 300, 298, 212)).to eq 2
    expect(Imposition.imposition(622, 440, 299, 212)).to eq 4
    expect(Imposition.imposition(602, 440, 210, 297)).to eq 4
    expect(Imposition.imposition(290, 452, 210, 297)).to eq 1
    expect(Imposition.imposition(300, 432, 300, 300)).to eq 1
    expect(Imposition.imposition(426, 299, 210, 297)).to eq 2
    expect(Imposition.imposition(602, 440, 426, 299)).to eq 2
    expect(Imposition.imposition(300, 432, 426, 299)).to eq 1
  end

  it "should calculate imposition as 0 if size product is bigger then paper" do
    expect(Imposition.imposition(200, 200, 300, 300)).to eq 0
    expect(Imposition.imposition(200, 200, 150, 300)).to eq 0
    expect(Imposition.imposition(200, 200, 300, 150)).to eq 0
  end

end
