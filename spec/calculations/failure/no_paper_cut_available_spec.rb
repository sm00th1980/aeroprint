# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  before(:each) do
    paper = Paper.first

    front_cmyk_color = create(:cmyk_color, value: 4)
    front_pantone_color = create(:pantone_color, value: 0)

    Additional.find_by(internal_name: 'material_price').update_attribute(:value, 0.2)
    Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 100)

    @component = create(:component,
                        calculation: create(:calculation, print_run: 1000, product: create(:list_product)),
                        front_cmyk_color: front_cmyk_color,
                        back_cmyk_color: front_cmyk_color,
                        front_pantone_color: front_pantone_color,
                        back_pantone_color: front_pantone_color,
                        product_gor_mm: 210,
                        product_ver_mm: 297,
                        paper: paper,
                        density: 90,
                        stripes_count: rand(10) + 1
    )
  end

  it "should not create results if calculation failed" do
    assert_difference 'ComponentResult.count', 0 do
      expect { @component.calculate_first }.not_to raise_error
      expect(@component.calculation.reload.success).to be false
    end
  end

end
