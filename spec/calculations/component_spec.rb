# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Component, :type => :model do

  before(:each) do
    paper = Paper.first

    currency_ = create(:currency, exchange_rates: [65, 75, 85, 95])

    paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency_)

    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 264, chunk_count: 3, extended: true)
    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)
    create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 280, chunk_count: 6, extended: true)

    paper_feature2 = create(:paper_feature, paper: paper, density: 90, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1802.66, currency: currency_)

    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.man, gor_mm: 602, ver_mm: 440, chunk_count: 2, extended: false)
    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.man, gor_mm: 602, ver_mm: 277, chunk_count: 3, extended: true)
    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.ryobi, gor_mm: 290, ver_mm: 452, chunk_count: 4, extended: false)
    create(:paper_cut, paper_feature: paper_feature2, machine: Machine.ryobi, gor_mm: 290, ver_mm: 295, chunk_count: 6, extended: true)

    front_cmyk_color = create(:cmyk_color, value: 4)
    front_pantone_color = create(:pantone_color, value: 0)

    Additional.find_by(internal_name: 'material_price').update_attribute(:value, 0.2)
    Additional.find_by(internal_name: 'preparation_cover_work').update_attribute(:value, 100)

    @component = create(:component,
                        calculation: create(:calculation, print_run: 1000, product: create(:list_product)),
                        front_cmyk_color: front_cmyk_color,
                        back_cmyk_color: front_cmyk_color,
                        front_pantone_color: front_pantone_color,
                        back_pantone_color: front_pantone_color,
                        product_gor_mm: 210,
                        product_ver_mm: 297,
                        paper: paper,
                        density: 90,
                        stripes_count: rand(10) + 1
    )

    create(:calculation_cover_work, price: rand(100) + 1, calculation: @component.calculation, type: create(:cover_work_type, owner: CoverWorkOwner.product))
    create(:calculation_cover_work, price: rand(100) + 1, calculation: @component.calculation, type: create(:cover_work_type, owner: CoverWorkOwner.component))

    @component.calculate_all
  end

  it "should create results after calculate component" do
    expect(@component.results.count).to eq Machine::Reference.form_prices.count * Machine::Reference.prices_per_hour.count

    results_ = @component.results.sort_by { |r| r.id }
    index = 0

    Machine::Reference.form_prices.keys.map do |form_price_index|
      Machine::Reference.prices_per_hour.keys.map do |price_per_hour_index|
        calculation_aux_ = CalculationAux.new(@component, form_price_index, price_per_hour_index, nil)

        result = results_[index]

        expect(result.component).to eq @component
        expect(result.paper_expense).to eq 200
        expect(result.machine).to eq Machine.ryobi.name

        expect(result.variable_price_per_1_ton).to eq calculation_aux_.price_per_1_ton
        expect(result.base_price_per_1_ton).to be true

        expect(result.paper_price).to eq calculation_aux_.paper_price
        expect(result.print_time).to eq calculation_aux_.print_time
        expect(result.print_run_weight).to eq print_run_weight(@component)

        expect(result.variable_form_price_index).to eq form_price_index
        expect(result.form_price).to eq calculation_aux_.form_price

        expect(result.material_price).to eq calculation_aux_.material_price
        expect(result.print_run_price).to eq calculation_aux_.print_run_price

        expect(result.variable_price_per_hour_index).to eq price_per_hour_index
        expect(result.print_price).to eq calculation_aux_.print_price

        expect(result.imposition).to eq calculation_aux_.imposition
        expect(result.chosen_paper).to eq chosen_paper(calculation_aux_.paper_feature)
        expect(result.front_colors).to eq front_colors(@component)
        expect(result.back_colors).to eq back_colors(@component)
        expect(result.print_run_price_without_nds).to eq ((result.print_run_price / Additional.nds).round(2))
        expect(result.price_per_unit).to eq ((result.print_run_price / @component.calculation.print_run.to_f).round(2))
        expect(result.price_per_unit_without_nds).to eq ((result.price_per_unit / Additional.nds).round(2))

        index += 1
      end
    end

  end

  private
  def print_run_weight(component)
    product_size_ = (component.product_gor_mm/1000.0) * (component.product_ver_mm/1000.0).round(4)
    paper_density_ = (component.density / 1000.0).round(4)

    (product_size_ * paper_density_ * component.calculation.print_run).round(4)
  end

  def chosen_paper(paper_feature)
    "#{paper_feature.paper.name} (#{paper_feature.gor_sm}x#{paper_feature.ver_sm}, #{paper_feature.density})"
  end

  def front_colors(component)
    "#{component.front_cmyk_color.value}/#{component.front_pantone_color.value}"
  end

  def back_colors(component)
    "#{component.back_cmyk_color.value}/#{component.back_pantone_color.value}"
  end

end
