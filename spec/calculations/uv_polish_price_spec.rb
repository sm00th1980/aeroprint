# -*- encoding : utf-8 -*-
require 'spec_helper'

describe UvPolishPrice do

  describe "calculate uv polish price for opaque/A2" do
    before(:each) do
      @type = UvPolishType.opaque
      @format = UvPolishFormat.A2
    end

    it "should correct calculate uv polish price for one side" do
      print_price_ = UvPolishPrice.print_price(1000, @type, @format, UvPolishCount.one_side)
      price_ = UvPolishPrice.price(@type, @format)

      expect(print_price_).to eq((1000 * price_.print_price + price_.preparation_price) * UvPolishCount.one_side.value)
    end

    it "should correct calculate uv polish price for both side" do
      print_price_ = UvPolishPrice.print_price(2000, @type, @format, UvPolishCount.both_side)
      price_ = UvPolishPrice.price(@type, @format)

      expect(print_price_).to eq((2000 * price_.print_price + price_.preparation_price) * UvPolishCount.both_side.value)
    end

  end

  describe "calculate uv polish price for glossy/A3" do
    before(:each) do
      @type = UvPolishType.glossy
      @format = UvPolishFormat.A3
    end

    it "should correct calculate uv polish price for one side" do
      print_price_ = UvPolishPrice.print_price(1000, @type, @format, UvPolishCount.one_side)
      price_ = UvPolishPrice.price(@type, @format)

      expect(print_price_).to eq((1000 * price_.print_price + price_.preparation_price) * UvPolishCount.one_side.value)
    end

    it "should correct calculate uv polish price for both side" do
      print_price_ = UvPolishPrice.print_price(2000, @type, @format, UvPolishCount.both_side)
      price_ = UvPolishPrice.price(@type, @format)

      expect(print_price_).to eq((2000 * price_.print_price + price_.preparation_price) * UvPolishCount.both_side.value)
    end

  end

end
