# -*- encoding : utf-8 -*-
require 'spec_helper'

describe PaperExpense do

  before(:each) do
    @product_type = ProductType.list
    @stripes_count = nil
    @binding_type = nil
    @product_gor_mm = 212
    @product_ver_mm = 299
    @print_run = 1000
    @cmyk_colors = 8
    @pantone_colors = 0
    @preparation_cover_work = 100
  end

  #internal block
  it "should correct calculate paper expense for RYOBI for internal block" do
    expect(PaperExpense.paper_expense(
               @product_type,
               false,
               @stripes_count,
               @binding_type,
               @product_gor_mm,
               @product_ver_mm,
               300,
               432,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               4)
    ).to eq 200
  end

  it "should correct calculate paper expense for MAN for internal block" do
    expect(PaperExpense.paper_expense(
               @product_type,
               false,
               @stripes_count,
               @binding_type,
               @product_gor_mm,
               @product_ver_mm,
               602,
               440,
               100000,
               @cmyk_colors,
               @pantone_colors,
               250,
               2)
    ).to eq 12725
  end

end
