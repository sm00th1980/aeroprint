# -*- encoding : utf-8 -*-
require 'spec_helper'

describe ProductSize do

  before(:each) do
    @product_gor_mm = 210
    @product_ver_mm = 297
    @cover = [true, false].sample
  end

  #list
  it "should correct calculate product_size for list product" do
    product_size = ProductSize.product_size(@product_gor_mm, @product_ver_mm, ProductType.list, nil, @cover, nil, nil, nil)

    expect(product_size.gor_mm).to eq 212
    expect(product_size.ver_mm).to eq 299
  end

  #many_pages, clip
  it "should correct calculate product_size for many pages product and clip" do
    product_size = ProductSize.product_size(@product_gor_mm, @product_ver_mm, ProductType.many_page, BindingType.clip, @cover, nil, nil, nil)

    expect(product_size.gor_mm).to eq 426
    expect(product_size.ver_mm).to eq 299
  end

  #many_pages, glue, cover
  it "should correct calculate product_size for many pages product and glue and cover" do
    product_size = ProductSize.product_size(@product_gor_mm, @product_ver_mm, ProductType.many_page, BindingType.glue, true, PaperType.melovka, 104, 120)

    expect(product_size.gor_mm).to eq 435
    expect(product_size.ver_mm).to eq 301
  end

  #many_pages, glue, block
  it "should correct calculate product_size for many pages product and glue and internal block" do
    product_size = ProductSize.product_size(@product_gor_mm, @product_ver_mm, ProductType.many_page, BindingType.glue, false, PaperType.melovka, 104, rand(100) + 10)

    expect(product_size.gor_mm).to eq 215
    expect(product_size.ver_mm).to eq 301
  end

end
