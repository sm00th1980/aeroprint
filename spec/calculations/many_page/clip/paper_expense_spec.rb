# -*- encoding : utf-8 -*-
require 'spec_helper'

describe PaperExpense do

  before(:each) do
    @product_type = ProductType.many_page
    @binding_type = BindingType.clip
    @product_gor_mm = 426
    @product_ver_mm = 299
    @print_run = 1000
    @cmyk_colors = 8
    @pantone_colors = 0
    @preparation_cover_work = 100
  end

  #internal block
  it "should correct calculate paper expense for MAN for internal block and clip" do
    expect(PaperExpense.paper_expense(
               @product_type,
               false,
               @binding_type,
               24,
               @product_gor_mm,
               @product_ver_mm,
               602,
               440,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               2)
    ).to eq 1950
  end

  it "should correct calculate paper expense for RYOBI for internal block and clip" do
    expect(PaperExpense.paper_expense(
               @product_type,
               false,
               @binding_type,
               24,
               @product_gor_mm,
               @product_ver_mm,
               300,
               432,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               4)
    ).to eq 1950
  end

  #cover
  it "should correct calculate paper expense for MAN for cover and clip" do
    expect(PaperExpense.paper_expense(
               @product_type,
               true,
               @binding_type,
               4,
               @product_gor_mm,
               @product_ver_mm,
               602,
               440,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               2)
    ).to eq 375
  end

  it "should correct calculate paper expense for RYOBI for cover and clip" do
    expect(PaperExpense.paper_expense(
               @product_type,
               true,
               @binding_type,
               4,
               @product_gor_mm,
               @product_ver_mm,
               300,
               432,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               4)
    ).to eq 325
  end

end
