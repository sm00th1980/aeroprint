# -*- encoding : utf-8 -*-
require 'spec_helper'

describe PaperExpense do

  before(:each) do
    @product_type = ProductType.many_page
    @binding_type = BindingType.glue
    @print_run = 1000
    @cmyk_colors = 8
    @pantone_colors = 0
    @preparation_cover_work = 100
  end

  #internal block
  it "should correct calculate paper expense for MAN for internal block and glue" do
    expect(PaperExpense.paper_expense(
               @product_type,
               false,
               @binding_type,
               120,
               215,
               301,
               602,
               440,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               2)
    ).to eq 9750
  end

  it "should correct calculate paper expense for RYOBI for internal block and glue" do
    expect(PaperExpense.paper_expense(
               @product_type,
               false,
               @binding_type,
               120,
               215,
               301,
               301,
               432,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               4)
    ).to eq 9750
  end

  #cover
  it "should correct calculate paper expense for MAN for cover and glue" do
    expect(PaperExpense.paper_expense(
               @product_type,
               true,
               @binding_type,
               4,
               435,
               301,
               602,
               440,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               2)
    ).to eq 375
  end

  it "should correct calculate paper expense for RYOBI for cover and glue" do
    expect(PaperExpense.paper_expense(
               @product_type,
               true,
               @binding_type,
               4,
               435,
               301,
               330,
               482,
               @print_run,
               @cmyk_colors,
               @pantone_colors,
               @preparation_cover_work,
               4)
    ).to eq 325
  end

end
