# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :paper do
    name { Faker::Name.name }
    type PaperType.melovka
    producer PaperProducer.bereg
    country PaperCountry.russia

    before(:create) do
      create(:paper_type_melovka) if PaperType.melovka.nil?
      create(:paper_producer_bereg) if PaperProducer.bereg.nil?
      create(:paper_country_russia) if PaperCountry.russia.nil?
    end
  end

end
