# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :specification do
    client
    sequence(:number) { |n| n }
    date { Date.today }

    factory :specification_with_file do
      sequence(:document_file_name) { |n| "file_#{n}.pdf" }
      document_content_type 'application/pdf'
      document_file_size { rand(10_000) + 100 }
      document_updated_at Time.now
    end
  end

end
