# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :additional do
    sequence(:name) { |n| n }

    factory :addition_cmyk_press_preparation do
      internal_name 'cmyk_press_preparation'
      value { rand(100) }
    end

    factory :addition_pantone_press_preparation do
      internal_name 'pantone_press_preparation'
      value { rand(100) }
    end

    factory :addition_material_price do
      internal_name 'material_price'
      value { rand(100) }
    end

    factory :addition_nds do
      internal_name 'nds'
      value 18
    end

    factory :addition_preparation_cover_work do
      internal_name 'preparation_cover_work'
      value { rand(100) }
    end

    factory :addition_paper_discount_price do
      internal_name 'paper_discount_price'
      value 12
    end

    factory :addition_paper_full_price do
      internal_name 'paper_full_price'
      value 22
    end

  end

end
