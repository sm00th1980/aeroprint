# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :calculation do
    product
    print_run { rand(10_000) }
    client { Client.all.sample }

    factory :calculation_with_one_component do
      after(:create) do |calculation|
        create(:component, calculation: calculation)
      end
    end

    factory :calculation_with_many_components do
      after(:create) do |calculation|
        5.times { create(:component, calculation: calculation) }
      end
    end
  end

end
