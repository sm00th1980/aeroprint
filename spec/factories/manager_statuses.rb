# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :manager_status do

    factory :manager_status_active do
      name 'Активный'
      internal_name 'active'
    end

    factory :manager_status_blocked do
      name 'Заблокирован'
      internal_name 'blocked'
    end

  end
end
