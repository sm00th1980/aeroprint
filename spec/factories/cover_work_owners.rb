# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :cover_work_owner do

    factory :cover_work_product_owner do
      name 'для изделия в целом'
      internal_name 'product'
    end

    factory :cover_work_component_owner do
      name 'для блока'
      internal_name 'component'
    end

  end

end
