# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :uv_polish_format do

    factory :uv_polish_formats_a1 do
      name 'A1'
      internal_name 'A1'
    end

    factory :uv_polish_formats_a2 do
      name 'A2'
      internal_name 'A2'
    end

    factory :uv_polish_formats_a3 do
      name 'A3'
      internal_name 'A3'
    end

    factory :uv_polish_formats_a2_plus do
      name 'A2_plus'
      internal_name 'A2_plus'
    end

    factory :uv_polish_formats_a3_plus do
      name 'A3_plus'
      internal_name 'A3_plus'
    end

  end

end
