# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :cover_work_operation do

    factory :cover_work_operation_manual do
      name 'manual'
      internal_name 'manual'
      price 13_000
    end

    factory :cover_work_operation_automatic do
      name 'automatic'
      internal_name 'automatic'
      price 15_000
    end

  end

end
