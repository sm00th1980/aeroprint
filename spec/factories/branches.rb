# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :branch do
    name { Faker::Name.name }
    description { Faker::Name.name }
    phone { Faker::Number.number(10) }
    email { Faker::Internet.email }
    contract { Faker::Name.name }
    payer { Faker::Name.name }
    client
    discount {Discount.all.sample}
    shipping_address { Faker::Name.name }
  end

end
