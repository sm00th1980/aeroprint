# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :component do
    name { Faker::Commerce.product_name }
    stripes_count { rand(10) + 1 }

    front_cmyk_color { CmykColor.all.sample }
    front_pantone_color { PantoneColor.all.sample }
    front_polish { Polish.all.sample }

    back_cmyk_color { CmykColor.all.sample }
    back_pantone_color { PantoneColor.all.sample }
    back_polish { Polish.all.sample }

    product_gor_mm 210
    product_ver_mm 297
    density 90
    paper
    frontend_component_id 0

  end

end
