# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :cover_work_type do
    name { Faker::Name.name }
    count_of_copies_per_hour { rand(10_000) }
    source_material_price { rand(10)/1000.0 }
    owner_id { CoverWorkOwner.all.sample.id }
    currency_id { CoverWorkCurrency.all.sample.id }
    operation_id { CoverWorkOperation.all.sample.id }
  end

end
