# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :paper_country do

    factory :paper_country_russia do
      name 'russian'
      internal_name 'russia'
    end

    factory :paper_country_china do
      name 'china'
      internal_name 'china'
    end

    factory :paper_country_europe do
      name 'europe'
      internal_name 'europe'
    end

  end

end
