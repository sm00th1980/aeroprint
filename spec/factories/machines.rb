# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :machine do

    factory :machine_man do
      name 'MAN'
      efficiency 6000
      color_count 4
      preparation_time 0.1
      additional_time 0.3
      skin 0
      max_x 718
      max_y 718
      format { MachineFormat.find_by(internal_name: 'A2') }
      form_prices { Machine::Reference::FORM_PRICE_COUNT.times.map { |i| (i+1) * 540 } }
      prices_per_hour { Machine::Reference::PRICE_PER_HOUR_COUNT.times.map { |i| (i+1) * 4000.0 } }
    end

    factory :machine_ryobi do
      name 'RYOBI'
      efficiency 6000
      color_count 2
      preparation_time 0.1
      additional_time 0.5
      skin 135
      max_x 347
      max_y 347
      format { MachineFormat.find_by(internal_name: 'A3') }
      form_prices { Machine::Reference::FORM_PRICE_COUNT.times.map { |i| (i+1) * 70 } }
      prices_per_hour { Machine::Reference::PRICE_PER_HOUR_COUNT.times.map { |i| (i+1) * 1800.0 } }
    end

    factory :machine_adast do
      name 'ADAST ROMAIER'
      efficiency 2000
      color_count 1
      preparation_time 0.1
      additional_time 0.1
      skin 135
      max_x 347
      max_y 347
      format { MachineFormat.find_by(internal_name: 'A3') }
      form_prices { Machine::Reference::FORM_PRICE_COUNT.times.map { |i| (i+1) * 70 } }
      prices_per_hour { Machine::Reference::PRICE_PER_HOUR_COUNT.times.map { |i| (i+1) * 500.0 } }
    end
  end

end
