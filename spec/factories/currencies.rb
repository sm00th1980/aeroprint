# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :currency do
    name { Faker::Name.name }
    internal_name { Faker::Name.name }
    exchange_rates { Currency::EXCHANGE_RATE_COUNT.times.map { |i| (i+1) * 100 } }
  end

end
