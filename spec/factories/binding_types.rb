# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :binding_type do

    factory :binding_type_clip do
      name 'на скрепку'
      internal_name 'clip'
    end

    factory :binding_type_glue do
      name 'клеевое'
      internal_name 'glue'
    end

    # factory :binding_type_spring do
    #   name 'на пружину'
    #   internal_name 'spring'
    # end
    #
    # factory :binding_type_valve do
    #   name 'на клапана'
    #   internal_name 'valve'
    # end
    #
    # factory :binding_type_hard_cover do
    #   name 'твёрдый переплёт'
    #   internal_name 'hard_cover'
    # end

  end

end
