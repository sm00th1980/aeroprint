# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :product do
    name { Faker::Commerce.product_name }
    product_type_id { ProductType.all.sample.id }

    factory :list_product do
      product_type_id { ProductType.list.id }
    end

    factory :many_page_product do
      product_type_id { ProductType.many_page.id }
    end
  end

end
