# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :uv_polish_price do
    print_price { rand(10) + 1 }
    preparation_price { rand(10_000) + 1 }

    factory :uv_polish_price_opaque do
      type { UvPolishType.opaque }

      factory :uv_polish_price_opaque_a1 do
        format { UvPolishFormat.A1 }
      end

      factory :uv_polish_price_opaque_a2 do
        format { UvPolishFormat.A2 }
      end

      factory :uv_polish_price_opaque_a3 do
        format { UvPolishFormat.A3 }
      end

      factory :uv_polish_price_opaque_a2_plus do
        format { UvPolishFormat.A2_plus }
      end

      factory :uv_polish_price_opaque_a3_plus do
        format { UvPolishFormat.A3_plus }
      end

    end

    factory :uv_polish_price_glossy do
      type { UvPolishType.glossy }

      factory :uv_polish_price_glossy_a2 do
        format { UvPolishFormat.A2 }
      end

      factory :uv_polish_price_glossy_a3 do
        format { UvPolishFormat.A3 }
      end

      factory :uv_polish_price_glossy_a2_plus do
        format { UvPolishFormat.A2_plus }
      end

      factory :uv_polish_price_glossy_a3_plus do
        format { UvPolishFormat.A3_plus }
      end

    end

  end

end
