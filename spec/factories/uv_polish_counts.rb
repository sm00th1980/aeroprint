# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :uv_polish_count do

    factory :uv_polish_count_one_side do
      name 'С одной стороны'
      internal_name 'one_side'
      value 1.0
    end

    factory :uv_polish_count_both_side do
      name 'С двух сторон'
      internal_name 'both_side'
      value 2.0
    end

  end

end
