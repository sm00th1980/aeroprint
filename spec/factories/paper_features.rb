# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :paper_feature do
    paper
    currency
    density { rand(100) }
    gor_sm { rand(100) }
    ver_sm { rand(100) }
    price_per_1_ton { rand(100) }
    uv_polish_format { UvPolishFormat.all.sample }

    after(:create) do |paper_feature|
      create(:paper_cut, paper_feature: paper_feature, machine: Machine.man, gor_mm: 602, ver_mm: 440, chunk_count: 2)
      create(:paper_cut, paper_feature: paper_feature, machine: Machine.ryobi, gor_mm: 290, ver_mm: 452, chunk_count: 4)
    end
  end

end
