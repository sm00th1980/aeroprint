# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :contract_status do
    name { Faker::Name.name }
    color { Faker::Name.name }
  end

end
