# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :material do
    name { Faker::Name.name }
    price { rand(10_000) }
  end

end
