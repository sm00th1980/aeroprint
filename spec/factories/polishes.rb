# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :polish do
    sequence(:name) { |n| n }

    factory :polish_positive do
      value false
    end

    factory :polish_negative do
      value true
    end

  end

end
