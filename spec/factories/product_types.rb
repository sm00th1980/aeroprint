# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :product_type do

    factory :product_type_list do
      name 'Листовая'
      internal_name 'list'
    end

    factory :product_type_many do
      name 'Многостраничная'
      internal_name 'many_page'
    end

  end

end
