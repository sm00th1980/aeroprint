# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :product_padding do

    factory :product_padding_list do
      product_type { ProductType.list }
      binding_type nil
      gor_mm 1
      ver_mm 1
    end

    factory :product_padding_many_page_clip do
      product_type { ProductType.many_page }
      binding_type { BindingType.clip }
      gor_mm 3
      ver_mm 1
    end

    factory :product_padding_many_page_glue do
      product_type { ProductType.many_page }
      binding_type { BindingType.glue }
      gor_mm 5
      ver_mm 2
    end

  end

end
