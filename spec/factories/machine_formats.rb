# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :machine_format do

    factory :machine_format_a2 do
      name 'A2'
      internal_name 'A2'
    end

    factory :machine_format_a3 do
      name 'A3'
      internal_name 'A3'
    end

  end

end
