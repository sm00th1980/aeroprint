# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :uv_polish_type do

    factory :uv_polish_type_opaque do
      name 'Матовый'
      internal_name 'opaque'
    end

    factory :uv_polish_type_glossy do
      name 'Глянцевый'
      internal_name 'glossy'
    end

  end

end
