# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :user do
    sequence(:email) { |n| "user_#{n}@aero-print.ru" }
    password '12345678'
    password_confirmation '12345678'
    admin false
    status ManagerStatus.active


    factory :admin do
      admin true
    end

    factory :manager do
      admin false

      factory :manager_blocked do
        status ManagerStatus.blocked
      end

      factory :manager_with_substitute do
        substitute { create(:manager) }
      end

      factory :manager_without_substitute do
        substitute nil
      end
    end

    before(:create) do
      create(:manager_status_active) if ManagerStatus.active.nil?
      create(:manager_status_blocked) if ManagerStatus.blocked.nil?
    end
  end

end
