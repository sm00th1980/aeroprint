# -*- encoding : utf-8 -*-
FactoryGirl.define do

  factory :cover_work_currency do

    factory :cover_work_currency_usd do
      name 'USD'
      internal_name 'usd'
      exchange_rate 50.0
    end

    factory :cover_work_currency_euro do
      name 'EURO'
      internal_name 'euro'
      exchange_rate 54.0
    end

  end

end
