# -*- encoding : utf-8 -*-
FactoryGirl.define do
  factory :paper_cut do
    paper_feature
    gor_mm { rand(500) }
    ver_mm { rand(500) }
    machine { Machine.all.sample }
    chunk_count { rand(10) }
    extended { [true, false].sample }
  end

end
