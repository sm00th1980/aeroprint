# -*- encoding : utf-8 -*-
require 'simplecov'
SimpleCov.start

# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'database_cleaner'
require 'sidekiq/testing'

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }


# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.check_pending! if defined?(ActiveRecord::Migration)

RSpec.configure do |config|
  # ## Mock Framework
  #
  # If you prefer to use mocha, flexmock or RR, uncomment the appropriate line:
  #
  # config.mock_with :mocha
  # config.mock_with :flexmock
  # config.mock_with :rr

  config.include FactoryGirl::Syntax::Methods
  config.include AssertDifference
  #config.order = "random"

  config.infer_spec_type_from_file_location!
  config.include Devise::TestHelpers, type: :controller

  # config.before(:suite) do
  #   DatabaseCleaner.strategy = :truncation
  #   DatabaseCleaner.clean_with(:truncation)
  # end

  config.before(:suite) do
    begin
      DatabaseCleaner.start
        #FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end

  config.before(:all) do
    UvPolishCount.delete_all
    UvPolishFormat.delete_all
    UvPolishType.delete_all
    UvPolishPrice.delete_all
    CalculationCoverWork.delete_all
    ProductType.delete_all
    Product.delete_all
    Machine.delete_all
    MachineFormat.delete_all
    User.delete_all
    Calculation.delete_all
    PaperFeature.delete_all
    CmykColor.delete_all
    PantoneColor.delete_all
    Polish.delete_all
    Additional.delete_all
    PaperCut.delete_all
    Discount.delete_all
    Branch.delete_all
    ContractStatus.delete_all
    CoverWorkOperation.delete_all
    CoverWorkCurrency.delete_all
    CoverWorkOwner.delete_all
    CoverWorkType.delete_all
    BindingType.delete_all
    ProductPadding.delete_all
    Currency.delete_all
    ComponentResult.delete_all
    Paper.delete_all

    FactoryGirl.create(:uv_polish_count_one_side)
    FactoryGirl.create(:uv_polish_count_both_side)

    FactoryGirl.create(:uv_polish_formats_a1)
    FactoryGirl.create(:uv_polish_formats_a2)
    FactoryGirl.create(:uv_polish_formats_a3)
    FactoryGirl.create(:uv_polish_formats_a2_plus)
    FactoryGirl.create(:uv_polish_formats_a3_plus)

    FactoryGirl.create(:uv_polish_type_opaque)
    FactoryGirl.create(:uv_polish_type_glossy)

    FactoryGirl.create(:uv_polish_price_glossy_a2)
    FactoryGirl.create(:uv_polish_price_glossy_a3)
    FactoryGirl.create(:uv_polish_price_glossy_a2_plus)
    FactoryGirl.create(:uv_polish_price_glossy_a3_plus)

    FactoryGirl.create(:uv_polish_price_opaque_a1)
    FactoryGirl.create(:uv_polish_price_opaque_a2)
    FactoryGirl.create(:uv_polish_price_opaque_a3)
    FactoryGirl.create(:uv_polish_price_opaque_a2_plus)
    FactoryGirl.create(:uv_polish_price_opaque_a3_plus)

    FactoryGirl.create(:binding_type_clip)
    FactoryGirl.create(:binding_type_glue)
    # FactoryGirl.create(:binding_type_spring)
    # FactoryGirl.create(:binding_type_valve)
    # FactoryGirl.create(:binding_type_hard_cover)

    FactoryGirl.create(:cover_work_product_owner)
    FactoryGirl.create(:cover_work_component_owner)

    create(:cover_work_currency_usd)
    create(:cover_work_currency_euro)

    create(:cover_work_operation_manual)
    create(:cover_work_operation_automatic)

    FactoryGirl.create(:cover_work_type)



    FactoryGirl.create(:product_type_list)
    FactoryGirl.create(:product_type_many)

    FactoryGirl.create(:discount_0)

    FactoryGirl.create(:machine_format_a2)
    FactoryGirl.create(:machine_format_a3)

    FactoryGirl.create(:product_padding_list)
    FactoryGirl.create(:product_padding_many_page_clip)
    FactoryGirl.create(:product_padding_many_page_glue)

    #machines
    FactoryGirl.create(:machine_man)
    FactoryGirl.create(:machine_ryobi)
    FactoryGirl.create(:machine_adast)

    #additionals
    FactoryGirl.create(:addition_cmyk_press_preparation)
    FactoryGirl.create(:addition_pantone_press_preparation)
    FactoryGirl.create(:addition_material_price)
    FactoryGirl.create(:addition_nds)
    FactoryGirl.create(:addition_preparation_cover_work)
    FactoryGirl.create(:addition_paper_discount_price)
    FactoryGirl.create(:addition_paper_full_price)

    #users
    FactoryGirl.create(:admin)
    FactoryGirl.create(:manager)

    #polishs
    FactoryGirl.create(:polish_negative)
    FactoryGirl.create(:polish_positive)

    FactoryGirl.create(:cmyk_color)
    FactoryGirl.create(:pantone_color)

    #client
    FactoryGirl.create(:client)

    #paper
    FactoryGirl.create(:paper)
  end

end
