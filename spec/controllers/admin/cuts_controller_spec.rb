# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CutsController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
    Paper.delete_all
    PaperFeature.delete_all
    PaperCut.delete_all
  end

  after(:all) do
    User.delete_all
    Paper.delete_all
    PaperFeature.delete_all
    PaperCut.delete_all
  end

  describe "GET index for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper_feature = create(:paper_feature, paper: create(:paper))

      5.times { create(:paper_cut, paper_feature: @paper_feature) }
    end

    it "should redirect to new_session if non-auth user" do
      get :index, id: @paper_feature
      expect(assigns(:paper_cuts)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show paper cut list for signed user" do
      sign_in @admin
      get :index, id: @paper_feature
      expect(response).to have_http_status(:success)
      expect(assigns(:paper_cuts)).to eq @paper_feature.cuts
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index, id: @paper_feature
      expect(assigns(:paper_cuts)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
