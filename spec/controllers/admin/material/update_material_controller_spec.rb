# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MaterialController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @material = create(:material)

      @params = {
          id: @material,
          name: 'new material',
          price: 123.67
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:material)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update material for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:material)).to eq @material

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('material.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(materials_path)


      expect(@material.reload.name).to eq @params[:name]
      expect(@material.reload.price).to eq @params[:price]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      post :update, @params
      expect(assigns(:material)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist material id" do
      sign_in @admin

      post :update, @params.merge({:id => -1})

      expect(assigns(:material)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(materials_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('material.failure.not_found')
    end

    it "should not update material without name" do
      old_material = @material

      sign_in @admin
      post :update, @params.except(:name)
      expect(assigns(:material)).to eq @material

      expect(flash[:alert]).to eq ["Наименование расходного материала не может быть пустым"]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_material_path(@material))

      expect(@material.reload.name).to eq old_material.name
      expect(@material.reload.price).to eq old_material.price
    end

  end

end
