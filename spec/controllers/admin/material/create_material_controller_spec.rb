# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MaterialController, :type => :controller do
  render_views


  describe "POST create for admin" do
    before(:each) do
      @admin = create(:admin)

      @params = {
          name: 'новый расходный материал',
          price: 123.34
      }
    end

    it "should create new machine" do
      sign_in @admin

      assert_difference 'Material.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(materials_path)

        new_material = Material.last
        expect(new_material.name).to eq @params[:name]
        expect(new_material.price).to eq @params[:price]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('material.success.created')
      end
    end

    it "should not create new material for user" do
      sign_in create(:user)

      assert_difference 'Material.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new material without name" do
      params = @params.except(:name)
      check_not_create_material(params, "Наименование расходного материала не может быть пустым")
    end

    it "should not create new machine without price" do
      params = @params.except(:price)
      check_not_create_material(params, "Стоимость расходного материала не может быть пустым")
    end

    private
    def check_not_create_material(params, error, user=@admin)
      sign_in user

      assert_difference 'Material.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_material_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
