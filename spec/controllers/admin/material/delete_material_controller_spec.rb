# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MaterialController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      @material = create(:material)
    end

    it "should destroy material" do
      sign_in @admin

      assert_difference 'Material.count', -1 do
        delete :destroy, :id => @material

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(materials_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('material.success.deleted')

        expect(Material.exists?(id: @material)).to be false
      end
    end

    it "should not destroy new material for non exists id" do
      sign_in @admin

      assert_difference 'Material.count', 0 do
        delete :destroy, :id => -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(materials_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('material.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      assert_difference 'Material.count', 0 do
        delete :destroy, :id => @material
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
