# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::PaperController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper = create(:paper)
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @paper
      expect(assigns(:material)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show paper for admin" do
      sign_in @admin
      get :edit, id: @paper
      expect(assigns(:paper)).to eq @paper
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      get :edit, id: @paper
      expect(assigns(:paper)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show cover_work_type for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:paper)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(papers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('paper.failure.not_found')
    end
  end

end
