# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::PaperController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper = create(:paper)
    end

    it "should destroy paper" do
      sign_in @admin

      assert_difference 'Paper.count', -1 do
        delete :destroy, id: @paper

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(papers_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('paper.success.deleted')

        expect(Paper.exists?(id: @paper)).to be false
      end
    end

    it "should not destroy new paper for non exists id" do
      sign_in @admin

      assert_difference 'Paper.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(papers_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('paper.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      assert_difference 'Paper.count', 0 do
        delete :destroy, id: @paper
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
