# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::PaperController, :type => :controller do
  render_views


  describe "POST create for admin" do
    before(:each) do
      @admin = create(:admin)

      @params = {
          name: 'новый тип бумаги',
          type: PaperType.melovka.id,
          producer: PaperProducer.bereg.id,
          country: PaperCountry.russia.id
      }
    end

    it "should create new paper" do
      sign_in @admin

      assert_difference 'Paper.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(papers_path)

        new_paper = Paper.last
        expect(new_paper.name).to eq @params[:name]
        expect(new_paper.type.id).to eq @params[:type]
        expect(new_paper.producer.id).to eq @params[:producer]
        expect(new_paper.country.id).to eq @params[:country]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('paper.success.created')
      end
    end

    it "should not create new paper for user" do
      sign_in create(:user)

      assert_difference 'Paper.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new paper without name" do
      params = @params.except(:name)
      check_not_create_paper(params, "Наименование бумаги не может быть пустым")
    end

    it "should not create new paper without type" do
      params = @params.except(:type)
      check_not_create_paper(params, "Тип бумаги не может быть пустым")
    end

    it "should not create new paper without producer" do
      params = @params.except(:producer)
      check_not_create_paper(params, "Поставщик бумаги не может быть пустым")
    end

    it "should not create new paper without country" do
      params = @params.except(:country)
      check_not_create_paper(params, "Страна-производитель бумаги не может быть пустым")
    end

    private
    def check_not_create_paper(params, error, user=@admin)
      sign_in user

      assert_difference 'Paper.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_paper_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
