# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::PaperController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @paper = create(:paper)

      @params = {
          id: @paper,
          name: 'new paper',
          type: PaperType.melovka.id,
          producer: PaperProducer.bereg.id,
          country: PaperCountry.russia.id
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:paper)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update paper for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:paper)).to eq @paper

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('paper.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(papers_path)


      expect(@paper.reload.name).to eq @params[:name]
      expect(@paper.reload.type.id).to eq @params[:type]
      expect(@paper.reload.producer.id).to eq @params[:producer]
      expect(@paper.reload.country.id).to eq @params[:country]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      post :update, @params
      expect(assigns(:paper)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist paper id" do
      sign_in @admin

      post :update, @params.merge({id: -1})

      expect(assigns(:paper)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(papers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('paper.failure.not_found')
    end

    it "should not update paper without name" do
      check_not_update_paper(@paper, :name, "Наименование бумаги не может быть пустым")
    end

    it "should not update paper without type" do
      check_not_update_paper(@paper, :type, "Тип бумаги не может быть пустым")
    end

    it "should not update paper without producer" do
      check_not_update_paper(@paper, :producer, "Поставщик бумаги не может быть пустым")
    end

    it "should not update paper without country" do
      check_not_update_paper(@paper, :country, "Страна-производитель бумаги не может быть пустым")
    end

    private
    def check_not_update_paper(paper, param, alert)
      old_paper = paper

      sign_in @admin
      post :update, @params.except(param)
      expect(assigns(:paper)).to eq paper

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_paper_path(paper))

      expect(paper.reload.name).to eq old_paper.name
      expect(paper.reload.type).to eq old_paper.type
      expect(paper.reload.producer).to eq old_paper.producer
      expect(paper.reload.country).to eq old_paper.country
    end
  end

end
