# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CutController, :type => :controller do
  render_views

  describe "update for admin" do
    before(:each) do
      @admin = User.admin
      @paper_cut= create(:paper_cut)

      @params = {
          id: @paper_cut,
          gor_mm: rand(500),
          ver_mm: rand(500),
          machine: Machine.all.sample.id,
          chunk_count: rand(10),
          extended: [true, false].sample
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:paper_cut)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update paper feature for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:paper_cut)).to eq @paper_cut

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('paper.feature.cut.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(cuts_feature_path(@paper_cut.paper_feature))

      expect(@paper_cut.reload.gor_mm).to eq @params[:gor_mm]
      expect(@paper_cut.reload.ver_mm).to eq @params[:ver_mm]
      expect(@paper_cut.reload.machine.id).to eq @params[:machine]
      expect(@paper_cut.reload.chunk_count).to eq @params[:chunk_count]
      expect(@paper_cut.reload.extended).to eq @params[:extended]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      post :update, @params
      expect(assigns(:paper_cut)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist paper feature id" do
      sign_in @admin

      post :update, @params.merge({id: -1})

      expect(assigns(:paper_cut)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(papers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('paper.feature.cut.failure.not_found')
    end

  end

end
