# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CutController, :type => :controller do
  render_views

  describe "destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper_cut = create(:paper_cut)
    end

    it "should destroy paper cut" do
      sign_in @admin

      assert_difference 'PaperCut.count', -1 do
        delete :destroy, id: @paper_cut

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(cuts_feature_path(@paper_cut.paper_feature))

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('paper.feature.cut.success.deleted')

        expect(PaperCut.exists?(id: @paper_cut)).to be false
      end
    end

    it "should not destroy paper cut for non exists id" do
      sign_in @admin

      assert_difference 'PaperCut.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(papers_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('paper.feature.cut.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      assert_difference 'PaperCut.count', 0 do
        delete :destroy, id: @paper_cut
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
