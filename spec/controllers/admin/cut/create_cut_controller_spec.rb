# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CutController, :type => :controller do
  render_views

  describe "create for admin" do
    before(:each) do
      @admin = User.admin

      @paper_feature = create(:paper_feature)

      @params = {
          id: @paper_feature,
          gor_mm: rand(500),
          ver_mm: rand(500),
          machine: Machine.all.sample.id,
          chunk_count: rand(10),
          extended: [true, false].sample
      }
    end

    it "should create new paper cut" do
      sign_in @admin

      assert_difference 'PaperCut.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(cuts_feature_path(@paper_feature))

        new_paper_cut = PaperCut.last
        expect(new_paper_cut.paper_feature).to eq @params[:id]
        expect(new_paper_cut.gor_mm).to eq @params[:gor_mm]
        expect(new_paper_cut.ver_mm).to eq @params[:ver_mm]
        expect(new_paper_cut.machine.id).to eq @params[:machine]
        expect(new_paper_cut.chunk_count).to eq @params[:chunk_count]
        expect(new_paper_cut.extended).to eq @params[:extended]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('paper.feature.cut.success.created')
      end
    end

    it "should not create new paper cut for manager" do
      sign_in create(:user)

      assert_difference 'PaperCut.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new paper cut without gor_mm" do
      params = @params.except(:gor_mm)
      check_not_create_paper_cut(params, "Размер по горизонтали не может быть пустым")
    end

    it "should not create new paper cut without ver_mm" do
      params = @params.except(:ver_mm)
      check_not_create_paper_cut(params, "Размер по вертикали не может быть пустым")
    end

    it "should not create new paper cut without machine" do
      params = @params.except(:machine)
      check_not_create_paper_cut(params, "Параметр машина не может быть пустым")
    end

    it "should not create new paper cut without chunk_count" do
      params = @params.except(:chunk_count)
      check_not_create_paper_cut(params, "Число частей после разреза не может быть пустым")
    end

    private
    def check_not_create_paper_cut(params, error, user=@admin)
      sign_in user

      assert_difference 'PaperCut.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_cut_path(@paper_feature))

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
