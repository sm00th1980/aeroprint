# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CutController, :type => :controller do
  render_views

  describe "GET new for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper_feature = create(:paper_feature)
    end

    it "should redirect to new_session if non-auth user" do
      get :new, id: @paper_feature
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show new paper feature" do
      sign_in @admin
      get :new, id: @paper_feature
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      get :new, id: @paper_feature
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
