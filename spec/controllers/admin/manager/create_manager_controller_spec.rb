# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ManagerController, :type => :controller do
  render_views

  describe "POST create for admin" do
    before(:each) do
      @admin = create(:admin)

      @params = {
          email: 'deyarov@gmail.com',
          password: 'criminal',
          password_confirmation: 'criminal',
          description: 'description',
          fio: 'Деяров Руслан',
          substitute: create(:manager).id
      }
    end

    it "should create new user" do
      sign_in @admin

      assert_difference 'User.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(managers_path)

        new_user = User.last
        expect(new_user.email).to eq @params[:email]
        expect(new_user.description).to eq @params[:description]
        expect(new_user.valid_password?(@params[:password])).to be true
        expect(new_user.fio).to eq @params[:fio]
        expect(new_user.admin).to be false
        expect(new_user.status).to eq ManagerStatus.active
        expect(new_user.substitute.id).to eq @params[:substitute]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('manager.success.created')
      end
    end

    it "should not create new manager for user" do
      sign_in FactoryGirl.create(:user)

      assert_difference 'User.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new user with different passwords" do
      params = @params.merge({password: @params[:password] + rand(100).to_s})
      check_not_create_user(params, "Подтверждение пароля не совпадает с паролем")
    end

    it "should not create new user without email" do
      params = @params.except(:email)
      check_not_create_user(params, "Логин не может быть пустым")
    end

    it "should not create new user with too short password" do
      params = @params.merge({password: '123', password_confirmation: '123'})
      check_not_create_user(params, "Пароль недостаточной длины (не может быть меньше 8 символов)")
    end

    it "should not create new user non unique email" do
      FactoryGirl.create(:user, email: @params[:email])
      params = @params
      check_not_create_user(params, "Логин уже существует", false)
    end

    private
    def check_not_create_user(params, error, drop_users=true, user=@admin)
      User.managers.delete_all if drop_users
      sign_in user

      assert_difference 'User.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_manager_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
