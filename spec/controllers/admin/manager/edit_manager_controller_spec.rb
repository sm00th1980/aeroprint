# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ManagerController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
  end

  after(:all) do
    User.delete_all
  end

  describe "GET edit for admin" do
    before(:each) do
      @admin = FactoryGirl.create(:admin)
      @manager = FactoryGirl.create(:manager)
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, :id => @manager
      expect(assigns(:manager)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show manager for admin" do
      sign_in @admin
      get :edit, :id => @manager
      expect(assigns(:manager)).to eq @manager
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in FactoryGirl.create(:user)
      get :edit, :id => @manager
      expect(assigns(:manager)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show for not exist manager" do
      sign_in @admin

      get :edit, :id => -1

      expect(assigns(:manager)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('manager.failure.not_found')
    end
  end

end
