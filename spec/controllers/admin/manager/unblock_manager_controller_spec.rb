# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ManagerController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
  end

  after(:all) do
    User.delete_all
  end

  describe "POST unblock for admin" do
    before(:each) do
      @admin = create(:admin)
      @manager_blocked = create(:manager_blocked)
    end

    it "should redirect to new_session if non-auth user" do
      expect(@manager_blocked.reload.status.blocked?).to be true

      post :unblock, :id => @manager_blocked
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)

      expect(@manager_blocked.reload.status.blocked?).to be true
    end

    it "should unblock blocked manager" do
      expect(@manager_blocked.reload.status.blocked?).to be true

      sign_in @admin
      post :unblock, :id => @manager_blocked
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('manager.success.unblocked')

      expect(@manager_blocked.reload.status.active?).to be true
    end

    it "should redirect to root page when try to unblock user" do
      expect(@manager_blocked.reload.status.blocked?).to be true

      sign_in FactoryGirl.create(:manager)
      post :unblock, :id => @manager_blocked
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)

      expect(@manager_blocked.reload.status.blocked?).to be true
    end

    it "should not block not exist manager" do
      sign_in @admin

      post :unblock, :id => -1

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('manager.failure.not_found')
    end
  end

end
