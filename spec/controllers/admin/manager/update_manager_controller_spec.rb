# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ManagerController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @manager = create(:manager)

      @params = {
          id: @manager,
          email: Faker::Internet.free_email,
          description: Faker::Internet.slug,
          fio: Faker::Internet.user_name,
          substitute: create(:manager).id
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:manager)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update manager for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:manager)).to eq @manager

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('manager.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(@manager.reload.fio).to eq @params[:fio]
      expect(@manager.reload.email).to eq @params[:email]
      expect(@manager.reload.description).to eq @params[:description]
      expect(@manager.reload.substitute.id).to eq @params[:substitute]
    end

    it "should update manager without substitute" do
      @manager.substitute = create(:manager)
      @manager.save!

      sign_in @admin
      post :update, @params.merge({substitute: 0})
      expect(assigns(:manager)).to eq @manager

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('manager.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(@manager.reload.fio).to eq @params[:fio]
      expect(@manager.reload.email).to eq @params[:email]
      expect(@manager.reload.description).to eq @params[:description]
      expect(@manager.reload.substitute).to be_nil
    end

    it "should redirect to root page for signed user" do
      sign_in FactoryGirl.create(:user)
      post :update, @params
      expect(assigns(:manager)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist manager" do
      sign_in @admin

      post :update, @params.merge({id: -1})

      expect(assigns(:manager)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('manager.failure.not_found')
    end

    it "should not update manager without email" do
      check_not_update_user(@params.except(:email), "Логин не может быть пустым")
    end

    it "should not update manager with exists email" do
      check_not_update_user(@params.merge({email: @admin.email}), "Логин уже существует")
    end

    it "should not update manager if substitute is him" do
      check_not_update_user(@params.merge({substitute: @manager.id}), "Заместителем не может быть сам человек")
    end

    private
    def check_not_update_user(params, error)
      updated_manager = params[:id]
      old_manager = params[:id]

      sign_in @admin
      post :update, params
      expect(assigns(:manager)).to eq updated_manager

      expect(flash[:alert]).to eq [error]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_manager_path(updated_manager))

      expect(updated_manager.reload.fio).to eq old_manager.fio
      expect(updated_manager.reload.email).to eq old_manager.email
      expect(updated_manager.reload.description).to eq old_manager.description
      expect(updated_manager.reload.substitute).to eq old_manager.substitute
    end

  end

end
