# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ManagerController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
  end

  after(:all) do
    User.delete_all
  end

  describe "POST block for admin" do
    before(:each) do
      @admin = create(:admin)
      @manager_active = create(:manager)
    end

    it "should redirect to new_session if non-auth user" do
      expect(@manager_active.reload.status.active?).to be true

      post :block, :id => @manager_active
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)

      expect(@manager_active.reload.status.active?).to be true
    end

    it "should block active manager" do
      expect(@manager_active.reload.status.active?).to be true

      sign_in @admin
      post :block, :id => @manager_active
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('manager.success.blocked')

      expect(@manager_active.reload.status.blocked?).to be true
    end

    it "should redirect to root page when try to block user" do
      expect(@manager_active.reload.status.active?).to be true

      sign_in FactoryGirl.create(:manager)
      post :block, :id => @manager_active
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)

      expect(@manager_active.reload.status.active?).to be true
    end

    it "should not block not exist manager" do
      sign_in @admin

      post :block, :id => -1

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('manager.failure.not_found')
    end
  end

end
