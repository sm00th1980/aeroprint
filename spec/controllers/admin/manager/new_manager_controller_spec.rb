# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ManagerController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
  end

  after(:all) do
    User.delete_all
  end

  describe "GET new for admin" do
    before(:each) do
      @admin = FactoryGirl.create(:admin)
    end

    it "should redirect to new_session if non-auth user" do
      get :new
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show new for signed user" do
      sign_in @admin
      get :new
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in FactoryGirl.create(:user)
      get :new
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
