# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::UvPolishPriceController, :type => :controller do
  render_views

  before(:all) do
    UvPolishPrice.delete_all

    create(:uv_polish_price_glossy_a2)
  end

  after(:all) do
    UvPolishPrice.delete_all
  end

  describe "GET edit for admin" do
    before(:each) do
      @admin = User.admin

      @uv_polish_price = UvPolishPrice.first
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @uv_polish_price
      expect(assigns(:uv_polish_price)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show uv_polish_price for admin" do
      sign_in @admin
      get :edit, id: @uv_polish_price
      expect(assigns(:uv_polish_price)).to eq @uv_polish_price
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      get :edit, id: @uv_polish_price
      expect(assigns(:uv_polish_price)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show uv_polish_price for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:uv_polish_price)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(uv_polish_prices_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('uv_polish_price.failure.not_found')
    end
  end

end
