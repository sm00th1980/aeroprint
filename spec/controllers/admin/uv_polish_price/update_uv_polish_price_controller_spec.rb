# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::UvPolishPriceController, :type => :controller do
  render_views

  before(:all) do
    UvPolishPrice.delete_all

    create(:uv_polish_price_glossy_a2)
  end

  after(:all) do
    UvPolishPrice.delete_all
  end

  describe "POST update for admin" do
    before(:each) do
      @admin = User.admin
      @uv_polish_price = UvPolishPrice.first

      @params = {
          id: @uv_polish_price,
          print_price: rand(100) + 1,
          preparation_price: rand(1000) + 1
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:uv_polish_price)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update uv_polish_price for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:uv_polish_price)).to eq @uv_polish_price

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('uv_polish_price.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(uv_polish_prices_path)

      expect(@uv_polish_price.reload.print_price).to eq @params[:print_price]
      expect(@uv_polish_price.reload.preparation_price).to eq @params[:preparation_price]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      post :update, @params
      expect(assigns(:uv_polish_price)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update uv_polish_price for not exist uv_polish_price id" do
      sign_in @admin

      post :update, @params.merge(id: -1)

      expect(assigns(:uv_polish_price)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(uv_polish_prices_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('uv_polish_price.failure.not_found')
    end

    #print_price
    it "should not update uv_polish_price with print_price is zero" do
      #check_not_update_uv_polish_price(@uv_polish_price, @params.merge(print_price: 0), "Стоимость оттиска должна быть больше нуля")
    end

    it "should not update uv_polish_price with print_price is not a number" do
      check_not_update_uv_polish_price(@uv_polish_price, @params.merge(print_price: 'nbjkfnjkbfg'), "Стоимость оттиска должна быть больше нуля")
    end

    it "should not update client without print_price" do
      check_not_update_uv_polish_price(@uv_polish_price, @params.except(:print_price), "Стоимость оттиска должна быть больше нуля")
    end

    #preparation_price
    it "should not update uv_polish_price with preparation_price is zero" do
      check_not_update_uv_polish_price(@uv_polish_price, @params.merge(preparation_price: 0), "Стоимость подготовительных работ должна быть больше нуля")
    end

    it "should not update uv_polish_price with preparation_price is not a number" do
      check_not_update_uv_polish_price(@uv_polish_price, @params.merge(preparation_price: 'nbjkfnjkbfg'), "Стоимость подготовительных работ должна быть больше нуля")
    end

    it "should not update client without preparation_price" do
      check_not_update_uv_polish_price(@uv_polish_price, @params.except(:preparation_price), "Стоимость подготовительных работ должна быть больше нуля")
    end

    private
    def check_not_update_uv_polish_price(uv_polish_price, params, alert)
      old_uv_polish_price = uv_polish_price

      sign_in @admin
      post :update, params
      expect(assigns(:uv_polish_price)).to eq uv_polish_price

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_uv_polish_price_path(uv_polish_price))

      expect(uv_polish_price.reload.print_price).to eq old_uv_polish_price.print_price
      expect(uv_polish_price.reload.preparation_price).to eq old_uv_polish_price.preparation_price
    end

  end

end
