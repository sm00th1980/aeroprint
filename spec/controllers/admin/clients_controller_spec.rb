# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ClientsController, :type => :controller do
  render_views

  before(:all) do
    Client.delete_all
    User.delete_all
  end

  after(:all) do
    Client.delete_all
    User.delete_all
  end

  describe "GET index for admin" do
    before(:each) do
      @admin = create(:admin)
      @manager = create(:manager)

      5.times { create(:client, manager: @manager) }
    end

    after(:each) do
      Client.delete_all
      User.delete_all
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show index for signed user" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:clients)).to eq Client.order('created_at DESC')
    end

    it "should redirect to root page for signed manager" do
      sign_in @manager
      get :index
      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
