# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ProductController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @product = create(:product)

      @params = {
          id: @product,
          name: 'new product',
          type: ProductType.all.sample.id
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:product)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update product for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:product)).to eq @product

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('product.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(products_path)

      expect(@product.reload.name).to eq @params[:name]
      expect(@product.reload.type.id).to eq @params[:type]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      post :update, @params
      expect(assigns(:product)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist product id" do
      sign_in @admin

      post :update, @params.merge({id: -1})

      expect(assigns(:product)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(products_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('product.failure.not_found')
    end

    it "should not update product without name" do
      check_not_update_product(@product, :name, "Наименование изделия не может быть пустым")
    end

    it "should not update product without type" do
      check_not_update_product(@product, :type, "Тип изделия не может быть пустым")
    end

    private
    def check_not_update_product(product, param, alert)
      old_product = product

      sign_in @admin
      post :update, @params.except(param)
      expect(assigns(:product)).to eq product

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_product_path(product))

      expect(product.reload.name).to eq old_product.name
      expect(product.reload.type).to eq old_product.type
    end
  end

end
