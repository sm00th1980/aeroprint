# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ProductController, :type => :controller do
  render_views


  describe "POST create for admin" do
    before(:each) do
      @admin = create(:admin)

      @params = {
          name: 'новое изделие',
          type: ProductType.all.sample.id
      }
    end

    it "should create new product" do
      sign_in @admin

      assert_difference 'Product.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(products_path)

        new_product = Product.last
        expect(new_product.name).to eq @params[:name]
        expect(new_product.type.id).to eq @params[:type]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('product.success.created')
      end
    end

    it "should not create new product for user" do
      sign_in create(:user)

      assert_difference 'Product.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new product without name" do
      params = @params.except(:name)
      check_not_create_product(params, "Наименование изделия не может быть пустым")
    end

    it "should not create new product without type" do
      params = @params.except(:type)
      check_not_create_product(params, "Тип изделия не может быть пустым")
    end

    private
    def check_not_create_product(params, error)
      sign_in @admin

      assert_difference 'Product.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_product_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
