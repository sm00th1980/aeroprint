# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ProductController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      @product = create(:product)
    end

    it "should destroy product" do
      sign_in @admin

      assert_difference 'Product.count', -1 do
        delete :destroy, id: @product

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(products_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('product.success.deleted')

        expect(Product.exists?(id: @product)).to be false
      end
    end

    it "should not destroy new product for non exists id" do
      sign_in @admin

      assert_difference 'Product.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(products_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('product.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      assert_difference 'Product.count', 0 do
        delete :destroy, id: @product
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
