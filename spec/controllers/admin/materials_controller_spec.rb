# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MaterialsController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
    Material.delete_all
  end

  after(:all) do
    User.delete_all
    Material.delete_all
  end

  describe "GET index for admin" do
    before(:each) do
      @admin = create(:admin)

      create(:material)
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:materials)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show machine list for signed user" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:materials)).to eq Material.all.order(:id)
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index
      expect(assigns(:materials)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
