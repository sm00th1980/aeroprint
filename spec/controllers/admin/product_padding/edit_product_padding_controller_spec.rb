# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ProductPaddingController, :type => :controller do
  render_views

  before(:all) do
    ProductPadding.delete_all
    create(:product_padding_list)
  end

  after(:all) do
    ProductPadding.delete_all
  end

  describe "GET edit for admin" do
    before(:each) do
      @admin = User.admin

      @product_padding = ProductPadding.all.sample
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @product_padding
      expect(assigns(:product_padding)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show product_padding for admin" do
      sign_in @admin
      get :edit, id: @product_padding
      expect(assigns(:product_padding)).to eq @product_padding
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      get :edit, id: @product_padding
      expect(assigns(:product_padding)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show product_padding for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:product_padding)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(product_paddings_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('product_padding.failure.not_found')
    end
  end

end
