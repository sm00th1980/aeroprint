# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ProductPaddingController, :type => :controller do
  render_views

  before(:all) do
    ProductPadding.delete_all
    create(:product_padding_list)
  end

  after(:all) do
    ProductPadding.delete_all
  end

  describe "update product_padding" do
    before(:each) do
      @admin = User.admin
      @product_padding = ProductPadding.all.sample

      @params = {
          id: @product_padding,
          gor_mm: rand(100),
          ver_mm: rand(100)
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:product_padding)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update product_padding for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:product_padding)).to eq @product_padding

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('product_padding.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(product_paddings_path)

      expect(@product_padding.reload.gor_mm).to eq @params[:gor_mm]
      expect(@product_padding.reload.ver_mm).to eq @params[:ver_mm]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      post :update, @params
      expect(assigns(:product_padding)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update additional for not exist product_padding id" do
      sign_in @admin

      post :update, @params.merge(id: -1)

      expect(assigns(:product_padding)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(product_paddings_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('product_padding.failure.not_found')
    end

  end

end
