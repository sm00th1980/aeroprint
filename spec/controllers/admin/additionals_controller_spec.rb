# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::AdditionalsController, :type => :controller do
  render_views

  before(:all) do
    Additional.delete_all

    create(:addition_cmyk_press_preparation)
    create(:addition_pantone_press_preparation)
    create(:addition_material_price)
    create(:addition_nds)
  end

  after(:all) do
    Additional.delete_all
  end

  describe "GET index for admin" do

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:additionals)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show index for signed user" do
      sign_in create(:admin)
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:additionals)).to eq Additional.order(:name)
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index
      expect(assigns(:additionals)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
