# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MachineController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = User.admin
      @machine = Machine.all.sample

      @params = {
          id: @machine,
          name: 'ADAST ROMAIER',
          efficiency: 6000,
          color_count: 4,
          preparation_time: 0.1,
          additional_time: 0.3,
          price_per_hour_1: 4000,
          price_per_hour_2: 5000,
          price_per_hour_3: 6000,
          form_price_1: 540,
          form_price_2: 640,
          skin: 135,
          max_x: 718,
          max_y: 718,
          format: MachineFormat.all.sample.id,
          scale: rand(20),
          cross: rand(20),
          valve: rand(20)
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:manager)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update machine for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:machine)).to eq @machine

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('machine.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(machines_path)


      expect(@machine.reload.name).to eq @params[:name]
      expect(@machine.reload.efficiency).to eq @params[:efficiency]
      expect(@machine.reload.color_count).to eq @params[:color_count]
      expect(@machine.reload.preparation_time).to eq @params[:preparation_time]
      expect(@machine.reload.additional_time).to eq @params[:additional_time]
      expect(@machine.reload.prices_per_hour).to eq [@params[:price_per_hour_1], @params[:price_per_hour_2], @params[:price_per_hour_3]]
      expect(@machine.reload.form_prices).to eq [@params[:form_price_1], @params[:form_price_2]]
      expect(@machine.reload.skin).to eq @params[:skin]
      expect(@machine.reload.max_x).to eq @params[:max_x]
      expect(@machine.reload.max_y).to eq @params[:max_y]
      expect(@machine.reload.format).to eq MachineFormat.find_by(id: @params[:format])
      expect(@machine.reload.scale).to eq @params[:scale]
      expect(@machine.reload.cross).to eq @params[:cross]
      expect(@machine.reload.valve).to eq @params[:valve]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      post :update, @params
      expect(assigns(:machine)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist machine id" do
      sign_in @admin

      post :update, @params.merge({:id => -1})

      expect(assigns(:machine)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(machines_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('machine.failure.not_found')
    end

    it "should not update machine without name" do
      old_machine = @machine

      sign_in @admin
      post :update, @params.except(:name)
      expect(assigns(:machine)).to eq @machine

      expect(flash[:alert]).to eq ["Наименование машины не может быть пустым"]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_machine_path(@machine))

      expect(@machine.reload.name).to eq old_machine.name
      expect(@machine.reload.efficiency).to eq old_machine.efficiency
      expect(@machine.reload.color_count).to eq old_machine.color_count
      expect(@machine.reload.preparation_time).to eq old_machine.preparation_time
      expect(@machine.reload.additional_time).to eq old_machine.additional_time
      expect(@machine.reload.prices_per_hour).to eq old_machine.prices_per_hour
      expect(@machine.reload.form_prices).to eq old_machine.form_prices
      expect(@machine.reload.skin).to eq old_machine.skin
      expect(@machine.reload.max_x).to eq old_machine.max_x
      expect(@machine.reload.max_y).to eq old_machine.max_y
      expect(@machine.reload.format).to eq old_machine.format
      expect(@machine.reload.scale).to eq old_machine.scale
      expect(@machine.reload.cross).to eq old_machine.cross
      expect(@machine.reload.valve).to eq old_machine.valve
    end

  end

end
