# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MachineController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @admin = create(:admin)

      @machine = Machine.all.sample
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, :id => @machine
      expect(assigns(:machine)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show machine for admin" do
      sign_in @admin
      get :edit, :id => @machine
      expect(assigns(:machine)).to eq @machine
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      get :edit, :id => @machine
      expect(assigns(:machine)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show machine for not exist id" do
      sign_in @admin

      get :edit, :id => -1

      expect(assigns(:machine)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(machines_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('machine.failure.not_found')
    end
  end

end
