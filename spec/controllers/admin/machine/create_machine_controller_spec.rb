# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MachineController, :type => :controller do
  render_views

  describe "POST create for admin" do
    before(:each) do
      @admin = User.admin
      @manager = User.managers.first

      @params = {
          name: 'ADAST ROMAIER',
          efficiency: 6000,
          color_count: 4,
          preparation_time: 0.1,
          additional_time: 0.3,

          price_per_hour_1: 4000,
          price_per_hour_2: 5000,
          price_per_hour_3: 6000,

          form_price_1: 540,
          form_price_2: 640,

          skin: 135,
          max_x: 718,
          max_y: 718,
          format: MachineFormat.all.sample.id,
          scale: rand(20),
          cross: rand(20),
          valve: rand(20)
      }
    end

    it "should create new machine" do
      sign_in @admin

      assert_difference 'Machine.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(machines_path)

        new_machine = Machine.last
        expect(new_machine.name).to eq @params[:name]
        expect(new_machine.efficiency).to eq @params[:efficiency]
        expect(new_machine.color_count).to eq @params[:color_count]
        expect(new_machine.preparation_time).to eq @params[:preparation_time]
        expect(new_machine.additional_time).to eq @params[:additional_time]
        expect(new_machine.prices_per_hour).to eq [@params[:price_per_hour_1], @params[:price_per_hour_2], @params[:price_per_hour_3]]
        expect(new_machine.form_prices).to eq [@params[:form_price_1], @params[:form_price_2]]
        expect(new_machine.skin).to eq @params[:skin]
        expect(new_machine.max_x).to eq @params[:max_x]
        expect(new_machine.max_y).to eq @params[:max_y]
        expect(new_machine.format).to eq MachineFormat.find_by(id: @params[:format])
        expect(new_machine.scale).to eq @params[:scale]
        expect(new_machine.cross).to eq @params[:cross]
        expect(new_machine.valve).to eq @params[:valve]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('machine.success.created')
      end
    end

    it "should not create new manager for user" do
      sign_in @manager

      assert_difference 'Machine.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new machine without name" do
      params = @params.except(:name)
      check_not_create_machine(params, "Наименование машины не может быть пустым")
    end

    it "should not create new machine without efficiency" do
      params = @params.except(:efficiency)
      check_not_create_machine(params, "Эффективность машины не может быть пустым")
    end

    it "should not create new machine without color_count" do
      params = @params.except(:color_count)
      check_not_create_machine(params, "Число цветов машины не может быть пустым")
    end

    it "should not create new machine without preparation_time" do
      params = @params.except(:preparation_time)
      check_not_create_machine(params, "Время приладки машины не может быть пустым")
    end

    it "should not create new machine without additional_time" do
      params = @params.except(:additional_time)
      check_not_create_machine(params, "Время на доп. обслуживание машины не может быть пустым")
    end

    it "should not create new machine without price_per_hour 1" do
      params = @params.except(:price_per_hour_1)
      check_not_create_machine(params, "Цены за час работы - число цен за час не корректно")
    end

    it "should not create new machine without price_per_hour 2" do
      params = @params.except(:price_per_hour_2)
      check_not_create_machine(params, "Цены за час работы - число цен за час не корректно")
    end

    it "should not create new machine without price_per_hour 3" do
      params = @params.except(:price_per_hour_3)
      check_not_create_machine(params, "Цены за час работы - число цен за час не корректно")
    end

    it "should not create new machine without form_price_1" do
      params = @params.except(:form_price_1)
      check_not_create_machine(params, "Цены за формы - число цен за формы не корректно")
    end

    it "should not create new machine without form_price_2" do
      params = @params.except(:form_price_2)
      check_not_create_machine(params, "Цены за формы - число цен за формы не корректно")
    end

    it "should not create new machine without skin" do
      params = @params.except(:skin)
      check_not_create_machine(params, "Плёнка машины не может быть пустым")
    end

    it "should not create new machine without max_x" do
      params = @params.except(:max_x)
      check_not_create_machine(params, "MaxX машины не может быть пустым")
    end

    it "should not create new machine without max_y" do
      params = @params.except(:max_y)
      check_not_create_machine(params, "MaxY машины не может быть пустым")
    end

    it "should not create new machine without format" do
      params = @params.except(:format)
      check_not_create_machine(params, "Формат машины не может быть пустым")
    end

    it "should not create new machine without scale" do
      params = @params.except(:scale)
      check_not_create_machine(params, "Шкала машины не может быть пустым")
    end

    it "should not create new machine without cross" do
      params = @params.except(:cross)
      check_not_create_machine(params, "Кресты/марки машины не может быть пустым")
    end

    it "should not create new machine without valve" do
      params = @params.except(:valve)
      check_not_create_machine(params, "Клапан машины не может быть пустым")
    end

    private
    def check_not_create_machine(params, error, drop_machines=true, user=@admin)
      Machine.delete_all if drop_machines
      sign_in user

      assert_difference 'Machine.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_machine_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
