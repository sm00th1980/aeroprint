# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MachineController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = User.admin

      @machine = Machine.all.sample
    end

    it "should destroy machine" do
      sign_in @admin

      assert_difference 'Machine.count', -1 do
        delete :destroy, :id => @machine

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(machines_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('machine.success.deleted')

        expect(Machine.exists?(id: @machine)).to be false
      end
    end

    it "should not destroy new user for non exists id" do
      sign_in @admin

      assert_difference 'Machine.count', 0 do
        delete :destroy, :id => -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(machines_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('machine.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in User.managers.sample
      assert_difference 'Machine.count', 0 do
        delete :destroy, :id => @machine
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
