# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::BranchesController, :type => :controller do
  render_views

  describe "GET branches list for admin" do
    before(:each) do
      @admin = create(:admin)

      @client = create(:client)

      5.times { create(:branch, client: @client) }
    end

    it "should redirect to new_session if non-auth user" do
      get :index, id: @client
      expect(assigns(:branches)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show branch list for signed user" do
      sign_in @admin
      get :index, id: @client
      expect(response).to have_http_status(:success)
      expect(assigns(:branches)).to eq @client.branches
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index, id: @client
      expect(assigns(:branches)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
