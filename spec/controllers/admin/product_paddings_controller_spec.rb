# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ProductPaddingsController, :type => :controller do
  render_views

  before(:all) do
    ProductPadding.delete_all

    create(:product_padding_list)
    create(:product_padding_many_page_clip)
    create(:product_padding_many_page_glue)
  end

  after(:all) do
    ProductPadding.delete_all
  end

  describe "GET index for admin" do

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:product_paddings)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show product_padding list for admin" do
      sign_in create(:admin)
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:product_paddings)).to eq ProductPadding.order(:id)
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index
      expect(assigns(:product_paddings)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
