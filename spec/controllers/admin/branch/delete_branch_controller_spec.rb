# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::BranchController, :type => :controller do
  render_views

  describe "DELETE branch" do
    before(:each) do
      @admin = create(:admin)

      @client = create(:client)
      @branch = create(:branch, client: @client)
    end

    it "should destroy branch" do
      sign_in @admin

      assert_difference 'Branch.count', -1 do
        delete :destroy, id: @branch

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(branches_client_path(@client))

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('branch.success.deleted')

        expect(Branch.exists?(id: @branch)).to be false
      end
    end

    it "should not destroy branch for non exists id" do
      sign_in @admin

      assert_difference 'Branch.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('branch.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      assert_difference 'Branch.count', 0 do
        delete :destroy, id: @branch
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
