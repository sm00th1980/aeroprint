# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::UvPolishPricesController, :type => :controller do
  render_views

  before(:all) do
    UvPolishPrice.delete_all

    create(:uv_polish_price_glossy_a2)
    create(:uv_polish_price_glossy_a3)
    create(:uv_polish_price_glossy_a2_plus)
    create(:uv_polish_price_glossy_a3_plus)

    create(:uv_polish_price_opaque_a1)
    create(:uv_polish_price_opaque_a2)
    create(:uv_polish_price_opaque_a3)
    create(:uv_polish_price_opaque_a2_plus)
    create(:uv_polish_price_opaque_a3_plus)
  end

  after(:all) do
    UvPolishPrice.delete_all
  end

  describe "GET index for admin" do

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:uv_polish_prices)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show index for signed user" do
      sign_in create(:admin)
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:uv_polish_prices)).to eq UvPolishPrice.order(:type_id, :format_id)
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index
      expect(assigns(:uv_polish_prices)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
