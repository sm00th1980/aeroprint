# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CurrencyController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @currency = create(:currency)

      rate = rand(100) + 10

      @params = {
          id: @currency,
          exchange_rate_0: rate,
          exchange_rate_1: rate+10,
          exchange_rate_2: rate+20,
          exchange_rate_3: rate+30
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update currency for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:currency)).to eq @currency

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('currency.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(currencies_path)

      expect(@currency.reload.exchange_rates).to eq [@params[:exchange_rate_0], @params[:exchange_rate_1], @params[:exchange_rate_2], @params[:exchange_rate_3]]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      post :update, @params
      expect(assigns(:currency)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update currency for not exist currency id" do
      sign_in @admin

      post :update, @params.merge(id: -1)

      expect(assigns(:currency)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(currencies_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('currency.failure.not_found')
    end

    it "should not update client with one of exchange_rate is zero" do
      check_not_update_client(@currency, @params.merge(exchange_rate_0: 0), "Курсы валюты должны быть больше нуля")
    end

    it "should not update client with one of exchange_rates is not a number" do
      check_not_update_client(@currency, @params.merge(exchange_rate_0: 'nbjkfnjkbfg'), "Курсы валюты должны быть больше нуля")
    end

    it "should not update client without exchange_rate_0" do
      check_not_update_client(@currency, @params.except(:exchange_rate_0), "Курсы валюты - число валют не корректно")
    end

    private
    def check_not_update_client(currency, params, alert)
      old_currency = currency

      sign_in @admin
      post :update, params
      expect(assigns(:currency)).to eq currency

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_currency_path(currency))

      expect(currency.reload.exchange_rates).to eq old_currency.exchange_rates
    end
  end

end
