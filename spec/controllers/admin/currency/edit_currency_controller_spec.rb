# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CurrencyController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @admin = User.admin
      @currency = create(:currency)
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @currency
      expect(assigns(:currency)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show currency for admin" do
      sign_in @admin
      get :edit, id: @currency
      expect(assigns(:currency)).to eq @currency
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      get :edit, id: @currency
      expect(assigns(:currency)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show currency for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:currency)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(currencies_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('currency.failure.not_found')
    end
  end

end
