# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::FeaturesController, :type => :controller do
  render_views

  describe "GET index for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper = create(:paper)
      5.times { create(:paper_feature, paper: @paper) }
    end

    it "should redirect to new_session if non-auth user" do
      get :index, id: @paper
      expect(assigns(:paper_features)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show paper list for signed user" do
      sign_in @admin
      get :index, id: @paper
      expect(response).to have_http_status(:success)
      expect(assigns(:paper_features)).to eq @paper.features.order(:density, :gor_sm, :ver_sm)
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index, id: @paper
      expect(assigns(:paper_features)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
