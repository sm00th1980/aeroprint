# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::AdditionalController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
    Additional.delete_all

    create(:addition_cmyk_press_preparation)
  end

  after(:all) do
    User.delete_all
    Additional.delete_all
  end

  describe "GET edit for admin" do
    before(:each) do
      @admin = create(:admin)

      @additional = Additional.find_by(internal_name: 'cmyk_press_preparation')
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @additional
      expect(assigns(:additional)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show additional for admin" do
      sign_in @admin
      get :edit, id: @additional
      expect(assigns(:additional)).to eq @additional
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      get :edit, id: @additional
      expect(assigns(:additional)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show additional for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:additional)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(additionals_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('additional.failure.not_found')
    end
  end

end
