# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::AdditionalController, :type => :controller do
  render_views

  before(:all) do
    User.delete_all
    Additional.delete_all

    create(:addition_cmyk_press_preparation)
  end

  after(:all) do
    User.delete_all
    Additional.delete_all
  end

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @additional = Additional.find_by(internal_name: 'cmyk_press_preparation')

      @params = {
          id: @additional,
          value: rand(200)
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:additional)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update additional for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:additional)).to eq @additional

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('additional.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(additionals_path)

      expect(@additional.reload.value).to eq @params[:value]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      post :update, @params
      expect(assigns(:additional)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update additional for not exist additional id" do
      sign_in @admin

      post :update, @params.merge(id: -1)

      expect(assigns(:additional)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(additionals_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('additional.failure.not_found')
    end

  end

end
