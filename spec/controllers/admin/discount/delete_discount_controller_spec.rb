# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::DiscountController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      Discount.delete_all
      create(:discount_0)
      @discount = create(:discount, name: 'скидка 10%', value: 10)
    end

    it "should destroy discount" do
      sign_in @admin

      assert_difference 'Discount.count', -1 do
        delete :destroy, id: @discount

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(discounts_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('discount.success.deleted')

        expect(Discount.exists?(id: @discount)).to be false
      end
    end

    it "should not destroy discount for non exists id" do
      sign_in @admin

      assert_difference 'Discount.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(discounts_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('discount.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      assert_difference 'Discount.count', 0 do
        delete :destroy, id: @discount
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
