# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::DiscountController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)

      Discount.delete_all
      create(:discount_0)
      @discount = create(:discount, name: 'скидка 10%', value: 10)

      @params = {
          id: @discount,
          name: 'новое имя скидки',
          value: @discount.value + 10.0
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:discount)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update discount for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:discount)).to eq @discount

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('discount.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(discounts_path)

      expect(@discount.reload.name).to eq @params[:name]
      expect(@discount.reload.value).to eq @params[:value]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      post :update, @params
      expect(assigns(:discount)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist discount id" do
      sign_in @admin

      post :update, @params.merge({id: -1})

      expect(assigns(:discount)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(discounts_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('discount.failure.not_found')
    end

    it "should not update discount without name" do
      check_not_update_discount(@discount, :name, "Имя скидки не может быть пустым")
    end

    it "should not update paper without value" do
      check_not_update_discount(@discount, :value, "Значение скидки не может быть пустым")
    end

    private
    def check_not_update_discount(discount, param, alert)
      old_discount = discount

      sign_in @admin
      post :update, @params.except(param)
      expect(assigns(:discount)).to eq discount

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_discount_path(discount))

      expect(discount.reload.name).to eq old_discount.name
      expect(discount.reload.value).to eq old_discount.value
    end
  end

end
