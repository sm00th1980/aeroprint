# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::DiscountController, :type => :controller do
  render_views


  describe "create for admin" do
    before(:each) do
      @admin = create(:admin)

      @params = {
          name: 'новое скидка',
          value: 15.0
      }
    end

    it "should create new discount" do
      sign_in @admin

      assert_difference 'Discount.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(discounts_path)

        new_discount = Discount.last
        expect(new_discount.name).to eq @params[:name]
        expect(new_discount.value).to eq @params[:value]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('discount.success.created')
      end
    end

    it "should not create new discount for user" do
      sign_in create(:user)

      assert_difference 'Discount.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new discount without name" do
      params = @params.except(:name)
      check_not_create_product(params, "Имя скидки не может быть пустым")
    end

    it "should not create new discount without value" do
      params = @params.except(:value)
      check_not_create_product(params, "Значение скидки не может быть пустым")
    end

    private
    def check_not_create_product(params, error)
      sign_in @admin

      assert_difference 'Discount.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_discount_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
