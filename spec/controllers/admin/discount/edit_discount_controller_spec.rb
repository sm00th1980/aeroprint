# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::DiscountController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @admin = create(:admin)

      Discount.delete_all
      create(:discount_0)
      @discount = create(:discount, name: 'скидка 10%', value: 10)
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @discount
      expect(assigns(:discount)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show discount for admin" do
      sign_in @admin
      get :edit, id: @discount
      expect(assigns(:discount)).to eq @discount
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:manager)
      get :edit, id: @discount
      expect(assigns(:discount)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show discount for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:discount)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(discounts_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('discount.failure.not_found')
    end
  end

end
