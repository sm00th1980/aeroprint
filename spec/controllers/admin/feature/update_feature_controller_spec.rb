# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::FeatureController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @paper_feature = create(:paper_feature)
      @currency = create(:currency)

      @params = {
          id: @paper_feature,
          density: 80,
          gor_sm: 62,
          ver_sm: 94,
          price_per_1_ton: 111,
          currency: @currency,
          uv_polish_format: UvPolishFormat.all.sample.id
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:paper_feature)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update paper feature for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:paper_feature)).to eq @paper_feature

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('paper.feature.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(features_paper_path(@paper_feature.paper))

      expect(@paper_feature.reload.density).to eq @params[:density]
      expect(@paper_feature.reload.gor_sm).to eq @params[:gor_sm]
      expect(@paper_feature.reload.ver_sm).to eq @params[:ver_sm]
      expect(@paper_feature.reload.price_per_1_ton).to eq @params[:price_per_1_ton]
      expect(@paper_feature.reload.currency).to eq @params[:currency]
      expect(@paper_feature.reload.uv_polish_format.id).to eq @params[:uv_polish_format]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      post :update, @params
      expect(assigns(:paper_feature)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist paper feature id" do
      sign_in @admin

      post :update, @params.merge({id: -1})

      expect(assigns(:paper_feature)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(papers_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('paper.feature.failure.not_found')
    end

    it "should not update with non unique params" do
      create(:paper_feature, paper: @paper_feature.paper, density: @params[:density], gor_sm: @params[:gor_sm], ver_sm: @params[:ver_sm])

      sign_in @admin
      post :update, @params
      expect(assigns(:paper_feature)).to eq @paper_feature

      expect(flash[:alert]).to eq [I18n.t('paper_feature.failure.non_unique')]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_feature_path(@paper_feature))
    end

    it "should not update paper feature without density" do
      check_not_update_paper_feature(@paper_feature, :density, "Плотность не может быть пустым")
    end

    it "should not update paper feature without gor_sm" do
      check_not_update_paper_feature(@paper_feature, :gor_sm, "gor_sm не может быть пустым")
    end

    it "should not update paper feature without ver_sm" do
      check_not_update_paper_feature(@paper_feature, :ver_sm, "ver_sm не может быть пустым")
    end

    it "should not update paper feature without price_per_1_ton" do
      check_not_update_paper_feature(@paper_feature, :price_per_1_ton, "Цена за 1 тонну не может быть пустым")
    end

    it "should not update paper feature without currency" do
      check_not_update_paper_feature(@paper_feature, :currency, "Валюта не может быть пустым")
    end

    it "should not update paper feature without uv_polish_format" do
      check_not_update_paper_feature(@paper_feature, :uv_polish_format, "Формат листа для УФ-лака не может быть пустым")
    end

    private
    def check_not_update_paper_feature(paper_feature, param, alert)
      old_paper_feature = paper_feature

      sign_in @admin
      post :update, @params.except(param)
      expect(assigns(:paper_feature)).to eq paper_feature

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_feature_path(paper_feature))

      expect(old_paper_feature.reload.density).to eq old_paper_feature.density
      expect(old_paper_feature.reload.gor_sm).to eq old_paper_feature.gor_sm
      expect(old_paper_feature.reload.ver_sm).to eq old_paper_feature.ver_sm
      expect(old_paper_feature.reload.price_per_1_ton).to eq old_paper_feature.price_per_1_ton
      expect(old_paper_feature.reload.currency).to eq old_paper_feature.currency
    end
  end

end
