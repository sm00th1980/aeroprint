# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::FeatureController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper_feature = create(:paper_feature, paper: create(:paper))
    end

    it "should destroy paper feature" do
      sign_in @admin

      assert_difference 'PaperFeature.count', -1 do
        delete :destroy, id: @paper_feature

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(features_paper_path(@paper_feature.paper))

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('paper.feature.success.deleted')

        expect(PaperFeature.exists?(id: @paper_feature)).to be false
      end
    end

    it "should not destroy new paper feature for non exists id" do
      sign_in @admin

      assert_difference 'PaperFeature.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(papers_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('paper.feature.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      assert_difference 'PaperFeature.count', 0 do
        delete :destroy, id: @paper_feature
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
