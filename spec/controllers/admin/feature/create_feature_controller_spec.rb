# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::FeatureController, :type => :controller do
  render_views

  describe "POST create for admin" do
    before(:each) do
      @admin = create(:admin)

      @paper = create(:paper)
      @currency = create(:currency)

      @params = {
          id: @paper,
          density: 80,
          gor_sm: 62,
          ver_sm: 94,
          price_per_1_ton: 100,
          currency: @currency,
          uv_polish_format: UvPolishFormat.all.sample.id
      }
    end

    it "should create new paper feature" do
      sign_in @admin

      assert_difference 'PaperFeature.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(features_paper_path(@paper))

        new_paper_feature = PaperFeature.last
        expect(new_paper_feature.paper).to eq @params[:id]
        expect(new_paper_feature.density).to eq @params[:density]
        expect(new_paper_feature.gor_sm).to eq @params[:gor_sm]
        expect(new_paper_feature.ver_sm).to eq @params[:ver_sm]
        expect(new_paper_feature.price_per_1_ton).to eq @params[:price_per_1_ton]
        expect(new_paper_feature.currency).to eq @params[:currency]
        expect(new_paper_feature.uv_polish_format.id).to eq @params[:uv_polish_format]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('paper.feature.success.created')
      end
    end

    it "should not create new paper feature for user" do
      sign_in create(:user)

      assert_difference 'PaperFeature.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new non unique paper feature" do
      create(:paper_feature, paper: @paper, density: @params[:density], gor_sm: @params[:gor_sm], ver_sm: @params[:ver_sm])
      check_not_create_paper_feature(@params, I18n.t('paper_feature.failure.non_unique'))
    end

    it "should not create new paper feature without density" do
      params = @params.except(:density)
      check_not_create_paper_feature(params, "Плотность не может быть пустым")
    end

    it "should not create new paper feature without gor_sm" do
      params = @params.except(:gor_sm)
      check_not_create_paper_feature(params, "gor_sm не может быть пустым")
    end

    it "should not create new paper feature without ver_sm" do
      params = @params.except(:ver_sm)
      check_not_create_paper_feature(params, "ver_sm не может быть пустым")
    end

    it "should not create new paper feature without price_per_1_ton" do
      params = @params.except(:price_per_1_ton)
      check_not_create_paper_feature(params, "Цена за 1 тонну не может быть пустым")
    end

    it "should not create new paper feature without currency" do
      params = @params.except(:currency)
      check_not_create_paper_feature(params, "Валюта не может быть пустым")
    end

    it "should not create new paper feature without uv_polish_format" do
      params = @params.except(:uv_polish_format)
      check_not_create_paper_feature(params, "Формат листа для УФ-лака не может быть пустым")
    end

    private
    def check_not_create_paper_feature(params, error, user=@admin)
      sign_in user

      assert_difference 'PaperFeature.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_feature_path(@paper))

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
