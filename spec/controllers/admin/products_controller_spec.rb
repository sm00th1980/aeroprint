# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ProductsController, :type => :controller do
  render_views

  describe "GET index for admin" do
    before(:each) do
      @admin = create(:admin)

      5.times { create(:product) }
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:products)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show product list for signed user" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:products)).to eq Product.all.order(:name)
    end

    it "should redirect to root page for signed manager" do
      sign_in create(:manager)
      get :index
      expect(assigns(:products)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
