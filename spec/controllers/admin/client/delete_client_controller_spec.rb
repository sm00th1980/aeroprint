# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ClientController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      @client = create(:client)
    end

    it "should destroy client" do
      sign_in @admin

      assert_difference 'Client.count', -1 do
        delete :destroy, id: @client

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(clients_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('client.success.deleted')

        expect(Client.exists?(id: @client)).to be false
      end
    end

    it "should not destroy client for non exists id" do
      sign_in @admin

      assert_difference 'Client.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      assert_difference 'Client.count', 0 do
        delete :destroy, id: @client
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
