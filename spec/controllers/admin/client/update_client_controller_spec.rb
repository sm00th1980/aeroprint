# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ClientController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @client = create(:client)

      @params = {
          id: @client,
          fio: 'новый клиент - имя',
          phone: '79379903835',
          email: 'sm00th1980@mail.ru',
          contract: '123',
          payer: 'Плательщик',
          description: 'новый клиент - описание',
          manager: create(:manager).id,
          discount: Discount.all.sample.id,
          shipping_address: 'новый адрес отгрузки',
          contract_status: ContractStatus.all.sample.id
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update client for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:client)).to eq @client

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('client.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(clients_path)


      expect(@client.reload.fio).to eq @params[:fio]
      expect(@client.reload.phone).to eq @params[:phone]
      expect(@client.reload.email).to eq @params[:email]
      expect(@client.reload.contract).to eq @params[:contract]
      expect(@client.reload.payer).to eq @params[:payer]
      expect(@client.reload.description).to eq @params[:description]
      expect(@client.reload.manager.id).to eq @params[:manager]
      expect(@client.reload.discount.id).to eq @params[:discount]
      expect(@client.reload.shipping_address).to eq @params[:shipping_address]
      expect(@client.reload.contract_status.id).to eq @params[:contract_status]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      post :update, @params
      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update client for not exist client id" do
      sign_in @admin

      post :update, @params.merge(id: -1)

      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
    end

    it "should not update client without fio" do
      check_not_update_client(@client, :fio, "ФИО клиента не может быть пустым")
    end

    it "should not update paper without manager" do
      check_not_update_client(@client, :manager, "Менеджер клиента не может быть пустым")
    end

    it "should not update paper without discount" do
      check_not_update_client(@client, :discount, "Скидка для клиента не может быть пустым")
    end

    it "should not update paper without contract_status" do
      check_not_update_client(@client, :contract_status, "Статус договора не может быть пустым")
    end

    private
    def check_not_update_client(client, param, alert)
      old_client = client

      sign_in @admin
      post :update, @params.except(param)
      expect(assigns(:client)).to eq client

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_client_path(client))

      expect(client.reload.fio).to eq old_client.fio
      expect(client.reload.phone).to eq old_client.phone
      expect(client.reload.email).to eq old_client.email
      expect(client.reload.contract).to eq old_client.contract
      expect(client.reload.description).to eq old_client.description
      expect(client.reload.manager).to eq old_client.manager
      expect(client.reload.contract_status).to eq old_client.contract_status
    end
  end

end
