# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ClientController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @admin = create(:admin)

      @client = create(:client)
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @client
      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show client for admin" do
      sign_in @admin
      get :edit, id: @client
      expect(assigns(:client)).to eq @client
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      get :edit, id: @client
      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show client for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
    end
  end

end
