# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ClientsController, :type => :controller do
  render_views

  describe "invalid search" do
    before(:all) do
      @admin = create(:admin)
      manager = create(:manager)
      discount = Discount.all.sample

      @client1 = create(:client, fio: 'Иванов', phone: '79379903835', email: 'ivanov@gmail.com', contract: 'N2015/01/01', manager: manager, discount: discount)
      @client2 = create(:client, fio: 'Петров', phone: '79379903836', email: 'petrov@mail.ru', contract: 'N2015/01/02', manager: manager, discount: discount)

      @params = {
          search: @client1.fio
      }
    end

    after(:all) do
      Client.delete_all
      User.delete_all
    end

    it "should redirect to new_session if non-auth user" do
      get :index, @params
      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      get :index, @params

      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

  describe "search" do
    before(:each) do
      Client.delete_all
      User.delete_all
      Discount.delete_all

      @admin = create(:admin)
      manager = create(:manager, fio: 'Руслан')
      discount = create(:discount_0)

      @client1 = create(:client,
                        fio: 'Иванов',
                        phone: '79379903835',
                        email: 'ivanov@gmail.com',
                        contract: 'N2015/01/01',
                        description: 'сфера деятельности 1',
                        payer: 'плательщик 1',
                        manager: manager,
                        discount: discount)

      @client2 = create(:client,
                        fio: 'Петров',
                        phone: '79379903836',
                        email: 'petrov@mail.ru',
                        contract: 'N2015/01/02',
                        description: 'сфера деятельности 2',
                        payer: 'плательщик 2',
                        manager: manager,
                        discount: discount)

      @params = {
          search: @client1.fio
      }
    end

    it "should search clients by fio" do
      check(@client1.fio, [1, 1, 1, 1])
    end

    it "should search clients by phone" do
      check(@client1.phone, [1, 1, 1, 2])
    end

    it "should search clients by email" do
      check(@client1.email, [1, 1, 1, 1])
    end

    it "should search clients by contract" do
      check(@client1.contract, [1, 1, 1, 2])
    end

    it "should search clients by description" do
      check(@client1.description, [1, 1, 1, 2])
    end

    it "should search clients by payer" do
      check(@client1.payer, [1, 1, 1, 2])
    end

    it "should search clients by manager fio" do
      check(@client1.manager.fio, [2, 2, 2, 2])
    end

    private
    def check(client_param, clients_count)
      check_clients_found(client_param, clients_count[0])
      check_clients_found(client_param.mb_chars.downcase.to_s, clients_count[1])
      check_clients_found(client_param.mb_chars.upcase.to_s, clients_count[2])
      check_clients_found(client_param[1..-2], clients_count[3])
    end

    def check_clients_found(search, clients_count)
      sign_in @admin
      get :index, {search: search}

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('client.success.found') % clients_count

      expect(response).to have_http_status(:success)
      search_ = "%#{search}%"
      by_params_ = Client.where("fio ilike ? or phone ilike ? or email ilike ? or contract ilike ? or description ilike ? or payer ilike ?", search_, search_, search_, search_, search_, search_).order('created_at DESC')
      by_manager_ = Client.where(manager: User.managers.where("fio ilike ?", search_))

      clients_ = [by_params_, by_manager_].flatten.uniq.sort { |c1, c2| c1.created_at <=> c2.created_at }

      expect(assigns(:clients)).to eq clients_
    end

  end

end
