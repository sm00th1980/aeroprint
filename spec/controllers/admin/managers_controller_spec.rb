# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::ManagersController, :type => :controller do
  render_views

  describe "GET index for admin" do
    before(:each) do
      @admin = FactoryGirl.create(:admin)
      @user = FactoryGirl.create(:user)
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:managers)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show index for signed user" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:managers)).to eq User.managers.sort
    end

    it "should redirect to root page for signed user" do
      sign_in @user
      get :index
      expect(assigns(:managers)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
