# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CoverWorkTypeController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @admin = create(:admin)

      @cover_work_type = create(:cover_work_type)
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @cover_work_type
      expect(assigns(:material)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show cover_work_type for admin" do
      sign_in @admin
      get :edit, id: @cover_work_type
      expect(assigns(:cover_work_type)).to eq @cover_work_type
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      get :edit, id: @cover_work_type
      expect(assigns(:cover_work_type)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not show cover_work_type for not exist id" do
      sign_in @admin

      get :edit, id: -1

      expect(assigns(:cover_work_type)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(cover_work_types_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('cover_work_type.failure.not_found')
    end
  end

end
