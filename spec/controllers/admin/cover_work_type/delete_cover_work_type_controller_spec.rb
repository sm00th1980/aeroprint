# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CoverWorkTypeController, :type => :controller do
  render_views

  describe "DELETE destroy for admin" do
    before(:each) do
      @admin = create(:admin)

      @cover_work_type = create(:cover_work_type)
    end

    it "should destroy cover_work_type" do
      sign_in @admin

      assert_difference 'CoverWorkType.count', -1 do
        delete :destroy, id: @cover_work_type

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(cover_work_types_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('cover_work_type.success.deleted')

        expect(CoverWorkType.exists?(id: @cover_work_type)).to be false
      end
    end

    it "should not destroy new cover_work_type for non exists id" do
      sign_in @admin

      assert_difference 'CoverWorkType.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(cover_work_types_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('cover_work_type.failure.not_found')
      end
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      assert_difference 'CoverWorkType.count', 0 do
        delete :destroy, id: @cover_work_type
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
