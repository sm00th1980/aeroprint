# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CoverWorkTypeController, :type => :controller do
  render_views


  describe "create for admin" do
    before(:each) do
      @admin = create(:admin)

      @params = {
          name: 'новый тип переплётных работы',
          count_of_copies_per_hour: rand(10_000),
          material_price: rand(10)/1000.0,
          owner: CoverWorkOwner.all.sample.id,
          currency: CoverWorkCurrency.all.sample.id,
          operation: CoverWorkOperation.all.sample.id
      }
    end

    it "should create new cover_work_type" do
      sign_in @admin

      assert_difference 'CoverWorkType.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(cover_work_types_path)

        new_cover_work_type = CoverWorkType.last
        expect(new_cover_work_type.name).to eq @params[:name]
        expect(new_cover_work_type.count_of_copies_per_hour).to eq @params[:count_of_copies_per_hour]
        expect(new_cover_work_type.source_material_price.to_f).to eq @params[:material_price]
        expect(new_cover_work_type.owner.id).to eq @params[:owner]
        expect(new_cover_work_type.currency.id).to eq @params[:currency]
        expect(new_cover_work_type.operation.id).to eq @params[:operation]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('cover_work_type.success.created')
      end
    end

    it "should not create new cover_work_type for user" do
      sign_in create(:user)

      assert_difference 'CoverWorkType.count', 0 do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(root_path)
      end
    end

    it "should not create new cover work type without name" do
      params = @params.except(:name)
      check_not_create_cover_work_type(params, "Наименование переплётной работы не может быть пустым")
    end

    it "should not create new cover work type without count_of_copies_per_hour" do
      params = @params.except(:count_of_copies_per_hour)
      check_not_create_cover_work_type(params, "Кол-во экземпляров в час не может быть пустым")
    end

    it "should not create new cover work type without material_price" do
      params = @params.except(:material_price)
      check_not_create_cover_work_type(params, "Стоимость материалов переплётной работы не может быть пустым")
    end

    it "should not create new cover work type without owner" do
      params = @params.except(:owner)
      check_not_create_cover_work_type(params, "Тип переплётной работы не может быть пустым")
    end

    it "should not create new cover work type without currency" do
      params = @params.except(:currency)
      check_not_create_cover_work_type(params, "Валюта переплётной работы не может быть пустым")
    end

    it "should not create new cover work type without operation" do
      params = @params.except(:operation)
      check_not_create_cover_work_type(params, "Тип операции переплётной работы не может быть пустым")
    end

    private
    def check_not_create_cover_work_type(params, error, user=@admin)
      sign_in user

      assert_difference 'CoverWorkType.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_cover_work_type_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
