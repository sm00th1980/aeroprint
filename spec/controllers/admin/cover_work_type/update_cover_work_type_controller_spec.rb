# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::CoverWorkTypeController, :type => :controller do
  render_views

  describe "POST update for admin" do
    before(:each) do
      @admin = create(:admin)
      @cover_work_type = create(:cover_work_type)

      @params = {
          id: @cover_work_type,
          name: 'new cover work',
          count_of_copies_per_hour: rand(10_000),
          material_price: rand(10)/1000.0,
          owner: CoverWorkOwner.all.sample.id,
          currency: CoverWorkCurrency.all.sample.id,
          operation: CoverWorkOperation.all.sample.id
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:cover_work_type)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update cover_work_type for admin" do
      sign_in @admin
      post :update, @params
      expect(assigns(:cover_work_type)).to eq @cover_work_type

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('cover_work_type.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(cover_work_types_path)

      expect(@cover_work_type.reload.name).to eq @params[:name]
      expect(@cover_work_type.reload.source_material_price.to_f).to eq @params[:material_price]
      expect(@cover_work_type.reload.count_of_copies_per_hour).to eq @params[:count_of_copies_per_hour]
      expect(@cover_work_type.reload.owner.id).to eq @params[:owner]
      expect(@cover_work_type.reload.currency.id).to eq @params[:currency]
      expect(@cover_work_type.reload.operation.id).to eq @params[:operation]
    end

    it "should redirect to root page for signed user" do
      sign_in create(:user)
      post :update, @params
      expect(assigns(:cover_work_type)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end

    it "should not update for not exist cover_work_type id" do
      sign_in @admin

      post :update, @params.merge({id: -1})

      expect(assigns(:cover_work_type)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(cover_work_types_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('cover_work_type.failure.not_found')
    end

    it "should not update cover_work_type without name" do
      old_cover_work_type = @cover_work_type

      sign_in @admin
      post :update, @params.except(:name)
      expect(assigns(:cover_work_type)).to eq @cover_work_type

      expect(flash[:alert]).to eq ["Наименование переплётной работы не может быть пустым"]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_cover_work_type_path(@cover_work_type))

      expect(@cover_work_type.reload.name).to eq old_cover_work_type.name
      expect(@cover_work_type.reload.source_material_price).to eq old_cover_work_type.source_material_price
      expect(@cover_work_type.reload.owner).to eq old_cover_work_type.owner
    end

    it "should not update cover_work_type without material_price" do
      old_cover_work_type = @cover_work_type

      sign_in @admin
      post :update, @params.except(:material_price)
      expect(assigns(:cover_work_type)).to eq @cover_work_type

      expect(flash[:alert]).to eq ["Стоимость материалов переплётной работы не может быть пустым"]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_cover_work_type_path(@cover_work_type))

      expect(@cover_work_type.reload.name).to eq old_cover_work_type.name
      expect(@cover_work_type.reload.source_material_price).to eq old_cover_work_type.source_material_price
      expect(@cover_work_type.reload.owner).to eq old_cover_work_type.owner
    end

    it "should not update cover_work_type without owner" do
      old_cover_work_type = @cover_work_type

      sign_in @admin
      post :update, @params.except(:owner)
      expect(assigns(:cover_work_type)).to eq @cover_work_type

      expect(flash[:alert]).to eq ["Тип переплётной работы не может быть пустым"]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(edit_cover_work_type_path(@cover_work_type))

      expect(@cover_work_type.reload.name).to eq old_cover_work_type.name
      expect(@cover_work_type.reload.source_material_price).to eq old_cover_work_type.source_material_price
      expect(@cover_work_type.reload.owner).to eq old_cover_work_type.owner
    end

  end

end
