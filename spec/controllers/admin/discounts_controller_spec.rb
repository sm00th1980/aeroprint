# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::DiscountsController, :type => :controller do
  render_views

  describe "GET index for admin" do
    before(:each) do
      @admin = create(:admin)
      @manager = create(:manager)

      Discount.delete_all
      create(:discount_0)
      create(:discount, name: 'Скидка 10%', value: 10)
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:discounts)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show discount list for signed user" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:discounts)).to eq Discount.all.order(:value)
    end

    it "should redirect to root page for signed manager" do
      sign_in @manager
      get :index
      expect(assigns(:discounts)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
