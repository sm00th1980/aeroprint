# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Admin::MachinesController, :type => :controller do
  render_views

  describe "GET index for admin" do
    before(:each) do
      @admin = create(:admin)
      @manager = create(:manager)
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show machine list for signed user" do
      sign_in @admin
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:machines)).to eq Machine.all.order(:name)
    end

    it "should redirect to root page for signed manager" do
      sign_in @manager
      get :index
      expect(assigns(:machines)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
