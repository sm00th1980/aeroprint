# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::MultipleController, :type => :controller do
  render_views

  before(:each) do
    @manager = User.managers.sample
    @calculation = create(:calculation, client: create(:client, manager: @manager))
  end

  describe "GET new for manager" do
    it "should redirect to new_session if non-auth user" do
      get :new, id: @calculation
      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show new multiple form for manager" do
      sign_in @manager
      get :new, id: @calculation
      expect(assigns(:calculation)).to eq @calculation
      expect(response).to have_http_status(:success)
    end

    it "should not show multiple form for not his own calculation" do
      sign_in create(:manager)
      get :new, id: @calculation
      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_calculations_path)
      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('calculation.failure.not_found')
    end
  end

end
