# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::MultipleController, :type => :controller do
  render_views

  describe "create multiple" do
    before(:each) do
      @manager = User.managers.sample
      @calculation = create(:calculation, client: create(:client, manager: @manager), product: create(:list_product))
      currency = create(:currency)

      5.times do
        paper = create(:paper)
        paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency)

        create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)
        create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 264, chunk_count: 3, extended: true)
        create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)
        create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 280, chunk_count: 6, extended: true)

        create(:component, calculation: @calculation, density: 90, paper: paper, product_gor_mm: 210, product_ver_mm: 297)
      end

      @params = {
          id: @calculation,
          print_runs: {
              print_run_1: rand(1000),
              print_run_2: rand(1000),
              print_run_3: rand(1000),
              print_run_4: rand(1000),
              print_run_5: rand(1000)
          }
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :create, @params
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
      expect(assigns(:calculation)).to eq nil
    end

    it "should create multiple for manager" do
      components_count_ = @calculation.components.count

      assert_difference 'Calculation.count', @params[:print_runs].count do
        sign_in @manager
        post :create, @params
        expect(response).to have_http_status(:success)

        expect(assigns(:calculation)).to eq @calculation
        expect(assigns(:calculations).count).to eq @params[:print_runs].count

        expect(@calculation.reload.components.count).to eq components_count_

        check_excel_download
      end
    end

    private
    def check_excel_download
      expect(response.headers["Content-Disposition"]).to eq "attachment; filename=\"report_#{Date.today.strftime('%Y-%m-%d')}.xls\""
      expect(response.headers["Content-Type"]).to eq "text/xml"

      #p response.body
    end

  end

end
