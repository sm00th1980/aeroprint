# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::BranchesController, :type => :controller do
  render_views

  describe "branch list" do
    before(:each) do
      @manager = create(:manager)

      @client = create(:client, manager: @manager)

      5.times { create(:branch, client: @client) }
    end

    it "should redirect to new_session if non-auth user" do
      get :index, id: @client
      expect(assigns(:branches)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show branch list for signed manager" do
      sign_in @manager
      get :index, id: @client
      expect(response).to have_http_status(:success)
      expect(assigns(:branches)).to eq @client.branches
    end

    it "should not show branch list for not his own client" do
      sign_in create(:manager)
      get :index, id: @client
      expect(assigns(:branches)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)
    end
  end

end
