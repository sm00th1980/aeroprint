# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  describe "create calculation" do
    before(:each) do
      paper = create(:paper)
      paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: create(:currency))
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)

      @client = create(:client)
      @product = create(:list_product, slashed: true)

      cmyk_color_1 = CmykColor.find_by(value: 1)
      pantone_color_1 = PantoneColor.find_by(value: 1)

      @params = {
          client: @client.id,
          product: @product.id,
          print_run: rand(10_000),
          use_extended_cuts: true,
          format: :json,
          components: {
              :"0" => {
                  paper: {
                      id: paper.id,
                      density: 90
                  },
                  format: {
                      a2: true,
                      repeat_count: 4
                  },
                  stamp: {
                      exists: true,
                      price: 0
                  },
                  cmyk: {
                      front: cmyk_color_1.id,
                      back: cmyk_color_1.id
                  },
                  pantone: {
                      front: pantone_color_1.id,
                      back: pantone_color_1.id
                  },
                  polish: {
                      front: Polish.all.sample.id,
                      back: Polish.all.sample.id
                  }
              }
          }
      }
    end

    it "should not yet create calculation for slashed product" do
      assert_difference 'Calculation.count', 0 do
        sign_in @client.manager
        post :create, @params

        expect(JSON.parse(response.body)["success"]).to eq false
        expect(JSON.parse(response.body)["error"]).to eq "Расчёт для изделия с вырубкой пока не готов"

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to be_nil

        expect(response).to have_http_status(:success)
      end
    end

  end

end
