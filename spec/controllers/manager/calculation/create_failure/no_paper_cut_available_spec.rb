# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  describe "create calculation" do
    before(:each) do
      paper = create(:paper)

      @client = create(:client)
      @product = create(:list_product)

      cmyk_color_1 = CmykColor.find_by(value: 1)
      pantone_color_1 = PantoneColor.find_by(value: 1)

      @params = {
          client: @client.id,
          product: @product.id,
          print_run: rand(10_000),
          use_extended_cuts: true,
          binding_type: BindingType.all.sample.id,
          format: :json,
          components: {
              :"0" => {
                  id: 0,
                  name: Faker::Commerce.product_name,

                  paper: paper.id,
                  density: 90,

                  front_cmyk_color: cmyk_color_1.id,
                  front_pantone_color: pantone_color_1.id,
                  front_polish: Polish.all.sample.id,

                  back_cmyk_color: cmyk_color_1.id,
                  back_pantone_color: pantone_color_1.id,
                  back_polish: Polish.all.sample.id,

                  product_gor_mm: rand(250),
                  product_ver_mm: rand(250),
                  stripes_count: rand(10)
              }
          }
      }
    end

    it "should not create calculation if no paper cut available" do
      assert_difference 'Calculation.count' do
        sign_in @client.manager
        post :create, @params

        expect(JSON.parse(response.body)["success"]).to eq false
        expect(JSON.parse(response.body)["error"]).to eq I18n.t('calculation.failure.no_paper_cut_available')

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to be_nil

        expect(response).to have_http_status(:success)

        expect(Calculation.last.success?).to be false
      end
    end

  end

end
