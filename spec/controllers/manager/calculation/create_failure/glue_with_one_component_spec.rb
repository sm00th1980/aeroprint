# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  describe "create calculation" do
    before(:each) do
      paper = create(:paper)
      paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: create(:currency))
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)

      @client = create(:client)
      @product = create(:many_page_product)

      cmyk_color_1 = CmykColor.find_by(value: 1)
      pantone_color_1 = PantoneColor.find_by(value: 1)

      @params = {
          client: @client.id,
          product: @product.id,
          print_run: rand(10_000),
          use_extended_cuts: true,
          binding_type: BindingType.glue.id,
          format: :json,
          components: {
              :"0" => {
                  id: 0,
                  name: Faker::Commerce.product_name,

                  paper: paper.id,
                  density: 90,

                  front_cmyk_color: cmyk_color_1.id,
                  front_pantone_color: pantone_color_1.id,
                  front_polish: Polish.all.sample.id,

                  back_cmyk_color: cmyk_color_1.id,
                  back_pantone_color: pantone_color_1.id,
                  back_polish: Polish.all.sample.id,

                  product_gor_mm: rand(250),
                  product_ver_mm: rand(250),
                  stripes_count: rand(10)
              }
          },
          cover_works: {
              :"0" => {type: CoverWorkType.all.first.id, price: rand(100)},
              :"1" => {type: CoverWorkType.all.last.id, price: rand(100)}
          }
      }
    end

    it "should not create calculation for many page product and only one component" do
      assert_difference 'Calculation.count', 0 do
        sign_in @client.manager
        post :create, @params

        expect(JSON.parse(response.body)["success"]).to eq false
        expect(JSON.parse(response.body)["error"]).to eq "Нельзя создать многостраничное изделие на клею, состоящее только из одного компонента"

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to be_nil

        expect(response).to have_http_status(:success)
      end
    end

  end

end
