# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  describe "save calculation" do
    before(:each) do
      @manager = create(:manager)
      @calculation = create(:calculation, saved: false, client: create(:client, manager: @manager))

      @params = {
          id: @calculation,
          name: Faker::Lorem.paragraph
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :save, @params
      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should save calculation for manager" do
      expect(@calculation.reload.saved).to eq false

      sign_in @manager
      post :save, @params
      expect(assigns(:calculation)).to eq @calculation

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('calculation.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_calculations_path)

      expect(@calculation.reload.saved).to eq true
      expect(@calculation.reload.name).to eq @params[:name]
    end

    it "should not save for not exist calculation" do
      expect(@calculation.reload.saved).to eq false

      sign_in @manager
      post :save, @params.merge({id: -1})

      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_calculations_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('calculation.failure.not_found')

      expect(@calculation.reload.saved).to eq false
    end

    it "should not save for not own calculation" do
      calculation2 = create(:calculation, saved: false, client: create(:client, manager: create(:manager)))

      expect(calculation2.reload.saved).to eq false

      sign_in @manager
      post :save, @params.merge({id: calculation2})

      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_calculations_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('calculation.failure.not_found')

      expect(calculation2.reload.saved).to eq false
    end

  end

end
