# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @manager = create(:manager)
      @calculation = create(:calculation, client: create(:client, manager: @manager))
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @calculation
      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show calculation for manager" do
      sign_in @manager
      get :edit, id: @calculation
      expect(assigns(:calculation)).to eq @calculation
      expect(response).to have_http_status(:success)
    end

    it "should not show calculation for not exist id" do
      sign_in @manager
      get :edit, id: -1

      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_calculations_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('calculation.failure.not_found')
    end

    it "should not show calculation for not own calculation" do
      manager2 = create(:manager)
      calculation2 = create(:calculation, client: create(:client, manager: manager2))

      sign_in @manager
      get :edit, id: calculation2

      expect(assigns(:calculation)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_calculations_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('calculation.failure.not_found')
    end
  end

end
