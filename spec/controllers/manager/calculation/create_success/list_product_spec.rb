# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  describe "create calculation" do
    before(:each) do
      paper = create(:paper)
      paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: create(:currency))
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)

      @client = create(:client)
      @product = create(:list_product)

      cmyk_color_1 = CmykColor.find_by(value: 1)
      pantone_color_1 = PantoneColor.find_by(value: 1)

      @params = {
          client: @client.id,
          product: @product.id,
          print_run: rand(10_000),
          use_extended_cuts: true,
          binding_type: BindingType.all.sample.id,
          format: :json,
          components: {
              :"0" => {
                  id: 0,
                  name: Faker::Commerce.product_name,

                  paper: paper.id,
                  density: 90,

                  front_cmyk_color: cmyk_color_1.id,
                  front_pantone_color: pantone_color_1.id,
                  front_polish: Polish.all.sample.id,

                  back_cmyk_color: cmyk_color_1.id,
                  back_pantone_color: pantone_color_1.id,
                  back_polish: Polish.all.sample.id,

                  product_gor_mm: rand(250),
                  product_ver_mm: rand(250)
              }
          },
          cover_works: {
              :"0" => {type: CoverWorkType.all.first.id},
              :"1" => {type: CoverWorkType.all.last.id}
          }
      }
    end

    it "should create calculation for manager with 1 component" do
      sign_in @client.manager
      post :create, @params

      expect(JSON.parse(response.body)["success"]).to eq true
      expect(JSON.parse(response.body)["url"]).to eq manager_edit_calculation_path(assigns(:calculation).id)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:success)

      new_calculation = assigns(:calculation)
      expect(new_calculation.new_record?).to eq false
      expect(new_calculation.client).to eq Client.find_by(id: @params[:client])
      expect(new_calculation.product).to eq Product.find_by(id: @params[:product])
      expect(new_calculation.print_run).to eq @params[:print_run]
      expect(new_calculation.client).to eq Client.find_by(id: @params[:client])
      expect(new_calculation.saved).to eq false
      expect(new_calculation.use_extended_cuts).to eq @params[:use_extended_cuts]
      expect(new_calculation.binding_type).to eq BindingType.find_by(id: @params[:binding_type])

      expect(new_calculation.cover_works.count).to eq @params[:cover_works].count
      new_calculation.cover_works.sort.each_index do |index|
        expect(new_calculation.cover_works[index].type.id).to eq @params[:cover_works].values[index][:type]
        expect(new_calculation.cover_works[index].price).to eq CoverWorkType.find_by(id: @params[:cover_works].values[index][:type]).price
      end

      new_component = new_calculation.components.first

      expect(new_calculation.components.count).to eq 1

      expect(new_component.new_record?).to eq false

      component_param = @params[:components].values[0]

      expect(new_component.name).to eq component_param[:name]
      expect(new_component.paper).to eq Paper.find_by(id: component_param[:paper])
      expect(new_component.density).to eq component_param[:density]
      expect(new_component.product_gor_mm).to eq component_param[:product_gor_mm]
      expect(new_component.product_ver_mm).to eq component_param[:product_ver_mm]
      expect(new_component.stripes_count).to eq component_param[:stripes_count]

      expect(new_component.front_cmyk_color).to eq CmykColor.find_by(id: component_param[:front_cmyk_color])
      expect(new_component.front_pantone_color).to eq PantoneColor.find_by(id: component_param[:front_pantone_color])
      expect(new_component.front_polish).to eq Polish.find_by(id: component_param[:front_polish])

      expect(new_component.back_cmyk_color).to eq CmykColor.find_by(id: component_param[:back_cmyk_color])
      expect(new_component.back_pantone_color).to eq PantoneColor.find_by(id: component_param[:back_pantone_color])
      expect(new_component.back_polish).to eq Polish.find_by(id: component_param[:back_polish])

      expect(assigns(:components)).to eq new_calculation.components
    end

  end

end
