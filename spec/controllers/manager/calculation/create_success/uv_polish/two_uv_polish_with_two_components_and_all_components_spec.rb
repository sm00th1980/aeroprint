# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  describe "create calculation" do
    before(:each) do
      paper = create(:paper)
      paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: create(:currency))
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)

      @client = create(:client)
      @product = create(:list_product)

      cmyk_color_1 = CmykColor.find_by(value: 1)
      pantone_color_1 = PantoneColor.find_by(value: 1)

      @params = {
          client: @client.id,
          product: @product.id,
          print_run: rand(10_000),
          use_extended_cuts: true,
          binding_type: BindingType.all.sample.id,
          format: :json,
          components: {
              :"0" => {
                  id: 0,
                  name: Faker::Commerce.product_name,

                  paper: paper.id,
                  density: 90,

                  front_cmyk_color: cmyk_color_1.id,
                  front_pantone_color: pantone_color_1.id,
                  front_polish: Polish.all.sample.id,

                  back_cmyk_color: cmyk_color_1.id,
                  back_pantone_color: pantone_color_1.id,
                  back_polish: Polish.all.sample.id,

                  product_gor_mm: rand(250),
                  product_ver_mm: rand(250)
              },
              :"1" => {
                  id: 1,
                  name: Faker::Commerce.product_name,

                  paper: paper.id,
                  density: 90,

                  front_cmyk_color: cmyk_color_1.id,
                  front_pantone_color: pantone_color_1.id,
                  front_polish: Polish.all.sample.id,

                  back_cmyk_color: cmyk_color_1.id,
                  back_pantone_color: pantone_color_1.id,
                  back_polish: Polish.all.sample.id,

                  product_gor_mm: rand(250),
                  product_ver_mm: rand(250)
              }
          },
          uv_polishes: {
              :"0" => {
                  type: UvPolishType.all.sample.id,
                  count: UvPolishCount.all.sample.id,
                  component: 0
              },
              :"1" => {
                  type: UvPolishType.all.sample.id,
                  count: UvPolishCount.all.sample.id,
                  component: -1
              }
          }
      }
    end

    it "should create calculation for manager with 1 component" do
      sign_in @client.manager
      post :create, @params

      expect(JSON.parse(response.body)["success"]).to eq true
      expect(JSON.parse(response.body)["url"]).to eq manager_edit_calculation_path(assigns(:calculation).id)

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:success)

      check_calculation(assigns(:calculation))
      check_components(assigns(:calculation), assigns(:components))
      check_uv_polishes(assigns(:calculation))
    end

    private
    def check_calculation(calculation)
      expect(calculation.new_record?).to eq false
      expect(calculation.client).to eq Client.find_by(id: @params[:client])
      expect(calculation.product).to eq Product.find_by(id: @params[:product])
      expect(calculation.print_run).to eq @params[:print_run]
      expect(calculation.client).to eq Client.find_by(id: @params[:client])
      expect(calculation.saved).to eq false
      expect(calculation.use_extended_cuts).to eq @params[:use_extended_cuts]
      expect(calculation.binding_type).to eq BindingType.find_by(id: @params[:binding_type])
      expect(calculation.cover_works.count).to eq 0
    end

    def check_uv_polishes(calculation)
      uv_polishes = CalculationUvPolish.where(calculation: calculation).sort_by { |p| p.id }

      expect(uv_polishes[0].type.id).to eq @params[:uv_polishes][:"0"][:type]
      expect(uv_polishes[0].count.id).to eq @params[:uv_polishes][:"0"][:count]
      expect(uv_polishes[0].components).to eq [calculation.components.sort_by { |c| c.id }.first.id]

      expect(uv_polishes[1].type.id).to eq @params[:uv_polishes][:"1"][:type]
      expect(uv_polishes[1].count.id).to eq @params[:uv_polishes][:"1"][:count]
      expect(uv_polishes[1].components.sort).to eq calculation.components.map { |c| c.id }.sort
    end

    def check_components(calculation, components)
      component_0 = calculation.components.sort_by { |c| c.id }.first
      component_1 = calculation.components.sort_by { |c| c.id }.last

      expect(calculation.components.count).to eq 2

      check_component(component_0, @params[:components].values[0])
      check_component(component_1, @params[:components].values[1])

      expect(components).to eq calculation.components
    end

    def check_component(component, params)
      expect(component.new_record?).to eq false
      expect(component.name).to eq params[:name]
      expect(component.paper).to eq Paper.find_by(id: params[:paper])
      expect(component.density).to eq params[:density]
      expect(component.product_gor_mm).to eq params[:product_gor_mm]
      expect(component.product_ver_mm).to eq params[:product_ver_mm]
      expect(component.stripes_count).to eq params[:stripes_count]
      expect(component.front_cmyk_color).to eq CmykColor.find_by(id: params[:front_cmyk_color])
      expect(component.front_pantone_color).to eq PantoneColor.find_by(id: params[:front_pantone_color])
      expect(component.front_polish).to eq Polish.find_by(id: params[:front_polish])
      expect(component.back_cmyk_color).to eq CmykColor.find_by(id: params[:back_cmyk_color])
      expect(component.back_pantone_color).to eq PantoneColor.find_by(id: params[:back_pantone_color])
      expect(component.back_polish).to eq Polish.find_by(id: params[:back_polish])
      expect(component.frontend_component_id).to eq params[:id].to_i
    end

  end

end
