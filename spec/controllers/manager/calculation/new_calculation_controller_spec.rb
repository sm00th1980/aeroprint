# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationController, :type => :controller do
  render_views

  before(:each) do
    @manager = User.managers.sample
    create(:client, manager: @manager)
  end

  describe "GET new for user" do
    it "should redirect to new_session if non-auth user" do
      get :new
      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show new for manager" do
      sign_in @manager
      get :new
      expect(assigns(:clients)).to eq @manager.clients.order(:fio)
      expect(response).to have_http_status(:success)
    end

    it "should redirect to root path for adminr" do
      sign_in User.admin
      get :new
      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
