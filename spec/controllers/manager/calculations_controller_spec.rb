# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::CalculationsController, :type => :controller do
  render_views

  describe "GET index for user" do
    before(:each) do
      @manager = User.managers.sample

      @calculations = [create(:calculation, saved: true, client: create(:client, manager: @manager), product: create(:product))]
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:calculations)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show index for signed user" do
      sign_in @manager
      get :index
      expect(assigns(:calculations)).to eq Calculation.where(client: @manager.clients, saved: true)
      expect(assigns(:calculations)).to be_a Draper::CollectionDecorator
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET index for admin" do
    it "should rederect to root for admin" do
      sign_in User.admin
      get :index
      expect(assigns(:calculations)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(root_path)
    end
  end

end
