# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ApiController, :type => :controller do
  render_views

  describe "GET densities" do
    before(:each) do
      @manager = User.managers.sample

      @paper = create(:paper)

      create(:paper_feature, paper: @paper, density: 70)
      create(:paper_feature, paper: @paper, density: 80)
      create(:paper_feature, paper: @paper, density: 90)
    end

    it "should redirect to new_session if non-auth user" do
      get :density, id: @paper
      expect(assigns(:paper)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show densities" do
      sign_in @manager
      get :density, id: @paper
      expect(assigns(:paper)).to eq @paper
      expect(response).to have_http_status(:success)
      expect(from_json(response.body)["success"]).to be true
      expect(from_json(response.body)["data"]).to eq PaperFeature.densities(@paper)
    end

    it "should show error for not exists density" do
      sign_in @manager
      get :density, id: -1
      expect(assigns(:paper)).to be_nil
      expect(response).to have_http_status(:success)

      expect(from_json(response.body)["success"]).to be false
      expect(from_json(response.body)["error"]).to eq I18n.t('api.failure.paper_not_found')
    end

    private
    def from_json(response)
      JSON.load(response)
    end
  end

end
