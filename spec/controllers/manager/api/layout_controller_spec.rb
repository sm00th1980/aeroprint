# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ApiController, :type => :controller do
  render_views

  describe "GET layout" do
    before(:each) do
      @manager = User.managers.sample
      create(:paper_feature, density: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1000, currency: create(:currency))

      @params = {
          width: 50
      }
    end

    it "should redirect to new_session if non-auth user" do
      get :layout, @params
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show layout" do
      sign_in @manager
      get :layout, @params
      expect(response).to have_http_status(:success)

      layout = Layout.new(@params[:width])

      expect(layout.width).to eq @params[:width]
      expect(from_json(response.body)["success"]).to be true
      expect(from_json(response.body)["image_data"]).to eq layout.source
      expect(from_json(response.body)["full_size"]).to eq layout.full_size
      expect(from_json(response.body)["offsets"]).to eq layout.offsets
    end

    private
    def from_json(response)
      JSON.load(response)
    end
  end

end
