# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::TransferedBranchController, :type => :controller do
  render_views

  describe "update transfered branch" do
    before(:each) do
      @manager = create(:manager_with_substitute)
      @branch = create(:branch, client: create(:client, manager: @manager))

      @params = {
          id: @branch,
          name: 'новое имя для подразделения',
          phone: '79379903835',
          email: 'sm00th1980@mail.ru',
          contract: '123',
          payer: 'Плательщик',
          description: 'новый клиент - описание',
          discount: Discount.all.sample.id,
          shipping_address: 'новый адрес отгрузки'
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:branch)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update branch for manager" do
      sign_in @manager.substitute
      post :update, @params
      expect(assigns(:branch)).to eq @branch

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('branch.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_transfered_branches_path(@branch.client))

      expect(@branch.reload.name).to eq @params[:name]
      expect(@branch.reload.phone).to eq @params[:phone]
      expect(@branch.reload.email).to eq @params[:email]
      expect(@branch.reload.contract).to eq @params[:contract]
      expect(@branch.reload.payer).to eq @params[:payer]
      expect(@branch.reload.description).to eq @params[:description]
      expect(@branch.reload.discount.id).to eq @params[:discount]
      expect(@branch.reload.shipping_address).to eq @params[:shipping_address]
      expect(@branch.reload.client).to eq @branch.client
    end

    it "should not update branch for not exist branch id" do
      sign_in @manager.substitute

      post :update, @params.merge(id: -1)

      expect(assigns(:branch)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_transfered_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('branch.failure.not_found')
    end

    it "should not update branch for not his own branch" do
      sign_in @manager.substitute

      post :update, @params.merge(id: create(:branch))

      expect(assigns(:branch)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_transfered_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('branch.failure.not_found')
    end

    it "should not update branch without name" do
      check_not_update_branch(@branch, :name, "Наименование подразделения не может быть пустым")
    end

    it "should not update branch without discount" do
      check_not_update_branch(@branch, :discount, "Скидка для подразделения не может быть пустым")
    end

    private
    def check_not_update_branch(branch, param, alert)
      old_branch = branch

      sign_in @manager.substitute
      post :update, @params.except(param)
      expect(assigns(:branch)).to eq branch

      expect(flash[:alert]).to eq [alert]
      expect(flash[:notice]).to be_nil

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_edit_transfered_branch_path(branch))

      expect(branch.reload.name).to eq old_branch.name
      expect(branch.reload.phone).to eq old_branch.phone
      expect(branch.reload.email).to eq old_branch.email
      expect(branch.reload.contract).to eq old_branch.contract
      expect(branch.reload.description).to eq old_branch.description
      expect(branch.reload.payer).to eq old_branch.payer
      expect(branch.reload.discount).to eq old_branch.discount
      expect(branch.reload.shipping_address).to eq old_branch.shipping_address
      expect(branch.reload.client).to eq old_branch.client
    end
  end

end
