# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::BranchController, :type => :controller do
  render_views

  describe "DELETE branch" do
    before(:each) do
      @manager = create(:manager)

      @client = create(:client, manager: @manager)
      @branch = create(:branch, client: @client)
    end

    it "should destroy branch" do
      sign_in @manager

      assert_difference 'Branch.count', -1 do
        delete :destroy, id: @branch

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_branches_path(@client))

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('branch.success.deleted')

        expect(Branch.exists?(id: @branch)).to be false
      end
    end

    it "should not destroy branch for non exists id" do
      sign_in @manager

      assert_difference 'Branch.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('branch.failure.not_found')
      end
    end

    it "should not destroy branch for not his own client and branch" do
      another_branch = create(:branch)

      sign_in @manager

      assert_difference 'Branch.count', 0 do
        delete :destroy, id: another_branch

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('branch.failure.not_found')
      end
    end

  end

end
