# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::BranchController, :type => :controller do
  render_views

  describe "show edit" do
    before(:each) do
      @manager = create(:manager)
      @branch = create(:branch, client: create(:client, manager: @manager))
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @branch
      expect(assigns(:branch)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show branch for manager" do
      sign_in @manager
      get :edit, id: @branch
      expect(assigns(:branch)).to eq @branch
      expect(response).to have_http_status(:success)
    end

    it "should not show branch for not exist id" do
      sign_in @manager

      get :edit, id: -1

      expect(assigns(:branch)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('branch.failure.not_found')
    end

    it "should not show branch for not his own branch" do
      sign_in create(:manager)

      get :edit, id: @branch

      expect(assigns(:branch)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('branch.failure.not_found')
    end
  end

end
