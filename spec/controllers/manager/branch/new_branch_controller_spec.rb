# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::BranchController, :type => :controller do
  render_views

  describe "GET new for admin" do
    before(:each) do
      @manager = create(:manager)
      @client = create(:client, manager: @manager)
    end

    it "should redirect to new_session if non-auth user" do
      get :new, id: @client

      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show new branch form for signed manager" do
      sign_in @manager
      get :new, id: @client

      expect(assigns(:client)).to eq @client
      expect(response).to have_http_status(:success)
    end

    it "should not show new branch for another manager" do
      sign_in create(:manager)
      get :new, id: @client

      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
    end
  end

end
