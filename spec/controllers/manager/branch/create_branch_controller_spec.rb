# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::BranchController, :type => :controller do
  render_views

  describe "create branch" do
    before(:each) do
      @manager = create(:manager)
      @client = create(:client, manager: @manager)

      @params = {
          id: @client,
          name: 'имя нового подразделения',
          phone: '79379903835',
          email: 'sm00th1980@mail.ru',
          contract: '123',
          payer: 'Плательщик',
          description: 'новое подразделение - описание',
          discount: Discount.all.first.id,
          shipping_address: 'новый адрес отгрузки'
      }
    end

    it "should create new branch" do
      sign_in @manager

      assert_difference 'Branch.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_branches_path(@client))

        new_branch = Branch.last
        expect(new_branch.name).to eq @params[:name]
        expect(new_branch.phone).to eq @params[:phone]
        expect(new_branch.email).to eq @params[:email]
        expect(new_branch.contract).to eq @params[:contract]
        expect(new_branch.payer).to eq @params[:payer]
        expect(new_branch.description).to eq @params[:description]
        expect(new_branch.client).to eq @params[:id]
        expect(new_branch.discount.id).to eq @params[:discount]
        expect(new_branch.shipping_address).to eq @params[:shipping_address]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('branch.success.created')
      end
    end

    it "should not create new branch without name" do
      params = @params.except(:name)
      check_not_create_branch(params, "Наименование подразделения не может быть пустым")
    end

    it "should not create new branch without discount" do
      params = @params.except(:discount)
      check_not_create_branch(params, "Скидка для подразделения не может быть пустым")
    end

    it "should not create new branch with not exists discount" do
      params = @params.merge(discount: -1)
      check_not_create_branch(params, "Скидка для подразделения не может быть пустым")
    end

    it "should not create new branch with not exists client" do
      params = @params.merge(id: -1)

      sign_in @manager
      assert_difference 'Branch.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
        expect(flash[:notice]).to be_nil
      end
    end

    it "should not create new branch for not his own client" do
      params = @params.merge(id: create(:client))

      sign_in @manager
      assert_difference 'Branch.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
        expect(flash[:notice]).to be_nil
      end
    end

    private
    def check_not_create_branch(params, error, user=@manager)
      sign_in user

      assert_difference 'Branch.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_new_branch_path(@client))

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
