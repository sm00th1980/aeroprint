# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::VariableController, :type => :controller do
  render_views

  before(:each) do
    @manager = User.managers.sample
    @calculation = create(:calculation, client: create(:client, manager: @manager), product: create(:list_product))

    currency = create(:currency)
    paper = create(:paper)
    paper_feature1 = create(:paper_feature, paper: paper, density: 90, gor_sm: 64, ver_sm: 90, price_per_1_ton: 1802.66, currency: currency)

    1.times do
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 420, chunk_count: 2, extended: false)
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.man, gor_mm: 622, ver_mm: 264, chunk_count: 3, extended: true)
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 432, chunk_count: 4, extended: false)
      create(:paper_cut, paper_feature: paper_feature1, machine: Machine.ryobi, gor_mm: 300, ver_mm: 280, chunk_count: 6, extended: true)

      create(:component, calculation: @calculation, density: 90, paper: paper, product_gor_mm: 210, product_ver_mm: 297)
    end

    @calculation.components.map { |c| c.calculate_all }

    @params = {
        id: @calculation.id,
        form_price: form_price_values.sample,
        price_per_hour: price_per_hour_values.sample,
        prices_per_1_ton: {
            @calculation.components[0].id => (paper_feature1.price_per_1_ton + rand(100) + 1)
        }
    }
  end

  after(:each) do
    ComponentResult.delete_all
  end

  describe "GET show for manager" do
    it "should redirect to new_session if non-auth user" do
      assert_difference 'ComponentResult.count', 0 do
        get :show, @params
        expect(assigns(:calculation)).to be_nil
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(new_user_session_path)
      end
    end

    it "should show variable form for manager" do
      sign_in @manager
      assert_difference 'ComponentResult.count' do
        get :show, @params
        expect(response).to have_http_status(:success)

        expect(assigns(:calculation)).to eq @calculation
        expect(assigns(:form_price)).to eq @params[:form_price]
        expect(assigns(:price_per_hour)).to eq @params[:price_per_hour]
        expect(assigns(:prices_per_1_ton)).to eq @params[:prices_per_1_ton].values

        expect(assigns(:results)).to eq results(@calculation.components, @params[:form_price], @params[:price_per_hour], @params[:prices_per_1_ton].values)

        result = ComponentResult.last
        expect(result.variable_price_per_1_ton).to eq @params[:prices_per_1_ton].values.first
        expect(result.base_price_per_1_ton).to be false
      end
    end

    it "should show variable form for manager if form_price is invalid" do
      sign_in @manager
      assert_difference 'ComponentResult.count' do
        get :show, @params.merge(form_price: -1)
        expect(response).to have_http_status(:success)

        expect(assigns(:calculation)).to eq @calculation
        expect(assigns(:form_price)).to eq form_price_values.first
        expect(assigns(:price_per_hour)).to eq @params[:price_per_hour]
        expect(assigns(:prices_per_1_ton)).to eq @params[:prices_per_1_ton].values

        result = ComponentResult.last
        expect(result.variable_price_per_1_ton).to eq @params[:prices_per_1_ton].values.first
        expect(result.base_price_per_1_ton).to be false
      end
    end

    it "should show variable form for manager if price per hour is invalid" do
      sign_in @manager
      assert_difference 'ComponentResult.count' do
        get :show, @params.merge(price_per_hour: '-1')
        expect(response).to have_http_status(:success)

        expect(assigns(:calculation)).to eq @calculation
        expect(assigns(:form_price)).to eq @params[:form_price]
        expect(assigns(:price_per_hour)).to eq price_per_hour_values.first
        expect(assigns(:prices_per_1_ton)).to eq @params[:prices_per_1_ton].values

        result = ComponentResult.last
        expect(result.variable_price_per_1_ton).to eq @params[:prices_per_1_ton].values.first
        expect(result.base_price_per_1_ton).to be false
      end
    end

    it "should not show variable form for not his own calculation" do
      sign_in create(:manager)
      assert_difference 'ComponentResult.count', 0 do
        get :show, @params
        expect(assigns(:calculation)).to be_nil
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_calculations_path)
        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('calculation.failure.not_found')
      end
    end
  end

  private
  def form_price_values
    Machine::Reference.form_prices.keys.map { |index| index }
  end

  def price_per_hour_values
    Machine::Reference.prices_per_hour.keys.map { |index| index }
  end

  def results(components, form_price_index, price_per_hour_index, price_per_1_ton)
    ComponentResult.where(
        component: components,
        variable_form_price_index: form_price_index,
        variable_price_per_hour_index: price_per_hour_index,
        variable_price_per_1_ton: price_per_1_ton
    )
  end

end
