# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::TransferedSpecificationController, :type => :controller do
  render_views

  describe "update specification" do
    before(:each) do
      @manager = create(:manager_with_substitute)
      @specification = create(:specification, client: create(:client, manager: @manager))

      @params = {
          id: @specification,
          number: 'новый номер спецификации',
          date: Date.today + 1.day,
          document: fixture_file_upload('spec/fixtures/files/file.pdf', 'text/pdf')
      }
    end

    it "should redirect to new_session if non-auth user" do
      post :update, @params
      expect(assigns(:specification)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should update specification for manager substitute" do
      sign_in @manager.substitute
      post :update, @params
      expect(assigns(:specification)).to eq @specification

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to eq I18n.t('specification.success.updated')

      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_transfered_specifications_path(@specification.client))

      expect(@specification.reload.number).to eq @params[:number]
      expect(@specification.reload.date).to eq @params[:date]
      expect(@specification.reload.client).to eq @specification.client
      expect(@specification.reload.document_file_name).to eq @params[:document].original_filename
    end

    it "should not update specification for not exist specification id" do
      sign_in @manager.substitute

      post :update, @params.merge(id: -1)

      expect(assigns(:specification)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_transfered_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('specification.failure.not_found')
    end

    it "should not update specification for not his own specification" do
      sign_in @manager.substitute

      post :update, @params.merge(id: create(:specification))

      expect(assigns(:specification)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_transfered_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('specification.failure.not_found')
    end

    it "should not update new specification with wrong format" do
      sign_in @manager.substitute

      post :update, @params.merge(document: fixture_file_upload('spec/fixtures/files/file.png', 'image/png'))

      expect(assigns(:specification)).to eq @specification
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_edit_transfered_specification_path(@specification))

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq ["Document имеет некорректный формат файла. Поддерживаются только xls, xlsx, doc, docx, pdf.", "Document имеет неверное значение", "Document file name имеет неверное значение"]
    end

  end

end
