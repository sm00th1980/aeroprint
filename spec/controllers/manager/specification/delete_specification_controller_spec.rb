# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::SpecificationController, :type => :controller do
  render_views

  describe "DELETE specification" do
    before(:each) do
      @manager = create(:manager)

      @client = create(:client, manager: @manager)
      @specification = create(:specification, client: @client)
    end

    it "should destroy specification" do
      sign_in @manager

      assert_difference 'Specification.count', -1 do
        delete :destroy, id: @specification

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_specifications_path(@client))

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('specification.success.deleted')

        expect(Specification.exists?(id: @specification)).to be false
      end
    end

    it "should not destroy specification for non exists id" do
      sign_in @manager

      assert_difference 'Specification.count', 0 do
        delete :destroy, id: -1

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('specification.failure.not_found')
      end
    end

    it "should not destroy specification for not his own client and specification" do
      another_specification = create(:specification)

      sign_in @manager

      assert_difference 'Specification.count', 0 do
        delete :destroy, id: another_specification

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('specification.failure.not_found')
      end
    end

  end

end
