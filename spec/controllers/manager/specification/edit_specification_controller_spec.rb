# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::SpecificationController, :type => :controller do
  render_views

  describe "show edit" do
    before(:each) do
      @manager = create(:manager)
      @specification = create(:specification, client: create(:client, manager: @manager))
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @specification
      expect(assigns(:specification)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show specification for manager" do
      sign_in @manager
      get :edit, id: @specification
      expect(assigns(:specification)).to eq @specification
      expect(response).to have_http_status(:success)
    end

    it "should not show specification for not exist id" do
      sign_in @manager

      get :edit, id: -1

      expect(assigns(:specification)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('specification.failure.not_found')
    end

    it "should not show specification for not his own specification" do
      sign_in create(:manager)

      get :edit, id: @specification

      expect(assigns(:specification)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('specification.failure.not_found')
    end
  end

end
