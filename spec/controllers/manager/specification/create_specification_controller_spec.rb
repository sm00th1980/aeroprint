# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::SpecificationController, :type => :controller do
  render_views

  describe "create specification" do
    before(:each) do
      @manager = create(:manager)
      @client = create(:client, manager: @manager)

      @params = {
          id: @client,
          number: 'новый номер для спецификации',
          date: Date.today,
          document: fixture_file_upload('spec/fixtures/files/file.pdf', 'text/pdf')
      }
    end

    it "should create new specification" do
      sign_in @manager

      assert_difference 'Specification.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_specifications_path(@client))

        new_specification = Specification.last
        expect(new_specification.number).to eq @params[:number]
        expect(new_specification.date).to eq @params[:date]
        expect(new_specification.client).to eq @params[:id]
        expect(new_specification.document_file_name).to eq @params[:document].original_filename

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('specification.success.created')
      end
    end

    it "should not create new specification with not exists client" do
      params = @params.merge(id: -1)

      sign_in @manager
      assert_difference 'Specification.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
        expect(flash[:notice]).to be_nil
      end
    end

    it "should not create new specification for not his own client" do
      params = @params.merge(id: create(:client))

      sign_in @manager
      assert_difference 'Specification.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
        expect(flash[:notice]).to be_nil
      end
    end

    it "should not create new specification with wrong format" do
      params = @params.merge(document: fixture_file_upload('spec/fixtures/files/file.png', 'image/png'))
      sign_in @manager

      assert_difference 'Specification.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_new_specification_path(@client))

        expect(flash[:alert]).to eq ["Document имеет некорректный формат файла. Поддерживаются только xls, xlsx, doc, docx, pdf.", "Document имеет неверное значение", "Document file name имеет неверное значение"]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
