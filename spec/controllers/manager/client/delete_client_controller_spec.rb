# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ClientController, :type => :controller do
  render_views

  describe "delete client" do
    before(:each) do
      @manager = create(:manager)

      @client = create(:client, manager: @manager)
    end

    it "should destroy client" do
      sign_in @manager

      assert_difference 'Client.count', -1 do
        delete :destroy, id: @client

        expect(assigns(:client)).to eq @client
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('client.success.deleted')

        expect(Client.exists?(id: @client)).to be false
      end
    end

    it "should not destroy client for non exists id" do
      sign_in @manager

      assert_difference 'Client.count', 0 do
        delete :destroy, id: -1

        expect(assigns(:client)).to be_nil
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
      end
    end

    it "should not destroy not his own client" do
      another_client = create(:client, manager: create(:manager))

      sign_in @manager

      assert_difference 'Client.count', 0 do
        delete :destroy, id: another_client

        expect(assigns(:client)).to be_nil
        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        expect(flash[:notice]).to be_nil
        expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
      end
    end

  end

end
