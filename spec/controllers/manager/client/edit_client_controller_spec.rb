# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ClientController, :type => :controller do
  render_views

  describe "GET edit for admin" do
    before(:each) do
      @manager = create(:manager)
      @client = create(:client, manager: @manager)
    end

    it "should redirect to new_session if non-auth user" do
      get :edit, id: @client
      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show client for manager" do
      sign_in @manager
      get :edit, id: @client
      expect(assigns(:client)).to eq @client
      expect(response).to have_http_status(:success)
    end

    it "should not show client for not exist id" do
      sign_in @manager

      get :edit, id: -1

      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
    end

    it "should not show not his own client" do
      sign_in @manager

      get :edit, id: create(:client)

      expect(assigns(:client)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_clients_path)

      expect(flash[:notice]).to be_nil
      expect(flash[:alert]).to eq I18n.t('client.failure.not_found')
    end
  end

end
