# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ClientController, :type => :controller do
  render_views

  describe "delete client" do
    before(:each) do
      @manager = create(:manager)
      @client = create(:client, manager: @manager)
    end

    it "should drop client" do
      sign_in @manager

      assert_difference 'Client.count', -1 do
        delete :destroy, {id: @client, format: :json}
        expect(assigns(:client)).to eq @client

        expect(JSON.parse(response.body)["success"]).to eq true
        expect(JSON.parse(response.body)["message"]).to eq I18n.t('client.success.deleted')

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to be_nil
        expect(Client.exists?(id: @client)).to be false
      end
    end

  end

end
