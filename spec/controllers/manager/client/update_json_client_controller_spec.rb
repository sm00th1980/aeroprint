# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ClientController, :type => :controller do
  render_views

  describe "update client" do
    before(:each) do
      @manager = create(:manager)
      @client = create(:client, manager: @manager)

      @params = {
          id: @client,
          fio: 'новый клиент - имя',
          phone: '79379903835',
          email: 'sm00th1980@mail.ru',
          contract: '123',
          payer: 'Плательщик',
          description: 'новый клиент - описание',
          discount: Discount.all.sample.id,
          shipping_address: 'новый адрес отгрузки',
          contract_status: ContractStatus.all.sample.id,
          format: :json
      }
    end

    it "should update client" do
      sign_in @manager
      post :update, @params
      expect(assigns(:client)).to eq @client

      expect(JSON.parse(response.body)["success"]).to eq true
      expect(JSON.parse(response.body)["message"]).to eq I18n.t('client.success.updated')

      expect(flash[:alert]).to be_nil
      expect(flash[:notice]).to be_nil

      expect(@client.reload.fio).to eq @params[:fio]
      expect(@client.reload.phone).to eq @params[:phone]
      expect(@client.reload.email).to eq @params[:email]
      expect(@client.reload.contract).to eq @params[:contract]
      expect(@client.reload.payer).to eq @params[:payer]
      expect(@client.reload.description).to eq @params[:description]
      expect(@client.reload.manager).to eq @manager
      expect(@client.reload.discount.id).to eq @params[:discount]
      expect(@client.reload.shipping_address).to eq @params[:shipping_address]
      expect(@client.reload.contract_status.id).to eq @params[:contract_status]
    end

  end

end
