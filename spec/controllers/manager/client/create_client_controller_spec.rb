# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ClientController, :type => :controller do
  render_views

  describe "create client" do
    before(:each) do
      @manager = create(:manager)

      @params = {
          fio: 'новый клиент - имя',
          phone: '79379903835',
          email: 'sm00th1980@mail.ru',
          contract: '123',
          payer: 'Плательщик',
          description: 'новый клиент - описание',
          discount: Discount.all.first.id,
          shipping_address: 'новый адрес отгрузки',
          contract_status: ContractStatus.all.sample.id
      }
    end

    it "should create new client" do
      sign_in @manager

      assert_difference 'Client.count' do
        post :create, @params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_clients_path)

        new_client = Client.last
        expect(new_client.fio).to eq @params[:fio]
        expect(new_client.phone).to eq @params[:phone]
        expect(new_client.email).to eq @params[:email]
        expect(new_client.contract).to eq @params[:contract]
        expect(new_client.payer).to eq @params[:payer]
        expect(new_client.description).to eq @params[:description]
        expect(new_client.manager).to eq @manager
        expect(new_client.discount.id).to eq @params[:discount]
        expect(new_client.shipping_address).to eq @params[:shipping_address]
        expect(new_client.contract_status.id).to eq @params[:contract_status]

        expect(flash[:alert]).to be_nil
        expect(flash[:notice]).to eq I18n.t('client.success.created')
      end
    end

    it "should not create new client without fio" do
      params = @params.except(:fio)
      check_not_create_client(params, "ФИО клиента не может быть пустым")
    end

    it "should not create new client without discount" do
      params = @params.except(:discount)
      check_not_create_client(params, "Скидка для клиента не может быть пустым")
    end

    it "should not create new client with not exists discount" do
      params = @params.merge(discount: -1)
      check_not_create_client(params, "Скидка для клиента не может быть пустым")
    end

    it "should not create new client without contract_status" do
      params = @params.except(:contract_status)
      check_not_create_client(params, "Статус договора не может быть пустым")
    end

    it "should not create new client with not exists contract_status" do
      params = @params.merge(contract_status: -1)
      check_not_create_client(params, "Статус договора не может быть пустым")
    end

    private
    def check_not_create_client(params, error, user=@manager)
      sign_in user

      assert_difference 'Client.count', 0 do
        post :create, params

        expect(response).to have_http_status(:redirect)
        expect(response).to redirect_to(manager_new_client_path)

        expect(flash[:alert]).to eq [error]
        expect(flash[:notice]).to be_nil
      end
    end

  end

end
