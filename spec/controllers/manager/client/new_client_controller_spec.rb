# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ClientController, :type => :controller do
  render_views

  describe "GET new client" do

    it "should redirect to new_session if non-auth user" do
      get :new
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show new client form for signed manager" do
      sign_in create(:manager)
      get :new
      expect(response).to have_http_status(:success)
    end

  end

end
