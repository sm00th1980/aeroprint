# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::ClientsController, :type => :controller do
  render_views

  describe "GET index for admin" do
    before(:each) do
      @manager = create(:manager)

      5.times { create(:client, manager: @manager) }
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show client list for signed manager" do
      sign_in @manager
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:clients)).to eq @manager.clients.order(:fio)
    end

  end

end
