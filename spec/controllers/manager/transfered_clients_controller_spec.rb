# -*- encoding : utf-8 -*-
require 'spec_helper'

describe Manager::TransferedClientsController, :type => :controller do
  render_views

  describe "GET index for admin" do
    before(:each) do
      manager = create(:manager)

      5.times { create(:client, manager: manager) }

      manager.substitute = create(:manager)
      manager.save!

      @substitute_manager = manager.substitute
    end

    it "should redirect to new_session if non-auth user" do
      get :index
      expect(assigns(:transfered_clients)).to be_nil
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end

    it "should show transfered client list for substitute manager" do
      sign_in @substitute_manager
      get :index
      expect(response).to have_http_status(:success)
      expect(assigns(:transfered_clients)).to eq @substitute_manager.transfered_clients.order(:fio)
    end

  end

end
