# -*- encoding : utf-8 -*-
require 'spec_helper'

describe RootController, :type => :controller do
  render_views

  #unsigned
  describe "GET index for unsigned" do
    it "should redirect to new_session if non-auth user" do
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  #manager
  describe "GET index for user" do
    it "should redirect to calculations for user" do
      sign_in User.managers.sample
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(manager_calculations_path)
    end
  end

  #admin
  describe "GET index for admin" do
    it "should redirect to managers for admin" do
      sign_in User.admin
      get :index
      expect(response).to have_http_status(:redirect)
      expect(response).to redirect_to(managers_path)
    end
  end

end
