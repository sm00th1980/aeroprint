# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/calculations", :type => :routing do

  it "routing get /manager/calculations to manager/calculations#index" do
    expect(get: "/manager/calculations").to route_to(controller: "manager/calculations", action: "index")
  end

end
