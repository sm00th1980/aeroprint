# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/specification", :type => :routing do

  #new
  it "routing get /manager/client/31/specification/new to manager/specification#new" do
    expect(get: "/manager/client/31/specification/new").to route_to(controller: "manager/specification", action: "new", id: "31")
  end

  #create
  it "routing post /manager/client/31/specification to manager/specification#create" do
    expect(post: "/manager/client/31/specification").to route_to(controller: "manager/specification", action: "create", id: "31")
  end

  #edit
  it "routing get /manager/specification/:id to manager/specification#edit" do
    expect(get: "/manager/specification/31").to route_to(controller: "manager/specification", action: "edit", id: "31")
  end

  #update
  it "routing post /manager/specification/:id to manager/specification#update" do
    expect(post: "/manager/specification/31").to route_to(controller: "manager/specification", action: "update", id: "31")
  end

  #delete
  it "routing delete /manager/specification/:id to manager/specification#destroy" do
    expect(delete: "/manager/specification/31").to route_to(controller: "manager/specification", action: "destroy", id: "31")
  end

end
