# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/transfered_clients", :type => :routing do

  it "routing get /manager/transfered_clients to manager/transfered_clients#index" do
    expect(get: "/manager/transfered_clients").to route_to(controller: "manager/transfered_clients", action: "index")
  end

end
