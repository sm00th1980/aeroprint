# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/transfered_branches", :type => :routing do

  it "routing get /manager/client/31/transfered_branches to manager/transfered_branches#index" do
    expect(get: "/manager/client/31/transfered_branches").to route_to(controller: "manager/transfered_branches", action: "index", id: "31")
  end

end
