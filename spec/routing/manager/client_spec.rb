# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/client", :type => :routing do

  #new
  it "routing get /manager/client/new to manager/client#new" do
    expect(get: "/manager/client/new").to route_to(controller: "manager/client", action: "new")
  end

  #create
  it "routing post /manager/client to manager/client#create" do
    expect(post: "/manager/client").to route_to(controller: "manager/client", action: "create")
  end

  #edit
  it "routing get /manager/client/:id to manager/client#edit" do
    expect(get: "/manager/client/31").to route_to(controller: "manager/client", action: "edit", id: "31")
  end

  #update
  it "routing post /manager/client/:id to manager/client#update" do
    expect(post: "/manager/client/31").to route_to(controller: "manager/client", action: "update", id: "31")
  end

  #delete
  it "routing delete /manager/client/:id to manager/client#destroy" do
    expect(delete: "/manager/client/31").to route_to(controller: "manager/client", action: "destroy", id: "31")
  end

  #update json
  it "routing post /manager/client/:id.json to manager/client#update" do
    expect(post: "/manager/client/31.json").to route_to(controller: "manager/client", action: "update", id: "31", format: "json")
  end

  #drop json
  it "routing delete /manager/client/:id.json to manager/client#destroy" do
    expect(delete: "/manager/client/31.json").to route_to(controller: "manager/client", action: "destroy", id: "31", format: "json")
  end

end
