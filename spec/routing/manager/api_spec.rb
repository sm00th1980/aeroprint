# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to /manager/api", :type => :routing do

  it "routing get /manager/api/density/31 to manager/api#density" do
    expect(get: "/manager/api/density/31").to route_to(controller: "manager/api", action: "density", id: "31")
  end

  it "routing get /manager/api/layout to manager/api#layout" do
    expect(get: "/manager/api/layout").to route_to(controller: "manager/api", action: "layout")
  end

end
