# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/transfered_specification", :type => :routing do

  #new
  it "no routing get /manager/client/31/transfered_specification/new" do
    expect(get: "/manager/client/31/transfered_specification/new").not_to be_routable
  end

  #create
  it "no routing post /manager/client/31/transfered_specification" do
    expect(post: "/manager/client/31/transfered_specification").not_to be_routable
  end

  #update
  it "routing post /manager/transfered_specification/:id to manager/transfered_specification#update" do
    expect(post: "/manager/transfered_specification/31").to route_to(controller: "manager/transfered_specification", action: "update", id: "31")
  end

  #edit
  it "routing get /manager/transfered_specification/:id to manager/transfered_specification#edit" do
    expect(get: "/manager/transfered_specification/31").to route_to(controller: "manager/transfered_specification", action: "edit", id: "31")
  end

  #drop
  it "no routing delete /manager/transfered_specification/:id" do
    expect(delete: "/manager/transfered_specification/31").not_to be_routable
  end

  #drop json
  it "no routing delete /manager/transfered_specification/:id.json" do
    expect(delete: "/manager/transfered_specification/31.json").not_to be_routable
  end

end
