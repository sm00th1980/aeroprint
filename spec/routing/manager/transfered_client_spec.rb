# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/transfered_client", :type => :routing do

  #new
  it "no routing for get /manager/transfered_client/new" do
    expect(get: "/manager/transfered_client/new").not_to be_routable
  end

  #create
  it "no routing for post /manager/transfered_client" do
    expect(post: "/manager/transfered_client").to route_to(controller: "manager/application", action: "page_not_found")
  end

  #delete
  it "no routing delete /manager/transfered_client/:id" do
    expect(delete: "/manager/transfered_client/31").not_to be_routable
  end

  #update json
  it "routing post /manager/transfered_client/:id.json to manager/transfered_client#update" do
    expect(post: "/manager/transfered_client/31.json").to route_to(controller: "manager/transfered_client", action: "update", id: "31", format: "json")
  end

  #drop json
  it "no routing delete /manager/transfered_client/:id.json" do
    expect(delete: "/manager/transfered_client/31.json").not_to be_routable
  end

end
