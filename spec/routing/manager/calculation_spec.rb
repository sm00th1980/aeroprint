# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/calculation", :type => :routing do

  #new
  it "routing get /manager/calculation/new to manager/calculation#new" do
    expect(get: "/manager/calculation/new").to route_to(controller: "manager/calculation", action: "new")
  end

  #create
  it "routing post /manager/calculation to manager/calculation#create" do
    expect(post: "/manager/calculation").to route_to(controller: "manager/calculation", action: "create")
  end

  #edit
  it "routing get /manager/calculation/:id to manager/calculation#edit" do
    expect(get: "/manager/calculation/31").to route_to(controller: "manager/calculation", action: "edit", id: "31")
  end

  #save
  it "routing post /manager/calculation/:id to manager/calculation#save" do
    expect(post: "/manager/calculation/31").to route_to(controller: "manager/calculation", action: "save", id: "31")
  end

end
