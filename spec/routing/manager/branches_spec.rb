# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/branches", :type => :routing do

  it "routing get /manager/client/31/branches to manager/branches#index" do
    expect(get: "/manager/client/31/branches").to route_to(controller: "manager/branches", action: "index", id: "31")
  end

end
