# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/clients", :type => :routing do

  it "routing get /manager/clients to manager/clients#index" do
    expect(get: "/manager/clients").to route_to(controller: "manager/clients", action: "index")
  end

end
