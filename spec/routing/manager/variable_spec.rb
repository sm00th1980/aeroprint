# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/variable", :type => :routing do

  #variable
  it "routing get /manager/variable/:id to manager/variable#show" do
    expect(get: "/manager/variable/31").to route_to(controller: "manager/variable", action: "show", id: "31")
  end

  it "routing post /manager/variable/:id to manager/variable#show" do
    expect(get: "/manager/variable/31").to route_to(controller: "manager/variable", action: "show", id: "31")
  end

end
