# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/branch", :type => :routing do

  #new
  it "routing get /manager/client/31/branch/new to manager/branch#new" do
    expect(get: "/manager/client/31/branch/new").to route_to(controller: "manager/branch", action: "new", id: "31")
  end

  #create
  it "routing post /manager/client/31/branch to manager/branch#create" do
    expect(post: "/manager/client/31/branch").to route_to(controller: "manager/branch", action: "create", id: "31")
  end

  #edit
  it "routing get /manager/branch/:id to manager/branch#edit" do
    expect(get: "/manager/branch/31").to route_to(controller: "manager/branch", action: "edit", id: "31")
  end

  #update
  it "routing post /manager/branch/:id to manager/branch#update" do
    expect(post: "/manager/branch/31").to route_to(controller: "manager/branch", action: "update", id: "31")
  end

  #delete
  it "routing delete /manager/branch/:id to manager/branch#destroy" do
    expect(delete: "/manager/branch/31").to route_to(controller: "manager/branch", action: "destroy", id: "31")
  end

end
