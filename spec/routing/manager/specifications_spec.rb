# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/specifications", :type => :routing do

  it "routing get /manager/client/31/specifications to manager/specifications#index" do
    expect(get: "/manager/client/31/specifications").to route_to(controller: "manager/specifications", action: "index", id: "31")
  end

end
