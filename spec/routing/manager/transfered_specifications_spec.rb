# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/transfered_specifications", :type => :routing do

  it "routing get /manager/client/31/transfered_specifications to manager/transfered_specifications#index" do
    expect(get: "/manager/client/31/transfered_specifications").to route_to(controller: "manager/transfered_specifications", action: "index", id: "31")
  end

end
