# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/transfered_branch", :type => :routing do

  #new
  it "no routing get /manager/client/31/transfered_branch/new" do
    expect(get: "/manager/client/31/transfered_branch/new").not_to be_routable
  end

  #create
  it "no routing post /manager/client/31/transfered_branch" do
    expect(post: "/manager/client/31/transfered_branch").not_to be_routable
  end

  #update
  it "routing post /manager/transfered_branch/:id to manager/transfered_branch#update" do
    expect(post: "/manager/transfered_branch/31").to route_to(controller: "manager/transfered_branch", action: "update", id: "31")
  end

  #edit
  it "routing get /manager/transfered_branch/:id to manager/transfered_branch#edit" do
    expect(get: "/manager/transfered_branch/31").to route_to(controller: "manager/transfered_branch", action: "edit", id: "31")
  end

  #drop
  it "no routing delete /manager/transfered_branch/:id" do
    expect(delete: "/manager/transfered_branch/31").not_to be_routable
  end

  #drop json
  it "no routing delete /manager/transfered_branch/:id.json" do
    expect(delete: "/manager/transfered_branch/31.json").not_to be_routable
  end

end
