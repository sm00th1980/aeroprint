# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager/multiple", :type => :routing do

  #multiple
  it "routing get /manager/multiple/:id to manager/multiple#new" do
    expect(get: "/manager/multiple/31").to route_to(controller: "manager/multiple", action: "new", id: "31")
  end

  it "routing post /manager/multiple/:id to manager/multiple#create" do
    expect(post: "/manager/multiple/31").to route_to(controller: "manager/multiple", action: "create", id: "31")
  end

end
