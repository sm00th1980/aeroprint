# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to features", :type => :routing do

  it "routing get /paper/features to admin/features#index" do
    expect(get: "/paper/31/features").to route_to(controller: "admin/features", action: "index", id: "31")
  end

end
