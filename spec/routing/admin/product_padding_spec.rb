# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to product_padding", :type => :routing do

  #edit
  it "routing get /product_padding/:id to admin/product_padding#edit" do
    expect(get: "/product_padding/31").to route_to(controller: "admin/product_padding", action: "edit", id: "31")
  end

  #update
  it "routing post /product_padding/:id to admin/product_padding#update" do
    expect(post: "/product_padding/31").to route_to(controller: "admin/product_padding", action: "update", id: "31")
  end

end
