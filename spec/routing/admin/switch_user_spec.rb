# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to users", :type => :routing do

  it "routing get /switch_user to switch_user#set_current_user" do
    expect(get: "/switch_user").to route_to(controller: "switch_user", action: "set_current_user")
  end

end
