# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to machine", :type => :routing do

  #new
  it "routing get /machine/new to admin/machine#new" do
    expect(get: "/machine/new").to route_to(controller: "admin/machine", action: "new")
  end

  #create
  it "routing post /machine to admin/machine#create" do
    expect(post: "/machine").to route_to(controller: "admin/machine", action: "create")
  end

  #edit
  it "routing get /machine/:id to admin/machine#edit" do
    expect(get: "/machine/31").to route_to(controller: "admin/machine", action: "edit", id: "31")
  end

  #update
  it "routing post /machine/:id to admin/machine#update" do
    expect(post: "/machine/31").to route_to(controller: "admin/machine", action: "update", id: "31")
  end

  #delete
  it "routing delete /machine/:id to admin/machine#destroy" do
    expect(delete: "/machine/31").to route_to(controller: "admin/machine", action: "destroy", id: "31")
  end

end
