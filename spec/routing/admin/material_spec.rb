# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to material", :type => :routing do

  #new
  it "routing get /material/new to admin/material#new" do
    expect(get: "/material/new").to route_to(controller: "admin/material", action: "new")
  end

  #create
  it "routing post /material to admin/material#create" do
    expect(post: "/material").to route_to(controller: "admin/material", action: "create")
  end

  #edit
  it "routing get /material/:id to admin/material#edit" do
    expect(get: "/material/31").to route_to(controller: "admin/material", action: "edit", id: "31")
  end

  #update
  it "routing post /material/:id to admin/material#update" do
    expect(post: "/material/31").to route_to(controller: "admin/material", action: "update", id: "31")
  end

  #delete
  it "routing delete /material/:id to admin/material#destroy" do
    expect(delete: "/material/31").to route_to(controller: "admin/material", action: "destroy", id: "31")
  end

end
