# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to uv_polish_price", :type => :routing do

  #edit
  it "routing get /uv_polish_price/:id to admin/uv_polish_price#edit" do
    expect(get: "/uv_polish_price/31").to route_to(controller: "admin/uv_polish_price", action: "edit", id: "31")
  end

  #update
  it "routing post /uv_polish_price/:id to admin/uv_polish_price#update" do
    expect(post: "/uv_polish_price/31").to route_to(controller: "admin/uv_polish_price", action: "update", id: "31")
  end

end
