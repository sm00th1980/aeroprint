# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to materials", :type => :routing do

  it "routing get /materials to admin/materials#index" do
    expect(get: "/materials").to route_to(controller: "admin/materials", action: "index")
  end

end
