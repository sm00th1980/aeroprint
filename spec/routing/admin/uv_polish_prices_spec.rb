# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to uv_polish_prices", :type => :routing do

  it "routing get /uv_polish_prices to admin/uv_polish_prices#index" do
    expect(get: "/uv_polish_prices").to route_to(controller: "admin/uv_polish_prices", action: "index")
  end

end
