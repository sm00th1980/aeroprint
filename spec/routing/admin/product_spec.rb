# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to product", :type => :routing do

  #new
  it "routing get /product/new to admin/product#new" do
    expect(get: "/product/new").to route_to(controller: "admin/product", action: "new")
  end

  #create
  it "routing post /product to admin/product#create" do
    expect(post: "/product").to route_to(controller: "admin/product", action: "create")
  end

  #edit
  it "routing get /product/:id to admin/product#edit" do
    expect(get: "/product/31").to route_to(controller: "admin/product", action: "edit", id: "31")
  end

  #update
  it "routing post /product/:id to admin/product#update" do
    expect(post: "/product/31").to route_to(controller: "admin/product", action: "update", id: "31")
  end

  #delete
  it "routing delete /product/:id to admin/product#destroy" do
    expect(delete: "/product/31").to route_to(controller: "admin/product", action: "destroy", id: "31")
  end

end
