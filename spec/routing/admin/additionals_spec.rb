# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to additionals", :type => :routing do

  it "routing get /additionals to admin/additionals#index" do
    expect(get: "/additionals").to route_to(controller: "admin/additionals", action: "index")
  end

end
