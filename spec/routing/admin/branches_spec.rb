# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to branches", :type => :routing do

  it "routing get /client/31/branches to admin/branches#index" do
    expect(get: "/client/31/branches").to route_to(controller: "admin/branches", action: "index", id: "31")
  end

end
