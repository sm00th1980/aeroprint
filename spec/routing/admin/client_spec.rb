# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to client", :type => :routing do

  #new
  it "routing get /client/new to admin/client#new" do
    expect(get: "/client/new").to route_to(controller: "admin/client", action: "new")
  end

  #create
  it "routing post /client to admin/client#create" do
    expect(post: "/client").to route_to(controller: "admin/client", action: "create")
  end

  #edit
  it "routing get /client/:id to admin/client#edit" do
    expect(get: "/client/31").to route_to(controller: "admin/client", action: "edit", id: "31")
  end

  #update
  it "routing post /client/:id to admin/client#update" do
    expect(post: "/client/31").to route_to(controller: "admin/client", action: "update", id: "31")
  end

  #delete
  it "routing delete /client/:id to admin/client#destroy" do
    expect(delete: "/client/31").to route_to(controller: "admin/client", action: "destroy", id: "31")
  end

end
