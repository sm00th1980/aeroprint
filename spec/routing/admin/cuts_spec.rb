# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to cuts", :type => :routing do

  it "routing get /feature/31/cuts to admin/cuts#index" do
    expect(get: "/feature/31/cuts").to route_to(controller: "admin/cuts", action: "index", id: "31")
  end

end
