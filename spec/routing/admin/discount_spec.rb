# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to discount", :type => :routing do

  #new
  it "routing get /discount/new to admin/discount#new" do
    expect(get: "/discount/new").to route_to(controller: "admin/discount", action: "new")
  end

  #create
  it "routing post /discount to admin/discount#create" do
    expect(post: "/discount").to route_to(controller: "admin/discount", action: "create")
  end

  #edit
  it "routing get /discount/:id to admin/discount#edit" do
    expect(get: "/discount/31").to route_to(controller: "admin/discount", action: "edit", id: "31")
  end

  #update
  it "routing post /discount/:id to admin/discount#update" do
    expect(post: "/discount/31").to route_to(controller: "admin/discount", action: "update", id: "31")
  end

  #delete
  it "routing delete /discount/:id to admin/discount#destroy" do
    expect(delete: "/discount/31").to route_to(controller: "admin/discount", action: "destroy", id: "31")
  end

end
