# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to clients", :type => :routing do

  it "routing get /clients to admin/clients#index" do
    expect(get: "/clients").to route_to(controller: "admin/clients", action: "index")
  end

end
