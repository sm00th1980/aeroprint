# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to managers", :type => :routing do

  it "routing get /managers to admin/managers#index" do
    expect(get: "/managers").to route_to(controller: "admin/managers", action: "index")
  end

end
