# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to product_paddings", :type => :routing do

  it "routing get /product_paddings to admin/product_paddings#index" do
    expect(get: "/product_paddings").to route_to(controller: "admin/product_paddings", action: "index")
  end

end
