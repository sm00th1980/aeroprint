# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to cover_work_type", :type => :routing do

  #new
  it "routing get /cover_work_type/new to admin/cover_work_type#new" do
    expect(get: "/cover_work_type/new").to route_to(controller: "admin/cover_work_type", action: "new")
  end

  #create
  it "routing post /cover_work_type to admin/cover_work_type#create" do
    expect(post: "/cover_work_type").to route_to(controller: "admin/cover_work_type", action: "create")
  end

  #edit
  it "routing get /cover_work_type/:id to admin/cover_work_type#edit" do
    expect(get: "/cover_work_type/31").to route_to(controller: "admin/cover_work_type", action: "edit", id: "31")
  end

  #update
  it "routing post /cover_work_type/:id to admin/cover_work_type#update" do
    expect(post: "/cover_work_type/31").to route_to(controller: "admin/cover_work_type", action: "update", id: "31")
  end

  #delete
  it "routing delete /cover_work_type/:id to admin/cover_work_type#destroy" do
    expect(delete: "/cover_work_type/31").to route_to(controller: "admin/cover_work_type", action: "destroy", id: "31")
  end

end
