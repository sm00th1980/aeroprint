# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to feature", :type => :routing do

  #new
  it "routing get /paper/31/feature/new to admin/feature#new" do
    expect(get: "/paper/31/feature/new").to route_to(controller: "admin/feature", action: "new", id: "31")
  end

  #create
  it "routing post /paper/31/feature to admin/feature#create" do
    expect(post: "/paper/31/feature").to route_to(controller: "admin/feature", action: "create", id: "31")
  end

  #edit
  it "routing get /feature/:id to admin/feature#edit" do
    expect(get: "/feature/31").to route_to(controller: "admin/feature", action: "edit", id: "31")
  end

  #update
  it "routing post /feature/:id to admin/feature#update" do
    expect(post: "/feature/31").to route_to(controller: "admin/feature", action: "update", id: "31")
  end

  #delete
  it "routing delete /feature/:id to admin/feature#destroy" do
    expect(delete: "/feature/31").to route_to(controller: "admin/feature", action: "destroy", id: "31")
  end

  #clone
  it "routing get /feature/:id/clone to admin/feature#clone" do
    expect(get: "/feature/31/clone").to route_to(controller: "admin/feature", action: "clone", id: "31")
  end

end
