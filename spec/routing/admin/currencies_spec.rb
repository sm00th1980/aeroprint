# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to currencies", :type => :routing do

  it "routing get /currencies to admin/currencies#index" do
    expect(get: "/currencies").to route_to(controller: "admin/currencies", action: "index")
  end

end
