# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to papers", :type => :routing do

  it "routing get /papers to admin/papers#index" do
    expect(get: "/papers").to route_to(controller: "admin/papers", action: "index")
  end

end
