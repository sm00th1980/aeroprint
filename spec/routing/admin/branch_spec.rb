# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to branch", :type => :routing do

  #new
  it "routing get /client/31/branch/new to admin/branch#new" do
    expect(get: "/client/31/branch/new").to route_to(controller: "admin/branch", action: "new", id: "31")
  end

  #create
  it "routing post /client/31/branch to admin/branch#create" do
    expect(post: "/client/31/branch").to route_to(controller: "admin/branch", action: "create", id: "31")
  end

  #edit
  it "routing get /branch/:id to admin/branch#edit" do
    expect(get: "/branch/31").to route_to(controller: "admin/branch", action: "edit", id: "31")
  end

  #update
  it "routing post /branch/:id to admin/branch#update" do
    expect(post: "/branch/31").to route_to(controller: "admin/branch", action: "update", id: "31")
  end

  #delete
  it "routing delete /branch/:id to admin/branch#destroy" do
    expect(delete: "/branch/31").to route_to(controller: "admin/branch", action: "destroy", id: "31")
  end

end
