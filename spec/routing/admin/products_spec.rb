# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to products", :type => :routing do

  it "routing get /products to admin/products#index" do
    expect(get: "/products").to route_to(controller: "admin/products", action: "index")
  end

end
