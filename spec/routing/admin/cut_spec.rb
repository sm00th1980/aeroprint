# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to cut", :type => :routing do

  #new
  it "routing get /feature/31/cut/new to admin/cut#new" do
    expect(get: "/feature/31/cut/new").to route_to(controller: "admin/cut", action: "new", id: "31")
  end

  #create
  it "routing post /feature/31/cut to admin/cut#create" do
    expect(post: "/feature/31/cut").to route_to(controller: "admin/cut", action: "create", id: "31")
  end

  #edit
  it "routing get /cut/:id to admin/cut#edit" do
    expect(get: "/cut/31").to route_to(controller: "admin/cut", action: "edit", id: "31")
  end

  #update
  it "routing post /cut/:id to admin/cut#update" do
    expect(post: "/cut/31").to route_to(controller: "admin/cut", action: "update", id: "31")
  end

  #delete
  it "routing delete /cut/:id to admin/cut#destroy" do
    expect(delete: "/cut/31").to route_to(controller: "admin/cut", action: "destroy", id: "31")
  end

end
