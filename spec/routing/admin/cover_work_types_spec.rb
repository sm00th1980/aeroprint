# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to cover_work_types", :type => :routing do

  it "routing get /cover_work_types to admin/cover_work_types#index" do
    expect(get: "/cover_work_types").to route_to(controller: "admin/cover_work_types", action: "index")
  end

end
