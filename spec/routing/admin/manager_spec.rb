# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to manager", :type => :routing do

  #new
  it "routing get /manager/new to admin/manager#new" do
    expect(get: "/manager/new").to route_to(controller: "admin/manager", action: "new")
  end

  #create
  it "routing post /manager to admin/manager#create" do
    expect(post: "/manager").to route_to(controller: "admin/manager", action: "create")
  end

  #edit
  it "routing get /manager/:id to admin/manager#edit" do
    expect(get: "/manager/31").to route_to(controller: "admin/manager", action: "edit", id: "31")
  end

  #update
  it "routing post /manager/:id to admin/manager#update" do
    expect(post: "/manager/31").to route_to(controller: "admin/manager", action: "update", id: "31")
  end

  #delete
  it "routing delete /manager/:id to admin/manager#destroy" do
    expect(delete: "/manager/31").to route_to(controller: "admin/manager", action: "destroy", id: "31")
  end

  #block
  it "routing post /manager/:id/block to admin/manager#block" do
    expect(post: "/manager/31/block").to route_to(controller: "admin/manager", action: "block", id: "31")
  end

  #unblock
  it "routing post /manager/:id/unblock to admin/manager#unblock" do
    expect(post: "/manager/31/unblock").to route_to(controller: "admin/manager", action: "unblock", id: "31")
  end

end
