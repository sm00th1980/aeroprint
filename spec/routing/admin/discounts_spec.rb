# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to discounts", :type => :routing do

  it "routing get /discounts to admin/discounts#index" do
    expect(get: "/discounts").to route_to(controller: "admin/discounts", action: "index")
  end

end
