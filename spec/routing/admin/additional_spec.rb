# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to additional", :type => :routing do

  #edit
  it "routing get /additional/:id to admin/additional#edit" do
    expect(get: "/additional/31").to route_to(controller: "admin/additional", action: "edit", id: "31")
  end

  #update
  it "routing post /additional/:id to admin/additional#update" do
    expect(post: "/additional/31").to route_to(controller: "admin/additional", action: "update", id: "31")
  end

end
