# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to paper", :type => :routing do

  #new
  it "routing get /paper/new to admin/paper#new" do
    expect(get: "/paper/new").to route_to(controller: "admin/paper", action: "new")
  end

  #create
  it "routing post /paper to admin/paper#create" do
    expect(post: "/paper").to route_to(controller: "admin/paper", action: "create")
  end

  #edit
  it "routing get /paper/:id to admin/paper#edit" do
    expect(get: "/paper/31").to route_to(controller: "admin/paper", action: "edit", id: "31")
  end

  #update
  it "routing post /paper/:id to admin/paper#update" do
    expect(post: "/paper/31").to route_to(controller: "admin/paper", action: "update", id: "31")
  end

  #delete
  it "routing delete /paper/:id to admin/paper#destroy" do
    expect(delete: "/paper/31").to route_to(controller: "admin/paper", action: "destroy", id: "31")
  end

end
