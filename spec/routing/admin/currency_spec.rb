# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to currency", :type => :routing do

  #edit
  it "routing get /currency/:id to admin/currency#edit" do
    expect(get: "/currency/31").to route_to(controller: "admin/currency", action: "edit", id: "31")
  end

  #update
  it "routing post /currency/:id to admin/currency#update" do
    expect(post: "/currency/31").to route_to(controller: "admin/currency", action: "update", id: "31")
  end

end
