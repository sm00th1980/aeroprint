# -*- encoding : utf-8 -*-
require 'spec_helper'

describe "routing to machines", :type => :routing do

  it "routing get /machines to admin/machines#index" do
    expect(get: "/machines").to route_to(controller: "admin/machines", action: "index")
  end

end
