# -*- encoding : utf-8 -*-
class CreateContractStatuses < ActiveRecord::Migration
  def change
    create_table :contract_statuses do |t|
      t.string :name, null: false
      t.string :color, null: false

      t.timestamps null: false
    end
  end
end
