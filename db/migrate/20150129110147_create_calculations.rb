# -*- encoding : utf-8 -*-
class CreateCalculations < ActiveRecord::Migration
  def change
    create_table :calculations do |t|
      t.integer :client_id, null: false
      t.text :comment, null: true

      t.timestamps null: false
    end
  end
end
