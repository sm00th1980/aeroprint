# -*- encoding : utf-8 -*-
class MakeStripesCountNullable < ActiveRecord::Migration
  def up
    change_column :components, :stripes_count, :integer, null: true
  end

  def down
    change_column :components, :stripes_count, :integer, null: false
  end
end
