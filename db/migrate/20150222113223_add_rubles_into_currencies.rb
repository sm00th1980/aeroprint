# -*- encoding : utf-8 -*-
class AddRublesIntoCurrencies < ActiveRecord::Migration
  def up
    Currency.create(name: 'Рубли', internal_name: 'russia', exchange_rate: 1.0)
  end
end
