# -*- encoding : utf-8 -*-
class CreateCoverWorks < ActiveRecord::Migration
  def change
    create_table :cover_works do |t|
      t.string :name, null: false
      t.decimal  :price, precision: 15, scale: 2, null: false
      t.integer :product_count, null: false, default: 1

      t.timestamps null: false
    end
  end
end
