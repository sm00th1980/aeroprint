# -*- encoding : utf-8 -*-
class CreatePaperCountries < ActiveRecord::Migration
  def change
    create_table :paper_countries do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      
      t.timestamps null: false
    end
  end
end
