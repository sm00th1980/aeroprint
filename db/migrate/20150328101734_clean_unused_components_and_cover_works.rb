# -*- encoding : utf-8 -*-
class CleanUnusedComponentsAndCoverWorks < ActiveRecord::Migration
  def up
    execute('delete from components where calculation_id not in (select id from calculations)')
    execute('delete from calculation_cover_works where calculation_id not in (select id from calculations)')
  end
end
