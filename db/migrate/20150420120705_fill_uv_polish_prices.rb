# -*- encoding : utf-8 -*-
class FillUvPolishPrices < ActiveRecord::Migration
  def up
    UvPolishPrice.create!(type: UvPolishType.glossy, format: UvPolishFormat.A3, print_price: 2.0, preparation_price: 2100)
    UvPolishPrice.create!(type: UvPolishType.glossy, format: UvPolishFormat.A3_plus, print_price: 2.2, preparation_price: 2200)
    UvPolishPrice.create!(type: UvPolishType.glossy, format: UvPolishFormat.A2, print_price: 3.0, preparation_price: 2800)
    UvPolishPrice.create!(type: UvPolishType.glossy, format: UvPolishFormat.A2_plus, print_price: 3.3, preparation_price: 2900)
    UvPolishPrice.create!(type: UvPolishType.glossy, format: UvPolishFormat.A1, print_price: 8.5, preparation_price: 4000)

    UvPolishPrice.create!(type: UvPolishType.opaque, format: UvPolishFormat.A3, print_price: 4.5, preparation_price: 2100)
    UvPolishPrice.create!(type: UvPolishType.opaque, format: UvPolishFormat.A2, print_price: 6.0, preparation_price: 2800)
    UvPolishPrice.create!(type: UvPolishType.opaque, format: UvPolishFormat.A3_plus, print_price: 4.5*1.25, preparation_price: 2100*1.25)
    UvPolishPrice.create!(type: UvPolishType.opaque, format: UvPolishFormat.A2_plus, print_price: 6.0*1.25, preparation_price: 2800*1.25)
  end
end
