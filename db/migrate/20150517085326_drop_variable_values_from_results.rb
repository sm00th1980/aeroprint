# -*- encoding : utf-8 -*-
class DropVariableValuesFromResults < ActiveRecord::Migration
  def up
    remove_column :component_results, :variable_exchange_rate
    remove_column :component_results, :variable_form_price
    remove_column :component_results, :variable_price_per_hour
  end
end
