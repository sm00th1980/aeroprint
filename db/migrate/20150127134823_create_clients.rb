# -*- encoding : utf-8 -*-
class CreateClients < ActiveRecord::Migration
  def change
    create_table :clients do |t|
      t.integer :manager_id, null: false
      t.string :name, null: false, default: ""
      t.text :description, null: true

      t.timestamps null: false
    end
  end
end
