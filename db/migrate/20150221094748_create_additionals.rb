# -*- encoding : utf-8 -*-
class CreateAdditionals < ActiveRecord::Migration
  def change
    create_table :additionals do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      t.decimal :value, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end
    add_index :additionals, [:internal_name], unique: true
  end
end
