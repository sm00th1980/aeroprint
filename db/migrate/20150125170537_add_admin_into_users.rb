# -*- encoding : utf-8 -*-
class AddAdminIntoUsers < ActiveRecord::Migration
  def up
    User.create!({email: "support@aero-print.ru", password: "SwFT4PXk", password_confirmation: "SwFT4PXk", admin: true })

    user = User.find_by(email: "user@aero-print.ru")
    user.password = 'aXvWG6pY'
    user.password_confirmation = 'aXvWG6pY'
    user.admin = false

    user.save!
  end

  def down
    User.find_by(email: "support@aero-print.ru").delete
  end
end
