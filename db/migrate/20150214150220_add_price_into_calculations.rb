# -*- encoding : utf-8 -*-
class AddPriceIntoCalculations < ActiveRecord::Migration
  def change
    add_column :calculations, :price, :decimal, precision: 15, scale: 2

    Calculation.find_each do |calculation|
      calculation.price = rand(100) + 10
      calculation.save!
    end

    change_column :calculations, :price, :decimal, precision: 15, scale: 2, null: false
  end
end
