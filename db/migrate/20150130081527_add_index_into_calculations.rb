# -*- encoding : utf-8 -*-
class AddIndexIntoCalculations < ActiveRecord::Migration
  def change
    add_index :calculations, :client_id
  end
end
