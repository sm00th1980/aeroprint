# -*- encoding : utf-8 -*-
class FillPapers < ActiveRecord::Migration
  def up
    Paper.create!(name: 'Меловка', type: PaperType.melovka, producer: PaperProducer.bereg)
    Paper.create!(name: 'Картон1', type: PaperType.karton_1_str, producer: PaperProducer.bereg)
    Paper.create!(name: 'Офсетка', type: PaperType.offset, producer: PaperProducer.bereg)
    Paper.create!(name: 'Картон2', type: PaperType.karton_1_str, producer: PaperProducer.bereg)
    Paper.create!(name: 'картон лен 1 стр. деш.', type: PaperType.karton_tis, producer: PaperProducer.bereg)
    Paper.create!(name: 'Giroform', type: PaperType.self_coping, producer: PaperProducer.bereg)
    Paper.create!(name: 'газета', type: PaperType.newspaper, producer: PaperProducer.bumajnik)
    Paper.create!(name: 'adestor', type: PaperType.self_glue, producer: PaperProducer.bereg)
  end

  def down
    Paper.delete_all
  end
end
