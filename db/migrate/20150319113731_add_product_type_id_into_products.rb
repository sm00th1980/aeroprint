# -*- encoding : utf-8 -*-
class AddProductTypeIdIntoProducts < ActiveRecord::Migration
  def change
    add_column :products, :product_type_id, :integer

    Product.all.each do |product|
      product.product_type_id = ProductType.all.sample.id
      product.save!
    end

    change_column :products, :product_type_id, :integer, null: false
  end
end
