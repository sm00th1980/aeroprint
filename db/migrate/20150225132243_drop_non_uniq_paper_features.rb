# -*- encoding : utf-8 -*-
class DropNonUniqPaperFeatures < ActiveRecord::Migration
  def up
    PaperFeature.delete_all
    Component.delete_all
    Calculation.delete_all

    PaperFeature.create!(paper_id: 1, weight: 80, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1802.66, currency_id: 1)
    PaperFeature.create!(paper_id: 1, weight: 90, gor_sm: 62, ver_sm: 94, price_per_1_ton: 1802.66, currency_id: 1)
  end
end
