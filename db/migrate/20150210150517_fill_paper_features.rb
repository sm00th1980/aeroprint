# -*- encoding : utf-8 -*-
class FillPaperFeatures < ActiveRecord::Migration
  def up
    PaperFeature.create!(paper: Paper.first, weight: 80, gor_sm: 62, ver_sm: 94, gor_mm: 290, ver_mm: 452, weight_per_1000: 46.62, price_per_1000: 84.04)
    PaperFeature.create!(paper: Paper.first, weight: 80, gor_sm: 62, ver_sm: 94, gor_mm: 440, ver_mm: 602, weight_per_1000: 46.62, price_per_1000: 84.04)
    PaperFeature.create!(paper: Paper.first, weight: 90, gor_sm: 62, ver_sm: 94, gor_mm: 290, ver_mm: 452, weight_per_1000: 52.45, price_per_1000: 94.54)
    PaperFeature.create!(paper: Paper.first, weight: 90, gor_sm: 62, ver_sm: 94, gor_mm: 440, ver_mm: 602, weight_per_1000: 52.45, price_per_1000: 94.54)
    PaperFeature.create!(paper: Paper.first, weight: 90, gor_sm: 64, ver_sm: 90, gor_mm: 300, ver_mm: 432, weight_per_1000: 51.84, price_per_1000: 93.44)
  end

  def down
    PaperFeature.delete_all
  end
end
