# -*- encoding : utf-8 -*-
class AddVariablePricePer1TonIntoResults < ActiveRecord::Migration
  def change
    add_column :component_results, :variable_price_per_1_ton, :decimal, precision: 15, scale: 2
  end
end
