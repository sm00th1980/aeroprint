# -*- encoding : utf-8 -*-
class RemoveFormPriceFromAdditionals < ActiveRecord::Migration
  def up
    Additional.find_by(internal_name: 'form_price').delete
  end
end
