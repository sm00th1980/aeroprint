# -*- encoding : utf-8 -*-
class CreateUvPolishPrices < ActiveRecord::Migration
  def change
    create_table :uv_polish_prices do |t|
      t.integer :type_id, null: false
      t.integer :format_id, null: false

      t.decimal :print_price, precision: 15, scale: 2, null: false
      t.decimal :preparation_price, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end
  end
end
