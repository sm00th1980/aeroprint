# -*- encoding : utf-8 -*-
class AddVariableFormPriceIntoComponentResults < ActiveRecord::Migration
  def change
    add_column :component_results, :variable_form_price, :decimal, precision: 15, scale: 2
  end
end
