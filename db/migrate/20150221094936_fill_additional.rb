# -*- encoding : utf-8 -*-
class FillAdditional < ActiveRecord::Migration
  def up
    Additional.create!(name: 'Приладка CMYK', internal_name: 'cmyk_press_preparation', value: 25)
    Additional.create!(name: 'Приладка Pantone', internal_name: 'pantone_press_preparation', value: 50)
  end

  def down
    Additional.delete_all
  end
end
