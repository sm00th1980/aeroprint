# -*- encoding : utf-8 -*-
class RefillUvPolishFormatInPaperFeatures < ActiveRecord::Migration
  def up
    PaperFeature.where(uv_polish_format: UvPolishFormat.A1).find_each do |feature|
      feature.uv_polish_format = UvPolishFormat.A2
      feature.save!
    end
  end
end
