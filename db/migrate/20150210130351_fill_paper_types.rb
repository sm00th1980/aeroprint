# -*- encoding : utf-8 -*-
class FillPaperTypes < ActiveRecord::Migration
  def up
    PaperType.create!(name: 'Меловка', internal_name: 'melovka')
    PaperType.create!(name: 'Картон 1 стр.', internal_name: 'karton_1_str')
    PaperType.create!(name: 'Картон 2 стр.', internal_name: 'karton_2_str')
    PaperType.create!(name: 'Офсетная', internal_name: 'offset')
    PaperType.create!(name: 'Картон тис.', internal_name: 'karton_tis')
    PaperType.create!(name: 'Самокопирка', internal_name: 'self_coping')
    PaperType.create!(name: 'Газетная', internal_name: 'newspaper')
    PaperType.create!(name: 'п/гл. самокл.', internal_name: 'self_glue')
  end

  def down
    PaperType.delete_all
  end
end
