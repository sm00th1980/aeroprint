# -*- encoding : utf-8 -*-
class AddMachineIntoCalculations < ActiveRecord::Migration
  def up
    add_column :calculations, :machine_id, :integer

    Calculation.find_each do |calc|
      calc.machine_id = Machine.all[rand(2)].id
      calc.save!
    end

    change_column :calculations, :machine_id, :integer, null: false
  end
end
