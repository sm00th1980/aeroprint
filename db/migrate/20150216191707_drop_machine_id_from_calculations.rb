# -*- encoding : utf-8 -*-
class DropMachineIdFromCalculations < ActiveRecord::Migration
  def up
    remove_column :calculations, :machine_id
  end
end
