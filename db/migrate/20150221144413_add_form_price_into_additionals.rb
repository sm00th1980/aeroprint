# -*- encoding : utf-8 -*-
class AddFormPriceIntoAdditionals < ActiveRecord::Migration
  def up
    Additional.create(name: 'Стоимость форм', internal_name: 'form_price', value: 70.0)
  end
end
