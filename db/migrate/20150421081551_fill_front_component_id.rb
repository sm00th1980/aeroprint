# -*- encoding : utf-8 -*-
class FillFrontComponentId < ActiveRecord::Migration
  def up
    Component.find_each do |component|
      component.frontend_component_id = index(component)
      component.save!
    end

    change_column :components, :frontend_component_id, :integer, null: false
  end

  def down
    change_column :components, :frontend_component_id, :integer, null: true

    Component.find_each do |component|
      component.frontend_component_id = nil
      component.save!
    end
  end

  private
  def index(component)
    component.calculation.components.sort_by { |c| c.id }.find_index(component)
  end
end
