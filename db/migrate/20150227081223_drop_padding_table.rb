# -*- encoding : utf-8 -*-
class DropPaddingTable < ActiveRecord::Migration
  def up
    drop_table :sheet_paddings
  end
end
