# -*- encoding : utf-8 -*-
class UpdateProductInCalculations < ActiveRecord::Migration
  def up
    Calculation.find_each do |calc|
      calc.product = Product.all.sample
      calc.save!
    end
  end
end
