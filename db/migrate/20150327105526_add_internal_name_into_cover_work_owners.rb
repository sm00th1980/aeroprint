# -*- encoding : utf-8 -*-
class AddInternalNameIntoCoverWorkOwners < ActiveRecord::Migration
  def up
    add_column :cover_work_owners, :internal_name, :string

    CoverWorkOwner.find_by(name: 'для изделия в целом').update_attribute(:internal_name, :product)
    CoverWorkOwner.find_by(name: 'для блока').update_attribute(:internal_name, :component)

    change_column :cover_work_owners, :internal_name, :string, null: false

    add_index :cover_work_owners, :internal_name, unique: true
  end
end
