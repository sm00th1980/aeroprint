# -*- encoding : utf-8 -*-
class DropGorMmAndVerMmFromPaperFeatures < ActiveRecord::Migration
  def up
    remove_column :paper_features, :gor_mm
    remove_column :paper_features, :ver_mm
  end
end
