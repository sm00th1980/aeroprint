# -*- encoding : utf-8 -*-
class AddNewMaterials < ActiveRecord::Migration
  def up
    Material.create!(name: 'краска', price: 0.2)
    Material.create!(name: 'доллар', price: 65)
    Material.create!(name: 'общая надбавка', price: 0)
  end

  def down
    Material.delete_all
  end
end
