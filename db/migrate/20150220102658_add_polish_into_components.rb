# -*- encoding : utf-8 -*-
class AddPolishIntoComponents < ActiveRecord::Migration
  def up
    add_column :components, :front_polish_id, :integer
    add_column :components, :back_polish_id, :integer

    Component.find_each do |component|
      component.front_polish_id = Polish.all.sample.id
      component.back_polish_id = Polish.all.sample.id

      component.save!
    end

    change_column :components, :front_polish_id, :integer, null: false
    change_column :components, :back_polish_id, :integer, null: false

    add_index :components, :front_polish_id
    add_index :components, :back_polish_id
  end

end
