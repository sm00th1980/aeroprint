# -*- encoding : utf-8 -*-
class FillCoverWorkOperations < ActiveRecord::Migration
  def up
    CoverWorkOperation.create!(name: 'ручной переплет', internal_name: 'manual', price: 13000.0)
    CoverWorkOperation.create!(name: 'машинный переплет', internal_name: 'automatic', price: 15000.0)
  end

  def down
    CoverWorkOperation.delete_all
  end
end
