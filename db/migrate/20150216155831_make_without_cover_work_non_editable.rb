# -*- encoding : utf-8 -*-
class MakeWithoutCoverWorkNonEditable < ActiveRecord::Migration
  def up
    CoverWorkType.find_by(name: 'Без переплётных работ').update_attribute(:editable, false)
  end
end
