# -*- encoding : utf-8 -*-
class FillManagerStatuses < ActiveRecord::Migration
  def up
    ManagerStatus.create!({name: 'Активный', internal_name: 'active'})
    ManagerStatus.create!({name: 'Заблокирован', internal_name: 'blocked'})
  end

  def down
    ManagerStatus.delete_all
  end
end
