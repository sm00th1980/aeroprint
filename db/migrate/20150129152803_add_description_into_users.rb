# -*- encoding : utf-8 -*-
class AddDescriptionIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :description, :text, null: true
  end
end
