# -*- encoding : utf-8 -*-
class FillDiscounts < ActiveRecord::Migration
  def up
    Discount.create!(name: 'Скидка 0%', value: 0)
    Discount.create!(name: 'Скидка 10%', value: 10)
    Discount.create!(name: 'Скидка 20%', value: 20)
  end
end
