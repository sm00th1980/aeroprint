# -*- encoding : utf-8 -*-
class DropCoverWorkFromComponents < ActiveRecord::Migration
  def up
    remove_column :components, :cover_work_id
  end
end
