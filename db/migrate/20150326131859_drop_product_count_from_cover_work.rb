# -*- encoding : utf-8 -*-
class DropProductCountFromCoverWork < ActiveRecord::Migration
  def up
    remove_column :cover_works, :product_count
  end
end
