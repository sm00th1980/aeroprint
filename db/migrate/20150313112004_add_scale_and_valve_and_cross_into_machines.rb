# -*- encoding : utf-8 -*-
class AddScaleAndValveAndCrossIntoMachines < ActiveRecord::Migration
  def change
    add_column :machines, :scale, :integer, null: false, default: 14
    add_column :machines, :valve, :integer, null: false, default: 14
    add_column :machines, :cross, :integer, null: false, default: 8
  end
end
