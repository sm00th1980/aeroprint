# -*- encoding : utf-8 -*-
class FillContractStatuses < ActiveRecord::Migration
  def up
    ContractStatus.create!(name: 'Primary', color: 'primary')
    ContractStatus.create!(name: 'Success', color: 'success')
    ContractStatus.create!(name: 'Info', color: 'info')
    ContractStatus.create!(name: 'Warning', color: 'warning')
    ContractStatus.create!(name: 'Danger', color: 'danger')
    ContractStatus.create!(name: 'Dark', color: 'dark')
  end

  def down
    ContractStatus.delete_all
  end
end
