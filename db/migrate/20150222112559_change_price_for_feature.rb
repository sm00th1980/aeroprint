# -*- encoding : utf-8 -*-
class ChangePriceForFeature < ActiveRecord::Migration
  def up
    paper_feature = PaperFeature.find_by(weight: 90, gor_sm: 64, ver_sm: 90)
    paper_feature.price_per_1_ton = 1802.66203
    paper_feature.save!
  end
end
