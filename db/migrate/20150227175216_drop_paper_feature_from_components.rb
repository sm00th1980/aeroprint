# -*- encoding : utf-8 -*-
class DropPaperFeatureFromComponents < ActiveRecord::Migration
  def up
    remove_column :components, :paper_feature_id
  end
end
