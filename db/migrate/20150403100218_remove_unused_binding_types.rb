# -*- encoding : utf-8 -*-
class RemoveUnusedBindingTypes < ActiveRecord::Migration
  def up
    BindingType.find_by(internal_name: 'spring').destroy if BindingType.find_by(internal_name: 'spring')
    BindingType.find_by(internal_name: 'valve').destroy if BindingType.find_by(internal_name: 'valve')
    BindingType.find_by(internal_name: 'hard_cover').destroy if BindingType.find_by(internal_name: 'hard_cover')
  end
end
