# -*- encoding : utf-8 -*-
class AddMachineIdIntoComponents < ActiveRecord::Migration
  def up
    add_column :components, :machine_id, :integer

    Component.find_each do |component|
      component.machine_id = Machine.all.sample.id
      component.save!
    end

    change_column :components, :machine_id, :integer, null: false
  end
end
