# -*- encoding : utf-8 -*-
class AddLaminationIntoCoverWorksTypes < ActiveRecord::Migration
  def up
    CoverWorkType.create!(name: 'обложка с ламинацией гл. 32 мкм', price: 1, editable: true, owner: CoverWorkOwner.component)
  end
end
