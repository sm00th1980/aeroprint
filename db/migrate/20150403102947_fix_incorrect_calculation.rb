# -*- encoding : utf-8 -*-
class FixIncorrectCalculation < ActiveRecord::Migration
  def up
    Calculation.find_by(id: 132).update_attribute(:saved, false) if Calculation.find_by(id: 132)
  end
end
