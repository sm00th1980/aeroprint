# -*- encoding : utf-8 -*-
class CreatePaperFeatures < ActiveRecord::Migration
  def change
    create_table :paper_features do |t|

      t.integer :paper_id, null: false

      t.integer :weight, null: false

      t.integer :gor_sm, null: false
      t.integer :ver_sm, null: false

      t.integer :gor_mm, null: false
      t.integer :ver_mm, null: false

      t.decimal  :weight_per_1000, precision: 15, scale: 2, null: false
      t.decimal  :price_per_1000, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end

    add_index :paper_features, :paper_id
  end
end
