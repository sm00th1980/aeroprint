# -*- encoding : utf-8 -*-
class AddKarton2IntoPaperFeatures < ActiveRecord::Migration
  def up
    PaperFeature.create!(weight: 300, gor_sm: 70, ver_sm: 100, gor_mm: 330, ver_mm: 482, price_per_1_ton: 1802.66, currency: Currency.find_by(internal_name: 'usa'), paper: Paper.find_by(name: 'Картон2'))
  end
end
