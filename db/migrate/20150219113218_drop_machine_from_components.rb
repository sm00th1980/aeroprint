# -*- encoding : utf-8 -*-
class DropMachineFromComponents < ActiveRecord::Migration
  def up
    remove_column :components, :machine_id
  end
end
