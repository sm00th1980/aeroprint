# -*- encoding : utf-8 -*-
class CreatePantoneColors < ActiveRecord::Migration
  def change
    create_table :pantone_colors do |t|
      t.integer :value, null: false
      
      t.timestamps null: false
    end

    add_index :pantone_colors, :value, unique: true
  end
end
