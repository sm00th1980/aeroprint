# -*- encoding : utf-8 -*-
class ChangePriceForPaperFeature < ActiveRecord::Migration
  def change
    paper_feature = PaperFeature.find_by(weight: 90, gor_sm: 64, ver_sm: 90)
    paper_feature.price_per_1_ton = 1796.6830985915493
    paper_feature.save!
  end
end
