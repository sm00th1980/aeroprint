# -*- encoding : utf-8 -*-
class CreateProductPaddings < ActiveRecord::Migration
  def change
    create_table :product_paddings do |t|
      t.integer :product_type_id, null: false
      t.integer :binding_type_id
      t.integer :ver_mm, null: false
      t.integer :gor_mm, null: false

      t.timestamps null: false
    end
  end
end
