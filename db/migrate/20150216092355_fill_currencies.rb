# -*- encoding : utf-8 -*-
class FillCurrencies < ActiveRecord::Migration
  def up
    Currency.create!(name: 'Доллар', internal_name: 'usa', exchange_rate: 65.0)
    Currency.create!(name: 'Евро', internal_name: 'europe', exchange_rate: 87.0)
    Currency.create!(name: 'Юань', internal_name: 'china', exchange_rate: 8.0)
  end
end
