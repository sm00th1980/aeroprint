# -*- encoding : utf-8 -*-
class FillComponentResults < ActiveRecord::Migration
  def up
    ComponentResult.delete_all

    Calculation.where(binding_type_id: nil).select { |c| c.product.type.many_page? }.each do |calculation|
      calculation.binding_type = BindingType.clip
      calculation.save!
    end

    Component.find_each do |component|
      component.calculate
    end
  end

  def down
    ComponentResult.delete_all
  end
end
