# -*- encoding : utf-8 -*-
class AddIndexIntoMachines < ActiveRecord::Migration
  def change
    add_index :machines, :format_id
  end
end
