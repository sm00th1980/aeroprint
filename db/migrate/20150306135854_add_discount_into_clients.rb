# -*- encoding : utf-8 -*-
class AddDiscountIntoClients < ActiveRecord::Migration
  def change
    add_column :clients, :discount_id, :integer

    Client.all.find_each do |client|
      client.discount_id = Discount.all.first.id
      client.save!
    end

    change_column :clients, :discount_id, :integer, null: false
  end
end
