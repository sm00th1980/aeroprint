# -*- encoding : utf-8 -*-
class FillBindingTypes < ActiveRecord::Migration
  def up
    BindingType.create!(name: 'на скрепку')
    BindingType.create!(name: 'КБС (клеевое)')
    BindingType.create!(name: 'сборка на пружину')
    BindingType.create!(name: 'сборка на клапана')
    BindingType.create!(name: 'твёрдый переплёт')
  end

  def down
    BindingType.delete_all
  end
end
