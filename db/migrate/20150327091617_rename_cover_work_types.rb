# -*- encoding : utf-8 -*-
class RenameCoverWorkTypes < ActiveRecord::Migration
  def up
    rename_table :cover_work_types, :cover_work_owners
    rename_column :cover_works, :type_id, :owner_id
  end

  def down
    rename_table :cover_work_owners, :cover_work_types
    rename_column :cover_works, :owner_id, :type_id
  end
end
