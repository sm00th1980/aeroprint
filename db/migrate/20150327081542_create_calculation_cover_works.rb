# -*- encoding : utf-8 -*-
class CreateCalculationCoverWorks < ActiveRecord::Migration
  def change
    create_table :calculation_cover_works do |t|
      t.integer :calculation_id, null: false
      t.integer :cover_work_id, null: false
      t.decimal :price, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end
  end
end
