# -*- encoding : utf-8 -*-
class CreateCoverWorkTypes < ActiveRecord::Migration
  def change
    create_table :cover_work_types do |t|
      t.string :name, null: false
      
      t.timestamps null: false
    end
  end
end
