# -*- encoding : utf-8 -*-
class FillCountOfCopiesIntoCoverWorkTypes < ActiveRecord::Migration
  def up
    CoverWorkType.find_each do |cover_work|
      cover_work.count_of_copies_per_hour = 350
      cover_work.save!
    end

    change_column :cover_work_types, :count_of_copies_per_hour, :integer, null: false
  end
end
