# -*- encoding : utf-8 -*-
class RemoveLamination < ActiveRecord::Migration
  def up
    lamination = CoverWorkType.find_by(name: 'обложка с ламинацией гл. 32 мкм')
    if lamination
      lamination.delete
    end
  end
end
