# -*- encoding : utf-8 -*-
class CreateCalculationUvPolishes < ActiveRecord::Migration
  def change
    create_table :calculation_uv_polishes do |t|
      t.integer :calculation_id, null: false
      t.integer :type_id, null: false
      t.integer :count_id, null: false
      t.integer :components, null: false, array: true

      t.timestamps null: false
    end
  end
end
