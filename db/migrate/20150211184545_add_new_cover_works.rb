# -*- encoding : utf-8 -*-
class AddNewCoverWorks < ActiveRecord::Migration
  def up
    CoverWorkType.create!(name: 'фальцовка машин.', price: 0.2)
    CoverWorkType.create!(name: 'биговка', price: 0.2)
    CoverWorkType.create!(name: 'перфорация', price: 0.2)
    CoverWorkType.create!(name: 'нумерация', price: 0.2)
    CoverWorkType.create!(name: 'скрепка до 40 пол.', price: 0.5)
    CoverWorkType.create!(name: 'скрепка до 60 пол.', price: 1)
    CoverWorkType.create!(name: 'скрепка до 76 пол.', price: 2)
    CoverWorkType.create!(name: 'склейка до 100 пол.', price: 6)
    CoverWorkType.create!(name: 'склейка до 200 пол.', price: 9)
  end

  def down
    CoverWorkType.delete_all
  end
end
