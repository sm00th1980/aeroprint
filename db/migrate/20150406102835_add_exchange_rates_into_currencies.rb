# -*- encoding : utf-8 -*-
class AddExchangeRatesIntoCurrencies < ActiveRecord::Migration
  def change
    add_column :currencies, :exchange_rates, :decimal, precision: 15, scale: 2, array: true

    Currency.find_each do |currency|
      currency.exchange_rates = [currency.exchange_rate, currency.exchange_rate * 2, currency.exchange_rate * 3, currency.exchange_rate * 4]
      currency.save!
    end

    change_column :currencies, :exchange_rates, :decimal, precision: 15, scale: 2, array: true, null: false
  end
end
