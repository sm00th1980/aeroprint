# -*- encoding : utf-8 -*-
class CreatePaperTypes < ActiveRecord::Migration
  def change
    create_table :paper_types do |t|
      t.string :name, null: false
      t.string :internal_name, null: false

      t.timestamps null: false
    end
  end
end
