# -*- encoding : utf-8 -*-
class RecalcAllComponents < ActiveRecord::Migration
  def up
    ComponentResult.delete_all

    Component.where(density: 80).find_each do |component|
      component.density = 90
      component.save!
    end

    Calculation.where(saved: true).find_each do |calculation|
      calculation.components.find_each do |component|
        component.calculate_all
      end
    end
  end
end
