# -*- encoding : utf-8 -*-
class FillComponents < ActiveRecord::Migration
  def up
    Calculation.find_each do |calculation|
      Component.create do |component|
        component.name = 'Обложка'
        component.calculation_id = calculation.id
        component.cover_work_id = CoverWorkType.without_cover_work.id
        component.paper_feature_id = PaperFeature.all.sample.id

        component.front_cmyk_color_id = CmykColor.all.sample.id
        component.front_pantone_color_id = PantoneColor.all.sample.id
        component.front_polish = [true, false].sample

        component.back_cmyk_color_id = CmykColor.all.sample.id
        component.back_pantone_color_id = PantoneColor.all.sample.id
        component.back_polish = [true, false].sample

        component.sheet_gor_mm = rand(50) + 200
        component.sheet_ver_mm = rand(50) + 200

        component.gor_padding_id = SheetPadding.all.sample.id
        component.ver_padding_id = SheetPadding.all.sample.id
      end
    end
  end

  def down
    Component.delete_all
  end
end
