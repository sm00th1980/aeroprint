# -*- encoding : utf-8 -*-
class CreatePaperProducers < ActiveRecord::Migration
  def change
    create_table :paper_producers do |t|
      t.string :name, null: false
      t.string :internal_name, null: false

      t.timestamps null: false
    end
  end
end
