# -*- encoding : utf-8 -*-
class FillUvPolishFormats < ActiveRecord::Migration
  def up
    UvPolishFormat.create!(name: 'A3', internal_name: 'A3')
    UvPolishFormat.create!(name: 'A3+', internal_name: 'A3_plus')
    UvPolishFormat.create!(name: 'A2', internal_name: 'A2')
    UvPolishFormat.create!(name: 'A2+', internal_name: 'A2_plus')
    UvPolishFormat.create!(name: 'A1', internal_name: 'A1')
  end

  def down
    UvPolishFormat.delete_all
  end
end
