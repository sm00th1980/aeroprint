# -*- encoding : utf-8 -*-
class DropPolishFromComponents < ActiveRecord::Migration
  def up
    remove_column :components, :front_polish
    remove_column :components, :back_polish
  end
end
