# -*- encoding : utf-8 -*-
class AddNotNullIntoIndexesResults < ActiveRecord::Migration
  def up
    change_column :component_results, :variable_exchange_rate_index, :integer, null: false
    change_column :component_results, :variable_form_price_index, :integer, null: false
    change_column :component_results, :variable_price_per_hour_index, :integer, null: false

    add_index :component_results, [:component_id, :variable_exchange_rate_index, :variable_form_price_index, :variable_price_per_hour_index], unique: true, name: 'indexes_unique'
  end
end
