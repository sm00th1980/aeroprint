# -*- encoding : utf-8 -*-
class CreateUvPolishCounts < ActiveRecord::Migration
  def change
    create_table :uv_polish_counts do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      
      t.timestamps null: false
    end
  end
end
