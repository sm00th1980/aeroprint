# -*- encoding : utf-8 -*-
class AddFrontComponentIdIntoComponents < ActiveRecord::Migration
  def change
    add_column :components, :frontend_component_id, :integer
  end
end
