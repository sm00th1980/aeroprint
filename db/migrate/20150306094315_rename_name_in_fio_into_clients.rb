# -*- encoding : utf-8 -*-
class RenameNameInFioIntoClients < ActiveRecord::Migration
  def change
    rename_column :clients, :name, :fio
  end
end
