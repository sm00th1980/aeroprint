# -*- encoding : utf-8 -*-
class DropPriceFromCalculations < ActiveRecord::Migration
  def up
    remove_column :calculations, :price
  end
end
