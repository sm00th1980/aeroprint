# -*- encoding : utf-8 -*-
class AddFormPricesIntoMachines < ActiveRecord::Migration
  def change
    add_column :machines, :form_prices, :decimal, precision: 15, scale: 2, array: true

    Machine.find_each do |machine|
      machine.form_prices = [machine.form_price, machine.form_price*1.5]
      machine.save!
    end

    change_column :machines, :form_prices, :decimal, precision: 15, scale: 2, array: true, null: false
  end
end
