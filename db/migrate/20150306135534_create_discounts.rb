# -*- encoding : utf-8 -*-
class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.string :name, null: false
      t.decimal :value, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end
  end
end
