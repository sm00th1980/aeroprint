# -*- encoding : utf-8 -*-
class RefillComponentResults < ActiveRecord::Migration
  def up
    ComponentResult.delete_all #clean old results

    Component.find_each do |component|
      component.calculate
    end

  end
end
