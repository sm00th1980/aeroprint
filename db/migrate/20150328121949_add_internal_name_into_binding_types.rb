# -*- encoding : utf-8 -*-
class AddInternalNameIntoBindingTypes < ActiveRecord::Migration
  def up
    add_column :binding_types, :internal_name, :string

    BindingType.find_by(name: 'на скрепку').update_attribute(:internal_name, :clip)
    BindingType.find_by(name: 'КБС (клеевое)').update_attribute(:internal_name, :glue)
    BindingType.find_by(name: 'сборка на пружину').update_attribute(:internal_name, :spring)
    BindingType.find_by(name: 'сборка на клапана').update_attribute(:internal_name, :valve)
    BindingType.find_by(name: 'твёрдый переплёт').update_attribute(:internal_name, :hard_cover)

    change_column :binding_types, :internal_name, :string, null: false

    add_index :binding_types, :internal_name, unique: true
  end

  def down
    remove_column :binding_types, :internal_name
  end
end
