# -*- encoding : utf-8 -*-
class AddUniqueIndexIntoManagerStatuses < ActiveRecord::Migration
  def change
    add_index :manager_statuses, [:internal_name], unique: true
  end
end
