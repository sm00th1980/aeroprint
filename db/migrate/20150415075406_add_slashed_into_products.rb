# -*- encoding : utf-8 -*-
class AddSlashedIntoProducts < ActiveRecord::Migration
  def change
    add_column :products, :slashed, :boolean, default: false, null: false
  end
end
