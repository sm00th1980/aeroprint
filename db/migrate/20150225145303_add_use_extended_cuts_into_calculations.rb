# -*- encoding : utf-8 -*-
class AddUseExtendedCutsIntoCalculations < ActiveRecord::Migration
  def change
    add_column :calculations, :use_extended_cuts, :boolean, null: false, default: false
  end
end
