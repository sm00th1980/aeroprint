# -*- encoding : utf-8 -*-
class CreateComponents < ActiveRecord::Migration
  def change
    create_table :components do |t|
      t.string :name, null: false
      t.integer :calculation_id, null: false
      t.integer :cover_work_id, null: false
      t.integer :paper_feature_id, null: false

      t.integer :front_cmyk_color_id, null: false
      t.integer :front_pantone_color_id, null: false
      t.boolean :front_polish, null: false, default: false

      t.integer :back_cmyk_color_id, null: false
      t.integer :back_pantone_color_id, null: false
      t.boolean :back_polish, null: false, default: false

      t.integer :sheet_gor_mm, null: false
      t.integer :sheet_ver_mm, null: false

      t.integer :gor_padding_id, null: false
      t.integer :ver_padding_id, null: false

      t.timestamps null: false
    end

    add_index :components, :cover_work_id
    add_index :components, :paper_feature_id

    add_index :components, :front_cmyk_color_id
    add_index :components, :front_pantone_color_id

    add_index :components, :back_cmyk_color_id
    add_index :components, :back_pantone_color_id

    add_index :components, :gor_padding_id
    add_index :components, :ver_padding_id
  end
end
