# -*- encoding : utf-8 -*-
class AddValueIntoUvPolishCounts < ActiveRecord::Migration
  def up
    add_column :uv_polish_counts, :value, :decimal, precision: 15, scale: 2, null: true
    UvPolishCount.one_side.update_attribute(:value, 1.0)
    UvPolishCount.both_side.update_attribute(:value, 2.0)

    change_column :uv_polish_counts, :value, :decimal, precision: 15, scale: 2, null: false
  end

  def down
    remove_column :uv_polish_counts, :value
  end
end
