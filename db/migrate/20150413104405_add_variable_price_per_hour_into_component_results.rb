# -*- encoding : utf-8 -*-
class AddVariablePricePerHourIntoComponentResults < ActiveRecord::Migration
  def change
    add_column :component_results, :variable_price_per_hour, :decimal, precision: 15, scale: 2
  end
end
