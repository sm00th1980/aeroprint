# -*- encoding : utf-8 -*-
class AddCountryIntoPapers < ActiveRecord::Migration
  def up
    add_column :papers, :country_id, :integer

    Paper.all.find_each do |paper|
      paper.country_id = PaperCountry.all[rand(3)].id

      paper.save!
    end

    change_column :papers, :country_id, :integer, null: false

    add_index :papers, [:country_id]
  end
end
