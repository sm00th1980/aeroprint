# -*- encoding : utf-8 -*-
class AddUvPolishFormatIntoPaperFeatures < ActiveRecord::Migration
  def up
    add_column :paper_features, :uv_polish_format_id, :integer

    PaperFeature.find_each do |feature|
      feature.uv_polish_format_id = UvPolishFormat.all.sample.id
      feature.save!
    end

    change_column :paper_features, :uv_polish_format_id, :integer, null: false
    add_index :paper_features, :uv_polish_format_id
  end

  def down
    remove_column :paper_features, :uv_polish_format_id
  end
end
