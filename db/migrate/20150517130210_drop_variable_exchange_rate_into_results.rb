# -*- encoding : utf-8 -*-
class DropVariableExchangeRateIntoResults < ActiveRecord::Migration
  def up
    remove_column :component_results, :variable_exchange_rate_index
  end
end
