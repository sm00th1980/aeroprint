# -*- encoding : utf-8 -*-
class FillPantoneColors < ActiveRecord::Migration
  def up
    PantoneColor.create!(value: 0)
    PantoneColor.create!(value: 1)
    PantoneColor.create!(value: 2)
    PantoneColor.create!(value: 3)
    PantoneColor.create!(value: 4)
  end

  def down
    PantoneColor.delete_all
  end
end
