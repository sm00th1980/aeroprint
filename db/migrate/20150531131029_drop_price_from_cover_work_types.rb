# -*- encoding : utf-8 -*-
class DropPriceFromCoverWorkTypes < ActiveRecord::Migration
  def up
    remove_column :cover_work_types, :price
  end
end
