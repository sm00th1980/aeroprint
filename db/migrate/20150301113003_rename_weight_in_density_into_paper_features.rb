# -*- encoding : utf-8 -*-
class RenameWeightInDensityIntoPaperFeatures < ActiveRecord::Migration
  def change
    rename_column :paper_features, :weight, :density
  end
end
