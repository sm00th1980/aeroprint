# -*- encoding : utf-8 -*-
class AddOperationIntoCoverWorkTypes < ActiveRecord::Migration
  def up
    add_column :cover_work_types, :operation_id, :integer

    CoverWorkType.find_each do |c|
      c.operation_id = CoverWorkOperation.all.sample.id
      c.save!
    end

    #change_column :cover_work_types, :operation_id, :integer, null: false
  end

  def down
    remove_column :cover_work_types, :operation_id, :integer
  end
end
