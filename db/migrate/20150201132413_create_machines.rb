# -*- encoding : utf-8 -*-
class CreateMachines < ActiveRecord::Migration
  def change
    create_table :machines do |t|
      t.string :name, null: false
      t.integer :efficiency, null: false
      t.integer :color_count, null: false
      t.float :preparation_time, null: false
      t.float :additional_time, null: false
      t.decimal  :price_per_hour, precision: 15, scale: 2, null: false
      t.integer :form, null: false
      t.integer :skin, null: false
      t.integer :max_x, null: false
      t.integer :max_y, null: false

      t.timestamps null: false
    end
  end
end
