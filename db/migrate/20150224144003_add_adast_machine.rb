# -*- encoding : utf-8 -*-
class AddAdastMachine < ActiveRecord::Migration
  def up
    Machine.create!(name: 'ADAST ROMAIER', efficiency: 2000, color_count: 1, preparation_time: 0.1, additional_time: 0.1,
                    price_per_hour: 500, form_price: 70, skin: 135, max_x: 347, max_y: 347, format: MachineFormat.find_by(internal_name: 'A3'))
  end
end
