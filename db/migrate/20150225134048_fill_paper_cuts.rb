# -*- encoding : utf-8 -*-
class FillPaperCuts < ActiveRecord::Migration
  def up
    PaperCut.delete_all

    man = Machine.find_by(name: 'MAN')
    ryobi = Machine.find_by(name: 'RYOBI')

    PaperFeature.all.find_each do |paper_feature|
      PaperCut.create!(paper_feature_id: paper_feature.id, machine_id: man.id, gor_mm: 602, ver_mm: 440, chunk_count: 2)
      PaperCut.create!(paper_feature_id: paper_feature.id, machine_id: ryobi.id, gor_mm: 290, ver_mm: 452, chunk_count: 4)
      PaperCut.create!(paper_feature_id: paper_feature.id, machine_id: man.id, gor_mm: 602, ver_mm: 277, chunk_count: 3)
      PaperCut.create!(paper_feature_id: paper_feature.id, machine_id: ryobi.id, gor_mm: 290, ver_mm: 295, chunk_count: 6)
    end

  end
end
