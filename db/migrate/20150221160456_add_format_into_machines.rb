# -*- encoding : utf-8 -*-
class AddFormatIntoMachines < ActiveRecord::Migration
  def up
    add_column :machines, :format_id, :integer

    man = Machine.find_by(name: 'MAN')
    man.format_id = MachineFormat.find_by(internal_name: 'A2').id
    man.save!

    ryobi = Machine.find_by(name: 'RYOBI')
    ryobi.format_id = MachineFormat.find_by(internal_name: 'A3').id
    ryobi.save!

    change_column :machines, :format_id, :integer, null: false
  end
end
