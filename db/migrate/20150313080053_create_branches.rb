# -*- encoding : utf-8 -*-
class CreateBranches < ActiveRecord::Migration
  def change
    create_table :branches do |t|
      t.integer "client_id", null: false
      t.string "name", default: "", null: false
      t.text "description"
      t.string "phone"
      t.string "email"
      t.string "contract"
      t.string "payer"
      t.integer "discount_id", null: false
      t.text "shipping_address"

      t.timestamps null: false
    end

    add_index :branches, :client_id
    add_index :branches, :discount_id
  end
end
