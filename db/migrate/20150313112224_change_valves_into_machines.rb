# -*- encoding : utf-8 -*-
class ChangeValvesIntoMachines < ActiveRecord::Migration
  def up
    Machine.man.update_attribute(:scale, 14)
    Machine.man.update_attribute(:cross, 8)
    Machine.man.update_attribute(:valve, 14)

    Machine.ryobi.update_attribute(:scale, 7)
    Machine.ryobi.update_attribute(:cross, 8)
    Machine.ryobi.update_attribute(:valve, 11)
  end
end
