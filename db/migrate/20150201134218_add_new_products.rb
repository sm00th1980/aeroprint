# -*- encoding : utf-8 -*-
class AddNewProducts < ActiveRecord::Migration
  def up
    Product.create!(:name => 'Листовка 1')
    Product.create!(:name => 'Листовка 2')
    Product.create!(:name => 'Листовка 3')
    Product.create!(:name => 'Листовка 4')
  end

  def down
    Product.delete_all
  end
end
