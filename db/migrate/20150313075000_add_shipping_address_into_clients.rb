# -*- encoding : utf-8 -*-
class AddShippingAddressIntoClients < ActiveRecord::Migration
  def change
    add_column :clients, :shipping_address, :text
  end
end
