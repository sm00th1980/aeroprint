# -*- encoding : utf-8 -*-
class CreateUvPolishFormats < ActiveRecord::Migration
  def change
    create_table :uv_polish_formats do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      
      t.timestamps null: false
    end
  end
end
