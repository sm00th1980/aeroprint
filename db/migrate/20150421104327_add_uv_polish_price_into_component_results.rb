# -*- encoding : utf-8 -*-
class AddUvPolishPriceIntoComponentResults < ActiveRecord::Migration
  def up
    add_column :component_results, :uv_polish_price, :decimal, precision: 15, scale: 2, null: true

    ComponentResult.find_each do |result|
      result.uv_polish_price = 0
      result.save!
    end

    change_column :component_results, :uv_polish_price, :decimal, precision: 15, scale: 2, null: false
  end

  def down
    remove_column :component_results, :uv_polish_price
  end
end
