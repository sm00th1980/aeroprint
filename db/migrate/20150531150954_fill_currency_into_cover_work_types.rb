# -*- encoding : utf-8 -*-
class FillCurrencyIntoCoverWorkTypes < ActiveRecord::Migration
  def up
    CoverWorkType.find_each do |c|
      c.currency_id = CoverWorkCurrency.all.sample.id
      c.save!
    end

    change_column :cover_work_types, :currency_id, :integer, null: false
  end
end
