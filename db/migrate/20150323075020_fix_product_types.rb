# -*- encoding : utf-8 -*-
class FixProductTypes < ActiveRecord::Migration
  def up
    lists = ['Листовка', 'Открытка', 'Календарь', 'Бланк']
    many_pages = ['Журнал', 'Буклет']

    lists.each do |product_name|
      product = Product.find_by(name: product_name)
      product.type = ProductType.find_by(internal_name: :list)
      product.save!
    end

    many_pages.each do |product_name|
      product = Product.find_by(name: product_name)
      product.type = ProductType.find_by(internal_name: :many_page)
      product.save!
    end
  end
end
