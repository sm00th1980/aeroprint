# -*- encoding : utf-8 -*-
class AddCurrencyIntoPaperFeatures < ActiveRecord::Migration
  def up
    add_column :paper_features, :currency_id, :integer

    PaperFeature.find_each do |paper_feature|
      paper_feature.currency_id = Currency.usa.id
      paper_feature.save!
    end

    change_column :paper_features, :currency_id, :integer, null: false

    add_index :paper_features, :currency_id
  end
end
