# -*- encoding : utf-8 -*-
class AddNewProducts20150219 < ActiveRecord::Migration
  def up
    Product.delete_all

    Product.create!(name: 'Листовка')
    Product.create!(name: 'Журнал')
    Product.create!(name: 'Открытка')
  end
end
