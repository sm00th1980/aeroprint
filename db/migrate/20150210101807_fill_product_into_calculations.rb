# -*- encoding : utf-8 -*-
class FillProductIntoCalculations < ActiveRecord::Migration
  def up
    Calculation.all.find_each do |calc|
      calc.product_id = Product.first.id
      calc.name = Faker::Commerce.department
      calc.save!
    end

    change_column :calculations, :product_id, :integer, null: false
  end
end
