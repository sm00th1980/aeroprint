# -*- encoding : utf-8 -*-
class AddIndexIntoDiscounts < ActiveRecord::Migration
  def change
    add_index :discounts, :name, unique: true
    add_index :discounts, :value, unique: true
  end
end
