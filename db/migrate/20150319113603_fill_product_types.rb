# -*- encoding : utf-8 -*-
class FillProductTypes < ActiveRecord::Migration
  def up
    ProductType.create!(name: 'Листовая')
    ProductType.create!(name: 'Многостраничная')
  end

  def down
    ProductType.delete_all
  end
end
