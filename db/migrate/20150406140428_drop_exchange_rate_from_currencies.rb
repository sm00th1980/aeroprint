# -*- encoding : utf-8 -*-
class DropExchangeRateFromCurrencies < ActiveRecord::Migration
  def up
    remove_column :currencies, :exchange_rate
  end
end
