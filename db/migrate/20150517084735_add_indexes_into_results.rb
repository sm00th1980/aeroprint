# -*- encoding : utf-8 -*-
class AddIndexesIntoResults < ActiveRecord::Migration
  def change
    add_column :component_results, :variable_exchange_rate_index, :integer
    add_column :component_results, :variable_form_price_index, :integer
    add_column :component_results, :variable_price_per_hour_index, :integer
  end
end
