# -*- encoding : utf-8 -*-
class AddPricePer1TonIntoFeatures < ActiveRecord::Migration
  def up
    add_column :paper_features, :price_per_1_ton, :decimal, precision: 15, scale: 2

    PaperFeature.find_each do |paper_feature|
      paper_feature.price_per_1_ton = 100
      paper_feature.save!
    end

    change_column :paper_features, :price_per_1_ton, :decimal, precision: 15, scale: 2, null: false
  end
end
