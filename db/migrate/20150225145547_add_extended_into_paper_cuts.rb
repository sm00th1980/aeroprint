# -*- encoding : utf-8 -*-
class AddExtendedIntoPaperCuts < ActiveRecord::Migration
  def change
    add_column :paper_cuts, :extended, :boolean, null: false, default: false
  end
end
