# -*- encoding : utf-8 -*-
class AddNewIndexIntoComponents < ActiveRecord::Migration
  def change
    add_index :components, :calculation_id
    add_index :components, :paper_id
  end
end
