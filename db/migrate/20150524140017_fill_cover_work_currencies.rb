# -*- encoding : utf-8 -*-
class FillCoverWorkCurrencies < ActiveRecord::Migration
  def up
    CoverWorkCurrency.create!(name: 'Курс USD', internal_name: 'usd', exchange_rate: 50.0)
    CoverWorkCurrency.create!(name: 'Курс Евро', internal_name: 'euro', exchange_rate: 54.0)
  end

  def down
    CoverWorkCurrency.delete_all
  end
end
