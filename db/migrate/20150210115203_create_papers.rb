# -*- encoding : utf-8 -*-
class CreatePapers < ActiveRecord::Migration
  def change
    create_table :papers do |t|
      t.string :name, null: false
      t.integer :type_id, null: false
      t.integer :producer_id, null: false

      t.timestamps null: false
    end
  end
end
