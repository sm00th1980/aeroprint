# -*- encoding : utf-8 -*-
class AddIndexIntoClients < ActiveRecord::Migration
  def change
    add_index :clients, :manager_id
  end
end
