# -*- encoding : utf-8 -*-
class RenameSheetsIntoComponents < ActiveRecord::Migration
  def up
    rename_column :components, :sheet_gor_mm, :product_gor_mm
    rename_column :components, :sheet_ver_mm, :product_ver_mm
  end

  def down
    rename_column :components, :product_gor_mm, :sheet_gor_mm
    rename_column :components, :product_ver_mm, :sheet_ver_mm
  end
end
