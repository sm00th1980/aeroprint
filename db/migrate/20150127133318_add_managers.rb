# -*- encoding : utf-8 -*-
class AddManagers < ActiveRecord::Migration
  def up

    add_column :users, :fio, :text

    User.managers.delete_all

    User.create!({email: "manager1@aero-print.ru", password: "jBV6z8E9", password_confirmation: "jBV6z8E9", fio: "Иванов", admin: false})
    User.create!({email: "manager2@aero-print.ru", password: "kKZsZ8ft", password_confirmation: "kKZsZ8ft", fio: "Петров", admin: false})
    User.create!({email: "manager3@aero-print.ru", password: "Jyey5w8A", password_confirmation: "Jyey5w8A", fio: "Сидоров", admin: false})
    User.create!({email: "manager4@aero-print.ru", password: "wG7jqFWc", password_confirmation: "wG7jqFWc", fio: "Петренко", admin: false})
    User.create!({email: "manager5@aero-print.ru", password: "rGmb54BZ", password_confirmation: "rGmb54BZ", fio: "Капица", admin: false})
  end

  def down
    User.managers.delete_all
  end
end
