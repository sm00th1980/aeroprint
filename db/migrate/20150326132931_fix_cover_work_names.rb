# -*- encoding : utf-8 -*-
class FixCoverWorkNames < ActiveRecord::Migration
  def up
    CoverWorkType.find_by(name: 'фальцовка машин.').update_attribute(:name, 'фальцовка машин')

    CoverWorkType.find_by(name: 'скрепка до 40 пол.').update_attribute(:name, 'скрепка до 40 полос')
    CoverWorkType.find_by(name: 'скрепка до 60 пол.').update_attribute(:name, 'скрепка до 60 полос')
    CoverWorkType.find_by(name: 'скрепка до 76 пол.').update_attribute(:name, 'скрепка до 76 полос')
    CoverWorkType.find_by(name: 'склейка до 100 пол.').update_attribute(:name, 'скрепка до 100 полос')
    CoverWorkType.find_by(name: 'склейка до 200 пол.').update_attribute(:name, 'скрепка до 200 полос')

    CoverWorkType.find_by(name: 'Без переплётных работ').delete
  end
end
