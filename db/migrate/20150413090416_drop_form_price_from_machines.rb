# -*- encoding : utf-8 -*-
class DropFormPriceFromMachines < ActiveRecord::Migration
  def up
    remove_column :machines, :form_price
  end
end
