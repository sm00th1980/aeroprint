# -*- encoding : utf-8 -*-
class AddUniqIndexIntoPaperProducers < ActiveRecord::Migration
  def change
    add_index :paper_producers, [:internal_name], unique: true
  end
end
