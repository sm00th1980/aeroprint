# -*- encoding : utf-8 -*-
class AddProductIntoCalculations < ActiveRecord::Migration
  def change
    add_column :calculations, :product_id, :integer
    add_column :calculations, :name, :string
  end
end
