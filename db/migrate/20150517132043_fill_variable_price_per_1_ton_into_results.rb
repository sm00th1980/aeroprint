# -*- encoding : utf-8 -*-
class FillVariablePricePer1TonIntoResults < ActiveRecord::Migration
  def change
    add_column :component_results, :base_price_per_1_ton, :boolean, null: false, default: true

    ComponentResult.delete_all

    Component.where(density: 90).find_each do |component|
      component.density = 80
      component.save!
    end

    Calculation.where(saved: true).find_each do |calculation|
      calculation.components.find_each do |component|
        component.calculate_all
      end
    end
  end
end
