# -*- encoding : utf-8 -*-
class DropPaddingFromComponents < ActiveRecord::Migration
  def up
    remove_column :components, :gor_padding_id
    remove_column :components, :ver_padding_id
  end
end
