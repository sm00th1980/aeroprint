# -*- encoding : utf-8 -*-
class AddUniqIndexIntoPaperTypes < ActiveRecord::Migration
  def change
    add_index :paper_types, [:internal_name], unique: true
  end
end
