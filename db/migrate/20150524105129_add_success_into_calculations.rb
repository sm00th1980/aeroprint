# -*- encoding : utf-8 -*-
class AddSuccessIntoCalculations < ActiveRecord::Migration
  def up
    add_column :calculations, :success, :boolean, default: false, null: false

    Calculation.find_each do |c|
      c.success = true
      c.save!
    end
  end

  def down
    remove_column :calculations, :success
  end
end
