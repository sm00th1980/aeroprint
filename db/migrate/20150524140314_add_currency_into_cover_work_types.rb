# -*- encoding : utf-8 -*-
class AddCurrencyIntoCoverWorkTypes < ActiveRecord::Migration
  def up
    #
    add_column :cover_work_types, :source_material_price, :decimal, precision: 15, scale: 5

    CoverWorkType.find_each do |cover_work|
      cover_work.source_material_price = 0.0001
      cover_work.save!
    end

    change_column :cover_work_types, :source_material_price, :decimal, precision: 15, scale: 5, null: false

    #
    add_column :cover_work_types, :count_of_copies_per_hour, :integer

    CoverWorkType.find_each do |cover_work|
      cover_work.count_of_copies_per_hour = 350
      cover_work.save!
    end

    #change_column :cover_work_types, :count_of_copies_per_hour, :integer, null: false

    #
    add_column :cover_work_types, :currency_id, :integer

    CoverWorkType.find_each do |c|
      c.currency_id = CoverWorkCurrency.all.sample.id
      c.save!
    end

    #change_column :cover_work_types, :currency_id, :integer, null: false
  end

  def down
    remove_column :cover_work_types, :currency_id, :integer
    remove_column :cover_work_types, :count_of_copies_per_hour, :integer
    remove_column :cover_work_types, :source_material_price, :integer
  end
end
