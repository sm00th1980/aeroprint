# -*- encoding : utf-8 -*-
class AddNotNullForVariablePricePer1TonIntoResults < ActiveRecord::Migration
  def up
    change_column :component_results, :variable_price_per_1_ton, :decimal, precision: 15, scale: 2, null: false
    add_index :component_results, [:component_id, :variable_form_price_index, :variable_price_per_hour_index, :variable_price_per_1_ton], unique: true, name: 'indexes_unique'
  end
end
