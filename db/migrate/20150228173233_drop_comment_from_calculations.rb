# -*- encoding : utf-8 -*-
class DropCommentFromCalculations < ActiveRecord::Migration
  def up
    remove_column :calculations, :comment
  end
end
