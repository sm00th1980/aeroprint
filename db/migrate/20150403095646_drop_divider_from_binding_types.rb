# -*- encoding : utf-8 -*-
class DropDividerFromBindingTypes < ActiveRecord::Migration
  def up
    remove_column :binding_types, :divider
  end
end
