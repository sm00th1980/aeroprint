# -*- encoding : utf-8 -*-
class CreateMaterials < ActiveRecord::Migration
  def change
    create_table :materials do |t|
      t.string :name, null: false
      t.decimal  :price, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end
  end
end
