# -*- encoding : utf-8 -*-
class FillCmykColors < ActiveRecord::Migration
  def up
    CmykColor.create!(value: 0)
    CmykColor.create!(value: 1)
    CmykColor.create!(value: 2)
    CmykColor.create!(value: 3)
    CmykColor.create!(value: 4)
  end

  def down
    CmykColor.delete_all
  end
end
