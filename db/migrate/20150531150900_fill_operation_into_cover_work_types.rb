# -*- encoding : utf-8 -*-
class FillOperationIntoCoverWorkTypes < ActiveRecord::Migration
  def up
    CoverWorkType.find_each do |c|
      c.operation_id = CoverWorkOperation.all.sample.id
      c.save!
    end

    change_column :cover_work_types, :operation_id, :integer, null: false
  end
end
