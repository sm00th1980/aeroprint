# -*- encoding : utf-8 -*-
class AddPhoneIntoClients < ActiveRecord::Migration
  def change
    add_column :clients, :phone, :string
    add_column :clients, :email, :string
  end
end
