# -*- encoding : utf-8 -*-
class AddMaterialPriceIntoAdditionals < ActiveRecord::Migration
  def up
    Additional.create!(name: 'Стомость материалов', internal_name: 'material_price', value: 0.2)
  end

  def down
    Additional.find_by(internal_name: 'material_price').delete
  end
end
