# -*- encoding : utf-8 -*-
class CreateSpecifications < ActiveRecord::Migration
  def change
    create_table :specifications do |t|
      t.integer :client_id, null: false
      t.string :number
      t.date :date

      t.timestamps null: false
    end

    add_index :specifications, :client_id
  end
end
