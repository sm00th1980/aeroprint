# -*- encoding : utf-8 -*-
class CreateComponentResults < ActiveRecord::Migration
  def change
    create_table :component_results do |t|
      t.integer :component_id, null: false
      t.decimal :exchange_rate, precision: 15, scale: 2, null: false
      t.integer :paper_expense, null: false
      t.decimal :paper_price, precision: 15, scale: 2, null: false
      t.float :print_time, null: false
      t.float :print_run_weight, null: false
      t.decimal :form_price, precision: 15, scale: 2, null: false
      t.decimal :material_price, precision: 15, scale: 2, null: false
      t.decimal :print_run_price, precision: 15, scale: 2, null: false
      t.decimal :print_price, precision: 15, scale: 2, null: false
      t.integer :imposition, null: false
      t.string :machine, null: false
      t.string :chosen_paper, null: false
      t.string :front_colors, null: false
      t.string :back_colors, null: false
      t.decimal :print_run_price_without_nds, precision: 15, scale: 2, null: false
      t.decimal :price_per_unit, precision: 15, scale: 2, null: false
      t.decimal :price_per_unit_without_nds, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end
  end
end
