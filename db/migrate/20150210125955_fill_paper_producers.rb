# -*- encoding : utf-8 -*-
class FillPaperProducers < ActiveRecord::Migration
  def up
    PaperProducer.create!(name: 'Берег', internal_name: 'bereg')
    PaperProducer.create!(name: 'Бумажник', internal_name: 'bumajnik')
  end

  def down
    PaperProducer.delete_all
  end
end
