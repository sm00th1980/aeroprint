# -*- encoding : utf-8 -*-
class RenameExchangeRateIntoComponentResults < ActiveRecord::Migration
  def change
    rename_column :component_results, :exchange_rate, :variable_exchange_rate
  end
end
