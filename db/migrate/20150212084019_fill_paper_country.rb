# -*- encoding : utf-8 -*-
class FillPaperCountry < ActiveRecord::Migration
  def up
    PaperCountry.create!(name: 'Россия', internal_name: 'russia')
    PaperCountry.create!(name: 'Китай', internal_name: 'china')
    PaperCountry.create!(name: 'Европа', internal_name: 'europe')
  end

  def down
    PaperCountry.delete_all
  end
end
