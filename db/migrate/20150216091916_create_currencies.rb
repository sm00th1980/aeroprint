# -*- encoding : utf-8 -*-
class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      t.decimal  :exchange_rate, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end

    add_index :currencies, [:internal_name], unique: true
  end
end
