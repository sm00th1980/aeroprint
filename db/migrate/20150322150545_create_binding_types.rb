# -*- encoding : utf-8 -*-
class CreateBindingTypes < ActiveRecord::Migration
  def change
    create_table :binding_types do |t|
      t.string :name, null: false
      
      t.timestamps null: false
    end
  end
end
