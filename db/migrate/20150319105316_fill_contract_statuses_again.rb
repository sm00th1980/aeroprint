# -*- encoding : utf-8 -*-
class FillContractStatusesAgain < ActiveRecord::Migration
  def up
    ContractStatus.delete_all

    ContractStatus.create!(name: 'Договор расторгнут', color: 'primary')
    ContractStatus.create!(name: 'Договор подписан', color: 'success')
    ContractStatus.create!(name: 'Договор на подписи', color: 'warning')

    Client.all.each do |client|
      client.contract_status = ContractStatus.all.sample
      client.save!
    end
  end

  def down
    ContractStatus.delete_all
  end
end
