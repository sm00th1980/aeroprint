# -*- encoding : utf-8 -*-
class AddIndexIntoPapers < ActiveRecord::Migration
  def change
    add_index :papers, :type_id
    add_index :papers, :producer_id
  end
end
