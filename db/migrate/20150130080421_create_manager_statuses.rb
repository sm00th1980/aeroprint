# -*- encoding : utf-8 -*-
class CreateManagerStatuses < ActiveRecord::Migration
  def change
    create_table :manager_statuses do |t|
      t.string :name, null: false
      t.string :internal_name, null: false

      t.timestamps null: false
    end
  end
end
