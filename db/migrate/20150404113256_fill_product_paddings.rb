# -*- encoding : utf-8 -*-
class FillProductPaddings < ActiveRecord::Migration
  def up
    ProductPadding.delete_all

    ProductPadding.create!(product_type_id: ProductType.list.id, binding_type_id: nil, gor_mm: 1, ver_mm: 1)
    ProductPadding.create!(product_type_id: ProductType.many_page.id, binding_type_id: BindingType.clip.id, gor_mm: 3, ver_mm: 1)
    ProductPadding.create!(product_type_id: ProductType.many_page.id, binding_type_id: BindingType.glue.id, gor_mm: 5, ver_mm: 2)
  end

  def down
    ProductPadding.delete_all
  end
end
