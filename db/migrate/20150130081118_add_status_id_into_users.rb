# -*- encoding : utf-8 -*-
class AddStatusIdIntoUsers < ActiveRecord::Migration
  def up
    add_column :users, :status_id, :integer

    User.all.find_each do |user|
      user.status = ManagerStatus.active
      user.save!
    end

    change_column :users, :status_id, :integer, null: false

    add_index :users, :status_id
  end

  def down
    remove_column :users, :status_id
  end
end
