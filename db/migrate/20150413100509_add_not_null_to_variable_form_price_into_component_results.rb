# -*- encoding : utf-8 -*-
class AddNotNullToVariableFormPriceIntoComponentResults < ActiveRecord::Migration
  def up
    change_column :component_results, :variable_form_price, :decimal, precision: 15, scale: 2, null: false
  end
end
