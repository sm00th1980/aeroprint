# -*- encoding : utf-8 -*-
class AddFormPriceIntoMachines < ActiveRecord::Migration
  def change
    add_column :machines, :form_price, :decimal, precision: 15, scale: 2

    Machine.find_each do |machine|
      machine.form_price = machine.form.to_f
      machine.save!
    end

    remove_column :machines, :form
    change_column :machines, :form_price, :decimal, precision: 15, scale: 2, null: false
  end
end
