# -*- encoding : utf-8 -*-
class AddNds < ActiveRecord::Migration
  def up
    Additional.create(name: 'НДС(в процентах)', internal_name: 'nds', value: 18)
  end
end
