# -*- encoding : utf-8 -*-
class AddBindingTypeIdIntoCalculation < ActiveRecord::Migration
  def change
    add_column :calculations, :binding_type_id, :integer, null: true
  end
end
