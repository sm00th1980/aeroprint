# -*- encoding : utf-8 -*-
class AddNewMachines < ActiveRecord::Migration
  def up
    Machine.create!(name: 'MAN', efficiency: 6000, color_count: 4, preparation_time: 0.1, additional_time: 0.3,
                    price_per_hour: 123, form: 540, skin: 0, max_x: 718, max_y: 718)

    Machine.create!(name: 'RYOBI', efficiency: 4500, color_count: 5, preparation_time: 0.3, additional_time: 0.6,
                    price_per_hour: 256, form: 70, skin: 135, max_x: 347, max_y: 347)
  end

  def down
    Machine.delete_all
  end
end
