# -*- encoding : utf-8 -*-
class FillUvPolishTypes < ActiveRecord::Migration
  def up
    UvPolishType.create!(name: 'Матовый', internal_name: 'opaque')
    UvPolishType.create!(name: 'Глянцевый', internal_name: 'glossy')
  end
end
