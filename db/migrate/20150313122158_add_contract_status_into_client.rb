# -*- encoding : utf-8 -*-
class AddContractStatusIntoClient < ActiveRecord::Migration
  def up
    add_column :clients, :contract_status_id, :integer

    Client.all.find_each do |client|
      client.contract_status_id = ContractStatus.pluck(:id).sample
      client.save!
    end

    change_column :clients, :contract_status_id, :integer, null: false
  end
end
