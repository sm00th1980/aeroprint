# -*- encoding : utf-8 -*-
class AddContractIntoClients < ActiveRecord::Migration
  def change
    add_column :clients, :contract, :string
  end
end
