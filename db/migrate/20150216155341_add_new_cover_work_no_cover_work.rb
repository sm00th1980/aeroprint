# -*- encoding : utf-8 -*-
class AddNewCoverWorkNoCoverWork < ActiveRecord::Migration
  def up
    CoverWorkType.create(name: 'Без переплётных работ', price: 0, product_count: 0)
  end
end
