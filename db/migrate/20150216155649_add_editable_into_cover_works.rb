# -*- encoding : utf-8 -*-
class AddEditableIntoCoverWorks < ActiveRecord::Migration
  def change
    add_column :cover_works, :editable, :boolean, null: false, default: true
  end
end
