# -*- encoding : utf-8 -*-
require 'faker'

class AddNewCalculations < ActiveRecord::Migration
  def up
    Calculation.delete_all

    User.managers.each do |manager|
      manager.clients.each do |client|
        5.times{Calculation.create!({comment: Faker::Lorem.paragraph, client: client})}
      end
    end

  end

  def down
    Calculation.delete_all
  end
end
