# -*- encoding : utf-8 -*-
class FillExtendedIntoCuts < ActiveRecord::Migration
  def up
    PaperCut.where(chunk_count: [3, 6]).find_each do |paper_cut|
      paper_cut.extended = true
      paper_cut.save!
    end
  end
end
