# -*- encoding : utf-8 -*-
class CreateMachineFormats < ActiveRecord::Migration
  def change
    create_table :machine_formats do |t|
      t.string :name, null: false
      t.string :internal_name, null: false

      t.timestamps null: false
    end
    add_index :machine_formats, [:internal_name], unique: true
  end
end
