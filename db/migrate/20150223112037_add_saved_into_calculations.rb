# -*- encoding : utf-8 -*-
class AddSavedIntoCalculations < ActiveRecord::Migration
  def change
    add_column :calculations, :saved, :boolean, null: false, default: false
  end
end
