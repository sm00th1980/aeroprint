# -*- encoding : utf-8 -*-
class AddStripesCountIntoComponents < ActiveRecord::Migration

  def up
    add_column :components, :stripes_count, :integer

    Component.all.each do |c|
      c.stripes_count = 1
      c.save!
    end

    change_column :components, :stripes_count, :integer, null: false
  end

  def down
    remove_column :components, :stripes_count
  end

end
