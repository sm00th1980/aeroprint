# -*- encoding : utf-8 -*-
class AddPayerIntoClients < ActiveRecord::Migration
  def change
    add_column :clients, :payer, :string
  end
end
