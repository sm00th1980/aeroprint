# -*- encoding : utf-8 -*-
class AddDividerIntoBindingTypes < ActiveRecord::Migration
  def up
    add_column :binding_types, :divider, :integer
    BindingType.all.find_each do |b|
      b.divider = 1
      b.save!
    end
    BindingType.clip.update_attribute(:divider, 4)

    change_column :binding_types, :divider, :integer, null: false
  end

  def down
    remove_column :binding_types, :divider
  end
end
