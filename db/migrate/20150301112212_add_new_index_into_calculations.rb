# -*- encoding : utf-8 -*-
class AddNewIndexIntoCalculations < ActiveRecord::Migration
  def change
    add_index :calculations, :product_id
  end
end
