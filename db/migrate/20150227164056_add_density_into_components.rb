# -*- encoding : utf-8 -*-
class AddDensityIntoComponents < ActiveRecord::Migration
  def up
    Calculation.delete_all
    Component.delete_all

    add_column :components, :density, :integer, null: false
    add_column :components, :paper_id, :integer, null: false
  end
end
