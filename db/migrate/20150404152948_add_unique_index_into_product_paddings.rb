# -*- encoding : utf-8 -*-
class AddUniqueIndexIntoProductPaddings < ActiveRecord::Migration
  def change
    add_index :product_paddings, [:product_type_id, :binding_type_id], unique: true
  end
end
