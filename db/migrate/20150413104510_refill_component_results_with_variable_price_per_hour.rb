# -*- encoding : utf-8 -*-
class RefillComponentResultsWithVariablePricePerHour < ActiveRecord::Migration
  def up
    ComponentResult.delete_all #clean old results

    Component.find_each do |component|
      component.calculate
    end

    change_column :component_results, :variable_price_per_hour, :decimal, precision: 15, scale: 2, null: false
  end
end
