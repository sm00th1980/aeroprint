# -*- encoding : utf-8 -*-
class FillCoverWorkTypes < ActiveRecord::Migration
  def up
    CoverWorkType.create!(name: 'для изделия в целом')
    CoverWorkType.create!(name: 'для блока')
  end

  def down
    CoverWorkType.delete_all
  end
end
