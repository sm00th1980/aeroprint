# -*- encoding : utf-8 -*-
class RenameCoverWorksIntoCoverWorkTypes < ActiveRecord::Migration

  def up
    rename_table :cover_works, :cover_work_types
    rename_column :calculation_cover_works, :cover_work_id, :cover_work_type_id
  end

  def down
    rename_table :cover_work_types, :cover_works
    rename_column :calculation_cover_works, :cover_work_type_id, :cover_work_id
  end

end
