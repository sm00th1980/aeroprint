# -*- encoding : utf-8 -*-
class Add150PaperFeature < ActiveRecord::Migration
  def up
    PaperFeature.create!(weight: 150, gor_sm: 64, ver_sm: 90, gor_mm: 300, ver_mm: 432, price_per_1_ton: 1802.66, currency: Currency.find_by(internal_name: 'usa'), paper: Paper.find_by(name: 'Меловка'))
  end
end
