# -*- encoding : utf-8 -*-
class AddPrintRunIntoCalculations < ActiveRecord::Migration
  def change
    add_column :calculations, :print_run, :integer

    Calculation.find_each do |calc|
      calc.print_run = rand(1000) + 100
      calc.save!
    end

    change_column :calculations, :print_run, :integer, null: false
  end
end
