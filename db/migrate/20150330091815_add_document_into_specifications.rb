# -*- encoding : utf-8 -*-
class AddDocumentIntoSpecifications < ActiveRecord::Migration
  def up
    add_attachment :specifications, :document
  end

  def down
    remove_attachment :specifications, :document
  end
end
