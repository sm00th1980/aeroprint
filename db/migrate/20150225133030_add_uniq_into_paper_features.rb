# -*- encoding : utf-8 -*-
class AddUniqIntoPaperFeatures < ActiveRecord::Migration
  def change
    add_index :paper_features, [:paper_id, :weight, :gor_sm, :ver_sm], unique: true, name: 'index_paper_features_unique'
  end
end
