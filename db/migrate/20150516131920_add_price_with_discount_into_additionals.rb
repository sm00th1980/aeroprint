# -*- encoding : utf-8 -*-
class AddPriceWithDiscountIntoAdditionals < ActiveRecord::Migration
  def up
    Additional.create!(name: 'Наценка на бумагу со скидкой', internal_name: 'paper_discount_price', value: 12)
    Additional.create!(name: 'Полная наценка на бумагу', internal_name: 'paper_full_price', value: 22)
  end

  def down
    Additional.find_by(internal_name: 'paper_discount_price').delete
    Additional.find_by(internal_name: 'paper_full_price').delete
  end
end
