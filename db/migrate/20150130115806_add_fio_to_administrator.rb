# -*- encoding : utf-8 -*-
class AddFioToAdministrator < ActiveRecord::Migration
  def up
    admin = User.admin
    admin.fio = 'Администратор'
    admin.save!
  end

  def down
    admin = User.admin
    admin.fio = nil
    admin.save!
  end
end
