# -*- encoding : utf-8 -*-
class AddSlashedProduct < ActiveRecord::Migration
  def up
    Product.create!(name: 'Изделие для вырубки', type: ProductType.list, slashed: true)
  end
end
