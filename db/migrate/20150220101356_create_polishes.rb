# -*- encoding : utf-8 -*-
class CreatePolishes < ActiveRecord::Migration
  def change
    create_table :polishes do |t|
      t.boolean :value, null: false, default: false
      t.string :name, null: false

      t.timestamps null: false
    end

    add_index :polishes, :value, unique: true
  end
end
