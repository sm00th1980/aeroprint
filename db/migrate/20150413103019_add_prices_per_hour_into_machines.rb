# -*- encoding : utf-8 -*-
class AddPricesPerHourIntoMachines < ActiveRecord::Migration
  def up
    add_column :machines, :prices_per_hour, :decimal, precision: 15, scale: 2, array: true

    Machine.find_each do |machine|
      machine.prices_per_hour = [machine.price_per_hour, machine.price_per_hour*2,  machine.price_per_hour*3]
      machine.save!
    end

    change_column :machines, :prices_per_hour, :decimal, precision: 15, scale: 2, array: true, null: false

    remove_column :machines, :price_per_hour
  end
end
