# -*- encoding : utf-8 -*-
class FillMachineFormats < ActiveRecord::Migration
  def up
    MachineFormat.create!(name: 'A2', internal_name: 'A2')
    MachineFormat.create!(name: 'A3', internal_name: 'A3')
  end

  def down
    MachineFormat.delete_all
  end
end
