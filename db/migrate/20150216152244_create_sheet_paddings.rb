# -*- encoding : utf-8 -*-
class CreateSheetPaddings < ActiveRecord::Migration
  def change
    create_table :sheet_paddings do |t|
      t.integer :value, null: false

      t.timestamps null: false
    end

    add_index :sheet_paddings, :value, unique: true
  end
end
