# -*- encoding : utf-8 -*-
class AddInternalTypeIntoProductTypes < ActiveRecord::Migration
  def up
    add_column :product_types, :internal_name, :string

    list = ProductType.find_by(name: 'Листовая')
    list.internal_name = :list
    list.save!

    many_page = ProductType.find_by(name: 'Многостраничная')
    many_page.internal_name = :many_page
    many_page.save!

    change_column :product_types, :internal_name, :string, null: false
  end
end
