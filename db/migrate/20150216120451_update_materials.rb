# -*- encoding : utf-8 -*-
class UpdateMaterials < ActiveRecord::Migration
  def up
    Material.find_by(name: 'краска').update_attribute(:name, 'материал')
    Material.find_by(name: 'доллар').delete
  end
end
