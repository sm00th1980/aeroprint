# -*- encoding : utf-8 -*-
class FillContractIntoClients < ActiveRecord::Migration
  def up
    Client.all.each do |client|
      date = ((Date.today-1.year)..(Date.today-1.month)).to_a.sample
      client.phone = ''
      client.email = ''
      client.contract = "N-#{date.year}/#{date.month}/#{date.day}"
      client.save!
    end
  end

  def down
    Client.all.each do |client|
      client.contract = ''
      client.save!
    end
  end
end
