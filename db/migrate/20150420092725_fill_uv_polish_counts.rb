# -*- encoding : utf-8 -*-
class FillUvPolishCounts < ActiveRecord::Migration
  def up
    UvPolishCount.create!(name: 'С одной стороны', internal_name: 'one_side')
    UvPolishCount.create!(name: 'С двух сторон', internal_name: 'both_side')
  end
end
