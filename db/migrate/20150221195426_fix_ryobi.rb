# -*- encoding : utf-8 -*-
class FixRyobi < ActiveRecord::Migration
  def up
    ryobi = Machine.find_by(name: 'RYOBI')
    ryobi.efficiency = 6000
    ryobi.color_count = 2
    ryobi.preparation_time = 0.1
    ryobi.additional_time = 0.5
    ryobi.price_per_hour = 1800

    ryobi.save!

    man = Machine.find_by(name: 'MAN')
    man.price_per_hour = 4000
    man.save!
  end
end
