# -*- encoding : utf-8 -*-
class CreatePaperCuts < ActiveRecord::Migration
  def change
    create_table :paper_cuts do |t|
      t.integer :paper_feature_id, null: false
      t.integer :gor_mm, null: false
      t.integer :ver_mm, null: false
      t.integer :machine_id, null: false
      t.integer :chunk_count, null: false

      t.timestamps null: false
    end

    add_index :paper_cuts, [:paper_feature_id]
    add_index :paper_cuts, [:machine_id]
  end
end
