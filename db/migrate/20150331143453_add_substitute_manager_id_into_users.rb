# -*- encoding : utf-8 -*-
class AddSubstituteManagerIdIntoUsers < ActiveRecord::Migration
  def change
    add_column :users, :substitute_manager_id, :integer
  end
end
