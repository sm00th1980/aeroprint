# -*- encoding : utf-8 -*-
class CreateSlashedComponents < ActiveRecord::Migration
  def change
    create_table :slashed_components do |t|
      t.integer :calculation_id, null: false

      t.integer :paper_id, null: false
      t.integer :paper_density, null: false

      t.boolean :format_a2, null: false
      t.integer :format_repeat_count, null: false

      t.boolean :stamp_exists, null: false
      t.decimal :stamp_price, precision: 15, scale: 2

      t.integer :front_cmyk_color_id, null: false
      t.integer :front_pantone_color_id, null: false
      t.boolean :front_polish, null: false, default: false

      t.integer :back_cmyk_color_id, null: false
      t.integer :back_pantone_color_id, null: false
      t.boolean :back_polish, null: false, default: false

      t.timestamps null: false
    end


  end
end
