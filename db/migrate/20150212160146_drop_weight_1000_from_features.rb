# -*- encoding : utf-8 -*-
class DropWeight1000FromFeatures < ActiveRecord::Migration
  def up
    remove_column :paper_features, :weight_per_1000
    remove_column :paper_features, :price_per_1000
  end
end
