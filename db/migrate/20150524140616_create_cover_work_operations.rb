# -*- encoding : utf-8 -*-
class CreateCoverWorkOperations < ActiveRecord::Migration
  def change
    create_table :cover_work_operations do |t|
      t.string :name, null: false
      t.string :internal_name, null: false
      t.decimal :price, precision: 15, scale: 2, null: false

      t.timestamps null: false
    end

    add_index :cover_work_operations, [:internal_name], unique: true
  end
end
