# -*- encoding : utf-8 -*-
class FillSheetPaddings < ActiveRecord::Migration
  def up
    SheetPadding.create!(value: 1)
    SheetPadding.create!(value: 2)
    SheetPadding.create!(value: 3)
    SheetPadding.create!(value: 4)
    SheetPadding.create!(value: 5)
    SheetPadding.create!(value: 6)
    SheetPadding.create!(value: 7)
    SheetPadding.create!(value: 8)
    SheetPadding.create!(value: 9)
    SheetPadding.create!(value: 10)
  end

  def down
    SheetPadding.delete_all
  end
end
