# -*- encoding : utf-8 -*-
class AddCoverWorkTypeIdIntoCoverTypes < ActiveRecord::Migration
  def change
    add_column :cover_works, :type_id, :integer

    CoverWorkType.all.find_each do |cover_work|
      cover_work.type_id = CoverWorkType.all.sample.id
      cover_work.save!
    end

    change_column :cover_works, :type_id, :integer, null: false
  end
end
