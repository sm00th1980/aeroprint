# -*- encoding : utf-8 -*-
class CreateCmykColors < ActiveRecord::Migration
  def change
    create_table :cmyk_colors do |t|
      t.integer :value, null: false

      t.timestamps null: false
    end

    add_index :cmyk_colors, :value, unique: true
  end
end
