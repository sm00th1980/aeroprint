# -*- encoding : utf-8 -*-
require 'faker'

class AddNewClients < ActiveRecord::Migration
  def up
    Client.delete_all

    User.managers.each do |manager|
      Client.create!({name: Faker::Company.name, manager: manager})
    end

  end

  def down
    Client.delete_all
  end
end
