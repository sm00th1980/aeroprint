# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150531151045) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "additionals", force: :cascade do |t|
    t.string   "name",                                   null: false
    t.string   "internal_name",                          null: false
    t.decimal  "value",         precision: 15, scale: 2, null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "additionals", ["internal_name"], name: "index_additionals_on_internal_name", unique: true, using: :btree

  create_table "binding_types", force: :cascade do |t|
    t.string   "name",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "internal_name", null: false
  end

  add_index "binding_types", ["internal_name"], name: "index_binding_types_on_internal_name", unique: true, using: :btree

  create_table "branches", force: :cascade do |t|
    t.integer  "client_id",                     null: false
    t.string   "name",             default: "", null: false
    t.text     "description"
    t.string   "phone"
    t.string   "email"
    t.string   "contract"
    t.string   "payer"
    t.integer  "discount_id",                   null: false
    t.text     "shipping_address"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "branches", ["client_id"], name: "index_branches_on_client_id", using: :btree
  add_index "branches", ["discount_id"], name: "index_branches_on_discount_id", using: :btree

  create_table "calculation_cover_works", force: :cascade do |t|
    t.integer  "calculation_id",                              null: false
    t.integer  "cover_work_type_id",                          null: false
    t.decimal  "price",              precision: 15, scale: 2, null: false
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
  end

  create_table "calculation_uv_polishes", force: :cascade do |t|
    t.integer  "calculation_id", null: false
    t.integer  "type_id",        null: false
    t.integer  "count_id",       null: false
    t.integer  "components",     null: false, array: true
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "calculations", force: :cascade do |t|
    t.integer  "client_id",                         null: false
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "product_id",                        null: false
    t.string   "name"
    t.integer  "print_run",                         null: false
    t.boolean  "saved",             default: false, null: false
    t.boolean  "use_extended_cuts", default: false, null: false
    t.integer  "binding_type_id"
    t.boolean  "success",           default: false, null: false
  end

  add_index "calculations", ["client_id"], name: "index_calculations_on_client_id", using: :btree
  add_index "calculations", ["product_id"], name: "index_calculations_on_product_id", using: :btree

  create_table "clients", force: :cascade do |t|
    t.integer  "manager_id",                      null: false
    t.string   "fio",                default: "", null: false
    t.text     "description"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "phone"
    t.string   "email"
    t.string   "contract"
    t.string   "payer"
    t.integer  "discount_id",                     null: false
    t.text     "shipping_address"
    t.integer  "contract_status_id",              null: false
  end

  add_index "clients", ["manager_id"], name: "index_clients_on_manager_id", using: :btree

  create_table "cmyk_colors", force: :cascade do |t|
    t.integer  "value",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cmyk_colors", ["value"], name: "index_cmyk_colors_on_value", unique: true, using: :btree

  create_table "component_results", force: :cascade do |t|
    t.integer  "component_id",                                                          null: false
    t.integer  "paper_expense",                                                         null: false
    t.decimal  "paper_price",                   precision: 15, scale: 2,                null: false
    t.float    "print_time",                                                            null: false
    t.float    "print_run_weight",                                                      null: false
    t.decimal  "form_price",                    precision: 15, scale: 2,                null: false
    t.decimal  "material_price",                precision: 15, scale: 2,                null: false
    t.decimal  "print_run_price",               precision: 15, scale: 2,                null: false
    t.decimal  "print_price",                   precision: 15, scale: 2,                null: false
    t.integer  "imposition",                                                            null: false
    t.string   "machine",                                                               null: false
    t.string   "chosen_paper",                                                          null: false
    t.string   "front_colors",                                                          null: false
    t.string   "back_colors",                                                           null: false
    t.decimal  "print_run_price_without_nds",   precision: 15, scale: 2,                null: false
    t.decimal  "price_per_unit",                precision: 15, scale: 2,                null: false
    t.decimal  "price_per_unit_without_nds",    precision: 15, scale: 2,                null: false
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.decimal  "uv_polish_price",               precision: 15, scale: 2,                null: false
    t.integer  "variable_form_price_index",                                             null: false
    t.integer  "variable_price_per_hour_index",                                         null: false
    t.decimal  "variable_price_per_1_ton",      precision: 15, scale: 2,                null: false
    t.boolean  "base_price_per_1_ton",                                   default: true, null: false
  end

  add_index "component_results", ["component_id", "variable_form_price_index", "variable_price_per_hour_index", "variable_price_per_1_ton"], name: "indexes_unique", unique: true, using: :btree

  create_table "components", force: :cascade do |t|
    t.string   "name",                   null: false
    t.integer  "calculation_id",         null: false
    t.integer  "front_cmyk_color_id",    null: false
    t.integer  "front_pantone_color_id", null: false
    t.integer  "back_cmyk_color_id",     null: false
    t.integer  "back_pantone_color_id",  null: false
    t.integer  "product_gor_mm",         null: false
    t.integer  "product_ver_mm",         null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "front_polish_id",        null: false
    t.integer  "back_polish_id",         null: false
    t.integer  "density",                null: false
    t.integer  "paper_id",               null: false
    t.integer  "stripes_count"
    t.integer  "frontend_component_id",  null: false
  end

  add_index "components", ["back_cmyk_color_id"], name: "index_components_on_back_cmyk_color_id", using: :btree
  add_index "components", ["back_pantone_color_id"], name: "index_components_on_back_pantone_color_id", using: :btree
  add_index "components", ["back_polish_id"], name: "index_components_on_back_polish_id", using: :btree
  add_index "components", ["calculation_id"], name: "index_components_on_calculation_id", using: :btree
  add_index "components", ["front_cmyk_color_id"], name: "index_components_on_front_cmyk_color_id", using: :btree
  add_index "components", ["front_pantone_color_id"], name: "index_components_on_front_pantone_color_id", using: :btree
  add_index "components", ["front_polish_id"], name: "index_components_on_front_polish_id", using: :btree
  add_index "components", ["paper_id"], name: "index_components_on_paper_id", using: :btree

  create_table "contract_statuses", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "color",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "cover_work_currencies", force: :cascade do |t|
    t.string   "name",                                   null: false
    t.string   "internal_name",                          null: false
    t.decimal  "exchange_rate", precision: 15, scale: 2, null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "cover_work_currencies", ["internal_name"], name: "index_cover_work_currencies_on_internal_name", unique: true, using: :btree

  create_table "cover_work_operations", force: :cascade do |t|
    t.string   "name",                                   null: false
    t.string   "internal_name",                          null: false
    t.decimal  "price",         precision: 15, scale: 2, null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "cover_work_operations", ["internal_name"], name: "index_cover_work_operations_on_internal_name", unique: true, using: :btree

  create_table "cover_work_owners", force: :cascade do |t|
    t.string   "name",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "internal_name", null: false
  end

  add_index "cover_work_owners", ["internal_name"], name: "index_cover_work_owners_on_internal_name", unique: true, using: :btree

  create_table "cover_work_types", force: :cascade do |t|
    t.string   "name",                                                             null: false
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.boolean  "editable",                                          default: true, null: false
    t.integer  "owner_id",                                                         null: false
    t.decimal  "source_material_price",    precision: 15, scale: 5,                null: false
    t.integer  "count_of_copies_per_hour",                                         null: false
    t.integer  "currency_id",                                                      null: false
    t.integer  "operation_id",                                                     null: false
  end

  create_table "currencies", force: :cascade do |t|
    t.string   "name",           null: false
    t.string   "internal_name",  null: false
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.decimal  "exchange_rates", null: false, array: true
  end

  add_index "currencies", ["internal_name"], name: "index_currencies_on_internal_name", unique: true, using: :btree

  create_table "discounts", force: :cascade do |t|
    t.string   "name",                                null: false
    t.decimal  "value",      precision: 15, scale: 2, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "discounts", ["name"], name: "index_discounts_on_name", unique: true, using: :btree
  add_index "discounts", ["value"], name: "index_discounts_on_value", unique: true, using: :btree

  create_table "machine_formats", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "machine_formats", ["internal_name"], name: "index_machine_formats_on_internal_name", unique: true, using: :btree

  create_table "machines", force: :cascade do |t|
    t.string   "name",                          null: false
    t.integer  "efficiency",                    null: false
    t.integer  "color_count",                   null: false
    t.float    "preparation_time",              null: false
    t.float    "additional_time",               null: false
    t.integer  "skin",                          null: false
    t.integer  "max_x",                         null: false
    t.integer  "max_y",                         null: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "format_id",                     null: false
    t.integer  "scale",            default: 14, null: false
    t.integer  "valve",            default: 14, null: false
    t.integer  "cross",            default: 8,  null: false
    t.decimal  "form_prices",                   null: false, array: true
    t.decimal  "prices_per_hour",               null: false, array: true
  end

  add_index "machines", ["format_id"], name: "index_machines_on_format_id", using: :btree

  create_table "manager_statuses", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "manager_statuses", ["internal_name"], name: "index_manager_statuses_on_internal_name", unique: true, using: :btree

  create_table "materials", force: :cascade do |t|
    t.string   "name",                                null: false
    t.decimal  "price",      precision: 15, scale: 2, null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  create_table "pantone_colors", force: :cascade do |t|
    t.integer  "value",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "pantone_colors", ["value"], name: "index_pantone_colors_on_value", unique: true, using: :btree

  create_table "paper_countries", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "paper_cuts", force: :cascade do |t|
    t.integer  "paper_feature_id",                 null: false
    t.integer  "gor_mm",                           null: false
    t.integer  "ver_mm",                           null: false
    t.integer  "machine_id",                       null: false
    t.integer  "chunk_count",                      null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "extended",         default: false, null: false
  end

  add_index "paper_cuts", ["machine_id"], name: "index_paper_cuts_on_machine_id", using: :btree
  add_index "paper_cuts", ["paper_feature_id"], name: "index_paper_cuts_on_paper_feature_id", using: :btree

  create_table "paper_features", force: :cascade do |t|
    t.integer  "paper_id",                                     null: false
    t.integer  "density",                                      null: false
    t.integer  "gor_sm",                                       null: false
    t.integer  "ver_sm",                                       null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.decimal  "price_per_1_ton",     precision: 15, scale: 2, null: false
    t.integer  "currency_id",                                  null: false
    t.integer  "uv_polish_format_id",                          null: false
  end

  add_index "paper_features", ["currency_id"], name: "index_paper_features_on_currency_id", using: :btree
  add_index "paper_features", ["paper_id", "density", "gor_sm", "ver_sm"], name: "index_paper_features_unique", unique: true, using: :btree
  add_index "paper_features", ["paper_id"], name: "index_paper_features_on_paper_id", using: :btree
  add_index "paper_features", ["uv_polish_format_id"], name: "index_paper_features_on_uv_polish_format_id", using: :btree

  create_table "paper_producers", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "paper_producers", ["internal_name"], name: "index_paper_producers_on_internal_name", unique: true, using: :btree

  create_table "paper_types", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "paper_types", ["internal_name"], name: "index_paper_types_on_internal_name", unique: true, using: :btree

  create_table "papers", force: :cascade do |t|
    t.string   "name",        null: false
    t.integer  "type_id",     null: false
    t.integer  "producer_id", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "country_id",  null: false
  end

  add_index "papers", ["country_id"], name: "index_papers_on_country_id", using: :btree
  add_index "papers", ["producer_id"], name: "index_papers_on_producer_id", using: :btree
  add_index "papers", ["type_id"], name: "index_papers_on_type_id", using: :btree

  create_table "polishes", force: :cascade do |t|
    t.boolean  "value",      default: false, null: false
    t.string   "name",                       null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  add_index "polishes", ["value"], name: "index_polishes_on_value", unique: true, using: :btree

  create_table "product_paddings", force: :cascade do |t|
    t.integer  "product_type_id", null: false
    t.integer  "binding_type_id"
    t.integer  "ver_mm",          null: false
    t.integer  "gor_mm",          null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "product_paddings", ["product_type_id", "binding_type_id"], name: "index_product_paddings_on_product_type_id_and_binding_type_id", unique: true, using: :btree

  create_table "product_types", force: :cascade do |t|
    t.string   "name",          null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "internal_name", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name",                            null: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "product_type_id",                 null: false
    t.boolean  "slashed",         default: false, null: false
  end

  create_table "slashed_components", force: :cascade do |t|
    t.integer  "calculation_id",                                                  null: false
    t.integer  "paper_id",                                                        null: false
    t.integer  "paper_density",                                                   null: false
    t.boolean  "format_a2",                                                       null: false
    t.integer  "format_repeat_count",                                             null: false
    t.boolean  "stamp_exists",                                                    null: false
    t.decimal  "stamp_price",            precision: 15, scale: 2
    t.integer  "front_cmyk_color_id",                                             null: false
    t.integer  "front_pantone_color_id",                                          null: false
    t.boolean  "front_polish",                                    default: false, null: false
    t.integer  "back_cmyk_color_id",                                              null: false
    t.integer  "back_pantone_color_id",                                           null: false
    t.boolean  "back_polish",                                     default: false, null: false
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
  end

  create_table "specifications", force: :cascade do |t|
    t.integer  "client_id",             null: false
    t.string   "number"
    t.date     "date"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "document_file_name"
    t.string   "document_content_type"
    t.integer  "document_file_size"
    t.datetime "document_updated_at"
  end

  add_index "specifications", ["client_id"], name: "index_specifications_on_client_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false, null: false
    t.text     "fio"
    t.text     "description"
    t.integer  "status_id",                              null: false
    t.integer  "substitute_manager_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["status_id"], name: "index_users_on_status_id", using: :btree

  create_table "uv_polish_counts", force: :cascade do |t|
    t.string   "name",                                   null: false
    t.string   "internal_name",                          null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.decimal  "value",         precision: 15, scale: 2, null: false
  end

  create_table "uv_polish_formats", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "uv_polish_prices", force: :cascade do |t|
    t.integer  "type_id",                                    null: false
    t.integer  "format_id",                                  null: false
    t.decimal  "print_price",       precision: 15, scale: 2, null: false
    t.decimal  "preparation_price", precision: 15, scale: 2, null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  create_table "uv_polish_types", force: :cascade do |t|
    t.string   "name",          null: false
    t.string   "internal_name", null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

end
