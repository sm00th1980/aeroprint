source 'https://rubygems.org'


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 4.2.1'
# Use sqlite3 as the database for Active Record
gem 'sqlite3'
# Use SCSS for stylesheets
gem 'sass-rails', '5.0.1'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'
gem 'less-rails'
gem 'devise'
gem 'draper'
gem 'pg'
gem 'russian'
gem 'rails-boilerplate'
gem 'switch_user'
gem 'will_paginate'
gem 'jquery-rails'
gem 'whenever', :require => false
gem 'chunky_png'
gem 'react-rails', '~> 1.0'

gem 'chosen-rails'
gem 'underscore-rails'
gem 'paperclip', '~> 4.2'
gem 'momentjs-rails', '>= 2.9.0'
gem 'bootstrap3-datetimepicker-rails', '~> 4.0.0'
gem 'sprockets', '2.12.3'
gem 'sidekiq'

group :development do
  gem 'magic_encoding'
  gem 'capistrano-rails'
  gem 'capistrano-rvm'
  gem 'capistrano3-unicorn'
  gem 'traceroute'
  # gem 'rack-mini-profiler'
  gem 'rubocop', require: false
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'pry-rails'
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'rspec-rails', '~> 3.0'
  gem 'factory_girl_rails'
  gem 'assert_difference'
  gem 'database_cleaner'
  gem 'faker'
  gem 'simplecov', require: false
end

group :production do
  # Use Unicorn as the app server
  gem 'unicorn'
  gem 'exception_notification'
end